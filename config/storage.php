<?php

/**
 * Конфигурация модуля storage
 */

return [
    /**
     * Ключ хранилища для пользовательских приватных файлов
     *
     * @see filesystems.php
     */
    'user_data_file_system' => 'user_data',
];
