<?php

return [
    'shopId' => env('YOOKASSA_SHOP_ID', '788751'),
    'secretKey' => env('YOOKASSA_SECRET_KEY', 'test__aIvLV-Hi8jAHFHFAK2d7nkOM-rX62QXoprFA2tAHIQ'),
    'currency' => 'RUB',
    // https://yookassa.ru/developers/using-api/webhooks#ip
    'notificationIpRanges' => [
        '185.71.76.0/27',
        '185.71.77.0/27',
        '77.75.153.0/25',
        '77.75.154.128/25',
        '2a02:5180:0:1509::/64',
        '2a02:5180:0:2655::/64',
        '2a02:5180:0:1533::/64',
        '2a02:5180:0:2669::/64',
    ],
];
