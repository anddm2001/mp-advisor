<?php

/**
 * Конфигурация модуля по работе с мерчантами
 */

use App\Modules\Merchant\Validation\Rules\WildberriesKeyCheckRule;
use App\Modules\Report\Contracts\MarketPlaceContract;
use App\Modules\Merchant\Validation\Rules\WildberriesKeyRule;
use App\Modules\Merchant\Validation\Filters\WildberriesKeyFilter;

return [
    /**
     * Конфигурация подключений к API
     */
    'connections_config' => [
        [
            'marketplace' => MarketPlaceContract::WILDBERRIES,
            'credentials' => [
                'key' => [
                    'title' => 'Ключ подключения (в base64)',
                    'validation' => [
                        'bail',
                        'required',
                        'string',
                        new WildberriesKeyRule(),
                        new WildberriesKeyCheckRule(),
                    ],
                    'filter' => new WildberriesKeyFilter(),
                ],
            ],
        ],
    ],
];
