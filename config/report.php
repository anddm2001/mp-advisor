<?php

/**
 * Конфигурация модуля отчётов
 */

return [
    /**
     * Хост для подключения к микросервису отчётов через gRPC
     */
    'wildberries_api_report_host' => env('WILDBERRIES_API_REPORT_HOST', ''),
];
