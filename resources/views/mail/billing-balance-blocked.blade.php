@php
/** @var string $userName */
/** @var float $monthAmount */
/** @var string $topUpUrl */
@endphp

@extends('mail.layouts.notification')

@section('userName', $userName)

@section('content')
                                <tr>
                                    <td colspan="2">
                                        <p style="margin: 32px 0; font-size: 17px; line-height: 21px; font-weight: 500; color: #444444;">
                                            В настоящий момент вам закрыт доступ к аналитике и отчётам сервиса mpAdvisor.<br>
                                            Причина: недостаточно средств на балансе.
                                        </p>
                                        <p style="margin: 32px 0; font-size: 15px; line-height: 21px; font-weight: 400; color: #444444;">
                                            Чтобы вы не потеряли важную информацию, мы продолжаем собирать и анализировать для вас данные.<br>
                                            Когда вы вернётесь к сервису, ваши отчёты будут актуальными.
                                        </p>
                                        <p style="margin: 32px 0; font-size: 15px; line-height: 21px; font-weight: 400; color: #444444;">
                                            Для продолжения работы в ближайший месяц пополните, пожалуйста,
                                            баланс на сумму {{ number_format($monthAmount, 0, ',', ' ') }} руб.
                                        </p>
                                        <a href="{{ $topUpUrl }}" target="_blank" style="display: inline-block;
                                        color: #ffffff; background-color: #4FBBBC; border-radius: 6px;
                                        box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px;
                                        font-weight: bold; margin: 0 16px 16px 0; padding: 8px 24px;
                                        border-color: #3498db;">
                                            Пополнить баланс</a>
                                        <p style="margin: 32px 0 0; font-size: 15px; line-height: 21px; font-weight: 500; color: #444444;">
                                            Успешного бизнеса!
                                        </p>
                                    </td>
                                </tr>
@endsection