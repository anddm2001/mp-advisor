@php
/** @var string $userName */
/** @var string $daysToFinishText */
/** @var string $activeItemsText */
/** @var string $billingRateName */
/** @var float|null $billingRateAmount */
/** @var float $monthAmount */
/** @var string $topUpUrl */
@endphp

@extends('mail.layouts.notification')

@section('userName', $userName)

@section('content')
                                <tr>
                                    <td colspan="2">
                                        <p style="margin: 32px 0; font-size: 17px; line-height: 21px; font-weight: 500; color: #444444;">
                                            Команда сервиса аналитики mpAdvisor благодарит вас за использование
                                            возможностей сервиса в течение пробного периода.
                                        </p>
                                        <p style="margin: 32px 0; font-size: 15px; line-height: 21px; font-weight: 400; color: #444444;">
                                            Чтобы вы могли вовремя скорректировать свои планы, напоминаем,
                                            что ваш пробный период завершится через {{ $daysToFinishText }}.
                                        </p>
                                        <p style="margin: 32px 0; font-size: 15px; line-height: 21px; font-weight: 400; color: #444444;">
                                            Будем рады, если вы решите продолжить пользоваться сервисом и пополните баланс.<br>
                                            В вашей номенклатуре {{ $activeItemsText }}, поэтому ваш тариф —
                                            "{{ $billingRateName }}"{{ $billingRateAmount ? ', ' . number_format($billingRateAmount, 0, ',', ' ') . ' руб. в сутки' : '' }}.
                                            Для работы с сервисом в течение следующего месяца, пожалуйста,
                                            пополните баланс на {{ number_format($monthAmount, 0, ',', ' ') }} руб.
                                        </p>
                                        <p style="margin: 32px 0; font-size: 15px; line-height: 21px; font-weight: 500; color: #444444;">
                                            Мы очень ценим обратную связь от пользователей и будем рады пополнить ваш баланс на 1 500 рублей за отзыв!
                                            Воспользоваться бонусом и оставить отзыв о работе с mpAdvisor вы можете по кнопке ниже.
                                        </p>
                                        <a href="{{ $topUpUrl }}" target="_blank" style="display: inline-block;
                                        color: #ffffff; background-color: #4FBBBC; border-radius: 6px;
                                        box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px;
                                        font-weight: bold; margin: 0 16px 16px 0; padding: 8px 24px;
                                        border-color: #3498db;">
                                            Пополнить баланс</a>
                                        <a href="{{ $pollUrl }}" target="_blank" style="display: inline-block;
                                        color: #ffffff; background-color: #7a6eac; border-radius: 6px;
                                        box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px;
                                        font-weight: bold; margin: 0 0 16px; padding: 8px 24px;
                                        border-color: #7a6eac;">
                                            Получить 1 500 руб.</a>
                                        <p style="margin: 32px 0 0; font-size: 15px; line-height: 21px; font-weight: 500; color: #444444;">
                                            Успешного бизнеса!
                                        </p>
                                    </td>
                                </tr>
@endsection