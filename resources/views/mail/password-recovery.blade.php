@extends('mail.layouts.notification')

@section('userName', $userName)

@section('content')
                                <tr>
                                    <td colspan="2">
                                        <p style="margin: 32px 0 12px; font-size: 15px; line-height: 21px; font-weight: 400; color: #444444;">
                                            Вы запросили сброс пароля в системе <a href="{{ $appUrl }}">{{ $appName }}</a>.
                                        </p>
                                        <p style="margin: 0 0 12px; font-size: 15px; line-height: 21px; font-weight: 400;
                                        color: #444444;">Чтобы сменить пароль, нажмите кнопку ниже:</p>
                                        <a href="{{ $resetUrl }}" target="_blank" style="display: inline-block;
                                        color: #ffffff; background-color: #4FBBBC; border-radius: 6px;
                                        box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px;
                                        font-weight: bold; margin: 0 0 16px; padding: 8px 24px; text-transform: capitalize;
                                        border-color: #3498db;">
                                            Установить новый пароль</a>
                                    </td>
                                </tr>
@endsection