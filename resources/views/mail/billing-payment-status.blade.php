@php
/** @var string $userName */
/** @var boolean $isSuccessful */
/** @var \App\Modules\Billing\Models\BillingOperation $billingOperation */
@endphp

@extends('mail.layouts.notification')

@section('userName', $userName)

@section('content')
                                <tr>
                                    <td colspan="2">
                                        <p style="margin: 32px 0; font-size: 17px; line-height: 21px; font-weight: 500; color: #444444;">
                                            Платёж №{{ $billingOperation->full_number }}
                                        </p>
                                        <p style="margin: 32px 0; font-size: 15px; line-height: 21px; font-weight: 400; color: #444444;">
                                            @if($isSuccessful)
                                                Вы внесли средства на баланс вашего счёта в размере:
                                                <b>{{ number_format($billingOperation->amount, 2, ',', html_entity_decode('&nbsp;')) }}&nbsp;&#8381;</b><br><br>
                                                Средства успешно зачислены.
                                            @else
                                                К сожалению, при обработке вашего платежа произошла ошибка:<br>
                                                <b>{{ $billingOperation->error_title }}</b><br>
                                                {{ $billingOperation->error_description }}
                                            @endif
                                        </p>
                                    </td>
                                </tr>
@endsection