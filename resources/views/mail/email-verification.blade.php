@extends('mail.layouts.notification')

@section('userName', $userName)

@section('content')
                                <tr>
                                    <td colspan="2">
                                        <p style="margin: 0 0 12px; font-size: 15px; line-height: 21px; font-weight: 400;
                                        color: #444444;">Для начала работы с сервисом mpAdvisor требуется активация аккаунта.
                                            Нажмите на кнопку ниже для активации или перейдите по ссылке.</p>
                                        <a href="{{ $verificationUrl }}" target="_blank" style="display: inline-block;
                                        color: #ffffff; background-color: #4FBBBC; border-radius: 6px;
                                        box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px;
                                        font-weight: bold; margin: 0 0 16px; padding: 8px 24px; text-transform: capitalize;
                                        border-color: #3498db;">
                                            Активировать аккаунт</a>
                                        <a href="{{ $verificationUrl }}" style="display: block">{{ $verificationUrl }}</a>
                                    </td>
                                </tr>
@endsection