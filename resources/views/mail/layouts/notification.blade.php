<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased;
font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt;
mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
    <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block;
        Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
            <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px;
            padding: 10px;">

                <!-- START CENTERED WHITE CONTAINER -->
                <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt;
                    background: #ffffff; border-radius: 3px; max-width: 560px; width: 560px;">

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top;
                        box-sizing: border-box; padding: 40px 40px 74px;">
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate;
                            mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                <tr>
                                    <td colspan="2">
                                        <img src="{{ $message->embed(resource_path('images/mail/logo.png')) }}"
                                             alt="hello" width="217" height="76" border="0"
                                             style="border:0; outline:none; text-decoration:none; margin-bottom: 58px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" width="66">
                                        <img src="{{ $message->embed(resource_path('images/mail/hand.png')) }}"
                                             alt="hello" width="50" height="50" border="0"
                                             style="border:0; outline:none; text-decoration:none;">
                                    </td>
                                    <td align="left" valign="middle" style="font-family: sans-serif;
                                    font-size: 24px; line-height: 34px; color: #464E5F; vertical-align: middle;">
                                        Здравствуйте, @yield('userName')!
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 32px"></td>
                                </tr>
                                @yield('content')
                                <tr>
                                    <td colspan="2">
                                        <p style="margin: 40px 0 16px; font-size: 15px; line-height: 21px; font-weight: 400;
                                        color: rgba(68,68,68,0.5);">С уважением, команда mpAdvisor</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <!-- END MAIN CONTENT AREA -->
                </table>

                <!-- END CENTERED WHITE CONTAINER -->
            </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
    </tr>
</table>
</body>
</html>