@extends('mail.layouts.notification')

@section('userName', $userName)

@section('content')
                                <tr>
                                    <td colspan="2">
                                        <p style="margin: 32px 0; font-size: 15px; line-height: 21px; font-weight: 400; color: #444444;">
                                            Спасибо за регистрацию в сервисе mpAdvisor. Мы просто хотели
                                            поприветствовать Вас.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p style="margin: 0 0 12px; font-size: 15px; line-height: 21px; font-weight: 400;
                                        color: #444444;">Пожалуйста, свяжитесь с нами, если Вам понадобится помощь.</p>
                                        <a href="{{ $appUrl }}" target="_blank" style="display: inline-block;
                                        color: #ffffff; background-color: #7B6DAF; border-radius: 6px;
                                        box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px;
                                        font-weight: bold; margin: 0 0 36px; padding: 8px 24px; text-transform: capitalize;
                                        border-color: #3498db;">
                                            Прейти в сервис</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p style="margin: 0 0 12px; font-size: 15px; line-height: 21px; font-weight: 400;
                                        color: #444444;">Для наших друзей создана группа в телеграм канале, присоединяйтесь!</p>
                                        <a href="https://t.me/mpadvisor" target="_blank" style="display: inline-block;
                                        color: #ffffff; background-color: #4FBBBC; border-radius: 6px;
                                        box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px;
                                        font-weight: bold; margin: 0; padding: 8px 24px; text-transform: capitalize;
                                        border-color: #3498db;">
                                            Присоединиться к группе</a>
                                    </td>
                                </tr>
@endsection