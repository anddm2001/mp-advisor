@php
    /** @var integer $marketplace */
    $fullName = !empty($fullName);
@endphp

{{ Html::tag('mark', \App\Modules\Marketplace\Entities\Marketplace::getNameById($marketplace, !$fullName), [
    'class' => 'text-monospace mr-1',
    'title' => $fullName ? null : \App\Modules\Marketplace\Entities\Marketplace::getNameById($marketplace),
]) }}
