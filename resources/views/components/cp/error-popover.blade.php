@php
    /** @var string|null $title */
    /** @var string|null $description */
@endphp

<i class="align-middle text-secondary fas fa-info-circle" data-toggle="popover" title="{{ $description ? $title : '' }}" data-content="{{ $description ?: $title }}"></i>
