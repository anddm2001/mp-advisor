@php
use App\Modules\Auth\Models\User;

/** @var string $class */
/** @var string $prefixClass */
/** @var string $prefix */
/** @var string $status */
/** @var string $statusName */
$badgeClass = function (string $status) {
    switch ($status) {
        case User::STATUS_ACTIVE:
            return 'success';
        case User::STATUS_PENDING:
        default:
            return 'warning';
    }
}
@endphp

<h5 class="{{ $class ?? 'mb-0' }}">
    {{ isset($prefix) ? Html::tag('span', $prefix, ['class' => $prefixClass ?? null]) : '' }}
    <span class="badge badge-{{ $badgeClass($status ?? '') }}">{{ $statusName ?? '' }}</span>
</h5>
