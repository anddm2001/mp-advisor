@php
    /** @var boolean $checked */
    /** @var \Illuminate\Support\MessageBag $errors */
    /** @var string $label */
    /** @var string $message */
    /** @var string $name */
    /** @var boolean $required */
    /** @var string $value */
    $id = \Illuminate\Support\Str::slug($name);
@endphp

<div class="form-group">
    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
        {{ Form::checkbox($name, $value ?? 1, $checked ?? null, [
            'id' => $id,
            'class' => 'custom-control-input' . ($errors->has($name) ? ' is-invalid' : ''),
            'aria-describedby' => $errors->has($name) ? $id . '-error' : null,
            'checked' => $checked ?? null,
            'required' => $required ?? null,
        ]) }}
        {{ Form::label($id, $label, ['class' => 'custom-control-label']) }}
        @error($name)
            {{ Html::tag('span', $message, [
                'id' => $id . '-error',
                'class' => 'error invalid-feedback',
            ]) }}
        @enderror
    </div>
</div>
