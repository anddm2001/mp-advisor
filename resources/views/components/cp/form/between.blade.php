@php
    /** @var \Illuminate\Support\MessageBag $errors */
    /** @var string $formGroupClass */
    /** @var string $label */
    /** @var string $labelClass */
    /** @var string $message */
    /** @var string $name */
    /** @var array $value */
    /** @var string $controlClass */
    /** @var boolean $required */
    $id = \Illuminate\Support\Str::slug($name);
@endphp

<div class="form-group{{ isset($formGroupClass) ? ' ' . $formGroupClass : '' }}">
    {{ Form::label($id . '-from', $label, ['class' => $labelClass ?? null]) }}
    <div class="form-row{{ $errors->has($name) || $errors->has($name . '.*') ? ' is-invalid' : '' }}">
        <div class="col">
            {{ Form::text($name . '[from]', $value['from'] ?? null, [
                'id' => $id . '-from',
                'class' => 'form-control' . (isset($controlClass) ? ' ' . $controlClass : ''),
                'placeholder' => 'от',
                'aria-describedby' => $errors->has($name . '.from') ? $id . '-error' : null,
                'required' => $required ?? null
            ]) }}
        </div>
        <div class="col">
            {{ Form::text($name . '[to]', $value['to'] ?? null, [
                'id' => $id . '-to',
                'class' => 'form-control' . (isset($controlClass) ? ' ' . $controlClass : ''),
                'placeholder' => 'до',
                'aria-describedby' => $errors->has($name . '.to') ? $id . '-error' : null,
                'required' => $required ?? null
            ]) }}
        </div>
    </div>
    @error($name)
        {{ Html::tag('span', $message, [
            'id' => $id . '-error',
            'class' => 'error invalid-feedback',
        ]) }}
    @enderror
    @error($name . '.*')
        {{ Html::tag('span', $message, [
            'id' => $id . '-error',
            'class' => 'error invalid-feedback',
        ]) }}
    @enderror
</div>
