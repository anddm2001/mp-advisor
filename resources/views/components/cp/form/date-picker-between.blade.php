@php
    /** @var \Illuminate\Support\MessageBag $errors */
    /** @var string $formGroupClass */
    /** @var string $label */
    /** @var string $labelClass */
    /** @var string $message */
    /** @var string $name */
    /** @var array $value */
    /** @var string $controlClass */
    /** @var boolean $required */
    $id = \Illuminate\Support\Str::slug($name);
    $dateOnly = !empty($dateOnly);
    $pickerClass = $dateOnly ? 'datepicker-input' : 'datetimepicker-input';
@endphp

@once
    @push('styles')
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="{{ asset('kit/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    @endpush
    @push('scripts')
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="{{ asset('kit/plugins/moment/moment-with-locales.min.js') }}"></script>
        <script src="{{ asset('kit/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    @endpush
@endonce

<div class="form-group{{ isset($formGroupClass) ? ' ' . $formGroupClass : '' }}">
    @unless(empty($label))
        {{ Form::label($id . '-from', $label, ['class' => $labelClass ?? null]) }}
    @endunless
    <div class="form-row{{ $errors->has($name) || $errors->has($name . '.*') ? ' is-invalid' : '' }}">
        <div class="col date">
            {{ Form::text($name . '[from]', null, [
                'id' => $id . '-from',
                'class' => 'form-control ' . $pickerClass . (isset($controlClass) ? ' ' . $controlClass : ''),
                'data-toggle' => 'datetimepicker',
                'data-target' => '#' . $id . '-from',
                'data-value' => $value['from'] ?? null,
                'placeholder' => 'от',
                'aria-describedby' => $errors->has($name . '.from') ? $id . '-error' : null,
                'required' => $required ?? null
            ]) }}
        </div>
        <div class="col date">
            {{ Form::text($name . '[to]', null, [
                'id' => $id . '-to',
                'class' => 'form-control ' . $pickerClass . (isset($controlClass) ? ' ' . $controlClass : ''),
                'data-toggle' => 'datetimepicker',
                'data-target' => '#' . $id . '-to',
                'data-value' => $value['to'] ?? null,
                'placeholder' => 'до',
                'aria-describedby' => $errors->has($name . '.to') ? $id . '-error' : null,
                'required' => $required ?? null
            ]) }}
        </div>
    </div>
    @error($name)
        {{ Html::tag('span', $message, [
            'id' => $id . '-error',
            'class' => 'error invalid-feedback',
        ]) }}
    @enderror
    @error($name . '.*')
        {{ Html::tag('span', $message, [
            'id' => $id . '-error',
            'class' => 'error invalid-feedback',
        ]) }}
    @enderror
</div>
