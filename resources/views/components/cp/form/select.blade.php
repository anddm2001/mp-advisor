@php
    /** @var \Illuminate\Support\MessageBag $errors */
    /** @var string $formGroupClass */
    /** @var string $label */
    /** @var string $labelClass */
    /** @var string $message */
    /** @var string $name */
    /** @var string $list */
    /** @var string $controlClass */
    /** @var boolean $required */
    /** @var string $selected */
    $id = \Illuminate\Support\Str::slug($name);
@endphp

<div class="form-group{{ isset($formGroupClass) ? ' ' . $formGroupClass : '' }}">
    {{ Form::label($id, $label, ['class' => $labelClass ?? null]) }}
    {{ Form::select($name, $list ?? [], $selected ?? null, [
        'id' => $id,
        'class' => ($controlClass ?? 'custom-select') . ($errors->has($name) ? ' is-invalid' : ''),
        'aria-describedby' => $errors->has($name) ? $id . '-error' : null,
        'required' => $required ?? null,
        'multiple' => $multiple ?? null,
    ]) }}
    @error($name)
        {{ Html::tag('span', $message, [
            'id' => $id . '-error',
            'class' => 'error invalid-feedback',
        ]) }}
    @enderror
</div>
