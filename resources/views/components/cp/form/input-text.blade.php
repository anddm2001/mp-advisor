@php
    /** @var \Illuminate\Support\MessageBag $errors */
    /** @var string $formGroupClass */
    /** @var string $label */
    /** @var string $labelClass */
    /** @var string $message */
    /** @var string $name */
    /** @var string $controlClass */
    /** @var string $placeholder */
    /** @var boolean $required */
    /** @var boolean $disabled */
    /** @var string $value */
    /** @var string $errorBag */
    $id = \Illuminate\Support\Str::slug($name);
    $errors = !empty($errorBag) && $errors ? $errors->$errorBag : $errors;
@endphp

<div class="form-group{{ isset($formGroupClass) ? ' ' . $formGroupClass : '' }}">
    {{ Form::label($id, $label, ['class' => $labelClass ?? null]) }}
    {{ Form::text($name, $value ?? null, [
        'id' => $id,
        'class' => 'form-control' . ($errors->has($name) ? ' is-invalid' : '') . (isset($controlClass) ? ' ' . $controlClass : ''),
        'placeholder' => $placeholder ?? null,
        'aria-describedby' => $errors->has($name) ? $id . '-error' : null,
        'required' => $required ?? null,
        'disabled' => $disabled ?? null,
    ]) }}
    @if($errors->has($name))
        {{ Html::tag('span', $errors->first($name), [
            'id' => $id . '-error',
            'class' => 'error invalid-feedback',
        ]) }}
    @endif
</div>
