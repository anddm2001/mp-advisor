@php
    /** @var \Illuminate\Support\MessageBag $errors */
    /** @var string $formGroupClass */
    /** @var string $label */
    /** @var string $labelClass */
    /** @var string $message */
    /** @var string $name */
    /** @var string $controlClass */
    /** @var string $placeholder */
    /** @var boolean $required */
    /** @var string $value */
    $id = \Illuminate\Support\Str::slug($name);
    $dateOnly = !empty($dateOnly);
    $pickerClass = $dateOnly ? 'datepicker-input' : 'datetimepicker-input';
@endphp

@once
    @push('styles')
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="{{ asset('kit/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    @endpush
    @push('scripts')
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="{{ asset('kit/plugins/moment/moment-with-locales.min.js') }}"></script>
        <script src="{{ asset('kit/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    @endpush
@endonce

<div class="form-group date{{ isset($formGroupClass) ? ' ' . $formGroupClass : '' }}">
    {{ Form::label($id, $label, ['class' => $labelClass ?? null]) }}
    {{ Form::text($name, null, [
        'id' => $id,
        'class' => 'form-control ' . $pickerClass . ($errors->has($name) ? ' is-invalid' : '') . (isset($controlClass) ? ' ' . $controlClass : ''),
        'data-toggle' => 'datetimepicker',
        'data-target' => '#' . $id,
        'data-value' => $value ?? null,
        'placeholder' => $placeholder ?? null,
        'aria-describedby' => $errors->has($name) ? $id . '-error' : null,
        'required' => $required ?? null
    ]) }}
    @error($name)
        {{ Html::tag('span', $message, [
            'id' => $id . '-error',
            'class' => 'error invalid-feedback',
        ]) }}
    @enderror
</div>
