@php
    /** @var \Illuminate\Support\MessageBag $errors */
    /** @var string $label */
    /** @var string $message */
    /** @var string $name */
    /** @var string $placeholder */
    /** @var boolean $required */
    /** @var boolean $withConfirmation */
    $id = \Illuminate\Support\Str::slug($name);
@endphp

<div class="form-group">
    {{ Form::label($id, $label) }}
    {{ Form::password($name, [
        'id' => $id,
        'class' => 'form-control' . ($errors->has($name) ? ' is-invalid' : ''),
        'placeholder' => $placeholder ?? null,
        'aria-describedby' => $errors->has($name) ? $id . '-error' : null,
        'required' => $required ?? null
    ]) }}
    @error($name)
        {{ Html::tag('span', $message, [
            'id' => $id . '-error',
            'class' => 'error invalid-feedback',
        ]) }}
    @enderror
</div>
@if($withConfirmation ?? false)
    <x-cp.form.input-password name="{{ $name . '_confirmation' }}" label="{{ $label }} ещё раз" placeholder="Введите {{ mb_strtolower($label) }} ещё раз" :required="$required ?? null" />
@endif
