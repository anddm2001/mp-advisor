@php
    /** @var \App\Modules\Billing\Models\BillingRate[]|\Illuminate\Database\Eloquent\Collection $rates */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Список тарифов — Тарифы')
@section('heading', 'Тарифы')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="mb-4 clearfix">
                <h3 class="card-title">Список тарифов</h3>
                {{ Html::linkRoute('cp.rates.create', 'Добавить новый тариф', [], ['class' => 'float-right btn btn-success']) }}
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 45px">ID</th>
                        <th>Название</th>
                        <th class="text-right">Сумма списания в день</th>
                        <th class="text-right">Минимальная сумма пополнения</th>
                        <th class="text-center">Кол-во активных SKU</th>
                        <th>Пробный период в днях</th>
                        <th class="text-center">Активен</th>
                        <th>Действия</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($rates as $rate)
                        <tr>
                            <td class="text-right">{{ $rate->id }}</td>
                            <td><b>{{ $rate->name }}</b></td>
                            <td class="text-right">{{ number_format($rate->amount, 2, ',', ' ') }}</td>
                            <td class="text-right">{{ $rate->min_amount ? number_format($rate->min_amount, 2, ',', ' ') : '—' }}</td>
                            <td class="text-center text-sm text-monospace">
                                {{ $rate->min_item_count }}
                                &mdash;
                                {!! $rate->max_item_count ?? '<i class="fas fa-infinity"></i>' !!}
                            </td>
                            <td>{{ $rate->trial_period_days ?? '&mdash;' }}</td>
                            <td class="text-center">
                                <i class="fas fa-{{ $rate->is_active ? 'check' : 'times' }}-circle text-{{ $rate->is_active ? 'success' : 'danger' }}"></i>
                            </td>
                            <td>
                                <a href="{{ route('cp.rates.edit', compact('rate')) }}" title="Редактировать тариф">
                                    <i class="fas fa-edit fa-fw"></i>
                                </a>
                                {{ Form::open(['route' => ['cp.rates.destroy', compact('rate')], 'method' => 'delete', 'class' => 'd-inline-block']) }}
                                    <button type="submit" class="btn btn-link js-confirmable" title="Удалить тариф">
                                        <i class="fas fa-times fa-fw"></i>
                                    </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="7" class="text-center">Пока нет ни одного тарифа... : (</td></tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
