@php
    /** @var \App\Modules\Billing\Models\BillingRate $rate */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Изменение тарифа — Тарифы')
@section('heading', 'Тарифы')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card card-success">
                <div class="card-header"><h3 class="card-title">Изменение тарифа</h3></div>
                {{ Form::model($rate, ['route' => ['cp.rates.update', compact('rate')], 'method' => 'put']) }}
                    <div class="card-body">
                        @include('cp.rates.partials.form')
                    </div>
                    <div class="card-footer"><button type="submit" class="btn btn-success">Сохранить</button></div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
