@php
    /** @var \App\Modules\Billing\Models\BillingRate $rate */
@endphp

<x-cp.form.input-text name="name" :value="$rate->name" label="Название" placeholder="Введите название тарифа" required />
<div class="row">
    <div class="col-sm-6">
        <x-cp.form.input-text name="amount" :value="$rate->amount" label="Сумма списания в день" required />
    </div>
    <div class="col-sm-6">
        <x-cp.form.input-text name="min_amount" :value="$rate->min_amount" label="Минимальная сумма пополнения" />
    </div>
    <div class="col-sm-6">
        <x-cp.form.between name="item_count" :value="$rate->item_count" label="Кол-во активных SKU" />
    </div>
    <div class="col-sm-6">
        <x-cp.form.input-text name="trial_period_days" :value="$rate->trial_period_days" label="Пробный период в днях" />
    </div>
</div>
<x-cp.form.input-checkbox name="is_active" :checked="$rate->is_active" label="Активен" />
