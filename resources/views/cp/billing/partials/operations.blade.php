@php
    /** @var \App\Modules\Billing\Models\BillingOperation[]|\Illuminate\Pagination\LengthAwarePaginator $operations */
    $hideMerchantColumn = !empty($hideMerchantColumn);
@endphp

<table class="table table-bordered table-sm">
    <thead>
        <tr>
            <th class="text-right">#</th>
            <th>Тип операции</th>
            <th class="text-center">Статус</th>
            <th class="text-right">Дата</th>
            <th class="text-center">Инициатор</th>
            <th>Описание</th>
            @unless($hideMerchantColumn)
                <th>Мерчант</th>
            @endunless
            <th class="text-right">Сумма</th>
        </tr>
    </thead>
    <tbody>
        @forelse($operations as $operation)
            <tr>
                <td class="text-right">{{ $operation->id }}</td>
                <td>{{ $operation->operation_type_name }}</td>
                <td class="text-center"><span class="badge badge-pill badge-{{ $operation->status_style }}">{{ $operation->status_name }}</span></td>
                <td class="text-right text-sm">{{ $operation->captured_at ? $operation->captured_at->format('d.m.y H:i:s') : ($operation->created_at ? $operation->created_at->format('d.m.y H:i:s') : '—') }}</td>
                <td class="text-center">
                    @if($operation->type !== \App\Modules\Billing\Models\BillingOperation::TYPE_PAYMENT)
                        <span class="font-italic">Система</span>
                    @elseif($operation->initiator)
                        @if($operation->initiator instanceof \App\Modules\Auth\Models\User)
                            {{ Html::linkRoute('cp.users.edit', $operation->initiator->getBillingOperationInitiatorName(), ['user' => $operation->initiator]) }}
                        @elseif($operation->initiator instanceof \App\Modules\ControlPanel\Models\Admin)
                            {{ Html::linkRoute('cp.administrators.edit', $operation->initiator->getBillingOperationInitiatorName(), ['administrator' => $operation->initiator]) }}
                            <sup class="text-danger text-bold">*</sup>
                        @else
                            {{ $operation->initiator->getBillingOperationInitiatorName() }}
                        @endif
                    @else
                        &mdash;
                    @endif
                </td>
                <td>
                    {{ $operation->description }}
                    @if($operation->payment_reference)
                        {!! $operation->description ? '<br>' : '' !!}
                        <span class="text-muted text-sm">{{ $operation->payment_reference }}</span>
                    @endif
                    @if($operation->error_title || $operation->error_description)
                        <p class="mb-0 bg-danger">
                            {!! $operation->error_title ? '<b>' . $operation->error_title . '</b>' : '' !!}
                            {!! $operation->error_title && $operation->error_description ? '<br>' : '' !!}
                            {{ $operation->error_description }}
                        </p>
                    @endif
                </td>
                @unless($hideMerchantColumn)
                    <td>{{ Html::linkRoute('cp.merchants.edit', '#' . $operation->merchant->id . ' ' . $operation->merchant->name, ['merchant' => $operation->merchant]) }}</td>
                @endunless
                <td class="text-right">{!! $operation->amount === (float) 0 ? '&mdash;' : number_format($operation->amount, 2, ',', '&nbsp;') !!}</td>
            </tr>
        @empty
            <tr>
                <td colspan="{{ $hideMerchantColumn ? '8' : '9' }}" class="text-center">
                    {{ empty($filter) ? 'Истории операций пока нет' : 'Операций не найдено' }}... : (
                </td>
            </tr>
        @endforelse
    </tbody>
</table>