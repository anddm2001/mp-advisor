@php
    /** @var array $filter */
    /** @var \App\Modules\Billing\Models\BillingOperation[]|\Illuminate\Pagination\LengthAwarePaginator|null $operations */
    $types = collect(\App\Modules\Billing\Models\BillingOperation::getTypes())->prepend('Все', '');
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'История операций — Биллинг')
@section('heading', 'Биллинг')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="mb-4 clearfix"><h3 class="card-title">История операций</h3></div>
            {{ Form::open(['route' => 'cp.billing.index', 'method' => 'get', 'class' => 'mb-4']) }}
                <div class="row">
                    <div class="col-sm-1">
                        <x-cp.form.input-text name="id" label="ID операции" :value="$filter['id'] ?? null" />
                    </div>
                    <div class="col-sm-1">
                        <x-cp.form.input-text name="merchant" label="ID мерчанта" :value="$filter['merchant'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.between name="amount" label="Сумма" :value="$filter['amount'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.date-picker-between name="date" label="Дата" :value="$filter['date'] ?? null" :dateOnly="true"  />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.select name="type" :list="$types" :selected="$filter['type'] ?? null" label="Показать" />
                    </div>
                    <div class="col-sm">
                        {{ Form::label('', '&nbsp;') }}
                        <div>
                            {{ Form::submit('Найти', ['class' => 'btn btn-primary mr-3']) }}
                            {{ Html::linkRoute('cp.billing.index', 'Сбросить', [], ['class' => 'btn btn-light']) }}
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
        <div class="card-body p-0">
            @include('cp.billing.partials.operations', compact('operations'))
        </div>
        <div class="card-footer clearfix">
            <div class="float-right">{{ $operations->links() }}</div>
        </div>
    </div>
@endsection
