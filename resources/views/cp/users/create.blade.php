@php
    /** @var \App\Modules\Auth\Models\User $user */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Добавление пользователя — Пользователи')
@section('heading', 'Пользователи')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card card-success">
                <div class="card-header"><h3 class="card-title">Добавление пользователя</h3></div>
                {{ Form::model($user, ['route' => 'cp.users.store']) }}
                    <div class="card-body">
                        <x-cp.form.select name="status" :list="\App\Modules\Auth\Models\User::getStatusNames()" label="Статус" required />
                        <x-cp.form.input-text name="email" label="Почта" placeholder="Введите e-mail адрес пользователя" required />
                        <x-cp.form.input-password name="password" label="Пароль" placeholder="Введите пароль пользователя" required withConfirmation />
                        <x-cp.form.input-text name="name" label="Имя" placeholder="Введите имя пользователя" required />
                        <x-cp.form.input-text name="phone" label="Телефон" placeholder="Введите номер телефона пользователя" />
                        <x-cp.form.input-text name="company_name" label="Название компании" placeholder="Введите название компании пользователя" />
                        <x-cp.form.input-text name="company_post" label="Должность" placeholder="Введите должность пользователя" />
                    </div>
                    <div class="card-footer"><button type="submit" class="btn btn-success">Сохранить</button></div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
