@php
    /** @var string $activeAction */
@endphp

<ul class="nav nav-tabs">
    <li class="nav-item">
        {{ Html::linkRoute('cp.users.edit', 'Общая информация', compact('user'), ['class' => 'nav-link' . ($activeAction === 'edit' ? ' active' : '')]) }}
    </li>
</ul>
