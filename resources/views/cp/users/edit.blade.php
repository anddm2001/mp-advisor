@php
    /** @var \App\Modules\Auth\Models\User $user */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Редактирование пользователя — Пользователи')
@section('heading', 'Пользователи')

@section('content')
    <div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 border-bottom-0">
            <div class="mb-4 pt-3 px-4 clearfix">
                <h3 class="card-title">Редактирование пользователя &mdash; {{ $user->id }} ({{ $user->email }})</h3>
            </div>
            @include('cp.users.partials.tabs', ['activeAction' => 'edit'])
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header"><h4 class="card-title">Информация</h4></div>
                        {{ Form::model($user, ['route' => ['cp.users.update', $user], 'method' => 'put']) }}
                        <div class="card-body">
                            <x-cp.form.select name="status" :list="\App\Modules\Auth\Models\User::getStatusNames()" :selected="$user->status" label="Статус" required />
                            <x-cp.form.input-text name="email" :value="$user->email" label="Почта" placeholder="Введите e-mail адрес пользователя" required />
                            <x-cp.form.input-text name="name" :value="$user->name" label="Имя" placeholder="Введите имя пользователя" required />
                            <x-cp.form.input-text name="phone" :value="$user->phone" label="Телефон" placeholder="Введите номер телефона пользователя" />
                            <x-cp.form.input-text name="company_name" :value="$user->company_name" label="Название компании" placeholder="Введите название компании пользователя" />
                            <x-cp.form.input-text name="company_post" :value="$user->company_post" label="Должность" placeholder="Введите должность пользователя" />
                        </div>
                        <div class="card-footer"><button type="submit" class="btn btn-primary">Сохранить</button></div>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-lightblue">
                        <div class="card-header"><h4 class="card-title">Данные</h4></div>
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-sm-3">Авторизация</dt>
                                <dd class="col-sm-9">
                                    {{ $lastUsedToken && $lastUsedToken->last_used_at ? $lastUsedToken->last_used_at->format('d.m.Y H:i:s') : '—' }}
                                </dd>
                                <dt class="col-sm-3">Регистрация</dt>
                                <dd class="col-sm-9">
                                    {{ $user->created_at->format('d.m.Y H:i:s') }}
                                </dd>
                                <dt class="col-sm-3">Активация</dt>
                                <dd class="col-sm-9">
                                    {{ $user->email_verified_at ? $user->email_verified_at->format('d.m.Y H:i:s') : '—' }}
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="card card-info">
                        <div class="card-header"><h4 class="card-title">Мерчанты</h4></div>
                        <div class="card-body">
                            <div class="float-right">{{ Html::linkRoute('cp.merchants.create', 'Создать', ['owner_id' => $user->id], ['class' => 'btn btn-info btn-sm']) }}</div>
                            @forelse($user->merchants as $merchant)
                                <a href="{{ route('cp.merchants.edit', compact('merchant')) }}">#<b>{{ $merchant->id }}</b> {{ $merchant->name }}</a>
                                @if(!$loop->last)
                                    <br>
                                @endif
                            @empty
                                <p class="mb-0">Нет мерчантов</p>
                            @endforelse
                        </div>
                    </div>
                    <div class="card card-warning">
                        <div class="card-header"><h4 class="card-title">Изменение пароля</h4></div>
                        {{ Form::open(['route' => ['cp.users.password', $user]]) }}
                        <div class="card-body">
                            <x-cp.form.input-password name="password" label="Новый пароль" placeholder="Введите новый пароль пользователя" required withConfirmation />
                        </div>
                        <div class="card-footer"><button type="submit" class="btn btn-warning">Сменить</button></div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
