@php
    /** @var array $filter */
    /** @var \App\Modules\Auth\Models\User[]|\Illuminate\Pagination\LengthAwarePaginator $users */
    /** @var integer $usersCount */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Список пользователей — Пользователи')
@section('heading', 'Пользователи')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="mb-4 clearfix">
                <h3 class="card-title">Список пользователей</h3>
                {{ Html::linkRoute('cp.users.create', 'Добавить нового пользователя', [], ['class' => 'float-right btn btn-success']) }}
            </div>
            {{ Form::open(['route' => 'cp.users.index', 'method' => 'get', 'class' => 'mb-4']) }}
                <div class="row">
                    <div class="col-sm">
                        <x-cp.form.input-text name="email" label="Почта" :value="$filter['email'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.input-text name="name" label="Имя" :value="$filter['name'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.input-text name="merchant" label="ID мерчанта" :value="$filter['merchant'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.input-text name="user" label="ID пользователя" :value="$filter['user'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.input-text name="company" label="Название компании" :value="$filter['company'] ?? null" />
                    </div>
                    <div class="col-sm">
                        {{ Form::label('', '&nbsp;') }}
                        <div>
                            {{ Form::submit('Найти', ['class' => 'btn btn-primary mr-3']) }}
                            {{ Html::linkRoute('cp.users.index', 'Сбросить', [], ['class' => 'btn btn-light']) }}
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
            <p class="mb-0">Всего найдено: <b>{{ $usersCount }}</b>&nbsp;шт.</p>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 45px">ID</th>
                        <th>E-mail</th>
                        <th>Имя</th>
                        <th>Мерчанты</th>
                        <th class="text-center">Статус</th>
                        <th class="text-center" style="width: 180px;">Посл. авторизация</th>
                        <th style="width: 120px">Действия</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($users as $user)
                        <tr>
                            <td class="text-right">{{ $user->id }}</td>
                            <td><b>{{ $user->email }}</b></td>
                            <td>{{ $user->name }}</td>
                            <td>
                                @forelse($user->merchants as $merchant)
                                    <a href="{{ route('cp.merchants.edit', compact('merchant')) }}">
                                        #<b>{{ $merchant->id }}</b> {{ $merchant->name }}
                                    </a>@if(!$loop->last),@endif
                                @empty
                                    Нет мерчантов
                                @endforelse
                            </td>
                            <td class="text-center">
                                <x-cp.user-status :status="$user->status" :statusName="$user->status_name" />
                            </td>
                            <td class="text-center">
                                {{ $user->tokens->isNotEmpty() && $user->tokens->first()->last_used_at ? $user->tokens->first()->last_used_at->format('d.m.Y H:i:s') : '—' }}
                            </td>
                            <td>
                                <a href="{{ route('cp.users.edit', compact('user')) }}" title="Редактировать пользователя">
                                    <i class="fas fa-user-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="6" class="text-center">Ни одного пользователя не найдено... : (</td></tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix">
            <div class="float-right">{{ $users->links() }}</div>
        </div>
    </div>
@endsection
