@php
/** @var string $message */
@endphp

@extends('cp.layouts.standalone')

@section('body-class', 'login-page')

@section('content')
    <div class="login-box">
        <div class="login-logo">Командный центр</div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Задать новый пароль</p>

                {{ Form::open(['route' => ['cp.auth.password.update', $token]]) }}
                    <div class="form-group mb-3">
                        <label class="sr-only" for="reset-email">E-mail адрес</label>
                        <div class="input-group @error('email') is-invalid @enderror">
                            <input type="email" name="email" value="{{ old('email') }}" id="reset-email" class="form-control @error('email') is-invalid @enderror" aria-describedby="reset-email-invalid-feedback" placeholder="Ваш e-mail адрес" required autocomplete="email" autofocus>
                            <div class="input-group-append"><div class="input-group-text"><span class="fas fa-envelope"></span></div></div>
                        </div>
                        @error('email')
                            <div id="reset-email-invalid-feedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group mb-3">
                        <label class="sr-only" for="reset-password">Новый пароль</label>
                        <div class="input-group @error('password') is-invalid @enderror">
                            <input type="password" name="password" id="reset-password" class="form-control @error('password') is-invalid @enderror" aria-describedby="reset-password-invalid-feedback" placeholder="Новый пароль" required>
                            <div class="input-group-append"><div class="input-group-text"><span class="fas fa-lock"></span></div></div>
                        </div>
                        @error('password')
                        <div id="reset-password-invalid-feedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group mb-3">
                        <label class="sr-only" for="reset-password-confirmation">Подтверждение нового пароля</label>
                        <div class="input-group @error('password_confirmation') is-invalid @enderror">
                            <input type="password" name="password_confirmation" id="reset-password-confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" aria-describedby="reset-password-confirmation-invalid-feedback" placeholder="Подтверждение нового пароля" required>
                            <div class="input-group-append"><div class="input-group-text"><span class="fas fa-lock"></span></div></div>
                        </div>
                        @error('password_confirmation')
                        <div id="reset-password-confirmation-invalid-feedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block">Сменить пароль</button>
                        </div>
                    </div>
                {{ Form::close() }}

                <p class="mb-1 pt-3">{{ Html::linkRoute('cp.auth.password.request', 'Нужен новый код сброса?') }}</p>
                <p class="mb-1">{{ Html::linkRoute('cp.auth.login', 'Уже вспомнили?') }}</p>
            </div>
        </div>
    </div>
@endsection
