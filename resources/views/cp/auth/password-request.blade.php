@php
/** @var string $message */
/** @var ?string $status */
$status = session()->get('status');
@endphp

@extends('cp.layouts.standalone')

@section('body-class', 'login-page')

@section('content')
    <div class="login-box">
        <div class="login-logo">Командный центр</div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Запросить сброс пароля</p>

                @if($status)
                    <div class="alert alert-success">
                        <h5><i class="icon fas fa-check"></i> Отправлено!</h5>
                        {{ $status }}
                    </div>
                @endif

                {{ Form::open(['route' => 'cp.auth.password.send']) }}
                    <div class="form-group mb-3">
                        <label class="sr-only" for="password-email">Почта</label>
                        <div class="input-group @error('email') is-invalid @enderror">
                            <input type="email" name="email" value="{{ old('email') }}" id="password-email" class="form-control @error('email') is-invalid @enderror" aria-describedby="password-email-invalid-feedback" placeholder="Почта" required autocomplete="email" autofocus{{ $status ? ' disabled' : '' }}>
                            <div class="input-group-append"><div class="input-group-text"><span class="fas fa-envelope"></span></div></div>
                        </div>
                        @error('email')
                            <div id="password-email-invalid-feedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block"{{ $status ? ' disabled' : '' }}>Отправить ссылку</button>
                        </div>
                    </div>
                {{ Form::close() }}

                <p class="mb-1 pt-3">{{ Html::linkRoute('cp.auth.login', 'Уже вспомнили?') }}</p>
            </div>
        </div>
    </div>
@endsection
