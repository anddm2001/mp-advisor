@php
/** @var string $message */
/** @var ?string $status */
$status = session()->get('status');
@endphp

@extends('cp.layouts.standalone')

@push('styles')
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('kit/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endpush
@section('body-class', 'login-page')

@section('content')
    <div class="login-box">
        <div class="login-logo">Командный центр</div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Вход</p>

                @if($status)
                    <div class="alert alert-success">
                        <h5><i class="icon fas fa-check"></i> Успешно!</h5>
                        {{ $status }}
                    </div>
                @endif

                {{ Form::open(['route' => 'cp.auth.login']) }}
                    <div class="form-group mb-3">
                        <label class="sr-only" for="login-email">Почта</label>
                        <div class="input-group @error('email') is-invalid @enderror">
                            <input type="email" name="email" value="{{ old('email') }}" id="login-email" class="form-control @error('email') is-invalid @enderror" aria-describedby="login-email-invalid-feedback" placeholder="Почта" required autocomplete="email" autofocus>
                            <div class="input-group-append"><div class="input-group-text"><span class="fas fa-envelope"></span></div></div>
                        </div>
                        @error('email')
                            <div id="login-email-invalid-feedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group mb-3">
                        <label class="sr-only" for="login-password">Пароль</label>
                        <div class="input-group @error('password') is-invalid @enderror">
                            <input type="password" name="password" id="login-password" class="form-control @error('password') is-invalid @enderror" aria-describedby="login-password-invalid-feedback" placeholder="Пароль" required>
                            <div class="input-group-append"><div class="input-group-text"><span class="fas fa-lock"></span></div></div>
                        </div>
                        @error('password')
                            <div id="login-password-invalid-feedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" name="remember" value="1" id="login-remember"{{ !empty(old('remember')) ? ' checked' : '' }}>
                                <label for="login-remember">Запомнить меня</label>
                            </div>
                        </div>
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Войти</button>
                        </div>
                    </div>
                {{ Form::close() }}

                <p class="mb-1 pt-3">{{ Html::linkRoute('cp.auth.password.request', 'Забыли пароль?') }}</p>
            </div>
        </div>
    </div>
@endsection
