@php
    /** @var array $filter */
    /** @var \App\Modules\Common\Models\MailLog[]|\Illuminate\Pagination\LengthAwarePaginator $logs */
    $types = collect(\App\Modules\Common\Models\MailLog::getTypeNames())->prepend('Любой', '');
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Письма — Логирование')
@section('heading', 'Логирование')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="mb-4 clearfix"><h3 class="card-title">Письма</h3></div>
            {{ Form::open(['route' => 'cp.mail-logs.index', 'method' => 'get', 'class' => 'mb-4']) }}
                <div class="row">
                    <div class="col-sm">
                        <x-cp.form.input-text name="email" label="E-mail" :value="$filter['email'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.input-text name="user" label="ID пользователя" :value="$filter['user'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.input-text name="merchant" label="ID мерчанта" :value="$filter['merchant'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.date-picker-between name="date" label="Дата" :value="$filter['date'] ?? null" :dateOnly="true" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.select name="type" :list="$types" :selected="$filter['type'] ?? null" label="Тип письма" />
                    </div>
                    <div class="col-sm">
                        {{ Form::label('', '&nbsp;') }}
                        <div>
                            {{ Form::submit('Найти', ['class' => 'btn btn-primary mr-3']) }}
                            {{ Html::linkRoute('cp.mail-logs.index', 'Сбросить', [], ['class' => 'btn btn-light']) }}
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
        <div class="card-body p-0">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th class="text-right">#</th>
                        <th>Пользователь</th>
                        <th>Мерчант</th>
                        <th>E-mail</th>
                        <th>Тип</th>
                        <th>Отправлено</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($logs as $log)
                        <tr>
                            <td class="text-right text-sm">{{ $log->id }}</td>
                            <td>
                                <a href="{{ route('cp.users.edit', ['user' => $log->user]) }}">
                                    #<b>{{ $log->user->id }}</b> {{ $log->user->name }}
                                </a>
                            </td>
                            <td>
                                @if($log->merchant)
                                    <a href="{{ route('cp.merchants.edit', ['merchant' => $log->merchant]) }}">
                                        #<b>{{ $log->merchant->id }}</b> {{ $log->merchant->name }}
                                    </a>
                                @else
                                    &mdash;
                                @endif
                            </td>
                            <td>{{ $log->email }}</td>
                            <td>{{ $log->type_name }}</td>
                            <td>{{ $log->created_at ? $log->created_at->format('d.m.Y H:i:s') : '—' }}</td>
                        </tr>
                    @empty
                        <tr><td colspan="6" class="text-center">Записей логов писем нет... : (</td></tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix">
            <div class="float-right">{{ $logs->links() }}</div>
        </div>
    </div>
@endsection
