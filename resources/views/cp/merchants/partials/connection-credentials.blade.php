@php
    /** @var \App\Modules\Merchant\Contracts\MerchantConnectionManagerContract $service */
    /** @var \App\Modules\Merchant\Models\MerchantConnection $connection */
    $connectionCredentialsNames = $service->getCredentialsNames($connection->marketplace);
    $connectionCredentialsValues = $service->getCredentialsValues($connection);
@endphp

@foreach($connectionCredentialsNames as $name => $label)
    <x-cp.form.input-text :name="$name" :label="$label" :value="$connectionCredentialsValues[$name] ?? null" />
@endforeach
