@php
    /** @var \App\Modules\Merchant\Models\MerchantConnection $connection */
    $fullName = !empty($fullName);
@endphp

<x-cp.marketplace :marketplace="$connection->marketplace" :fullName="$fullName" />
@if($connection->last_sync)
    <span class="mr-1 badge badge-pill badge-{{ empty($connection->error) ? 'success' : 'danger' }}">
        {{ empty($connection->error) ? 'Успешно' : 'Ошибка' }}
    </span>
    <samp class="small">{{ empty($connection->error) ? '' : $connection->error }}</samp>
    <em class="small">{{ $connection->last_sync->format('d.m.Y H:i:s') }}</em>
@elseif(empty($connection->credentials))
    <span class="badge badge-pill badge-danger">Не указаны параметры доступа</span>
@else
    <span class="badge badge-pill badge-secondary">В процессе первой синхронизации</span>
@endif
