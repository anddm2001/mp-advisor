@php
/** @var string $activeAction */
@endphp

<ul class="nav nav-tabs">
    <li class="nav-item">
        {{ Html::linkRoute('cp.merchants.edit', 'Общая информация', compact('merchant'), ['class' => 'nav-link' . ($activeAction === 'edit' ? ' active' : '')]) }}
    </li>
    <li class="nav-item">
        {{ Html::linkRoute('cp.merchants.sync', 'Синхронизация', compact('merchant'), ['class' => 'nav-link' . ($activeAction === 'sync' ? ' active' : '')]) }}
    </li>
    <li class="nav-item">
        {{ Html::linkRoute('cp.merchants.billing', 'Биллинг', compact('merchant'), ['class' => 'nav-link' . ($activeAction === 'billing' ? ' active' : '')]) }}
    </li>
</ul>
