@php
    /** @var \App\Modules\Merchant\Models\Merchant $merchant */
    /** @var \App\Modules\Merchant\Models\MerchantConnectionSync[]|\Illuminate\Pagination\LengthAwarePaginator $syncs */
    $wildberriesMarketplaceId = \App\Modules\Report\Contracts\MarketPlaceContract::WILDBERRIES;
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Синхронизация — Мерчанты')
@section('heading', 'Мерчанты')

@section('content')
    <div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 border-bottom-0">
            <div class="mb-4 pt-3 px-4 clearfix">
                <h3 class="card-title">Синхронизация &mdash; {{ $merchant->id }} ({{ $merchant->name }})</h3>
            </div>
            @include('cp.merchants.partials.tabs', ['activeAction' => 'sync'])
        </div>
        <div class="card-body">
            <div class="mb-5">
                <h5 class="mr-3">Инициировать синхронизацию:</h5>
                <div class="row">
                    <div class="col-sm">
                        {{ Form::open(['route' => ['cp.merchants.start-sync', compact('merchant')]]) }}
                            <x-cp.marketplace :marketplace="$wildberriesMarketplaceId" :fullName="true" />
                            {{ Form::hidden('marketplace', $wildberriesMarketplaceId) }}
                            {{ Form::select('type', \App\Modules\ControlPanel\Http\Requests\MerchantStartSync::getTypes($wildberriesMarketplaceId), null, ['class' => 'custom-select mr-2', 'style' => 'width: auto;']) }}
                            {{ Form::submit('Запустить', ['class' => 'btn btn-success']) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <div class="card card-info">
                <div class="card-header"><h4 class="card-title">Маркетплейсы — последняя синхронизация</h4></div>
                <div class="card-body">
                    <div class="row">
                        @forelse($merchant->marketplaceConnections as $connection)
                            <div class="col-sm">
                                @include('cp.merchants.partials.connection-sync-inline', ['fullName' => true])
                            </div>
                        @empty
                            Данных о синхронизации нет
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="card card-secondary">
                <div class="card-header"><h4 class="card-title">История</h4></div>
                <div class="card-body p-0">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 75px">МП</th>
                                <th style="width: 350px;">Тип синхронизации</th>
                                <th class="text-right" style="width: 180px;">Дата</th>
                                <th class="text-center" style="width: 120px;">Статус</th>
                                <th>Данные</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($syncs as $sync)
                                <tr>
                                    <td><x-cp.marketplace :marketplace="$sync->connection->marketplace" /></td>
                                    <td>{{ $sync->type_name }}</td>
                                    <td class="text-right">{{ $sync->date->format('d.m.Y H:i:s') }}</td>
                                    <td class="text-center">
                                        <span class="badge badge-pill badge-{{ $sync->status === \App\Modules\Merchant\Models\MerchantConnectionSync::STATUS_SUCCESS ? 'success' : 'danger' }}">
                                            {{ $sync->status_name }}
                                        </span>
                                    </td>
                                    <td>{{ $sync->data }}</td>
                                </tr>
                            @empty
                                <tr><td colspan="5" class="text-center">Данных истории синхронизаций нет</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix">
                    <div class="float-right">{{ $syncs->links() }}</div>
                </div>
            </div>
        </div>
    </div>
@endsection
