@php
    /** @var \App\Modules\Merchant\Models\Merchant $merchant */
    $users = \App\Modules\Auth\Models\User::query()->pluck('name', 'id')->prepend('Выберите пользователя из списка', 0);
@endphp

@extends('cp.layouts.wrapped')

@push('styles')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('kit/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('kit/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush

@push('scripts')
    <!-- Select2 -->
    <script src="{{ asset('kit/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('kit/plugins/select2/js/i18n/ru.js') }}"></script>
@endpush

@section('title', 'Добавление мерчанта — Мерчанты')
@section('heading', 'Мерчанты')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card card-success">
                <div class="card-header"><h3 class="card-title">Добавление мерчанта</h3></div>
                {{ Form::model($merchant, ['route' => 'cp.merchants.store']) }}
                    <div class="card-body">
                        <x-cp.form.input-text name="name" label="Название" placeholder="Введите название мерчанта" required />
                        <x-cp.form.select name="owner_id" label="Владелец" :list="$users" controlClass="select2bs4 w-100" required />
                        <x-cp.form.select name="users[]" label="Подключённые пользователи" :list="$users" controlClass="select2bs4 w-100" multiple />
                    </div>
                    <div class="card-footer"><button type="submit" class="btn btn-success">Сохранить</button></div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
