@php
    /** @var \App\Modules\Merchant\Models\Merchant $merchant */
    /** @var \App\Modules\Merchant\Contracts\MerchantConnectionManagerContract $service */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Редактирование мерчанта — Мерчанты')
@section('heading', 'Мерчанты')

@section('content')
    <div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 border-bottom-0">
            <div class="mb-4 pt-3 px-4 clearfix">
                <h3 class="card-title">Редактирование мерчанта &mdash; {{ $merchant->id }} ({{ $merchant->name }})</h3>
            </div>
            @include('cp.merchants.partials.tabs', ['activeAction' => 'edit'])
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header"><h4 class="card-title">Параметры доступа к маркетплейсам</h4></div>
                        <div class="card-body">
                            <p>Изменение параметров доступа не удалит текущие данные, но новые будут поступать при ежедневном обновлении.</p>
                            @forelse($merchant->marketplaceConnections as $connection)
                                <div class="card card-light">
                                    {{ Form::model($connection, ['route' => ['cp.merchants.connection.update', compact('merchant', 'connection')], 'method' => 'put']) }}
                                        <div class="card-header"><h5 class="card-title"><x-cp.marketplace :marketplace="$connection->marketplace" /></h5></div>
                                        <div class="card-body">
                                            @include('cp.merchants.partials.connection-credentials', compact('service', 'connection'))
                                        </div>
                                        <div class="card-footer"><button type="submit" class="btn btn-secondary">Изменить</button></div>
                                    {{ Form::close() }}
                                </div>
                            @empty
                                <p>Нет подключенных маркетплейсов.</p>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-info">
                        <div class="card-header"><h4 class="card-title">Подключённые пользователи</h4></div>
                        <div class="card-body">
                            @forelse($merchant->users as $user)
                                <a href="{{ route('cp.users.edit', compact('user')) }}">
                                    #<b>{{ $user->id }}</b>
                                    {{ $user->name }}
                                    ({{ $user->email }})
                                </a>
                                @if(!$loop->last)
                                    <br>
                                @endif
                            @empty
                                Нет пользователей
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
