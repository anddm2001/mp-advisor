@php
    /** @var \App\Modules\Merchant\Models\Merchant $merchant */
    /** @var \App\Modules\Billing\Models\BillingOperation[]|\Illuminate\Pagination\LengthAwarePaginator $operations */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Биллинг — Мерчанты')
@section('heading', 'Мерчанты')

@section('content')
    <div class="card card-primary card-outline card-outline-tabs">
        <div class="card-header p-0 border-bottom-0">
            <div class="mb-4 pt-3 px-4 clearfix">
                <h3 class="card-title">Биллинг &mdash; {{ $merchant->id }} ({{ $merchant->name }})</h3>
            </div>
            @include('cp.merchants.partials.tabs', ['activeAction' => 'billing'])
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::open(['route' => ['cp.merchants.billing.top-up', compact('merchant')]]) }}
                        <div class="card card-success">
                            <div class="card-header"><h4 class="card-title">Ручное пополнение баланса</h4></div>
                            <div class="card-body">
                                <x-cp.form.input-text name="amount" label="Сумма пополнения" placeholder="Введите сумму пополнения" />
                            </div>
                            <div class="card-footer"><button type="submit" class="btn btn-success">Пополнить</button></div>
                        </div>
                    {{ Form::close() }}
                </div>
                <div class="col-sm-6">
                    {{ Form::open(['route' => ['cp.merchants.billing.charge-on', compact('merchant')]]) }}
                        <div class="card card-success">
                            <div class="card-header"><h4 class="card-title">Ручное назначение даты следующего списания</h4></div>
                            <div class="card-body">
                                <x-cp.form.date-picker name="date" :value="$merchant->charge_on ? $merchant->charge_on->format('d.m.Y H:i:s') : null" label="Дата следующего списания" placeholder="Введите дату и время в формате DD.MM.YYYY HH:mm:ss" />
                            </div>
                            <div class="card-footer"><button type="submit" class="btn btn-success">Назначить</button></div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="card card-primary">
                <div class="card-header"><h4 class="card-title">Информация о тарифе и балансе</h4></div>
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-3">Баланс</dt>
                        <dd class="col-sm-9">
                            {{ number_format($merchant->balance, 2, ',', ' ') }} <small>руб.</small>
                            @if($merchant->is_balance_blocked)
                                &mdash;
                                <b class="text-danger">заблокирован</b>
                                <small>(не хватает {{ number_format($merchant->lack_amount, 2, ',', ' ') }} руб. для разблокировки)</small>
                            @endif
                        </dd>
                        <dt class="col-sm-3">Текущий тариф</dt>
                        <dd class="col-sm-9">
                            @if($merchant->billingRate)
                                <b>{{ $merchant->billingRate->name }}</b>
                                &nbsp;
                                [<span class="text-sm text-monospace">
                                    {{ $merchant->billingRate->min_item_count }}
                                    &mdash;
                                    {!! $merchant->billingRate->max_item_count ?? '<i class="fas fa-infinity"></i>' !!}
                                </span>]
                                &mdash;
                                {{ number_format($merchant->billingRate->amount, 2, ',', ' ') }} <small>руб./день</small>
                            @else
                                <i class="text-danger">Не определён</i>
                            @endif
                        </dd>
                        <dt class="col-sm-3">Дата следующего списания</dt>
                        <dd class="col-sm-9">
                            @if($merchant->charge_on)
                                {{ $merchant->charge_on->format('d.m.Y H:i:s') }}
                                <span class="text-muted">
                                    (приблизительно {{ $merchant->charge_on->longRelativeToNowDiffForHumans(2) }})
                                </span>
                            @else
                                <i class="text-danger">Не определена</i>
                            @endif
                        </dd>
                        <dt class="col-sm-3">Завершение пробного периода</dt>
                        <dd class="col-sm-9">
                            @if($merchant->trial_period_finish)
                                {{ $merchant->trial_period_finish->format('d.m.Y') }}
                                <span class="text-muted">(день первого списания)</span>
                            @else
                                <i class="text-danger">Не определено</i>
                            @endif
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="card card-secondary">
                <div class="card-header"><h4 class="card-title">История операций</h4></div>
                <div class="card-body p-0">
                    @include('cp.billing.partials.operations', ['operations' => $operations, 'hideMerchantColumn' => true])
                </div>
                <div class="card-footer clearfix">
                    <div class="float-right">{{ $operations->links() }}</div>
                    {{ Html::linkRoute('cp.billing.index', 'Все операции мерчанта', compact('merchant')) }}
                </div>
            </div>
        </div>
    </div>
@endsection
