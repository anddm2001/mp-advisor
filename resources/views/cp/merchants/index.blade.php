@php
    /** @var array $filter */
    /** @var \App\Modules\Merchant\Models\Merchant[]|\Illuminate\Pagination\LengthAwarePaginator $merchants */
    /** @var integer $merchantsCount */
    $syncStatuses = [
        '' => 'Любой',
        'success' => 'Успешно',
        'error' => 'Ошибка',
        'none' => 'Нет данных о синхронизации',
    ];
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Список мерчантов — Мерчанты')
@section('heading', 'Мерчанты')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="mb-4 clearfix">
                <h3 class="card-title">Список мерчантов</h3>
                {{ Html::linkRoute('cp.merchants.create', 'Добавить нового мерчанта', [], ['class' => 'float-right btn btn-success']) }}
            </div>
            {{ Form::open(['route' => 'cp.merchants.index', 'method' => 'get', 'class' => 'mb-4']) }}
                <div class="row">
                    <div class="col-sm">
                        <x-cp.form.input-text name="merchant" label="ID мерчанта" :value="$filter['merchant'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.input-text name="user" label="ID пользователя" :value="$filter['user'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.between name="items" label="Кол-во SKU" :value="$filter['items'] ?? null" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.select name="status" :list="$syncStatuses" :selected="$filter['status'] ?? null" label="Статус синхронизации" />
                    </div>
                    <div class="col-sm">
                        {{ Form::label('', '&nbsp;') }}
                        <div>
                            {{ Form::submit('Найти', ['class' => 'btn btn-primary mr-3']) }}
                            {{ Html::linkRoute('cp.merchants.index', 'Сбросить', [], ['class' => 'btn btn-light']) }}
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
            <p class="mb-0">Всего найдено: <b>{{ $merchantsCount }}</b>&nbsp;шт.</p>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 45px">ID</th>
                        <th>Название</th>
                        <th>Пользователи</th>
                        <th>Кол-во SKU (активных / всего)</th>
                        <th class="text-center">Доступ по балансу</th>
                        <th>Статус синхронизации</th>
                        <th class="text-center" style="width: 180px;">Посл. авторизация</th>
                        <th style="width: 120px">Действия</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($merchants as $merchant)
                        <tr>
                            <td class="text-right">{{ $merchant->id }}</td>
                            <td><b>{{ $merchant->name }}</b></td>
                            <td>
                                @forelse($merchant->users as $user)
                                    <a href="{{ route('cp.users.edit', compact('user')) }}">#<b>{{ $user->id }}</b> {{ $user->email }}</a>@if(!$loop->last),@endif
                                @empty
                                    Нет пользователей
                                @endforelse
                            </td>
                            <td>
                                @forelse($merchant->marketplaceConnections as $connection)
                                    <x-cp.marketplace :marketplace="$connection->marketplace" />
                                    <span class="text-monospace" title="Активных / всего">
                                        <b>{{ $connection->item_active_count ?? '?' }}</b> / {{ $connection->item_total_count ?? '?' }}
                                    </span>
                                    @if(!$loop->last)<br>@endif
                                @empty
                                    Данных о номенклатурах нет
                                @endforelse
                            </td>
                            <td class="text-center">
                                <span class="badge badge-{{ $merchant->is_balance_blocked ? 'danger' : 'success' }}">
                                    {{ $merchant->is_balance_blocked ? 'Не хватает средств' : 'Открыт' }}
                                </span>
                            </td>
                            <td>
                                @forelse($merchant->marketplaceConnections as $connection)
                                    @include('cp.merchants.partials.connection-sync-inline')
                                    @if(!$loop->last)<br>@endif
                                @empty
                                    Данных о синхронизации нет
                                @endforelse
                            </td>
                            <td class="text-center">
                                @php
                                    /** @var \App\Modules\Merchant\Models\Merchant $merchant */
                                    /** @var \Illuminate\Database\Eloquent\Collection $lastUsedTokenDates */
                                    $lastUsedTokenDates = $merchant->users->map(function (\App\Modules\Auth\Models\User $user) {
                                        return $user->tokens->first();
                                    })->filter()->pluck('last_used_at')->filter();
                                @endphp
                                {{ $lastUsedTokenDates->isNotEmpty() ? $lastUsedTokenDates->max()->format('d.m.Y H:i:s') : '—' }}
                            </td>
                            <td>
                                {{ Html::linkRoute('cp.merchants.edit', 'Подробнее', compact('merchant'), ['class' => 'btn btn-primary btn-sm']) }}
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="6" class="text-center">Ни одного мерчанта не найдено... : (</td></tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix">
            <div class="float-right">{{ $merchants->links() }}</div>
        </div>
    </div>
@endsection
