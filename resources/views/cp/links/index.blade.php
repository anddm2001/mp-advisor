@php
    /** @var \App\Modules\Common\Models\LinkHandbook[]|\Illuminate\Database\Eloquent\Collection $links */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Ссылки — Справочники')
@section('heading', 'Ссылки')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="clearfix">
                <h5 class="mb-3">Добавить ссылку</h5>
                {{ Form::model(new \App\Modules\Common\Models\LinkHandbook(), ['route' => 'cp.links.store']) }}
                    <div class="row">
                        <div class="col-sm">
                            <x-cp.form.input-text name="code" label="Уникальный код" placeholder="license-agreement" errorBag="store" />
                        </div>
                        <div class="col-sm">
                            <x-cp.form.input-text name="name" label="Название" placeholder="Лицензионное соглашение" errorBag="store" required />
                        </div>
                        <div class="col-sm">
                            <x-cp.form.input-text name="link" label="Ссылка" placeholder="https://app.mpadvisor.ru/docs/license_agreement.docx" errorBag="store" required />
                        </div>
                        <div class="col-sm">
                            {{ Form::label('', '&nbsp;') }}
                            <div>{{ Form::submit('Добавить', ['class' => 'btn btn-success']) }}</div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
        <div class="card-body">
            <h5 class="mb-3">Существующие ссылки</h5>
            @forelse($links as $link)
                {{ Form::model($link, ['route' => ['cp.links.update', compact('link')], 'method' => 'put']) }}
                    <div class="row">
                        <div class="col-sm">
                            <x-cp.form.input-text name="update{{ $link->id }}-code" label="Уникальный код" :value="$link->code ?? null" disabled />
                        </div>
                        <div class="col-sm">
                            <x-cp.form.input-text name="update{{ $link->id }}-name" label="Название" :value="$link->name ?? null" errorBag="update{{ $link->id }}" required />
                        </div>
                        <div class="col-sm">
                            <x-cp.form.input-text name="update{{ $link->id }}-link" label="Ссылка" :value="$link->link ?? null" errorBag="update{{ $link->id }}" required />
                        </div>
                        <div class="col-sm">
                            {{ Form::label('', '&nbsp;') }}
                            <div>{{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}</div>
                        </div>
                    </div>
                {{ Form::close() }}
                @if(!$loop->last)
                    <hr>
                @endif
            @empty
                <p>Ссылок пока нет...</p>
            @endforelse
        </div>
        <div class="card-footer clearfix">
            <h5>Загрузка файлов</h5>
            @if(\Illuminate\Support\Facades\Session::has('uploaded_file_url'))
                <h6 class="text-success">Успешно загружено!</h6>
                <div class="mb-3 font-weight-bold">
                    Ссылка на загруженный файл:
                    <div class="p-2 bg-gradient-success text-monospace">{{ env('APP_URL') }}{{ \Illuminate\Support\Facades\Session::get('uploaded_file_url') }}</div>
                </div>
                <h6>Загрузить ещё:</h6>
            @endif
            {{ Form::open(['route' => 'cp.links.upload', 'files' => true, 'class' => 'js-simple-submit-onchange', 'data-hide-loader' => 1]) }}
                <div class="custom-file">
                    <input type="file" name="file" class="custom-file-input" id="file">
                    <label class="custom-file-label" for="file">Выберите файл для загрузки</label>
                </div>
            {{ Form::close() }}
            <p class="m-0 mt-2 text-muted">Файлы с одинаковыми именами будут перезаписаны.</p>
        </div>
    </div>
@endsection
