@php
    /** @var \App\Modules\ControlPanel\Models\Admin $adminUser */
    /** @var \App\Modules\ControlPanel\Models\Admin $administrator */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Редактирование администратора — Администраторы')
@section('heading', 'Администраторы')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header"><h3 class="card-title">Редактирование администратора &mdash; {{ $administrator->id }}</h3></div>
                {{ Form::model($administrator, ['route' => ['cp.administrators.update', $administrator], 'method' => 'put']) }}
                    <div class="card-body">
                        <x-cp.form.input-text name="email" :value="$administrator->email" label="E-mail адрес" placeholder="Введите e-mail адрес администратора" required />
                        <x-cp.form.input-text name="first_name" :value="$administrator->first_name" label="Имя" placeholder="Введите имя администратора" required />
                        <x-cp.form.input-text name="last_name" :value="$administrator->last_name" label="Фамилия" placeholder="Введите фамилию администратора" required />
                        <x-cp.form.input-checkbox name="is_active" :checked="$administrator->is_active" label="Доступ в панель управления" />
                    </div>
                    <div class="card-footer"><button type="submit" class="btn btn-primary">Сохранить</button></div>
                {{ Form::close() }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-warning">
                <div class="card-header"><h3 class="card-title">Смена пароля</h3></div>
                {{ Form::open(['route' => ['cp.administrators.password', $administrator]]) }}
                    <div class="card-body">
                        <x-cp.form.input-password name="password" label="Новый пароль" placeholder="Введите новый пароль для входа" required withConfirmation />
                        <x-cp.form.input-password name="admin_password" label="Пароль текущего администратора ({{ $adminUser->full_name }})" placeholder="Подтвердите действие своим паролем" required />
                    </div>
                    <div class="card-footer"><button type="submit" class="btn btn-warning">Сменить</button></div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
