@php
    /** @var \App\Modules\ControlPanel\Models\Admin $administrator */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Добавление администратора — Администраторы')
@section('heading', 'Администраторы')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card card-success">
                <div class="card-header"><h3 class="card-title">Добавление администратора</h3></div>
                {{ Form::model($administrator, ['route' => 'cp.administrators.store']) }}
                    <div class="card-body">
                        <x-cp.form.input-text name="email" label="E-mail адрес" placeholder="Введите e-mail адрес администратора" required />
                        <x-cp.form.input-password name="password" label="Пароль" placeholder="Пароль для входа" required withConfirmation />
                        <x-cp.form.input-text name="first_name" label="Имя" placeholder="Введите имя администратора" required />
                        <x-cp.form.input-text name="last_name" label="Фамилия" placeholder="Введите фамилию администратора" required />
                        <x-cp.form.input-checkbox name="is_active" label="Доступ в панель управления" />
                    </div>
                    <div class="card-footer"><button type="submit" class="btn btn-success">Сохранить</button></div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
