@php
    /** @var \App\Modules\ControlPanel\Models\Admin[]|\Illuminate\Pagination\LengthAwarePaginator $administrators */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Список администраторов — Администраторы')
@section('heading', 'Администраторы')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title mt-2">Список администраторов</h3>
            {{ Html::linkRoute('cp.administrators.create', 'Добавить нового администратора', [], ['class' => 'float-right btn btn-success']) }}
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 45px">ID</th>
                        <th>E-mail</th>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th class="text-center" style="width: 60px">Доступ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($administrators as $administrator)
                        <tr>
                            <td class="text-right">{{ $administrator->id }}</td>
                            <td><b>{{ Html::linkRoute('cp.administrators.edit', $administrator->email, compact('administrator')) }}</b></td>
                            <td>{{ $administrator->last_name }}</td>
                            <td>{{ $administrator->first_name }}</td>
                            <td class="text-center">
                                <i class="fas fa-{{ $administrator->is_active ? 'check' : 'times' }}-circle text-{{ $administrator->is_active ? 'success' : 'danger' }}"></i>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix">
            <div class="float-right">{{ $administrators->links() }}</div>
        </div>
    </div>
@endsection
