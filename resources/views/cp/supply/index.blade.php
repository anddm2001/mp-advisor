@php
    /** @var array $filter */
    /** @var \Report\NomenclatureSettings[]|\Illuminate\Support\Collection $items */
    /** @var array $pagination */
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Кванты поставок по умолчанию — Поставки')
@section('heading', 'Поставки')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="mb-4 clearfix">
                <h3 class="card-title">Кванты поставок</h3>
                <div class="float-right">
                    {{ Form::open(['route' => 'cp.supply.import', 'files' => true, 'class' => 'js-simple-submit-onchange']) }}
                        {{ Form::label('file', 'Импорт XLSX', ['class' => 'm-0 btn btn-sm btn-success align-middle']) }}
                        {{ Form::file('file', ['class' => 'invisible', 'style' => 'width: 0;']) }}
                    {{ Form::close() }}
                </div>
            </div>
            {{ Form::open(['route' => 'cp.supply.index', 'method' => 'get', 'class' => 'mb-4']) }}
            <div class="row">
                <div class="col-sm">
                    <x-cp.form.input-text name="filter[subject]" label="Название категории" :value="$filter['subject'] ?? null" />
                </div>
                <div class="col-sm">
                    {{ Form::label('', '&nbsp;') }}
                    <div>
                        {{ Form::submit('Найти', ['class' => 'btn btn-primary mr-3']) }}
                        {{ Html::linkRoute('cp.supply.index', 'Сбросить', [], ['class' => 'btn btn-light']) }}
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
        <div class="card-body">
            <table class="table table-bordered table-valign-middle">
                <thead>
                <tr>
                    <th colspan="3"><i>Добавить квант поставок по умолчанию для категории:</i></th>
                </tr>
                <tr>
                    <th>Название категории</th>
                    <th>Квант поставки по умолчанию</th>
                    <th class="text-center">Добавление</th>
                </tr>
                {{ Form::open(['route' => 'cp.supply.store']) }}
                    <tr>
                        <td>
                            {{ Form::text('subject', null, ['class' => 'form-control' . ($errors->has('subject') ? ' is-invalid' : '')]) }}
                            @error('subject')
                                {{ Html::tag('span', $message, ['id' => 'subject-error', 'class' => 'error invalid-feedback']) }}
                            @enderror
                        </td>
                        <td>
                            {{ Form::number('quantum', null, ['class' => 'form-control' . ($errors->has('quantum') ? ' is-invalid' : ''), 'min' => 0]) }}
                            @error('quantum')
                            {{ Html::tag('span', $message, ['id' => 'quantum-error', 'class' => 'error invalid-feedback']) }}
                            @enderror
                        </td>
                        <td class="text-center">{{ Form::submit('Добавить', ['class' => 'btn btn-success']) }}</td>
                    </tr>
                {{ Form::close() }}
                </thead>
                <thead>
                <tr>
                    <th colspan="3" class="bg-gray-light"><br></th>
                </tr>
                <tr>
                    <th colspan="3"><i>Текущие значения:</i></th>
                </tr>
                <tr>
                    <th class="text-right">Категория</th>
                    <th>Квант поставки</th>
                    <th class="text-center">Удаление</th>
                </tr>
                </thead>
                <tbody>
                @forelse($items as $item)
                    <tr>
                        <td class="text-right">
                            <b class="text-lg">{{ $item->getSubject() }}</b>
                        </td>
                        <td>
                        {{ Form::open(['route' => 'cp.supply.update', 'class' => 'js-submit-onchange']) }}
                            {{ Form::hidden('subject', $item->getSubject()) }}
                            {{ Form::number('quantum', $item->getDefaultQuantum(), ['class' => 'form-control', 'min' => 0]) }}
                        {{ Form::close() }}
                        </td>
                        <td class="text-center">
                            {{ Form::open(['route' => 'cp.supply.destroy', 'method' => 'delete']) }}
                            {{ Form::hidden('subject', $item->getSubject()) }}
                            {{ Form::submit('Удалить', ['class' => 'btn btn-danger js-confirmable']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center">Текущих значений пока нет...</td>
                    </tr>
                @endforelse
                </tbody>
                @if(($pagination[1] ?? false) || ($pagination[0] ?? false))
                    <tfoot>
                    <tr>
                        <th colspan="3" class="text-center">
                            <ul class="pagination justify-content-center m-0">
                                @if($pagination[1] ?? false)
                                    <li class="page-item">
                                        <a class="page-link" href="{{ $pagination[1] }}">
                                            &laquo; Предыдущая страница
                                        </a>
                                    </li>
                                @endif
                                @if($pagination[0] ?? false)
                                    <li class="page-item">
                                        <a class="page-link" href="{{ $pagination[0] }}">
                                            Следующая страница &raquo;
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </th>
                    </tr>
                    </tfoot>
                @endif
            </table>
        </div>
    </div>
@endsection
