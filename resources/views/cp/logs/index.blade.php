@php
    use App\Modules\Billing\Services\BillingLogger;
    use App\Modules\Merchant\Services\MerchantConnectionLogger;

    /** @var array $filter */
    /** @var \App\Modules\Common\Models\Log[]|\Illuminate\Pagination\LengthAwarePaginator $logs */
    $categories = [
        '' => 'Любая',
        with(new BillingLogger(BillingLogger::CATEGORY_PAYMENT_SYSTEM))->getCategory() => BillingLogger::getCategoryName(BillingLogger::CATEGORY_PAYMENT_SYSTEM),
        with(new BillingLogger(BillingLogger::CATEGORY_RATE_CHANGE))->getCategory() => BillingLogger::getCategoryName(BillingLogger::CATEGORY_RATE_CHANGE),
        with(new MerchantConnectionLogger(MerchantConnectionLogger::CATEGORY_CREDENTIALS_CHECK))->getCategory() => MerchantConnectionLogger::getCategoryName(MerchantConnectionLogger::CATEGORY_CREDENTIALS_CHECK),
        with(new MerchantConnectionLogger(MerchantConnectionLogger::CATEGORY_VALIDATION_FAILED))->getCategory() => MerchantConnectionLogger::getCategoryName(MerchantConnectionLogger::CATEGORY_VALIDATION_FAILED),
    ];
@endphp

@extends('cp.layouts.wrapped')

@section('title', 'Логи — Логирование')
@section('heading', 'Логирование')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="mb-4 clearfix"><h3 class="card-title">Логи</h3></div>
            {{ Form::open(['route' => 'cp.logs.index', 'method' => 'get', 'class' => 'mb-4']) }}
                <div class="row">
                    <div class="col-sm">
                        <x-cp.form.select name="category" :list="$categories" :selected="$filter['category'] ?? null" label="Категория" />
                    </div>
                    <div class="col-sm">
                        {{ Form::label('', '&nbsp;') }}
                        <div>
                            {{ Form::submit('Найти', ['class' => 'btn btn-primary mr-3']) }}
                            {{ Html::linkRoute('cp.logs.index', 'Сбросить', [], ['class' => 'btn btn-light']) }}
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
        <div class="card-body p-0">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th class="text-right" style="width: 175px;">Дата и время</th>
                        <th class="text-center" style="width: 90px;">Уровень</th>
                        <th style="width: 410px;">Категория</th>
                        <th>Запись</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($logs as $log)
                        <tr>
                            <td class="text-right">{{ $log->date->format('d.m.Y H:i:s') }}</td>
                            <td class="text-center">
                                <span class="badge badge-{{ $log->level < 5 ? 'danger' : 'info' }}">
                                    {{ \Illuminate\Support\Str::upper($log->getLevelName()) }}
                                </span>
                            </td>
                            <td>{{ $log->getCategoryName() }}</td>
                            <td>{{ $log->message }}</td>
                        </tr>
                    @empty
                        <tr><td colspan="6" class="text-center">Записей логов нет... : (</td></tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix">
            <div class="float-right">{{ $logs->links() }}</div>
        </div>
    </div>
@endsection
