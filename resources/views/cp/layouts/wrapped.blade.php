@php
/**
 * @var \App\Modules\ControlPanel\Models\Admin $adminUser
 * @var string $currentRouteName
 */
@endphp

@extends('cp.layouts.general')

@push('styles')
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('kit/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <!-- SweetAlert2 -->
    <script src="{{ asset('kit/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
@endpush

@section('body')
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    {{ Html::linkRoute('cp.dashboard', 'На главную', [], ['class' => 'nav-link']) }}
                </li>
            </ul>
            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('cp.auth.logout') }}">
                        Выйти&nbsp;&nbsp;<i class="fas fa-fw fa-sign-out-alt"></i>
                    </a>
                </li>
            </ul>
        </nav>

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{ route('cp.dashboard') }}" class="brand-link">
                <img src="{{ asset('img/icon.png') }}" alt="mpAdvisor" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">mp<b>Advisor</b></span>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user -->
                @if($adminUser)
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="{{ $adminUser->avatar_url }}" class="img-circle elevation-2" alt="">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block">{{ $adminUser->full_name }}</a>
                        </div>
                    </div>
                @endif
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-header">Главное</li>
                        <li class="nav-item">
                            <a href="{{ route('cp.dashboard') }}" class="nav-link{{ strpos($currentRouteName, 'cp.dashboard') === 0 ? ' active' : '' }}">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>Обзор</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('cp.users.index') }}" class="nav-link{{ strpos($currentRouteName, 'cp.users') === 0 ? ' active' : '' }}">
                                <i class="nav-icon fas fa-users"></i>
                                <p>Пользователи</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('cp.merchants.index') }}" class="nav-link{{ strpos($currentRouteName, 'cp.merchants') === 0 ? ' active' : '' }}">
                                <i class="nav-icon fas fa-hot-tub"></i>
                                <p>Мерчанты</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('cp.supply.index') }}" class="nav-link{{ strpos($currentRouteName, 'cp.supply') === 0 ? ' active' : '' }}">
                                <i class="nav-icon fas fa-parachute-box"></i>
                                <p>Квант поставки</p>
                            </a>
                        </li>
                        <li class="nav-header">Биллинг</li>
                        <li class="nav-item">
                            <a href="{{ route('cp.rates.index') }}" class="nav-link{{ strpos($currentRouteName, 'cp.rates') === 0 ? ' active' : '' }}">
                                <i class="nav-icon fas fa-money-check-alt"></i>
                                <p>Тарифы</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('cp.billing.index') }}" class="nav-link{{ strpos($currentRouteName, 'cp.billing') === 0 ? ' active' : '' }}">
                                <i class="nav-icon fas fa-exchange-alt"></i>
                                <p>История операций</p>
                            </a>
                        </li>
                        <li class="nav-header">Справочники</li>
                        <li class="nav-item">
                            <a href="{{ route('cp.links.index') }}" class="nav-link{{ strpos($currentRouteName, 'cp.links') === 0 ? ' active' : '' }}">
                                <i class="nav-icon fas fa-link"></i>
                                <p>Ссылки</p>
                            </a>
                        </li>
                        <li class="nav-header">Управление</li>
                        <li class="nav-item">
                            <a href="{{ route('cp.administrators.index') }}" class="nav-link{{ strpos($currentRouteName, 'cp.administrators') === 0 ? ' active' : '' }}">
                                <i class="nav-icon fas fa-users-cog"></i>
                                <p>Администраторы</p>
                            </a>
                        </li>
                        <li class="nav-header">Логирование</li>
                        <li class="nav-item">
                            <a href="{{ route('cp.logs.index') }}" class="nav-link{{ strpos($currentRouteName, 'cp.logs') === 0 ? ' active' : '' }}">
                                <i class="nav-icon fas fa-boxes"></i>
                                <p>Все логи</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('cp.mail-logs.index') }}" class="nav-link{{ strpos($currentRouteName, 'cp.mail-logs') === 0 ? ' active' : '' }}">
                                <i class="nav-icon fas fa-mail-bulk"></i>
                                <p>Письма</p>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6"><h1>@yield('heading')</h1></div>
                        <div class="col-sm-6">{{-- Breadcrumbs --}}</div>
                    </div>
                </div>
            </section>
            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
        </div>
        <footer class="main-footer">Командный центр <b>mpAdvisor</b></footer>
    </div>
@endsection
