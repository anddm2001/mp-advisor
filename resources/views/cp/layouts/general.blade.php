<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title', 'Командный центр')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('kit/plugins/fontawesome-free/css/all.min.css') }}">
        @stack('styles')
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('kit/dist/css/adminlte.min.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700">
        <!-- CP -->
        <link rel="stylesheet" href="{{ asset('css/cp.css') }}">
    </head>
    <body class="hold-transition @yield('body-class', 'sidebar-mini')">
        @yield('body')
        <!-- jQuery -->
        <script src="{{ asset('kit/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap 4 -->
        <script src="{{ asset('kit/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('kit/dist/js/adminlte.min.js') }}"></script>
        @stack('scripts')
        <!-- CP -->
        <script src="{{ asset('js/cp.js') }}"></script>
    </body>
</html>