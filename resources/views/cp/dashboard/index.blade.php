@php
/**
 * @var \App\Modules\ControlPanel\Models\Admin $adminUser
 * @var array $periodsParams
 * @var array $charts
 */
@endphp

@extends('cp.layouts.wrapped')

@push('scripts')
    <!-- Chart.js -->
    <script src="{{ asset('kit/plugins/chart.js/Chart.min.js') }}"></script>
    <script>
        const ctx = [], charts = [], datasetMainOptions = {
            type: 'line',
            label: 'Текущий период',
            backgroundColor: 'transparent',
            borderColor: '#007bff',
            pointBorderColor: '#007bff',
            pointBackgroundColor: '#007bff',
            fill: false
        }, datasetCompareOptions = {
            type: 'line',
            label: 'Сравнение',
            backgroundColor: 'transparent',
            borderColor: '#ced4da',
            pointBorderColor: '#ced4da',
            pointBackgroundColor: '#ced4da',
            fill: false
        }, ticksStyle = {
            fontColor: '#495057',
            fontStyle: 'bold'
        }, chartOptions = {
            maintainAspectRatio: false,
            tooltips: {mode: 'index', intersect: true},
            hover: {mode: 'index', intersect: true},
            // legend: {display: false},
            scales: {
                yAxes: [{
                    gridLines: {
                        display: true,
                        lineWidth: '4px',
                        color: 'rgba(0, 0, 0, .2)',
                        zeroLineColor: 'transparent'
                    },
                    ticks: jQuery.extend({beginAtZero: true}, ticksStyle)
                }],
                xAxes: [{
                    display: true,
                    gridLines: {display: false},
                    ticks: ticksStyle
                }]
            }
        };
    </script>
@endpush

@section('title', 'Панель управления')
@section('heading', 'Обзор')

@section('content')
    <div class="card card-primary">
        <div class="card-header"><h4 class="card-title">Выгрузить</h4></div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm">
                    {{ Form::open(['route' => ['cp.dashboard.export', ['date' => $periodsParams['date'] ?? []]]]) }}
                        <h5>Расшифровка графиков в Excel</h5>
                        {{ Form::submit('Выгрузить', ['class' => 'btn btn-primary']) }}
                        @unless(empty($periodsParams['date']['from'] && $periodsParams['date']['to']))
                            <span class="pl-3 text-muted">
                                (с {{ $periodsParams['date']['from'] }} по {{ $periodsParams['date']['to'] }})
                            </span>
                        @endunless
                    {{ Form::close() }}
                </div>
                <div class="col-sm">
                    <h5>Данные пользователей за период</h5>
                    {{ Form::open(['route' => ['cp.dashboard.export-details']]) }}
                        <div class="row">
                            <div class="col-sm">
                                <x-cp.form.date-picker-between name="export_date" :label="false" :dateOnly="true" />
                            </div>
                            <div class="col-sm">
                                {{ Form::submit('Выгрузить', ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            {{ Form::open(['route' => ['cp.dashboard'], 'method' => 'get']) }}
                <div class="row">
                    <div class="col-sm">
                        <x-cp.form.date-picker-between name="date" label="Период" :value="$periodsParams['date'] ?? []" :dateOnly="true" />
                    </div>
                    <div class="col-sm">
                        <x-cp.form.date-picker-between name="compare_date" label="Период сравнения" :value="$periodsParams['compare_date'] ?? []" :dateOnly="true" />
                    </div>
                    <div class="col-sm">
                        {{ Form::label('', '&nbsp;') }}
                        <div>
                            {{ Form::submit('Задать', ['class' => 'btn btn-primary mr-3']) }}
                            {{ Html::linkRoute('cp.dashboard', 'Сбросить', [], ['class' => 'btn btn-light']) }}
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
        <div class="card-body">
            @foreach($charts as $chart)
                @include('cp.dashboard.partials.chart', compact('chart'))
            @endforeach
        </div>
    </div>
@endsection
