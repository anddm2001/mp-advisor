@php
    /**
     * @var array $chart
     */
@endphp

@push('scripts')
    <script>
        jQuery(function () {
            ctx.{{ $chart['code'] }} = document.getElementById('dashboard-chart-{{ $chart['code'] }}').getContext('2d');
            charts.{{ $chart['code'] }} = new Chart(ctx.{{ $chart['code'] }}, {
                data: {
                    labels: ['{!! implode("', '", $chart['labels']) !!}'],
                    datasets: [
                        jQuery.extend({data: [{{ implode(', ', $chart['data']['main']) }}]}, datasetMainOptions),
                        jQuery.extend({data: [{{ implode(', ', $chart['data']['compare']) }}]}, datasetCompareOptions)
                    ]
                },
                options: chartOptions
            });
        });
    </script>
@endpush

<div class="card card-light">
    <div class="card-header"><h5 class="mb-0">{{ $chart['name'] }}</h5></div>
    <div class="card-body">
        <canvas id="dashboard-chart-{{ $chart['code'] }}" height="300"></canvas>
    </div>
</div>