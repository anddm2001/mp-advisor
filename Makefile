define PWD
pwd
endef

deploy:
	rsync -e "ssh -i ~/.ssh/id_rsa" -rl --exclude ".git" --exclude "vendor" --exclude ".env" $$PWD/ $(SSH_USER)@$(SSH_HOST):$(BUILD_PATH)/
	ssh -i ~/.ssh/id_rsa $(SSH_USER)@$(SSH_HOST) "ln -s /var/www/logs/app/backend $(BUILD_PATH)/storage/logs && ln -s /var/www/storage $(BUILD_PATH)/public/storage && cp $(ENV_BASE) $(BUILD_PATH)/.env && cd $(BUILD_PATH) && ~/bin/composer install && php artisan migrate"
	ssh -i ~/.ssh/id_rsa $(SSH_USER)@$(SSH_HOST) "rm -Rf $(PREVIOUS_BUILD_PATH) && mv $(DEPLOY_PATH) $(PREVIOUS_BUILD_PATH) && mv $(BUILD_PATH) $(DEPLOY_PATH)"
	ssh -i ~/.ssh/id_rsa $(SSH_USER)@$(SSH_HOST) "cd $(DEPLOY_PATH) && php artisan queue:restart"
analyze:
	./vendor/bin/phpstan clear-result-cache
	./vendor/bin/phpstan analyze
