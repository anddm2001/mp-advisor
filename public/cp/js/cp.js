jQuery(function () {
    // Toasts
    if (typeof Swal !== 'undefined') {
        const Toast = Swal.mixin({toast: true, position: 'top', showConfirmButton: false, timer: 5000});
        if (location.hash === '#success') {
            Toast.fire({icon: 'success', title: '&nbsp;&nbsp;&nbsp;&nbsp;Данные были успешно сохранены!'});
        } else if (location.hash === '#failed') {
            Toast.fire({icon: 'error', title: '&nbsp;&nbsp;&nbsp;&nbsp;При сохранении произошла ошибка!'});
        } else if (location.hash === '#deleted') {
            Toast.fire({icon: 'success', title: '&nbsp;&nbsp;&nbsp;&nbsp;Данные были успешно удалены!'});
        } else if (location.hash === '#imported') {
            Toast.fire({icon: 'success', title: '&nbsp;&nbsp;&nbsp;&nbsp;Данные успешно импортированы!'});
        } else if (location.hash === '#queued') {
            Toast.fire({icon: 'success', title: '&nbsp;&nbsp;&nbsp;&nbsp;Задание поставлено в очередь!'});
        }
    }
    // Select2
    if (typeof jQuery.fn.select2 !== 'undefined') {
        jQuery('.select2bs4').select2({language: "ru", placeholder: '', theme: 'bootstrap4'});
    }
    // Popover
    if (typeof jQuery.fn.popover !== 'undefined') {
        jQuery('[data-toggle="popover"]').popover({"placement": "top", "trigger": "hover"});
    }
    // Date & time picker
    if (typeof jQuery.fn.datetimepicker !== 'undefined') {
        const $datePickerInputs = jQuery('.datepicker-input');
        $datePickerInputs.datetimepicker({
            "locale": "ru",
            "format": "DD.MM.YYYY"
        });
        $datePickerInputs.each(function (k, $datePickerInput) {
            $datePickerInput = jQuery($datePickerInput);
            if ($datePickerInput.data('value')) {
                $datePickerInput.datetimepicker('defaultDate', $datePickerInput.data('value'));
            }
        });
        const $dateTimePickerInputs = jQuery('.datetimepicker-input');
        $dateTimePickerInputs.datetimepicker({
            "locale": "ru",
            "format": "DD.MM.YYYY HH:mm:ss"
        });
        $dateTimePickerInputs.each(function (k, $dateTimePickerInput) {
            $dateTimePickerInput = jQuery($dateTimePickerInput);
            if ($dateTimePickerInput.data('value')) {
                $dateTimePickerInput.datetimepicker('defaultDate', $dateTimePickerInput.data('value'));
            }
        });
    }
    // Confirmation
    jQuery('.js-confirmable').on('click submit', function () {
        const confirmation = jQuery(this).data('confirmation');
        return confirm(confirmation ? confirmation : 'Вы уверены?');
    });
    // Submit on change
    jQuery('.js-submit-onchange :input').on('change', function (event) {
        const form = jQuery(this).closest('.js-submit-onchange'),
            inputs = form.find(':input');

        inputs.prop('readonly', true).removeClass('border-danger');
        jQuery.post(form.attr('action'), form.serialize(), 'json').done(function (response) {
            if (response === 'ok') {
                if (typeof Swal !== 'undefined') {
                    const Toast = Swal.mixin({toast: true, position: 'top', showConfirmButton: false, timer: 2500});
                    Toast.fire({icon: 'success', title: '&nbsp;&nbsp;&nbsp;&nbsp;Данные были успешно сохранены!'});
                }
            } else {
                alert('Что-то пошло решительно не так...');
            }
        }).fail(function (response) {
            const errors = response.responseJSON.errors || {};
            let alertText = '';

            if (response.status === 500) {
                alertText = response.responseJSON.message || 'Непредвиденная ошибка при работе с микросервисом.';
            }

            for (let fieldCode in errors) {
                if (!errors.hasOwnProperty(fieldCode)) {
                    continue;
                }
                alertText += alertText === '' ? errors[fieldCode] : "\n" + errors[fieldCode];

                let input = inputs.filter(function () {
                    return jQuery(this).attr('name') === fieldCode;
                });
                if (input) {
                    input.addClass('border-danger');
                }
            }

            if (alertText.length > 0) {
                alert(alertText);
            }
        }).always(function () {
            inputs.prop('readonly', false);
        });
    });
    jQuery('.js-simple-submit-onchange :input').on('change', function (event) {
        const form = jQuery(this).closest('.js-simple-submit-onchange');
        if (typeof form.data('hide-loader') === 'undefined') {
            jQuery(this).after('<i class="fas fa-cog fa-lg fa-spin align-middle text-success"></i>');
            jQuery(this).siblings('label').attr('for', 'unlinkedFromControl').addClass('disabled');
        }
        form.submit();
    });
});
