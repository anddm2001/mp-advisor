<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * описание номенклатур в ответе на запрос каталога номенклатур мерчанта
 *
 * Generated from protobuf message <code>report.Nomenclature</code>
 */
class Nomenclature extends \Google\Protobuf\Internal\Message
{
    /**
     * внутренний идентификатор номенклатуры
     *
     * Generated from protobuf field <code>string ID = 1;</code>
     */
    protected $ID = '';
    /**
     * общее описание номенклатуры
     *
     * Generated from protobuf field <code>.report.NomenclatureCommonData commonData = 2;</code>
     */
    protected $commonData = null;
    /**
     * номенклатурные размеры
     *
     * Generated from protobuf field <code>repeated .report.Nomenclature.Size items = 7;</code>
     */
    private $items;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $ID
     *           внутренний идентификатор номенклатуры
     *     @type \Report\NomenclatureCommonData $commonData
     *           общее описание номенклатуры
     *     @type \Report\Nomenclature\Size[]|\Google\Protobuf\Internal\RepeatedField $items
     *           номенклатурные размеры
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Report::initOnce();
        parent::__construct($data);
    }

    /**
     * внутренний идентификатор номенклатуры
     *
     * Generated from protobuf field <code>string ID = 1;</code>
     * @return string
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * внутренний идентификатор номенклатуры
     *
     * Generated from protobuf field <code>string ID = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setID($var)
    {
        GPBUtil::checkString($var, True);
        $this->ID = $var;

        return $this;
    }

    /**
     * общее описание номенклатуры
     *
     * Generated from protobuf field <code>.report.NomenclatureCommonData commonData = 2;</code>
     * @return \Report\NomenclatureCommonData
     */
    public function getCommonData()
    {
        return isset($this->commonData) ? $this->commonData : null;
    }

    public function hasCommonData()
    {
        return isset($this->commonData);
    }

    public function clearCommonData()
    {
        unset($this->commonData);
    }

    /**
     * общее описание номенклатуры
     *
     * Generated from protobuf field <code>.report.NomenclatureCommonData commonData = 2;</code>
     * @param \Report\NomenclatureCommonData $var
     * @return $this
     */
    public function setCommonData($var)
    {
        GPBUtil::checkMessage($var, \Report\NomenclatureCommonData::class);
        $this->commonData = $var;

        return $this;
    }

    /**
     * номенклатурные размеры
     *
     * Generated from protobuf field <code>repeated .report.Nomenclature.Size items = 7;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * номенклатурные размеры
     *
     * Generated from protobuf field <code>repeated .report.Nomenclature.Size items = 7;</code>
     * @param \Report\Nomenclature\Size[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setItems($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Report\Nomenclature\Size::class);
        $this->items = $arr;

        return $this;
    }

}

