<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report\StockReportSort;

use UnexpectedValueException;

/**
 * направление сортировки
 *
 * Protobuf type <code>report.StockReportSort.SortDirection</code>
 */
class SortDirection
{
    /**
     * по убыванию
     *
     * Generated from protobuf enum <code>DESC = 0;</code>
     */
    const DESC = 0;
    /**
     * по возрастанию
     *
     * Generated from protobuf enum <code>ASC = 1;</code>
     */
    const ASC = 1;

    private static $valueToName = [
        self::DESC => 'DESC',
        self::ASC => 'ASC',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no name defined for value %s', __CLASS__, $value));
        }
        return self::$valueToName[$value];
    }


    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no value defined for name %s', __CLASS__, $name));
        }
        return constant($const);
    }
}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(SortDirection::class, \Report\StockReportSort_SortDirection::class);

