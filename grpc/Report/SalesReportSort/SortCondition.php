<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report\SalesReportSort;

use UnexpectedValueException;

/**
 * способ сортировки
 *
 * Protobuf type <code>report.SalesReportSort.SortCondition</code>
 */
class SortCondition
{
    /**
     * сортировка по выручке
     *
     * Generated from protobuf enum <code>RECEIPT = 0;</code>
     */
    const RECEIPT = 0;
    /**
     * сортировка по прибыли
     *
     * Generated from protobuf enum <code>PROFIT = 1;</code>
     */
    const PROFIT = 1;
    /**
     * сортировка по марже
     *
     * Generated from protobuf enum <code>MARGINALITY = 2;</code>
     */
    const MARGINALITY = 2;
    /**
     * сортировка по заказам
     *
     * Generated from protobuf enum <code>ORDERS = 3;</code>
     */
    const ORDERS = 3;
    /**
     * сортировка по продажам
     *
     * Generated from protobuf enum <code>SALES = 4;</code>
     */
    const SALES = 4;
    /**
     * сортировка по возвратам
     *
     * Generated from protobuf enum <code>RETURNS = 5;</code>
     */
    const RETURNS = 5;
    /**
     * сортировка по скорости заказов
     *
     * Generated from protobuf enum <code>SPEED = 6;</code>
     */
    const SPEED = 6;
    /**
     * сортировка по выкупу
     *
     * Generated from protobuf enum <code>BUYOUT = 7;</code>
     */
    const BUYOUT = 7;
    /**
     * сортировка по скидке
     *
     * Generated from protobuf enum <code>DISCOUNT = 8;</code>
     */
    const DISCOUNT = 8;

    private static $valueToName = [
        self::RECEIPT => 'RECEIPT',
        self::PROFIT => 'PROFIT',
        self::MARGINALITY => 'MARGINALITY',
        self::ORDERS => 'ORDERS',
        self::SALES => 'SALES',
        self::RETURNS => 'RETURNS',
        self::SPEED => 'SPEED',
        self::BUYOUT => 'BUYOUT',
        self::DISCOUNT => 'DISCOUNT',
    ];

    public static function name($value)
    {
        if (!isset(self::$valueToName[$value])) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no name defined for value %s', __CLASS__, $value));
        }
        return self::$valueToName[$value];
    }


    public static function value($name)
    {
        $const = __CLASS__ . '::' . strtoupper($name);
        if (!defined($const)) {
            throw new UnexpectedValueException(sprintf(
                    'Enum %s has no value defined for name %s', __CLASS__, $name));
        }
        return constant($const);
    }
}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(SortCondition::class, \Report\SalesReportSort_SortCondition::class);

