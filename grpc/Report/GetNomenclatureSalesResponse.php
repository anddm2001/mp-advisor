<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * ответ на получение отчёта о продажах номенклатур
 *
 * Generated from protobuf message <code>report.GetNomenclatureSalesResponse</code>
 */
class GetNomenclatureSalesResponse extends \Google\Protobuf\Internal\Message
{
    /**
     * данные на текущей странице
     *
     * Generated from protobuf field <code>repeated .report.GetNomenclatureSalesResponse.NomenclatureSalesReport items = 1;</code>
     */
    private $items;
    /**
     * постраничная навигация
     *
     * Generated from protobuf field <code>.report.PageResponse page = 2;</code>
     */
    protected $page = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Report\GetNomenclatureSalesResponse\NomenclatureSalesReport[]|\Google\Protobuf\Internal\RepeatedField $items
     *           данные на текущей странице
     *     @type \Report\PageResponse $page
     *           постраничная навигация
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Report::initOnce();
        parent::__construct($data);
    }

    /**
     * данные на текущей странице
     *
     * Generated from protobuf field <code>repeated .report.GetNomenclatureSalesResponse.NomenclatureSalesReport items = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * данные на текущей странице
     *
     * Generated from protobuf field <code>repeated .report.GetNomenclatureSalesResponse.NomenclatureSalesReport items = 1;</code>
     * @param \Report\GetNomenclatureSalesResponse\NomenclatureSalesReport[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setItems($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Report\GetNomenclatureSalesResponse\NomenclatureSalesReport::class);
        $this->items = $arr;

        return $this;
    }

    /**
     * постраничная навигация
     *
     * Generated from protobuf field <code>.report.PageResponse page = 2;</code>
     * @return \Report\PageResponse
     */
    public function getPage()
    {
        return isset($this->page) ? $this->page : null;
    }

    public function hasPage()
    {
        return isset($this->page);
    }

    public function clearPage()
    {
        unset($this->page);
    }

    /**
     * постраничная навигация
     *
     * Generated from protobuf field <code>.report.PageResponse page = 2;</code>
     * @param \Report\PageResponse $var
     * @return $this
     */
    public function setPage($var)
    {
        GPBUtil::checkMessage($var, \Report\PageResponse::class);
        $this->page = $var;

        return $this;
    }

}

