<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * ответ на создание поставки
 *
 * Generated from protobuf message <code>report.CreateIncomeResponse</code>
 */
class CreateIncomeResponse extends \Google\Protobuf\Internal\Message
{
    /**
     * количество добавленных позиций
     *
     * Generated from protobuf field <code>int64 nomenclatures = 1;</code>
     */
    protected $nomenclatures = 0;
    /**
     * количество добавленного товара
     *
     * Generated from protobuf field <code>int64 quantity = 2;</code>
     */
    protected $quantity = 0;
    /**
     * не найденных номенклатур
     *
     * Generated from protobuf field <code>int64 unknownNomenclatures = 3;</code>
     */
    protected $unknownNomenclatures = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $nomenclatures
     *           количество добавленных позиций
     *     @type int|string $quantity
     *           количество добавленного товара
     *     @type int|string $unknownNomenclatures
     *           не найденных номенклатур
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Report::initOnce();
        parent::__construct($data);
    }

    /**
     * количество добавленных позиций
     *
     * Generated from protobuf field <code>int64 nomenclatures = 1;</code>
     * @return int|string
     */
    public function getNomenclatures()
    {
        return $this->nomenclatures;
    }

    /**
     * количество добавленных позиций
     *
     * Generated from protobuf field <code>int64 nomenclatures = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setNomenclatures($var)
    {
        GPBUtil::checkInt64($var);
        $this->nomenclatures = $var;

        return $this;
    }

    /**
     * количество добавленного товара
     *
     * Generated from protobuf field <code>int64 quantity = 2;</code>
     * @return int|string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * количество добавленного товара
     *
     * Generated from protobuf field <code>int64 quantity = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setQuantity($var)
    {
        GPBUtil::checkInt64($var);
        $this->quantity = $var;

        return $this;
    }

    /**
     * не найденных номенклатур
     *
     * Generated from protobuf field <code>int64 unknownNomenclatures = 3;</code>
     * @return int|string
     */
    public function getUnknownNomenclatures()
    {
        return $this->unknownNomenclatures;
    }

    /**
     * не найденных номенклатур
     *
     * Generated from protobuf field <code>int64 unknownNomenclatures = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setUnknownNomenclatures($var)
    {
        GPBUtil::checkInt64($var);
        $this->unknownNomenclatures = $var;

        return $this;
    }

}

