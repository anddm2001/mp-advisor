<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * общий вывод результата в отчёте продаж с показателями продаж
 *
 * Generated from protobuf message <code>report.SalesReportItem</code>
 */
class SalesReportItem extends \Google\Protobuf\Internal\Message
{
    /**
     * дата начала сбора
     *
     * Generated from protobuf field <code>.google.protobuf.Timestamp dateFrom = 1;</code>
     */
    protected $dateFrom = null;
    /**
     * дата окончания сбора
     *
     * Generated from protobuf field <code>.google.protobuf.Timestamp dateTo = 2;</code>
     */
    protected $dateTo = null;
    /**
     * текущие показатели
     *
     * Generated from protobuf field <code>.report.SalesReportItem.SalesReportItemData currentData = 3;</code>
     */
    protected $currentData = null;
    /**
     * изменения в показателях
     *
     * Generated from protobuf field <code>.report.SalesReportItem.SalesReportItemData incData = 4;</code>
     */
    protected $incData = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Google\Protobuf\Timestamp $dateFrom
     *           дата начала сбора
     *     @type \Google\Protobuf\Timestamp $dateTo
     *           дата окончания сбора
     *     @type \Report\SalesReportItem\SalesReportItemData $currentData
     *           текущие показатели
     *     @type \Report\SalesReportItem\SalesReportItemData $incData
     *           изменения в показателях
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Report::initOnce();
        parent::__construct($data);
    }

    /**
     * дата начала сбора
     *
     * Generated from protobuf field <code>.google.protobuf.Timestamp dateFrom = 1;</code>
     * @return \Google\Protobuf\Timestamp
     */
    public function getDateFrom()
    {
        return isset($this->dateFrom) ? $this->dateFrom : null;
    }

    public function hasDateFrom()
    {
        return isset($this->dateFrom);
    }

    public function clearDateFrom()
    {
        unset($this->dateFrom);
    }

    /**
     * дата начала сбора
     *
     * Generated from protobuf field <code>.google.protobuf.Timestamp dateFrom = 1;</code>
     * @param \Google\Protobuf\Timestamp $var
     * @return $this
     */
    public function setDateFrom($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\Timestamp::class);
        $this->dateFrom = $var;

        return $this;
    }

    /**
     * дата окончания сбора
     *
     * Generated from protobuf field <code>.google.protobuf.Timestamp dateTo = 2;</code>
     * @return \Google\Protobuf\Timestamp
     */
    public function getDateTo()
    {
        return isset($this->dateTo) ? $this->dateTo : null;
    }

    public function hasDateTo()
    {
        return isset($this->dateTo);
    }

    public function clearDateTo()
    {
        unset($this->dateTo);
    }

    /**
     * дата окончания сбора
     *
     * Generated from protobuf field <code>.google.protobuf.Timestamp dateTo = 2;</code>
     * @param \Google\Protobuf\Timestamp $var
     * @return $this
     */
    public function setDateTo($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\Timestamp::class);
        $this->dateTo = $var;

        return $this;
    }

    /**
     * текущие показатели
     *
     * Generated from protobuf field <code>.report.SalesReportItem.SalesReportItemData currentData = 3;</code>
     * @return \Report\SalesReportItem\SalesReportItemData
     */
    public function getCurrentData()
    {
        return isset($this->currentData) ? $this->currentData : null;
    }

    public function hasCurrentData()
    {
        return isset($this->currentData);
    }

    public function clearCurrentData()
    {
        unset($this->currentData);
    }

    /**
     * текущие показатели
     *
     * Generated from protobuf field <code>.report.SalesReportItem.SalesReportItemData currentData = 3;</code>
     * @param \Report\SalesReportItem\SalesReportItemData $var
     * @return $this
     */
    public function setCurrentData($var)
    {
        GPBUtil::checkMessage($var, \Report\SalesReportItem\SalesReportItemData::class);
        $this->currentData = $var;

        return $this;
    }

    /**
     * изменения в показателях
     *
     * Generated from protobuf field <code>.report.SalesReportItem.SalesReportItemData incData = 4;</code>
     * @return \Report\SalesReportItem\SalesReportItemData
     */
    public function getIncData()
    {
        return isset($this->incData) ? $this->incData : null;
    }

    public function hasIncData()
    {
        return isset($this->incData);
    }

    public function clearIncData()
    {
        unset($this->incData);
    }

    /**
     * изменения в показателях
     *
     * Generated from protobuf field <code>.report.SalesReportItem.SalesReportItemData incData = 4;</code>
     * @param \Report\SalesReportItem\SalesReportItemData $var
     * @return $this
     */
    public function setIncData($var)
    {
        GPBUtil::checkMessage($var, \Report\SalesReportItem\SalesReportItemData::class);
        $this->incData = $var;

        return $this;
    }

}

