<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report;

if (false) {
    /**
     * This class is deprecated. Use Report\GetRecommendationsReportRequest\SortDirection instead.
     * @deprecated
     */
    class GetRecommendationsReportRequest_SortDirection {}
}
class_exists(GetRecommendationsReportRequest\SortDirection::class);
@trigger_error('Report\GetRecommendationsReportRequest_SortDirection is deprecated and will be removed in the next major release. Use Report\GetRecommendationsReportRequest\SortDirection instead', E_USER_DEPRECATED);

