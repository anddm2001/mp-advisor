<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * детальная информация о поставке
 *
 * Generated from protobuf message <code>report.IncomeDetails</code>
 */
class IncomeDetails extends \Google\Protobuf\Internal\Message
{
    /**
     * информация о поставке
     *
     * Generated from protobuf field <code>.report.Income income = 1;</code>
     */
    protected $income = null;
    /**
     * номенклатуры в поставке
     *
     * Generated from protobuf field <code>repeated .report.IncomeNomenclature items = 2;</code>
     */
    private $items;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Report\Income $income
     *           информация о поставке
     *     @type \Report\IncomeNomenclature[]|\Google\Protobuf\Internal\RepeatedField $items
     *           номенклатуры в поставке
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Report::initOnce();
        parent::__construct($data);
    }

    /**
     * информация о поставке
     *
     * Generated from protobuf field <code>.report.Income income = 1;</code>
     * @return \Report\Income
     */
    public function getIncome()
    {
        return isset($this->income) ? $this->income : null;
    }

    public function hasIncome()
    {
        return isset($this->income);
    }

    public function clearIncome()
    {
        unset($this->income);
    }

    /**
     * информация о поставке
     *
     * Generated from protobuf field <code>.report.Income income = 1;</code>
     * @param \Report\Income $var
     * @return $this
     */
    public function setIncome($var)
    {
        GPBUtil::checkMessage($var, \Report\Income::class);
        $this->income = $var;

        return $this;
    }

    /**
     * номенклатуры в поставке
     *
     * Generated from protobuf field <code>repeated .report.IncomeNomenclature items = 2;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * номенклатуры в поставке
     *
     * Generated from protobuf field <code>repeated .report.IncomeNomenclature items = 2;</code>
     * @param \Report\IncomeNomenclature[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setItems($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Report\IncomeNomenclature::class);
        $this->items = $arr;

        return $this;
    }

}

