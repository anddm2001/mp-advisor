<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * ответ на получение сводной информации о номенклатурах мерчанта
 *
 * Generated from protobuf message <code>report.GetNomenclatureSummaryResponse</code>
 */
class GetNomenclatureSummaryResponse extends \Google\Protobuf\Internal\Message
{
    /**
     * общее количество номенклатур, включая неактивные
     *
     * Generated from protobuf field <code>int64 totalNomenclatures = 1;</code>
     */
    protected $totalNomenclatures = 0;
    /**
     * количество активных номенклатур
     *
     * Generated from protobuf field <code>int64 activeNomenclatures = 2;</code>
     */
    protected $activeNomenclatures = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $totalNomenclatures
     *           общее количество номенклатур, включая неактивные
     *     @type int|string $activeNomenclatures
     *           количество активных номенклатур
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Report::initOnce();
        parent::__construct($data);
    }

    /**
     * общее количество номенклатур, включая неактивные
     *
     * Generated from protobuf field <code>int64 totalNomenclatures = 1;</code>
     * @return int|string
     */
    public function getTotalNomenclatures()
    {
        return $this->totalNomenclatures;
    }

    /**
     * общее количество номенклатур, включая неактивные
     *
     * Generated from protobuf field <code>int64 totalNomenclatures = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setTotalNomenclatures($var)
    {
        GPBUtil::checkInt64($var);
        $this->totalNomenclatures = $var;

        return $this;
    }

    /**
     * количество активных номенклатур
     *
     * Generated from protobuf field <code>int64 activeNomenclatures = 2;</code>
     * @return int|string
     */
    public function getActiveNomenclatures()
    {
        return $this->activeNomenclatures;
    }

    /**
     * количество активных номенклатур
     *
     * Generated from protobuf field <code>int64 activeNomenclatures = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setActiveNomenclatures($var)
    {
        GPBUtil::checkInt64($var);
        $this->activeNomenclatures = $var;

        return $this;
    }

}

