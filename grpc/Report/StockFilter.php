<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * фильтр по складам ..
 *
 * Generated from protobuf message <code>report.StockFilter</code>
 */
class StockFilter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.report.NomenclatureFilter filter = 1;</code>
     */
    protected $filter = null;
    /**
     * название скалада
     *
     * Generated from protobuf field <code>string warehouse = 2;</code>
     */
    protected $warehouse = '';
    /**
     * доступно для заказа от
     *
     * Generated from protobuf field <code>int64 availableToOrderFrom = 3;</code>
     */
    protected $availableToOrderFrom = 0;
    /**
     * доступно для заказа до
     *
     * Generated from protobuf field <code>int64 availableToOrderTo = 4;</code>
     */
    protected $availableToOrderTo = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Report\NomenclatureFilter $filter
     *     @type string $warehouse
     *           название скалада
     *     @type int|string $availableToOrderFrom
     *           доступно для заказа от
     *     @type int|string $availableToOrderTo
     *           доступно для заказа до
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Report::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.report.NomenclatureFilter filter = 1;</code>
     * @return \Report\NomenclatureFilter
     */
    public function getFilter()
    {
        return isset($this->filter) ? $this->filter : null;
    }

    public function hasFilter()
    {
        return isset($this->filter);
    }

    public function clearFilter()
    {
        unset($this->filter);
    }

    /**
     * Generated from protobuf field <code>.report.NomenclatureFilter filter = 1;</code>
     * @param \Report\NomenclatureFilter $var
     * @return $this
     */
    public function setFilter($var)
    {
        GPBUtil::checkMessage($var, \Report\NomenclatureFilter::class);
        $this->filter = $var;

        return $this;
    }

    /**
     * название скалада
     *
     * Generated from protobuf field <code>string warehouse = 2;</code>
     * @return string
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * название скалада
     *
     * Generated from protobuf field <code>string warehouse = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setWarehouse($var)
    {
        GPBUtil::checkString($var, True);
        $this->warehouse = $var;

        return $this;
    }

    /**
     * доступно для заказа от
     *
     * Generated from protobuf field <code>int64 availableToOrderFrom = 3;</code>
     * @return int|string
     */
    public function getAvailableToOrderFrom()
    {
        return $this->availableToOrderFrom;
    }

    /**
     * доступно для заказа от
     *
     * Generated from protobuf field <code>int64 availableToOrderFrom = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setAvailableToOrderFrom($var)
    {
        GPBUtil::checkInt64($var);
        $this->availableToOrderFrom = $var;

        return $this;
    }

    /**
     * доступно для заказа до
     *
     * Generated from protobuf field <code>int64 availableToOrderTo = 4;</code>
     * @return int|string
     */
    public function getAvailableToOrderTo()
    {
        return $this->availableToOrderTo;
    }

    /**
     * доступно для заказа до
     *
     * Generated from protobuf field <code>int64 availableToOrderTo = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setAvailableToOrderTo($var)
    {
        GPBUtil::checkInt64($var);
        $this->availableToOrderTo = $var;

        return $this;
    }

}

