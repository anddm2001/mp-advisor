<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * запрос на массовый сбор данных из Wildberries API
 *
 * Generated from protobuf message <code>report.CollectDataRequest</code>
 */
class CollectDataRequest extends \Google\Protobuf\Internal\Message
{
    /**
     * идентификатор мерчанта, для которого собираем данные
     *
     * Generated from protobuf field <code>int64 merchantID = 1;</code>
     */
    protected $merchantID = 0;
    /**
     * ключ дл подключения к Wildberries API
     *
     * Generated from protobuf field <code>string base64Key = 2;</code>
     */
    protected $base64Key = '';
    /**
     * дата, когда был последний сбор данных
     *
     * Generated from protobuf field <code>.google.protobuf.Timestamp dateFrom = 3;</code>
     */
    protected $dateFrom = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $merchantID
     *           идентификатор мерчанта, для которого собираем данные
     *     @type string $base64Key
     *           ключ дл подключения к Wildberries API
     *     @type \Google\Protobuf\Timestamp $dateFrom
     *           дата, когда был последний сбор данных
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Report::initOnce();
        parent::__construct($data);
    }

    /**
     * идентификатор мерчанта, для которого собираем данные
     *
     * Generated from protobuf field <code>int64 merchantID = 1;</code>
     * @return int|string
     */
    public function getMerchantID()
    {
        return $this->merchantID;
    }

    /**
     * идентификатор мерчанта, для которого собираем данные
     *
     * Generated from protobuf field <code>int64 merchantID = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setMerchantID($var)
    {
        GPBUtil::checkInt64($var);
        $this->merchantID = $var;

        return $this;
    }

    /**
     * ключ дл подключения к Wildberries API
     *
     * Generated from protobuf field <code>string base64Key = 2;</code>
     * @return string
     */
    public function getBase64Key()
    {
        return $this->base64Key;
    }

    /**
     * ключ дл подключения к Wildberries API
     *
     * Generated from protobuf field <code>string base64Key = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setBase64Key($var)
    {
        GPBUtil::checkString($var, True);
        $this->base64Key = $var;

        return $this;
    }

    /**
     * дата, когда был последний сбор данных
     *
     * Generated from protobuf field <code>.google.protobuf.Timestamp dateFrom = 3;</code>
     * @return \Google\Protobuf\Timestamp
     */
    public function getDateFrom()
    {
        return isset($this->dateFrom) ? $this->dateFrom : null;
    }

    public function hasDateFrom()
    {
        return isset($this->dateFrom);
    }

    public function clearDateFrom()
    {
        unset($this->dateFrom);
    }

    /**
     * дата, когда был последний сбор данных
     *
     * Generated from protobuf field <code>.google.protobuf.Timestamp dateFrom = 3;</code>
     * @param \Google\Protobuf\Timestamp $var
     * @return $this
     */
    public function setDateFrom($var)
    {
        GPBUtil::checkMessage($var, \Google\Protobuf\Timestamp::class);
        $this->dateFrom = $var;

        return $this;
    }

}

