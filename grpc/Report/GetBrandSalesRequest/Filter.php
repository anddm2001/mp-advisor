<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: report.proto

namespace Report\GetBrandSalesRequest;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>report.GetBrandSalesRequest.Filter</code>
 */
class Filter extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated string brand = 1;</code>
     */
    private $brand;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string[]|\Google\Protobuf\Internal\RepeatedField $brand
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Report::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated string brand = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Generated from protobuf field <code>repeated string brand = 1;</code>
     * @param string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setBrand($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::STRING);
        $this->brand = $arr;

        return $this;
    }

}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(Filter::class, \Report\GetBrandSalesRequest_Filter::class);

