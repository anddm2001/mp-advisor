<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Report;

/**
 * описание микросервиса
 */
class WildberriesReportClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * генерация отчёта рекомендаций
     * @param \Report\GenerateRecommendationsReportRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GenerateRecommendationsReport(\Report\GenerateRecommendationsReportRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GenerateRecommendationsReport',
        $argument,
        ['\Report\GenerateRecommendationsReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * обработка запроса для получения отчёта рекомендаций по всем складам и регионам
     * @param \Report\GetRecommendationsReportRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetRecommendationsReport(\Report\GetRecommendationsReportRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetRecommendationsReport',
        $argument,
        ['\Report\GetRecommendationsReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * получение сводной информации об отчёте рекомендаций
     * @param \Report\GetRecommendationsReportSummaryRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetRecommendationsReportSummary(\Report\GetRecommendationsReportSummaryRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetRecommendationsReportSummary',
        $argument,
        ['\Report\GetRecommendationsReportSummaryResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * запрос на перекалькуляцию сводной информации об отчёте рекомендаций
     * @param \Report\GetRecommendationsReportSummaryRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function RecalculateRecommendationsReportSummary(\Report\GetRecommendationsReportSummaryRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/RecalculateRecommendationsReportSummary',
        $argument,
        ['\Report\GetRecommendationsReportSummaryResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * обновление данные номенклатуры в рекомендации к поставке
     * @param \Report\UpdateRecommendationsNomenclatureRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function UpdateRecommendationsNomenclature(\Report\UpdateRecommendationsNomenclatureRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/UpdateRecommendationsNomenclature',
        $argument,
        ['\Report\UpdateRecommendationsNomenclatureRequest', 'decode'],
        $metadata, $options);
    }

    /**
     * обновление всех данных номенклатуры в рекомендации к поставке
     * @param \Report\UpdateRecommendationsNomenclatureRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function UpdateRecommendationsNomenclatureAll(\Report\UpdateRecommendationsNomenclatureRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/UpdateRecommendationsNomenclatureAll',
        $argument,
        ['\Report\UpdateRecommendationsNomenclatureRequest', 'decode'],
        $metadata, $options);
    }

    /**
     * обработка запроса на сбор данных из Wildberries API
     * @param \Report\CollectDataRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function CollectData(\Report\CollectDataRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/CollectData',
        $argument,
        ['\Report\CollectDataResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * получение сводной информации о номенклатурах мерчанта
     * @param \Report\GetNomenclatureSummaryRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetNomenclatureSummary(\Report\GetNomenclatureSummaryRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetNomenclatureSummary',
        $argument,
        ['\Report\GetNomenclatureSummaryResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * обработка запроса на получение списка доступных брендов
     * @param \Report\GetBrandNamesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetBrandNames(\Report\GetBrandNamesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetBrandNames',
        $argument,
        ['\Report\GetBrandNamesResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * обработка запроса на получение списка доступных названий
     * @param \Report\GetSubjectNamesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetSubjectNames(\Report\GetSubjectNamesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetSubjectNames',
        $argument,
        ['\Report\GetSubjectNamesResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * обработка запроса на получение списка номенклатур мерчанта
     * @param \Report\GetNomenclatureRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetNomenclatures(\Report\GetNomenclatureRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetNomenclatures',
        $argument,
        ['\Report\GetNomenclatureResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * обработка запроса на обновление размера номенклатуры
     * @param \Report\UpdateNomenclatureSizeRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function UpdateNomenclatureSize(\Report\UpdateNomenclatureSizeRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/UpdateNomenclatureSize',
        $argument,
        ['\Report\UpdateNomenclatureSizeRequest', 'decode'],
        $metadata, $options);
    }

    /**
     * получение поставок
     * @param \Report\IncomesListRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetIncomesList(\Report\IncomesListRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetIncomesList',
        $argument,
        ['\Report\IncomesListResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * получение детальной информации о поставке
     * @param \Report\IncomeDetailsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetIncomeDetails(\Report\IncomeDetailsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetIncomeDetails',
        $argument,
        ['\Report\IncomeDetails', 'decode'],
        $metadata, $options);
    }

    /**
     * создание поставки
     * @param \Report\CreateIncomeRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function CreateIncome(\Report\CreateIncomeRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/CreateIncome',
        $argument,
        ['\Report\CreateIncomeResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * удаление поставки
     * @param \Report\IncomeDetailsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function RemoveIncome(\Report\IncomeDetailsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/RemoveIncome',
        $argument,
        ['\Report\IncomeUpdateResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * редактирование поставки
     * @param \Report\IncomeUpdateRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function UpdateIncome(\Report\IncomeUpdateRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/UpdateIncome',
        $argument,
        ['\Report\IncomeUpdateResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * получение общего отчёта продаж для мерчанта
     * @param \Report\SalesReportItemRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetSalesAllReport(\Report\SalesReportItemRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetSalesAllReport',
        $argument,
        ['\Report\SalesReportItem', 'decode'],
        $metadata, $options);
    }

    /**
     * сбор продаж на указанную дату для мерчанта
     * @param \Report\CollectSalesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function CollectSales(\Report\CollectSalesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/CollectSales',
        $argument,
        ['\Report\CollectSalesResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * получить информацию о продажах номенклатур
     * @param \Report\GetNomenclatureSalesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetNomenclaturesSales(\Report\GetNomenclatureSalesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetNomenclaturesSales',
        $argument,
        ['\Report\GetNomenclatureSalesResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * получить информацию о продажах брендов
     * @param \Report\GetBrandSalesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetBrandsSales(\Report\GetBrandSalesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetBrandsSales',
        $argument,
        ['\Report\GetBrandSalesResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * получить информацию о продажах категорий
     * @param \Report\GetCategorySalesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetCategoriesSales(\Report\GetCategorySalesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetCategoriesSales',
        $argument,
        ['\Report\GetCategorySalesResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * получить склады
     * @param \Report\GetWarehousesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetWarehouses(\Report\GetWarehousesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetWarehouses',
        $argument,
        ['\Report\GetWarehousesResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * Создание минимального количества при доставке, для определенного объекта
     * @param \Report\CreateNomenclatureSettingsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function CreatePresetQuantumOfDelivery(\Report\CreateNomenclatureSettingsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/CreatePresetQuantumOfDelivery',
        $argument,
        ['\Report\NomenclatureSettings', 'decode'],
        $metadata, $options);
    }

    /**
     * Вернуть все NomenclatureSettings
     * @param \Report\ListNomenclatureSettingsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function ListNomenclatureSettings(\Report\ListNomenclatureSettingsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/ListNomenclatureSettings',
        $argument,
        ['\Report\ListNomenclatureSettingsResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * Удаление NomenclatureSettings
     * @param \Report\DeleteNomenclatureSettingsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function DeleteNomenclatureSettings(\Report\DeleteNomenclatureSettingsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/DeleteNomenclatureSettings',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

    /**
     * Обновление NomenclatureSettings
     * @param \Report\UpdateNomenclatureSettingsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function UpdateNomenclatureSettings(\Report\UpdateNomenclatureSettingsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/UpdateNomenclatureSettings',
        $argument,
        ['\Report\NomenclatureSettings', 'decode'],
        $metadata, $options);
    }

    /**
     * Получить общую информацию по складам
     * @param \Report\GetStockInfoRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetInfoStocks(\Report\GetStockInfoRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetInfoStocks',
        $argument,
        ['\Report\GetStockInfoResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * Получение агрегированной информации продажам|заказам
     * @param \Report\GetStatisticSalesNomenclatureByRangeRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetStatisticNomenclatureByRange(\Report\GetStatisticSalesNomenclatureByRangeRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetStatisticNomenclatureByRange',
        $argument,
        ['\Report\GetStatisticSalesByRangeResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * Получение агрегированной информации продажам|заказам
     * @param \Report\GetStatisticSalesBrandByRangeRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetStatisticBrandByRange(\Report\GetStatisticSalesBrandByRangeRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetStatisticBrandByRange',
        $argument,
        ['\Report\GetStatisticSalesByRangeResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * Получение агрегированной информации продажам|заказам
     * @param \Report\GetStatisticSalesCategoriesByRangeRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetStatisticCategoriesByRange(\Report\GetStatisticSalesCategoriesByRangeRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetStatisticCategoriesByRange',
        $argument,
        ['\Report\GetStatisticSalesByRangeResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * Получение общих данных по складам
     * @param \Report\GetStocksReportRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetStockReport(\Report\GetStocksReportRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/GetStockReport',
        $argument,
        ['\Report\GetStocksReportResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * Обновление размеров на складе
     * @param \Report\UpdateStockSizeRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function UpdateStockSize(\Report\UpdateStockSizeRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/report.WildberriesReport/UpdateStockSize',
        $argument,
        ['\Report\UpdateStockSizeRequest', 'decode'],
        $metadata, $options);
    }

}
