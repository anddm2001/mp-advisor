<?php

use Illuminate\Support\Facades\Route;

Route::permanentRedirect('/', 'dashboard');

// Authentication (guest part)
Route::middleware('guest:cp')->name('auth.')->group(function () {
    // Login
    Route::prefix('login')->group(function () {
        Route::view('/', 'cp.auth.login-form')->name('login-form');
        Route::post('/', 'AuthController@login')->middleware('throttle:cp-login')->name('login');
    });
    // Password reset
    Route::prefix('password')->name('password.')->group(function () {
        Route::get('/', 'AuthController@passwordRequest')->name('request');
        Route::post('/', 'AuthController@passwordSend')->middleware('throttle:cp-password')->name('send');
        Route::get('{token}', 'AuthController@passwordReset')->name('reset');
        Route::post('{token}', 'AuthController@passwordUpdate')->middleware('throttle:cp-password')->name('update');
    });
});

Route::middleware(['auth:cp', 'admin.active'])->group(function () {
    // Logout
    Route::get('logout', 'AuthController@logout')->name('auth.logout');
    // Dashboard
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::post('dashboard/export', 'DashboardController@export')->name('dashboard.export');
    Route::post('dashboard/export-details', 'DashboardController@exportDetails')->name('dashboard.export-details');
    // Administrators
    Route::resource('administrators', 'AdministratorController')->except(['show', 'destroy']);
    Route::post('administrators/{administrator}/password', 'AdministratorController@password')
        ->name('administrators.password');
    // Users
    Route::resource('users', 'UserController')->except(['show', 'destroy']);
    Route::post('users/{user}/password', 'UserController@password')->name('users.password');
    // Merchants
    Route::resource('merchants', 'MerchantController')->except(['show', 'update', 'destroy']);
    Route::put('merchants/{merchant}/connection/{connection}', 'MerchantController@updateConnection')->name('merchants.connection.update');
    Route::get('merchants/{merchant}/sync', 'MerchantController@sync')->name('merchants.sync');
    Route::post('merchants/{merchant}/sync', 'MerchantController@startSync')->name('merchants.start-sync');
    Route::get('merchants/{merchant}/billing', 'MerchantController@billing')->name('merchants.billing');
    Route::post('merchants/{merchant}/billing/top-up', 'MerchantController@topUp')->name('merchants.billing.top-up');
    Route::post('merchants/{merchant}/billing/charge-on', 'MerchantController@chargeOn')->name('merchants.billing.charge-on');
    // Supply
    Route::resource('supply', 'SupplyController')->only(['index', 'store']);
    Route::post('supply/import', 'SupplyController@import')->name('supply.import');
    Route::post('supply/update', 'SupplyController@update')->name('supply.update');
    Route::delete('supply', 'SupplyController@destroy')->name('supply.destroy');
    // Links handbook
    Route::resource('links', 'LinkHandbookController')->only(['index', 'store', 'update']);
    Route::post('links/upload', 'LinkHandbookController@upload')->name('links.upload');
    // Rates
    Route::resource('rates', 'RateController')->except(['show']);
    // Billing
    Route::get('billing', 'BillingController@index')->name('billing.index');
    // Logs
    Route::get('logs', 'LogController@index')->name('logs.index');
    Route::get('mail-logs', 'MailLogController@index')->name('mail-logs.index');
});
