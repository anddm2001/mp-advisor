<?php

use App\Modules\Billing\Http\Middleware\BillingNotificationMiddleware;
use App\Modules\Billing\Services\BillingService;
use App\Modules\Merchant\Http\Middleware\MerchantIsNotBalanceBlocked;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// API
Route::get('/', function () {
    abort(404);
});

// Auth
Route::namespace('App\Modules\Auth\Http\Controllers')->group(function () {
    // Вход и получение токена
    Route::post('auth/login', 'AuthController@login');

    // Регистрация пользователя
    Route::post('register', 'RegistrationController@register');

    // Сброс пароля
    Route::prefix('password')->group(function () {
        // Отправка письма со ссылкой
        Route::post('request', 'PasswordRecoveryController@request')->name('password.email');

        // Назначение нового пароля
        Route::post('reset/{token}', 'PasswordRecoveryController@reset')->name('password.reset');
    });

    // Верификация e-mail
    Route::post('email/verify/{id}/{hash}', 'EmailVerificationController@verify')
        ->middleware('signed')
        ->name('verification.verify');
});

// Billing
Route::post('billing/notification', function (Request $request, BillingService $billingService) {
    $billingService->handleWebhook($request);
    return \response('');
})->middleware(BillingNotificationMiddleware::class);

// Handbooks
Route::prefix('handbooks')->namespace('App\Modules\Common\Http\Controllers')->group(function () {
    // Ссылки
    Route::get('links', 'HandbookController@links');
});

// Роуты, требующие аутентификацию
Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    // Auth
    Route::namespace('App\Modules\Auth\Http\Controllers')->group(function () {
        // Повторная отправка письма для верификации
        Route::post('email/resend', 'EmailVerificationController@resend')
            ->middleware(['throttle:1'])
            ->withoutMiddleware('verified')
            ->name('verification.send');

        // Приветственный опросник
        Route::get('welcome-survey', 'WelcomeSurveyController@show');
        Route::post('welcome-survey', 'WelcomeSurveyController@update');

        // Данные пользователя
        Route::get('me', 'UserController@show');
        Route::post('me', 'UserController@update');
        Route::post('me/password', 'UserController@password');

        // Выход из системы
        Route::post('auth/logout', 'AuthController@logout');
    });

    // Balance
    Route::namespace('App\Modules\Billing\Http\Controllers')->group(function () {
        // Top-up link
        Route::get('balance/top-up', 'BalanceController@topUp')->name('billing.balance.top-up');
    });

    // Marketplace
    Route::namespace('App\Modules\Marketplace\Http\Controllers')->group(function () {
        // Wildberries
        Route::middleware('marketplace:wildberries')->prefix('wildberries')->group(function () {
            // Склады
            Route::prefix('warehouses')->group(function () {
                Route::get('/', 'WarehouseController@index');
            });
        });
    });

    // Merchant
    Route::prefix('merchant/{merchant}')->middleware('can:view-merchant,merchant')
        ->namespace('App\Modules\Merchant\Http\Controllers')->group(function () {
            // Проверка блокировки по балансу
            Route::middleware(MerchantIsNotBalanceBlocked::class)->group(function () {
                // Wildberries
                Route::middleware('marketplace:wildberries')->prefix('wildberries')->group(function () {
                    // Справочники
                    Route::prefix('handbooks')->group(function () {
                        Route::get('brand-names', 'HandbookController@brandNames');
                        Route::get('subject-names', 'HandbookController@subjectNames');
                    });

                    // Поставки
                    Route::prefix('incomes')->group(function () {
                        Route::get('/', 'IncomeController@index');
                        Route::post('/', 'IncomeController@store');
                        Route::get('{incomeId}', 'IncomeController@show')->where('incomeId', '\d+');
                        Route::post('{incomeId}', 'IncomeController@update')->where('incomeId', '\d+');
                        Route::delete('{incomeId}', 'IncomeController@destroy')->where('incomeId', '\d+');
                    });

                    // Номенклатуры
                    Route::prefix('nomenclatures')->group(function () {
                        Route::get('/', 'NomenclatureController@index');
                        Route::post('/', 'NomenclatureController@update');
                        Route::post('export', 'NomenclatureController@export');
                        Route::get('import-template', 'NomenclatureController@importTemplate');
                        Route::post('import', 'NomenclatureController@import');
                    });

                    // Расчёт поставки
                    Route::prefix('recommendations')->group(function () {
                        Route::get('/', 'RecommendationController@show');
                        Route::post('/', 'RecommendationController@update');
                        Route::get('summary', 'RecommendationController@summary');
                        Route::post('summary', 'RecommendationController@updateSummary');
                        Route::post('nomenclature', 'RecommendationController@nomenclature');
                        Route::post('nomenclature-apply-all', 'RecommendationController@nomenclatureApplyAll');
                        Route::post('export', 'RecommendationController@export');
                    });

                    // Продажи
                    Route::prefix('sales')->group(function () {
                        // Обобщённые данные
                        Route::get('/', 'SaleController@index');
                        // Синхронизация
                        Route::post('sync', 'SaleController@sync');
                        // Номенклатуры
                        Route::prefix('nomenclatures')->group(function () {
                            Route::get('/', 'SaleController@nomenclatures');
                            Route::get('statistic', 'SaleController@statisticNomenclatures');
                            Route::post('export', 'SaleController@exportNomenclatures');
                        });
                        // Бренды
                        Route::prefix('brands')->group(function () {
                            Route::get('/', 'SaleController@brands');
                            Route::get('statistic', 'SaleController@statisticBrands');
                            Route::post('export', 'SaleController@exportBrands');
                        });
                        // Предметы
                        Route::prefix('categories')->group(function () {
                            Route::get('/', 'SaleController@categories');
                            Route::get('statistic', 'SaleController@statisticCategories');
                            Route::post('export', 'SaleController@exportCategories');
                        });
                    });

                    // Склады
                    Route::prefix('stocks')->group(function () {
                        // Обобщённые данные
                        Route::get('/', 'StockController@index');
                        // Отчёт
                        Route::get('report', 'StockController@report');
                        // Экспорт
                        Route::post('export', 'StockController@export');
                        // Импорт
                        Route::get('import-template', 'StockController@importTemplate');
                        Route::post('import', 'StockController@import');
                    });

                    // Настройки
                    Route::prefix('settings')
                        ->middleware('can:update-merchant,merchant')
                        ->group(function () {
                            // Настройки подключения к маркетплейсу
                            Route::get('/', 'SettingController@index');
                            Route::post('/', 'SettingController@update');

                        });

                    // Данные последней синхронизации с маркетплейсом
                    Route::get('last-sync', 'MerchantConnectionController@lastSync')
                        ->middleware('can:update-merchant,merchant')
                        ->withoutMiddleware(MerchantIsNotBalanceBlocked::class);

                    // Общая биллинг-информация для мерчанта
                    Route::get('billing', 'BillingController@index')
                        ->withoutMiddleware(MerchantIsNotBalanceBlocked::class);
                });

                // Файлы
                Route::prefix('files')->group(function () {
                    Route::middleware('can:view-report-result-file,file')->group(function () {
                        Route::get('{file:id}/status', 'FileController@status');
                    });
                });
            });

            // Billing
            Route::prefix('billing')->namespace('\App\Modules\Billing\Http\Controllers')->group(function () {
                // Платежи
                Route::prefix('payments')->name('billing.payment.')->group(function () {
                    // Создание
                    Route::post('/', 'PaymentController@store')->name('store');
                    // Данные платежа
                    Route::get('{billingOperation:id}', 'PaymentController@show')->name('show');
                });
            });

            // Feedback
            Route::post('feedback', 'FeedbackController@store');
        });
});
