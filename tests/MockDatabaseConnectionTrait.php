<?php

namespace Tests;

use Illuminate\Database\Connection;
use Illuminate\Database\PostgresConnection;
use Illuminate\Database\Query\Grammars\PostgresGrammar;
use Illuminate\Database\Query\Processors\Processor;

/**
 * Мокирование подключения к БД в тестах
 *
 * @package Tests
 */
trait MockDatabaseConnectionTrait
{
    /**
     * Эмулировать работу подключения к базе данных
     *
     * @return Connection
     */
    protected function mockConnection(): Connection
    {
        $connection = $this->createMock(PostgresConnection::class);

        $connection->method('transaction')->will(
            $this->returnCallback(
                function($transactionCallback) {
                    return $transactionCallback();
                }
            )
        );

        // мокирование синтаксиса запросов
        $connection->method('getQueryGrammar')->willReturn(new PostgresGrammar());

        $connection->method('getPostProcessor')->willReturn(new Processor());

        /** @var Connection $connection */
        return $connection;
    }

}
