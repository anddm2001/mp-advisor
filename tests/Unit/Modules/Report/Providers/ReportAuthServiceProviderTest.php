<?php

namespace Tests\Unit\Modules\Report\Providers;

use App\Modules\Report\Policies\ReportPolicy;
use App\Modules\Report\Providers\ReportAuthServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate;
use Tests\TestCase;

/**
 * @see ReportAuthServiceProvider
 * @package Tests\Unit\Modules\Report\Providers
 */
class ReportAuthServiceProviderTest extends TestCase
{
    /**
     * @covers \App\Modules\Report\Providers\ReportAuthServiceProvider::policies
     */
    public function testPolicies()
    {
        $app = $this->createApplication();

        $app->register(ReportAuthServiceProvider::class);

        $expected = [
            'view-report-result-file' => ReportPolicy::class . '@allowViewResultFile',
        ];

        /** @var Gate $gateAccessor */
        $gateAccessor = $app->make(Gate::class);

        foreach ($expected as $key => $value) {
            $this->assertTrue($gateAccessor->has($key));
        }
    }
}
