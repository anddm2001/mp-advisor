<?php

namespace Tests\Unit\Modules\Report\Providers;

use App\Modules\Merchant\Providers\MerchantServiceProvider;
use App\Modules\Report\Events\WildberriesApiCollectDataEvent;
use App\Modules\Report\Events\WildberriesApiCollectEachDaySalesEvent;
use App\Modules\Report\Events\WildberriesApiScheduledDataSyncEvent;
use App\Modules\Report\Events\WildberriesBrandsSalesExportEvent;
use App\Modules\Report\Events\WildberriesCategoriesSalesExportEvent;
use App\Modules\Report\Events\WildberriesNomenclaturesExportEvent;
use App\Modules\Report\Events\WildberriesNomenclaturesSalesExportEvent;
use App\Modules\Report\Events\WildberriesRecommendationsExportEvent;
use App\Modules\Report\Events\WildberriesStocksExportEvent;
use App\Modules\Report\Listeners\WildberriesApiCollectDataListener;
use App\Modules\Report\Listeners\WildberriesNomenclaturesExportListener;
use App\Modules\Report\Listeners\WildberriesRecommendationsExportListener;
use App\Modules\Report\Listeners\WildberriesSalesExportListener;
use App\Modules\Report\Listeners\WildberriesStocksExportListener;
use App\Modules\Report\Providers\ReportEventServiceProvider;
use App\Modules\Report\Providers\ReportServiceProvider;
use App\Modules\Storage\Providers\StorageServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use Tests\TestCase;

/**
 * @see ReportEventServiceProvider
 * @package Tests\Unit\Modules\Report\Providers
 */
class ReportEventServiceProviderTest extends TestCase
{
    public function testSubscribers()
    {
        $app = $this->createApplication();

        $app->register(MerchantServiceProvider::class);
        $app->register(StorageServiceProvider::class);
        $app->register(ReportServiceProvider::class);
        $app->register(ReportEventServiceProvider::class);

        $expectedEvents = [
            WildberriesApiCollectDataEvent::class,
            WildberriesApiScheduledDataSyncEvent::class,
            WildberriesNomenclaturesExportEvent::class,
            WildberriesRecommendationsExportEvent::class,
            WildberriesNomenclaturesSalesExportEvent::class,
            WildberriesBrandsSalesExportEvent::class,
            WildberriesCategoriesSalesExportEvent::class,
            WildberriesStocksExportEvent::class,
            WildberriesApiCollectEachDaySalesEvent::class,
        ];

        /** @var Dispatcher $events */
        $events = $app->make('events');

        foreach ($expectedEvents as $event) {
            $this->assertTrue($events->hasListeners($event));
        }
    }

    public function testServices()
    {
        $app = $this->createApplication();

        $app->register(MerchantServiceProvider::class);
        $app->register(StorageServiceProvider::class);
        $app->register(ReportServiceProvider::class);
        $app->register(ReportEventServiceProvider::class);

        $expectedServices = [
            WildberriesApiCollectDataListener::class => WildberriesApiCollectDataListener::class,
            WildberriesNomenclaturesExportListener::class => WildberriesNomenclaturesExportListener::class,
            WildberriesRecommendationsExportListener::class => WildberriesRecommendationsExportListener::class,
            WildberriesSalesExportListener::class => WildberriesSalesExportListener::class,
            WildberriesStocksExportListener::class => WildberriesStocksExportListener::class,
        ];

        foreach ($expectedServices as $serviceKey => $serviceClass) {
            $this->assertInstanceOf($serviceClass, $app->make($serviceKey));
        }
    }
}
