<?php

namespace Tests\Unit\Modules\Report\Providers;

use App\Modules\Report\Contracts\ReportResultFileServiceContract;
use App\Modules\Report\Exporter\WildberriesIncomeExporter;
use App\Modules\Report\Exporter\WildberriesNomenclaturesExporter;
use App\Modules\Report\Exporter\WildberriesSalesExporter;
use App\Modules\Report\Exporter\WildberriesStocksExporter;
use App\Modules\Report\Providers\ReportServiceProvider;
use App\Modules\Report\Services\ReportResultFileService;
use App\Modules\Report\Exporter\WildberriesRecommendationsExporter;
use App\Modules\Report\Services\WildberriesReportApi;
use App\Modules\Storage\Providers\StorageServiceProvider;
use Tests\TestCase;

/**
 * @see ReportServiceProvider
 * @package Tests\Unit\Modules\Report\Providers
 */
class ReportServiceProviderTest extends TestCase
{
    /**
     * @covers \App\Modules\Report\Providers\ReportServiceProvider::register
     */
    public function testRegister()
    {
        $app = $this->createApplication();

        $app->register(ReportServiceProvider::class);
        $app->register(StorageServiceProvider::class);

        $expectedServices = [
            ReportResultFileServiceContract::class => ReportResultFileService::class,
            WildberriesReportApi::class => WildberriesReportApi::class,
            WildberriesIncomeExporter::class => WildberriesIncomeExporter::class,
            WildberriesRecommendationsExporter::class => WildberriesRecommendationsExporter::class,
            WildberriesSalesExporter::class => WildberriesSalesExporter::class,
            WildberriesNomenclaturesExporter::class => WildberriesNomenclaturesExporter::class,
            WildberriesStocksExporter::class => WildberriesStocksExporter::class,
        ];

        foreach ($expectedServices as $key => $serviceClass) {
            $this->assertInstanceOf($serviceClass, $app->make($key));
        }
    }
}
