<?php

namespace Tests\Unit\Modules\Merchant\Providers;

use App\Modules\Merchant\Providers\MerchantAuthServiceProvider;
use App\Modules\Merchant\Policies\MerchantPolicy;
use Illuminate\Contracts\Auth\Access\Gate;
use Tests\TestCase;

/**
 * @see MerchantAuthServiceProvider
 * @package Tests\Unit\Modules\Merchant\Http\Controllers
 */
class MerchantAuthServiceProviderTest extends TestCase
{
    /**
     * @covers \App\Modules\Report\Providers\ReportAuthServiceProvider::policies
     */
    public function testPolicies()
    {
        $app = $this->createApplication();

        $app->register(MerchantAuthServiceProvider::class);

        $expected = [
            'view-merchant' => MerchantPolicy::class . '@allowViewMerchant',
            'update-merchant' => MerchantPolicy::class . '@allowUpdateMerchant',
            'view-merchant-connection' => MerchantPolicy::class . '@allowViewMerchantConnection',
        ];

        /** @var Gate $gateAccessor */
        $gateAccessor = $app->make(Gate::class);

        foreach ($expected as $key => $value) {
            $this->assertTrue($gateAccessor->has($key));
        }
    }
}
