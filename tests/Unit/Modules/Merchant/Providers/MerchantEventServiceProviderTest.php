<?php

namespace Tests\Unit\Modules\Merchant\Providers;

use App\Modules\Common\Listeners\LogEventListener;
use App\Modules\Merchant\Events\MerchantConnectionCredentialsCreatedEvent;
use App\Modules\Merchant\Listeners\MerchantConnectionFixCreatedAtListener;
use App\Modules\Merchant\Providers\MerchantEventServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use Tests\TestCase;

/**
 * @see MerchantEventServiceProvider
 * @package Tests\Unit\Modules\Merchant\Providers
 */
class MerchantEventServiceProviderTest extends TestCase
{
    public function testSubscribers()
    {
        $app = $this->createApplication();

        $app->register(MerchantEventServiceProvider::class);

        $expectedEvents = [
            MerchantConnectionCredentialsCreatedEvent::class,
        ];

        /** @var Dispatcher $events */
        $events = $app->make('events');

        foreach ($expectedEvents as $event) {
            $this->assertTrue($events->hasListeners($event));
        }
    }

    public function testServices()
    {
        $app = $this->createApplication();

        $app->register(MerchantEventServiceProvider::class);

        $expectedServices = [
            MerchantConnectionFixCreatedAtListener::class => MerchantConnectionFixCreatedAtListener::class,
            LogEventListener::class => LogEventListener::class,
        ];

        foreach ($expectedServices as $serviceKey => $serviceClass) {
            $this->assertInstanceOf($serviceClass, $app->make($serviceKey));
        }
    }
}
