<?php

namespace Tests\Unit\Modules\Merchant\Providers;

use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Providers\MerchantServiceProvider;
use App\Modules\Merchant\Services\MerchantConnectionManagerService;
use Tests\TestCase;

/**
 * @see MerchantServiceProvider
 * @package Tests\Unit\Modules\Merchant\Providers
 */
class MerchantServiceProviderTest extends TestCase
{
    /**
     * @covers \App\Modules\Merchant\Providers\MerchantServiceProvider::register
     */
    public function testRegister()
    {
        $app = $this->createApplication();

        $app->register(MerchantServiceProvider::class);

        $expectedServices = [
            MerchantConnectionManagerContract::class => MerchantConnectionManagerService::class,
        ];

        foreach ($expectedServices as $serviceKey => $serviceClass) {
            $this->assertInstanceOf($serviceClass, $app->make($serviceKey));
        }
    }
}
