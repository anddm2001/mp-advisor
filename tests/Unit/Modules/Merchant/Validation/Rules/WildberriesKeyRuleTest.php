<?php

namespace Tests\Unit\Modules\Merchant\Validation\Rules;

use App\Modules\Merchant\Validation\Rules\WildberriesKeyRule;
use PHPUnit\Framework\TestCase;

/**
 * @see WildberriesKeyRule
 * @package Tests\Unit\Modules\Merchant\Validation\Rules
 */
class WildberriesKeyRuleTest extends TestCase
{
    public function getDataProvider()
    {
        return [
            [
                'ae822260-3333-5c76-888c-f729aa1239e8',
                true,
            ],
            [
                'YWU4MjIyNjAtMzMzMy01Yzc2LTg4OGMtZjcyOWFhMTIzOWU4',
                true,
            ],
            [
                'ae82226033335c76888cf729aa1239e8',
                false,
            ],
            [
                'YWU4MjsysjAtszMzSy01Yzc2LTg4OGMtZjcyOWFhMTIzOWU4',
                false,
            ],
            [
                'YWU4MjIyNjAtMzMzMy01Yzc2LTg4OGMtZjcyOWFhMTIzOWU',
                false,
            ],
            [
                str_repeat('A', 48),
                false,
            ],
            [
                str_repeat('A', 36),
                false,
            ],
        ];
    }

    /**
     * @covers \App\Modules\Merchant\Validation\Rules\WildberriesKeyRule::passes
     * @dataProvider getDataProvider
     *
     * @param string $key
     * @param bool $isValid
     */
    public function testPasses(string $key, bool $isValid)
    {
        $rule = new WildberriesKeyRule();
        $this->assertEquals($isValid, $rule->passes('key', $key));
    }
}
