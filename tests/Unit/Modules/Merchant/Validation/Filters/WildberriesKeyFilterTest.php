<?php

namespace Tests\Unit\Modules\Merchant\Validation\Filters;

use App\Modules\Merchant\Validation\Filters\WildberriesKeyFilter;
use PHPUnit\Framework\TestCase;

/**
 * @see WildberriesKeyFilter
 * @package Tests\Unit\Modules\Merchant\Validation\Filters
 */
class WildberriesKeyFilterTest extends TestCase
{
    public function getDataProvider()
    {
        return [
            [
                'ae822260-3333-5c76-888c-f729aa1239e8',
                'YWU4MjIyNjAtMzMzMy01Yzc2LTg4OGMtZjcyOWFhMTIzOWU4',
            ],
            [
                'ae82226033335c76888cf729aa1239e8',
                'ae82226033335c76888cf729aa1239e8',
            ],
            [
                'YWU4MjIyNjAtMzMzMy01Yzc2LTg4OGMtZjcyOWFhMTIzOWU4',
                'YWU4MjIyNjAtMzMzMy01Yzc2LTg4OGMtZjcyOWFhMTIzOWU4',
            ],
        ];
    }

    /**
     * @dataProvider getDataProvider
     * @covers \App\Modules\Merchant\Validation\Filters\WildberriesKeyFilter::filter
     *
     * @param string $input
     * @param string $output
     */
    public function testFilter(string $input, string $output)
    {
        $filter = new WildberriesKeyFilter();

        $this->assertEquals($output, $filter->filter($input));
    }
}
