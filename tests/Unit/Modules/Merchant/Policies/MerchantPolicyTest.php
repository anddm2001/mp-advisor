<?php

namespace Tests\Unit\Modules\Merchant\Policies;

use App\Modules\Auth\Models\User;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Policies\MerchantPolicy;
use Illuminate\Support\Collection;
use Tests\TestCase;

/**
 * @see MerchantPolicy
 * @package Tests\Unit\Modules\Merchant\Policies
 */
class MerchantPolicyTest extends TestCase
{
    public function getAllowViewMerchantDataProvider()
    {
        return [
            [
                [1, 2, 3], // current user merchants
                10, // current merchant
                false, // allow
            ],
            [
                [], // current user merchants
                10, // current merchant
                false, // allow
            ],
            [
                [10, 1, 2, 3], // current user merchants
                10, // current merchant
                true, // allow
            ],
            [
                [10], // current user merchants
                10, // current merchant
                true, // allow
            ],
        ];
    }

    /**
     * @dataProvider getAllowViewMerchantDataProvider
     * @covers \App\Modules\Merchant\Policies\MerchantPolicy::allowViewMerchant
     *
     * @param array $currentUserMerchants
     * @param int $currentMerchantId
     * @param bool $allow
     */
    public function testAllowViewMerchant(array $currentUserMerchants, int $currentMerchantId, bool $allow)
    {
        $user = new User();
        $user->merchants = (new Collection($currentUserMerchants))
            ->map(function(int $merchantId): Merchant {
                $model = new Merchant();
                $model->id = $merchantId;
                return $model;
            });

        $merchant = new Merchant();
        $merchant->id = $currentMerchantId;

        $policy = new MerchantPolicy();

        $this->assertEquals($allow, $policy->allowViewMerchant($user, $merchant));
    }

    public function getUpdateMerchantDataProvider()
    {
        return [
            [
                10, // current user id
                100, // current merchant owner id
                false, // allow
            ],
            [
                10, // current user id
                10, // current merchant owner id
                true, // allow
            ]
        ];
    }

    /**
     * @dataProvider getUpdateMerchantDataProvider
     * @covers \App\Modules\Merchant\Policies\MerchantPolicy::allowUpdateMerchant
     *
     * @param int $currentUserId
     * @param int $currentMerchantOwnerId
     * @param bool $allow
     */
    public function testUpdateMerchant(int $currentUserId, int $currentMerchantOwnerId, bool $allow)
    {
        $user = new User();
        $user->id = $currentUserId;

        $merchant = new Merchant();
        $merchant->owner_id = $currentMerchantOwnerId;

        $policy = new MerchantPolicy();
        $this->assertEquals($allow, $policy->allowUpdateMerchant($user, $merchant));
    }

    public function getViewMerchantConnectionDataProvider()
    {
        return [
            [
                10, // current user id
                100, // current merchant owner id
                1, // current merchant id
                1, // connection merchant id
                false, // allow
            ],
            [
                10, // current user id
                10, // current merchant owner id
                1, // current merchant id
                1, // connection merchant id
                true, // allow
            ],
            [
                10, // current user id
                11, // current merchant owner id
                1, // current merchant id
                2, // connection merchant id
                false, // allow
            ],
            [
                10, // current user id
                10, // current merchant owner id
                1, // current merchant id
                2, // connection merchant id
                false, // allow
            ],
        ];
    }

    /**
     * @dataProvider getViewMerchantConnectionDataProvider
     * @covers \App\Modules\Merchant\Policies\MerchantPolicy::allowViewMerchantConnection
     *
     * @param int $currentUserId
     * @param int $currentMerchantOwnerId
     * @param int $currentMerchantId
     * @param int $connectionMerchantId
     *
     * @param bool $allow
     */
    public function testViewMerchantConnection(int $currentUserId, int $currentMerchantOwnerId, int $currentMerchantId, int $connectionMerchantId, bool $allow)
    {
        $user = new User();
        $user->id = $currentUserId;

        $merchant = new Merchant();
        $merchant->id = $currentMerchantId;
        $merchant->owner_id = $currentMerchantOwnerId;

        $connection = new MerchantConnection();
        $connection->merchant_id = $connectionMerchantId;

        $policy = new MerchantPolicy();
        $this->assertEquals($allow, $policy->allowViewMerchantConnection($user, $merchant, $connection));
    }
}
