<?php

namespace Tests\Unit\Modules\Merchant\Services;

use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Requests\StoreMerchantConnectionRequest;
use App\Modules\Merchant\Services\MerchantConnectionManagerService;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

/**
 * @see MerchantConnectionManagerService
 * @package Tests\Unit\Modules\Merchant\Services
 */
class MerchantConnectionManagerServiceTest extends TestCase
{
    protected array $staticConfig = [
        [
            'marketplace' => 1,
            'credentials' => [
                'field_1' => [
                    'validation' => [
                        'required',
                        'string',
                    ],
                    'title' => 'Some field 1',
                ],
            ],
        ],
        [
            'marketplace' => 2,
            'credentials' => [
                'username' => [
                    'validation' => [
                        'required',
                        'string',
                        'min:10'
                    ],
                    'title' => 'Username',
                ],
                'password' => [
                    'validation' => [
                        'required',
                        'string',
                        'min:32',
                    ],
                    'title' => 'Password',
                ],
            ],
        ],
    ];

    protected function mockEncrypter(): Encrypter
    {
        $encrypter = $this->createPartialMock(Encrypter::class, ['encrypt', 'decrypt']);

        $encrypter->method('encrypt')->will($this->returnCallback(function(array $data) {
            return json_encode($data);
        }));

        $encrypter->method('decrypt')->will($this->returnCallback(function(string $data) {
            return json_decode($data, true);
        }));

        /** @var Encrypter $encrypter */
        return $encrypter;
    }

    public function getCredentialsValidationDataProvider()
    {
        $items = [];

        foreach ($this->staticConfig as $marketplaceApiConfig) {
            $item = [];
            $item[] = $this->staticConfig;
            $item[] = $marketplaceApiConfig['marketplace'];

            $fields = [];
            foreach ($marketplaceApiConfig['credentials'] as $field => $fieldConfig) {
                $fields[$field] = $fieldConfig['validation'];
            }

            $item[] = $fields;

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @dataProvider getCredentialsValidationDataProvider
     * @covers \App\Modules\Merchant\Services\MerchantConnectionManagerService::getCredentialsValidation
     *
     * @param array $commonConfig
     * @param int $marketplace
     * @param array $expectedResult
     */
    public function testGetCredentialsValidation(array $commonConfig, int $marketplace, array $expectedResult)
    {
        $service = new MerchantConnectionManagerService($commonConfig, $this->createPartialMock(Encrypter::class, []));

        $this->assertEquals($expectedResult, $service->getCredentialsValidation($marketplace));
    }

    public function getCredentialsNamesDataProvider()
    {
        $items = [];

        foreach ($this->staticConfig as $marketplaceApiConfig) {
            $item = [];
            $item[] = $this->staticConfig;
            $item[] = $marketplaceApiConfig['marketplace'];

            $fields = [];
            foreach ($marketplaceApiConfig['credentials'] as $field => $fieldConfig) {
                $fields[$field] = $fieldConfig['title'];
            }

            $item[] = $fields;

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @dataProvider getCredentialsNamesDataProvider
     * @covers \App\Modules\Merchant\Services\MerchantConnectionManagerService::getCredentialsNames
     *
     * @param array $commonConfig
     * @param int $marketplace
     * @param array $expectedResult
     */
    public function testGetCredentialsNames(array $commonConfig, int $marketplace, array $expectedResult)
    {
        $service = new MerchantConnectionManagerService($commonConfig, $this->createPartialMock(Encrypter::class, []));

        $this->assertEquals($expectedResult, $service->getCredentialsNames($marketplace));
    }

    public function getStoreDataProvider()
    {
        $items = [];

        foreach ($this->staticConfig as $marketplaceApiConfig) {
            $item = [];
            $item[] = $this->staticConfig;
            $item[] = $marketplaceApiConfig['marketplace'];

            $fields = [];
            foreach ($marketplaceApiConfig['credentials'] as $field => $fieldConfig) {
                $fields[$field] = 'value_of_' . $field;
            }

            $item[] = $fields;

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @dataProvider getStoreDataProvider
     * @covers \App\Modules\Merchant\Services\MerchantConnectionManagerService::store
     *
     * @param array $staticConfig
     * @param int $marketplace
     * @param array $requestData
     */
    public function testStore(array $staticConfig, int $marketplace, array $requestData)
    {
        $merchantId = 100;

        $request = $this->getMockBuilder(StoreMerchantConnectionRequest::class)
            ->setConstructorArgs([$requestData])
            ->onlyMethods(['validateResolved', 'getMarketplace'])
            ->getMock();

        $request->expects($this->any())->method('validateResolved')->willReturn(null);
        $request->expects($this->any())->method('getMarketplace')->willReturn($marketplace);

        /** @var StoreMerchantConnectionRequest $request */

        $encrypter = $this->mockEncrypter();

        $merchant = new Merchant();
        $merchant->id = $merchantId;

        $model = $this->createPartialMock(MerchantConnection::class, ['save']);
        $model->method('save')->willReturn(true);

        /** @var MerchantConnection $model */

        $service = new MerchantConnectionManagerService($staticConfig, $encrypter);

        $this->assertTrue($service->store($request, $merchant, $model));

        $this->assertEquals($merchantId, $model->merchant_id);
        $this->assertEquals($marketplace, $model->marketplace);
        $this->assertEquals(json_encode($requestData), $model->credentials);
    }

    public function getCredentialsValuesDataProvider()
    {
        $config = $this->staticConfig;

        $items = [];

        foreach ($config as $marketplaceConfig) {
            $item = [];

            $item[] = $config;
            $item[] = $marketplaceConfig['marketplace'];

            $connectionValues = [];

            foreach ($marketplaceConfig['credentials'] as $field => $fieldConfig) {
                $connectionValues[$field] = 'value of ' . $field;
            }

            $item[] = $connectionValues;

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @dataProvider getCredentialsValuesDataProvider
     * @covers \App\Modules\Merchant\Services\MerchantConnectionManagerService::getCredentialsValues
     *
     * @param array $staticConfig
     * @param int $marketplace
     * @param array $expectedValues
     */
    public function testGetCredentialsValues(array $staticConfig, int $marketplace, array $expectedValues)
    {
        $encrypter = $this->mockEncrypter();

        $connection = new MerchantConnection();
        $connection->marketplace = $marketplace;
        $connection->credentials = $encrypter->encrypt($expectedValues);

        $service = new MerchantConnectionManagerService($staticConfig, $encrypter);

        $this->assertEquals($expectedValues, $service->getCredentialsValues($connection));
    }

    /**
     * @dataProvider getCredentialsValuesDataProvider
     * @covers       \App\Modules\Merchant\Services\MerchantConnectionManagerService::credentialsHaveChanged
     *
     * @param array $staticConfig
     * @param int $marketplace
     * @param array $newCredentials
     */
    public function testCredentialsHaveChanged(array $staticConfig, int $marketplace, array $newCredentials)
    {
        $encrypter = $this->mockEncrypter();

        $connection = new MerchantConnection();
        $connection->marketplace = $marketplace;
        $connection->credentials = $encrypter->encrypt($newCredentials);

        $service = new MerchantConnectionManagerService($staticConfig, $encrypter);

        // unchanged credentials should return false
        $this->assertFalse($service->credentialsHaveChanged($connection, $newCredentials));

        // changed credentials should return true
        $newCredentials[array_key_first($newCredentials)] .= ' changed';
        $this->assertTrue($service->credentialsHaveChanged($connection, $newCredentials));

        // empty old credentials should return true
        $connection->credentials = [];
        $this->assertTrue($service->credentialsHaveChanged($connection, $newCredentials));
    }
}
