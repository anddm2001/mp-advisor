<?php

namespace Tests\Unit\Modules\Storage\Services;

use App\Modules\Storage\Models\File;
use App\Modules\Storage\Services\StorageService;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

/**
 * @see StorageService::upload()
 *
 * @package Tests\Unit\Modules\Storage\Services
 */
class StorageServiceTest extends TestCase
{
    /**
     * @covers \App\Modules\Storage\Services\StorageService::upload
     */
    public function testUpload()
    {
        $fileSystemAdapter = $this->createMock(FilesystemAdapter::class);

        $fileSystemAdapter->method('putFileAs')->willReturn(true);

        /** @var FilesystemAdapter $fileSystemAdapter */

        $uploadedFile = $this->createPartialMock(UploadedFile::class, [
            'getPathname',
            'getClientOriginalExtension',
            'getClientOriginalName',
            'getSize',
            'getClientMimeType',
        ]);

        $uploadedFile->method('getPathname')->willReturn('some/path/file.jpeg');
        $uploadedFile->method('getClientOriginalExtension')->willReturn('jpeg');
        $uploadedFile->method('getClientOriginalName')->willReturn('file.jpeg');
        $uploadedFile->method('getSize')->willReturn(1024);
        $uploadedFile->method('getClientMimeType')->willReturn('image/jpeg');

        /** @var UploadedFile $uploadedFile */

        $fileModel = $this->createPartialMock(File::class, [
            'getConnection',
            'save',
        ]);

        $fileModel->method('getConnection')->willReturn($this->mockConnection());
        $fileModel->method('save')->willReturn(true);

        /** @var File $fileModel */

        $service = new StorageService($fileSystemAdapter, $fileModel);

        $result = $service->upload($uploadedFile);

        $this->assertInstanceOf(File::class, $result);

        $this->assertNotEmpty($result->path);
        $this->assertEquals('file.jpeg', $result->original_name);
        $this->assertEquals(1024, $result->size);
        $this->assertStringEndsWith('.jpeg', $result->name);
        $this->assertEquals('image/jpeg', $result->mime);
    }
}
