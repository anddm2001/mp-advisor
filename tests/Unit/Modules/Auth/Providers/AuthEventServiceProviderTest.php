<?php

namespace Tests\Unit\Modules\Auth\Providers;

use App\Modules\Auth\Listeners\SendUserWelcomeNotification;
use App\Modules\Auth\Providers\AuthEventServiceProvider;
use App\Modules\Common\Listeners\LogEventListener;
use App\Modules\Common\Listeners\NotificationSentMailLog;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Notifications\Events\NotificationSent;
use Tests\TestCase;

/**
 * @see AuthEventServiceProviderer
 * @package Tests\Unit\Modules\Auth\Providers
 */
class AuthEventServiceProviderTest extends TestCase
{
    public function testSubscribers()
    {
        $app = $this->createApplication();

        $app->register(AuthEventServiceProvider::class);

        $expectedEvents = [
            Login::class,
            Registered::class,
            Verified::class,
            NotificationSent::class,
        ];

        /** @var Dispatcher $events */
        $events = $app->make('events');

        foreach ($expectedEvents as $event) {
            $this->assertTrue($events->hasListeners($event));
        }
    }

    public function testServices()
    {
        $app = $this->createApplication();

        $app->register(AuthEventServiceProvider::class);

        $expectedServices = [
            LogEventListener::class => LogEventListener::class,
            SendEmailVerificationNotification::class => SendEmailVerificationNotification::class,
            SendUserWelcomeNotification::class => SendUserWelcomeNotification::class,
            NotificationSentMailLog::class => NotificationSentMailLog::class,
        ];

        foreach ($expectedServices as $serviceKey => $serviceClass) {
            $this->assertInstanceOf($serviceClass, $app->make($serviceKey));
        }
    }
}
