<?php

namespace Tests\Unit\Modules\Auth\Providers;

use App\Modules\Auth\Http\Middleware\HttpBasicAuth;
use App\Modules\Auth\Providers\AuthServiceProvider;
use Tests\TestCase;

/**
 * @see AuthServiceProvider
 * @package Tests\Unit\Modules\Auth\Providers
 */
class AuthServiceProviderTest extends TestCase
{
    /**
     * @covers \App\Modules\Auth\Providers\AuthServiceProvider::register
     */
    public function testRegister()
    {
        $app = $this->createApplication();

        $app->register(AuthServiceProvider::class);

        $expectedServices = [
            HttpBasicAuth::class => HttpBasicAuth::class,
        ];

        foreach ($expectedServices as $key => $serviceClass) {
            $this->assertInstanceOf($serviceClass, $app->make($serviceClass));
        }
    }
}
