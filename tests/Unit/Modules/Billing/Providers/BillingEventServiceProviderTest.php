<?php

namespace Tests\Unit\Modules\Billing\Providers;

use App\Modules\Billing\Events\BillingChargeEvent;
use App\Modules\Billing\Events\BillingPaymentToEventLogEvent;
use App\Modules\Billing\Events\BillingTrialPeriodFinishSetEvent;
use App\Modules\Billing\Listeners\BillingChargeListener;
use App\Modules\Billing\Providers\BillingEventServiceProvider;
use App\Modules\Common\Listeners\LogEventListener;
use Illuminate\Contracts\Events\Dispatcher;
use Tests\TestCase;

/**
 * @see BillingEventServiceProvider
 * @package Tests\Unit\Modules\Billing\Providers
 */
class BillingEventServiceProviderTest extends TestCase
{
    public function testSubscribers()
    {
        $app = $this->createApplication();

        $app->register(BillingEventServiceProvider::class);

        $expectedEvents = [
            BillingChargeEvent::class,
            BillingTrialPeriodFinishSetEvent::class,
            BillingPaymentToEventLogEvent::class,
        ];

        /** @var Dispatcher $events */
        $events = $app->make('events');

        foreach ($expectedEvents as $event) {
            $this->assertTrue($events->hasListeners($event));
        }
    }

    public function testServices()
    {
        $app = $this->createApplication();

        $app->register(BillingEventServiceProvider::class);

        $expectedServices = [
            BillingChargeListener::class => BillingChargeListener::class,
            LogEventListener::class => LogEventListener::class,
        ];

        foreach ($expectedServices as $serviceKey => $serviceClass) {
            $this->assertInstanceOf($serviceClass, $app->make($serviceKey));
        }
    }
}
