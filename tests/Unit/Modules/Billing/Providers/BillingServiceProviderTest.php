<?php

namespace Tests\Unit\Modules\Billing\Providers;

use App\Modules\Billing\Http\Middleware\BillingNotificationMiddleware;
use App\Modules\Billing\Http\Middleware\YooKassaCheckNotificationIp;
use App\Modules\Billing\Providers\BillingServiceProvider;
use App\Modules\Billing\Services\BillingService;
use Tests\TestCase;

/**
 * @see BillingServiceProvider
 * @package Tests\Unit\Modules\Billing\Providers
 */
class BillingServiceProviderTest extends TestCase
{
    /**
     * @covers \App\Modules\Billing\Providers\BillingServiceProvider::register
     */
    public function testRegister()
    {
        $app = $this->createApplication();

        $app->register(BillingServiceProvider::class);

        $expectedServices = [
            BillingService::class => BillingService::class,
            BillingNotificationMiddleware::class => YooKassaCheckNotificationIp::class,
        ];

        foreach ($expectedServices as $key => $serviceClass) {
            $this->assertInstanceOf($serviceClass, $app->make($serviceClass));
        }
    }
}
