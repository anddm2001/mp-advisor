<?php


namespace App\Queue;


use Illuminate\Queue\WorkerOptions;

trait WithoutTimeout
{
    protected function registerTimeoutHandler($job, WorkerOptions $options)
    {
        // We will register a signal handler for the alarm signal so that we can kill this
        // process if it is running too long because it has frozen. This uses the async
        // signals supported in recent versions of PHP to accomplish it conveniently.
        pcntl_signal(SIGALRM, function () use ($job, $options) {
            if ($job) {
                $this->markJobAsFailedIfWillExceedMaxAttempts(
                    $job->getConnectionName(), $job, (int) $options->maxTries, $e = $this->maxAttemptsExceededException($job)
                );

                $this->markJobAsFailedIfWillExceedMaxExceptions(
                    $job->getConnectionName(), $job, $e
                );
            }

            $this->kill(static::EXIT_ERROR);
        });

        // @todo: не подписываться по времени выполнения скрипта
//        pcntl_alarm(
//            max($this->timeoutForJob($job, $options), 0)
//        );
    }

    protected function resetTimeoutHandler()
    {
        // @todo: не пересоздавать счётчик
    }
}
