<?php

namespace App\Queue;

use VladimirYuldashev\LaravelQueueRabbitMQ\Consumer as BaseConsumer;

/**
 * @inheritDoc
 * @package App\Queue
 */
class Consumer extends BaseConsumer
{
    use WithoutTimeout;
}
