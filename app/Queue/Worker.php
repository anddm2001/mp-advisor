<?php


namespace App\Queue;

/**
 * @inheritDoc
 * @package App\Queue
 */
class Worker extends \Illuminate\Queue\Worker
{
    use WithoutTimeout;
}
