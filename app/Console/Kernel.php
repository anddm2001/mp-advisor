<?php

namespace App\Console;

use App\Modules\Billing\Console\Commands\BillingSyncPendingCommand;
use App\Modules\Merchant\Console\Commands\BillingChargeCommand;
use App\Modules\Merchant\Console\Commands\BillingSendNotificationsCommand;
use App\Modules\Merchant\Console\Commands\WildberriesApiCollectDataCommand;
use App\Modules\Merchant\Console\Commands\WildberriesApiScheduledDataSyncCommand;
use App\Modules\Report\Events\WildberriesApiScheduledDataSyncEvent;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // сбор сырых данных из API Wildberries
        $schedule
            ->command(WildberriesApiCollectDataCommand::class)
            ->everyFourHours();

        // добор данных за вчера
        $schedule->command(WildberriesApiScheduledDataSyncCommand::class, [
            WildberriesApiScheduledDataSyncEvent::TYPE_YESTERDAY
        ])->dailyAt('07:00');

        // добор данных за прошлый месяц
        $schedule->command(WildberriesApiScheduledDataSyncCommand::class, [
            WildberriesApiScheduledDataSyncEvent::TYPE_PREV_MONTH
        ])->monthlyOn(5, '03:00');

        // списание средств по тарифу
        $schedule->command(BillingChargeCommand::class)->everyFifteenMinutes();

        // отправка уведомления по событиям биллинга
        $schedule->command(BillingSendNotificationsCommand::class)->dailyAt('12:05');

        // синхронизация данных по платетежам "в ожидании" более часа
        $schedule->command(BillingSyncPendingCommand::class)->everyTenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
