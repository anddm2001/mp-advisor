<?php

namespace App\Modules\Report\Listeners;

use App\Modules\Billing\Events\BillingTrialPeriodFinishSetEvent;
use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Billing\Services\BillingLogger;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Models\MerchantConnectionSync;
use App\Modules\Merchant\Notifications\ManagerSyncErrorNotification;
use App\Modules\Report\Events\WildberriesApiCollectDataEvent;
use App\Modules\Report\Events\WildberriesApiCollectEachDaySalesEvent;
use App\Modules\Report\Events\WildberriesApiScheduledDataSyncEvent;
use App\Modules\Report\Services\WildberriesReportApi;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;

/**
 * Обработка события на сбор данных для мерчанта из Wildberries API
 */
class WildberriesApiCollectDataListener implements ShouldQueue
{
    /** @var WildberriesReportApi */
    protected WildberriesReportApi $api;

    /**
     * Подключение сервиса взаимодействия с микросервисом
     *
     * @param WildberriesReportApi $api
     */
    public function __construct(WildberriesReportApi $api)
    {
        $this->api = $api;
    }

    /**
     * Отправка запроса на сбор в API
     *
     * @param WildberriesApiCollectDataEvent $event
     * @throws Exception
     */
    public function handleAPIData(WildberriesApiCollectDataEvent $event): void
    {
        $connection = $event->getConnection();
        $base64Key = $event->getBase64Key();

        // запрещается запускать чаще чем 1 раз в час
        if (!empty($connection->last_sync) && !$event->isIgnoreLastSync()) {
            $lastSyncAt = new \DateTime($connection->last_sync);
            $diff = abs($lastSyncAt->getTimestamp() - time());
            if ($diff < 3600) {
                return;
            }
        }

        // дата, с которой собираем данные в API - текущее время - 24 часа
        // только в том случае, если ранее уже были синхронизации
        // если их не былоили запрашивается максимальная синхронизация - начиная с 90-дневней давности
        $dateFrom = new \DateTime();
        if (empty($connection->last_sync) || $event->getSyncType() === MerchantConnectionSync::TYPE_MAXIMUM_CP) {
            $dateFrom = $dateFrom->sub(new \DateInterval('P90D'));
        } else {
            $dateFrom = $dateFrom->sub(new \DateInterval('P1D'));
        }

        $this->updateConnectionAfterCollecting(
            $connection,
            $this->api->collectData($connection->merchant->id, $base64Key, $dateFrom),
            $event->getSyncType()
        );
    }

    /**
     * Добор данных из Wildberries API по расписанию
     *
     * @param WildberriesApiScheduledDataSyncEvent $event
     */
    public function handleAPIScheduledDataSync(WildberriesApiScheduledDataSyncEvent $event): void
    {
        $connection = $event->getConnection();
        $base64Key = $event->getBase64Key();
        $dateFrom = $event->getDateFrom();

        $this->updateConnectionAfterCollecting(
            $connection,
            $this->api->collectData($connection->merchant->id, $base64Key, $dateFrom),
            MerchantConnectionSync::TYPE_SCHEDULED
        );
    }

    /**
     * Обновить данные соединения в соответствии с результатами сбора данных
     *
     * @param MerchantConnection $connection
     * @param array $collectingResult
     * @param string $syncType
     */
    private function updateConnectionAfterCollecting(
        MerchantConnection $connection,
        array $collectingResult,
        string $syncType
    ): void
    {
        [$processed, $error] = $collectingResult;

        $now = Carbon::now();

        // данные синхронизации
        $syncData = [
            'date' => $now,
            'type' => $syncType,
            'status' => $processed && empty($error)
                ? MerchantConnectionSync::STATUS_SUCCESS
                : MerchantConnectionSync::STATUS_ERROR,
        ];

        $connection = $connection->fresh(['merchant']);
        $merchant = $connection->merchant;

        // количества номенклатур
        $itemTotalCount = null;
        $itemActiveCount = null;
        if ($processed) {
            $summaryResponse = $this->api->getNomenclatureSummary($merchant->id);
            $itemTotalCount = $summaryResponse->getTotalNomenclatures();
            $itemActiveCount = $summaryResponse->getActiveNomenclatures();
        }

        // примечания к синхронизации
        $syncData['data'] = $syncData['status'] === MerchantConnectionSync::STATUS_SUCCESS && $itemTotalCount
            ? 'Номенклатур: ' . $itemActiveCount . ' / ' . $itemTotalCount
            : ($error ?: null);

        // сохранение данных синхронизации в историю
        /** @var MerchantConnectionSync $sync */
        $sync = $connection->syncs()->create($syncData);

        // обновление данных последней синхронизации
        $connection->last_sync = $syncData['date'];
        $connection->error = $error;
        $connection->item_total_count = $itemTotalCount;
        $connection->item_active_count = $itemActiveCount;

        // новый подходящий тариф
        $billingRate = $connection->getSuitableBillingRate();

        // логгер смены тарифа
        $billingRateChangeLogger = with(new BillingLogger(BillingLogger::CATEGORY_RATE_CHANGE));

        // если применённый тариф изменился
        if ($billingRate && $billingRate->id !== $merchant->billing_rate_id) {
            // идентификаторы и названия тарифов
            $newBillingRateId = $billingRate->id;
            $oldBillingRateName = $merchant->billingRate ? $merchant->billingRate->name : null;
            $newBillingRateName = $billingRate->name;

            // обновление применённого тарифа
            $merchant->billing_rate_id = $newBillingRateId;

            // назначение пробного периода
            if (empty($merchant->charge_on)) {
                $chargeOn = Carbon::now()->addDay()->addDays($billingRate->trial_period_days ?? 0);
                $merchant->charge_on = $chargeOn;
                $merchant->trial_period_finish = $chargeOn;
                event(new BillingTrialPeriodFinishSetEvent($merchant, $chargeOn));
            }

            if ($merchant->save()) {
                // добавление операции смены тарифа
                $merchant->billingOperations()->create([
                    'merchant_connection_id' => $connection->id,
                    'billing_rate_id' => $newBillingRateId,
                    'billing_rate_name' => $newBillingRateName,
                    'prev_billing_rate_name' => $oldBillingRateName,
                    'item_total_count' => $connection->item_total_count,
                    'item_active_count' => $connection->item_active_count,
                    'type' => BillingOperation::TYPE_RATE_CHANGE,
                    'status' => BillingOperation::STATUS_SUCCESS,
                    'amount' => 0,
                    'description' => $oldBillingRateName
                        ? 'Смена тарифного плана с "' . $oldBillingRateName . '" на "' . $newBillingRateName . '"'
                        : 'Назначен тарифный план "' . $newBillingRateName . '"',
                    'captured_at' => $now,
                ]);

                // запись в лог о смене тарифа
                $billingRateChangeLogger->info(
                    'Тариф для мерчанта #' . $merchant->id .
                    ' изменился с "' . $oldBillingRateName . '" на "' . $newBillingRateName . '"' .
                    ' (Wildberries SKU: ' .
                    ($connection->item_active_count ?? '?') . ' / ' . ($connection->item_total_count ?? '?') .
                    ')'
                );
            } else {
                // критическая ошибка в лог, если данные мерчанта не сохранились
                $billingRateChangeLogger->critical(
                    'Не удалось сменить тариф для мерчанта #' . $merchant->id . ' (модель не сохранена)'
                );
            }
        } elseif (!$billingRate) {
            // алерт в лог, если тариф подобрать не удалось
            $billingRateChangeLogger->alert(
                'Не удалось подобрать тариф для мерчанта #' . $merchant->id .
                ' (Wildberries SKU: ' .
                ($connection->item_active_count ?? '?') . ' / ' . ($connection->item_total_count ?? '?') .
                ')'
            );
        }

        $connection->save();

        if (!empty($error)) {
            // отправка уведомления об ошибке синхронизации
            Notification::route('mail', 'info@mpadvisor.ru')->notify(new ManagerSyncErrorNotification($sync));
        }
    }

    /**
     * Собрать продажи на каждый день, начиная с указанной даты
     *
     * @param WildberriesApiCollectEachDaySalesEvent $event
     */
    public function handleSalesEachDay(WildberriesApiCollectEachDaySalesEvent $event)
    {
        $since = $event->getSince()->setTime(0, 0, 0, 0);
        $dateTo = new \DateTime();

        $this->api->collectSales($event->getMerchantID(), $since, $dateTo);
    }

    /**
     * Подписка на события
     *
     * @return array
     */
    public function subscribe(): array
    {
        return [
            WildberriesApiCollectDataEvent::class => [__CLASS__ . '@handleAPIData'],
            WildberriesApiScheduledDataSyncEvent::class => [__CLASS__ . '@handleAPIScheduledDataSync'],
            WildberriesApiCollectEachDaySalesEvent::class => [__CLASS__ . '@handleSalesEachDay'],
        ];
    }
}
