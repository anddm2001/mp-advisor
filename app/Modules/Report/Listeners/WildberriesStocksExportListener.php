<?php

namespace App\Modules\Report\Listeners;

use App\Modules\Report\Contracts\ReportResultFileServiceContract;
use App\Modules\Report\Events\WildberriesStocksExportEvent;
use App\Modules\Report\Exporter\WildberriesStocksExporter;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\StockFilterRequest;
use App\Modules\Report\Requests\StockReportSortRequest;
use App\Modules\Report\Services\WildberriesReportApi;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Queue\ShouldQueue;
use Report\PageResponse;
use Report\StockItem;
use Throwable;

/**
 * Генерация файлов данных складов
 */
class WildberriesStocksExportListener implements ShouldQueue
{
    /** @var Application */
    protected Application $application;

    /** @var WildberriesReportApi Подключение к API */
    protected WildberriesReportApi $api;

    /** @var ReportResultFileServiceContract Сервис для обработки файлов */
    protected ReportResultFileServiceContract $resultService;

    /** @var string Путь к временному хранилищу файлов на локальном жестком диске */
    protected string $tmpPath;

    /**
     * Определение параметров обработчика
     *
     * @param Application $application
     * @param WildberriesReportApi $api
     * @param ReportResultFileServiceContract $resultService
     * @param string $tmpPath
     */
    public function __construct(Application $application, WildberriesReportApi $api, ReportResultFileServiceContract $resultService, string $tmpPath)
    {
        $this->application = $application;
        $this->api = $api;
        $this->resultService = $resultService;
        $this->tmpPath = $tmpPath;
    }

    /**
     * Получить фильтр по номенклатурам
     *
     * @param WildberriesStocksExportEvent $event
     * @return NomenclatureFilterRequest
     */
    protected function getNomenclatureFilterRequest(WildberriesStocksExportEvent $event): NomenclatureFilterRequest
    {
        return NomenclatureFilterRequest::createFromArray($event->getNomenclatureFilterData());
    }

    /**
     * Получить фильтр по складам
     *
     * @param WildberriesStocksExportEvent $event
     * @return StockFilterRequest
     */
    protected function getStockFilterRequest(WildberriesStocksExportEvent $event): StockFilterRequest
    {
        return StockFilterRequest::createFromArray($event->getStockFilterData());
    }

    /**
     * Получить сортировку отчёта по складам
     *
     * @param WildberriesStocksExportEvent $event
     * @return StockReportSortRequest
     */
    protected function getSortRequest(WildberriesStocksExportEvent $event): StockReportSortRequest
    {
        return StockReportSortRequest::createFromArray($event->getSortData());
    }

    /**
     * Получить запрос на постраничную навигацию
     *
     * @param int $page
     * @return PageRequest
     */
    protected function getPageRequest(int $page): PageRequest
    {
        return PageRequest::createFromArray(['page' => $page]);
    }

    /**
     * Записать отчёт по складам
     *
     * @param WildberriesStocksExportEvent $event
     * @param WildberriesStocksExporter $exporter
     * @return void
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    protected function makeStocksFormat(WildberriesStocksExportEvent $event, WildberriesStocksExporter $exporter): void
    {
        $exporter->writeStocksHeader();
        $page = 0;
        do {
            $response = $this->api->getStockReport(
                $event->getMerchantId(),
                $this->getNomenclatureFilterRequest($event),
                $this->getStockFilterRequest($event),
                $this->getSortRequest($event),
                $this->getPageRequest($page++),
                true
            );
            foreach ($response->getItems() as $item) {
                /** @var StockItem $item */
                $exporter->writeStockData($item);
            }
            /** @var PageResponse|null $pageResponse */
            $pageResponse = $response->getPage();
        } while ($pageResponse && $pageResponse->getHasNextPage());
    }

    /**
     * Обработка события на генерацию экспорта
     *
     * @param WildberriesStocksExportEvent $event
     * @return void
     * @throws BindingResolutionException
     * @throws IOException
     * @throws Throwable
     * @throws WriterNotOpenedException
     */
    public function handle(WildberriesStocksExportEvent $event): void
    {
        $tmpFilePath = $this->tmpPath . '/' . md5($event->getMerchantId() . '_' . microtime()) . '.xlsx';

        $file = $event->getResultFile();

        try {
            $this->resultService->beginGenerate($file);

            /** @var WildberriesStocksExporter $exporter */
            $exporter = $this->application->make(WildberriesStocksExporter::class);

            $exporter->openFile($tmpFilePath);

            $this->makeStocksFormat($event, $exporter);

            $exporter->finish();

            if ($this->resultService->uploadResultFile($file, $tmpFilePath)) {
                $this->resultService->endGenerate($file, false);
            } else {
                $this->resultService->endGenerate($file, true);
            }
        } catch (Throwable $ex) {
            $this->resultService->endGenerate($file, true);
            throw $ex;
        } finally {
            if (is_file($tmpFilePath)) {
                unlink($tmpFilePath);
            }
        }
    }

    /**
     * Подписка на события
     *
     * @return array
     */
    public function subscribe(): array
    {
        return [
            WildberriesStocksExportEvent::class => [__CLASS__ . '@handle'],
        ];
    }
}
