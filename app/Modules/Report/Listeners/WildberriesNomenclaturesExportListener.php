<?php

namespace App\Modules\Report\Listeners;

use App\Modules\Report\Contracts\ReportResultFileServiceContract;
use App\Modules\Report\Events\WildberriesNomenclaturesExportEvent;
use App\Modules\Report\Exporter\WildberriesNomenclaturesExporter;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\NomenclatureRemovedFilterRequest;
use App\Modules\Report\Requests\NomenclatureSortRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Services\WildberriesReportApi;
use Box\Spout\Common\Exception\IOException;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Queue\ShouldQueue;
use Report\Nomenclature;
use Report\PageResponse;
use Throwable;

/**
 * Генерация файлов данных номенклатур
 *
 * @package App\Modules\Report\Listeners
 */
class WildberriesNomenclaturesExportListener implements ShouldQueue
{
    /**
     * @var Application
     */
    protected Application $application;

    /**
     * @var WildberriesReportApi Подключение к API
     */
    protected WildberriesReportApi $api;

    /**
     * @var ReportResultFileServiceContract Сервис для обработки файлов
     */
    protected ReportResultFileServiceContract $resultService;

    /**
     * @var string Путь к временному хранилищу файлов на локальном жестком диске
     */
    protected string $tmpPath;

    /**
     * Определение параметров обработчика
     *
     * @param Application $application
     * @param WildberriesReportApi $api
     * @param ReportResultFileServiceContract $resultService
     * @param string $tmpPath
     */
    public function __construct(Application $application, WildberriesReportApi $api, ReportResultFileServiceContract $resultService, string $tmpPath)
    {
        $this->application = $application;
        $this->api = $api;
        $this->resultService = $resultService;
        $this->tmpPath = $tmpPath;
    }

    /**
     * Получить фильтр по номенклатурам
     *
     * @param WildberriesNomenclaturesExportEvent $event
     * @return NomenclatureFilterRequest
     */
    protected function getFilterRequest(WildberriesNomenclaturesExportEvent $event): NomenclatureFilterRequest
    {
        return NomenclatureFilterRequest::createFromArray($event->getFilterData());
    }

    /**
     * Получить фильтр по поставляемому/архиву
     *
     * @param WildberriesNomenclaturesExportEvent $event
     * @return NomenclatureRemovedFilterRequest
     */
    protected function getRemovedFilterRequest(WildberriesNomenclaturesExportEvent $event): NomenclatureRemovedFilterRequest
    {
        return NomenclatureRemovedFilterRequest::createFromArray($event->getRemovedFilterData());
    }

    /**
     * Запрос на сортировку
     *
     * @return NomenclatureSortRequest
     */
    protected function getSortRequest(): NomenclatureSortRequest
    {
        return NomenclatureSortRequest::createFromArray([]);
    }

    /**
     * Получить запрос на постраничную навигацию
     *
     * @param int $page
     * @return PageRequest
     */
    protected function getPageRequest(int $page): PageRequest
    {
        return PageRequest::createFromArray(['page' => $page]);
    }

    /**
     * Записать отчёт по номенклатурам
     *
     * @param WildberriesNomenclaturesExportEvent $event
     * @param WildberriesNomenclaturesExporter $exporter
     * @return void
     * @throws Exception
     */
    protected function makeNomenclaturesFormat(WildberriesNomenclaturesExportEvent $event, WildberriesNomenclaturesExporter $exporter): void
    {
        $exporter->writeNomenclatureHeader();
        $page = 0;
        do {
            $response = $this->api->getMerchantNomenclatures(
                $event->getMerchantID(),
                $this->getFilterRequest($event),
                $this->getRemovedFilterRequest($event),
                $this->getSortRequest(),
                $this->getPageRequest($page++)
            );
            foreach ($response->getItems() as $item) {
                /** @var Nomenclature $item */
                $exporter->writeNomenclature($item);
            }
            /** @var PageResponse|null $pageResponse */
            $pageResponse = $response->getPage();
        } while ($pageResponse && $pageResponse->getHasNextPage());
    }

    /**
     * Обработка события на генерацию экспорта
     *
     * @param WildberriesNomenclaturesExportEvent $event
     * @return void
     * @throws BindingResolutionException
     * @throws IOException
     * @throws Throwable
     */
    public function handle(WildberriesNomenclaturesExportEvent $event): void
    {
        $tmpFilePath = $this->tmpPath . '/' . md5($event->getMerchantID() . '_' . microtime()) . '.xlsx';

        $file = $event->getResultFile();

        try {
            $this->resultService->beginGenerate($file);

            /** @var WildberriesNomenclaturesExporter $exporter */
            $exporter = $this->application->make(WildberriesNomenclaturesExporter::class);

            $exporter->openFile($tmpFilePath);

            $this->makeNomenclaturesFormat($event, $exporter);

            $exporter->finish();

            if ($this->resultService->uploadResultFile($file, $tmpFilePath)) {
                $this->resultService->endGenerate($file, false);
            } else {
                $this->resultService->endGenerate($file, true);
            }
        } catch (Throwable $ex) {
            $this->resultService->endGenerate($file, true);
            throw $ex;
        } finally {
            if (is_file($tmpFilePath)) {
                unlink($tmpFilePath);
            }
        }
    }

    /**
     * Подписка на события
     *
     * @return array
     */
    public function subscribe(): array
    {
        return [
            WildberriesNomenclaturesExportEvent::class => [
                __CLASS__ . '@handle',
            ],
        ];
    }
}
