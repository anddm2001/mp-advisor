<?php

namespace App\Modules\Report\Listeners;

use App\Modules\Report\Contracts\ReportResultFileServiceContract;
use App\Modules\Report\Events\WildberriesRecommendationsExportEvent;
use App\Modules\Report\Exporter\WildberriesIncomeExporter;
use App\Modules\Report\Exporter\WildberriesRecommendationsExporter;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\RecommendationsReportFilterRequest;
use App\Modules\Report\Requests\RecommendationsReportSortRequest;
use App\Modules\Report\Services\WildberriesReportApi;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Queue\ShouldQueue;
use Report\GetRecommendationsReportResponse;
use Report\GetRecommendationsReportResponse\NomenclatureRecommendations;
use Throwable;

/**
 * Генерация XLSX файла для отчёта о продажах из Wildberries API
 *
 * @package App\Modules\Report\Listeners
 */
class WildberriesRecommendationsExportListener implements ShouldQueue
{
    /**
     * Ограничение на странице
     */
    protected const DEFAULT_PAGE_LIMIT = 1000;

    /**
     * @var Application
     */
    protected Application $application;

    /**
     * @var WildberriesReportApi Подключение к API
     */
    protected WildberriesReportApi $api;

    /**
     * @var ReportResultFileServiceContract Сервис для обработки файлов
     */
    protected ReportResultFileServiceContract $resultService;

    /**
     * @var string Путь к временному хранилищу файлов на локальном жестком диске
     */
    protected string $tmpPath;

    /**
     * WildberriesApiExportListener constructor.
     *
     * @param Application $application
     * @param WildberriesReportApi $api
     * @param ReportResultFileServiceContract $resultService
     * @param string $tmpPath
     */
    public function __construct(
        Application $application,
        WildberriesReportApi $api,
        ReportResultFileServiceContract $resultService,
        string $tmpPath
    ) {
        $this->application = $application;
        $this->api = $api;
        $this->resultService = $resultService;
        $this->tmpPath = $tmpPath;
    }

    /**
     * Запросить данные исходя из страницы
     *
     * @param WildberriesRecommendationsExportEvent $event
     * @param int $page
     * @return GetRecommendationsReportResponse
     */
    protected function getPageData(WildberriesRecommendationsExportEvent $event, int $page): GetRecommendationsReportResponse
    {
        return $this->api->getRecommendationsReport(
            $event->getMerchantID(),
            $event->getReportID(),
            NomenclatureFilterRequest::createFromArray([]),
            RecommendationsReportFilterRequest::createFromArray([
                'only_apply' => $event->getExportFormat() !== WildberriesRecommendationsExportEvent::EXPORT_FORMAT_EXCEL_FULL,
            ]),
            RecommendationsReportSortRequest::createFromArray([]),
            PageRequest::createFromArray(['page' => $page])
        );
    }

    /**
     * Сформировать файл по формату, предназначенному для формирования заявки поставки в Wildberries
     *
     * @param WildberriesRecommendationsExportEvent $event
     * @param string $tmpFilePath
     * @throws BindingResolutionException
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    protected function makeWildberriesFormatFile(WildberriesRecommendationsExportEvent $event, string $tmpFilePath): void
    {
        $currentPage = 0;
        /** @var WildberriesIncomeExporter $exporter */
        $exporter = $this->application->make(WildberriesIncomeExporter::class);
        $exporter->openFile($tmpFilePath)->writeHeaderData();

        do {
            $data = $this->getPageData($event, $currentPage);
            $hasNextPage = $data->getPage()->getHasNextPage();

            foreach ($data->getItems() as $nomenclature) {
                /** @var NomenclatureRecommendations $nomenclature */
                $exporter->writeNomenclature($nomenclature);
            }

            $currentPage++;
        } while ($hasNextPage);

        $exporter->finish();
    }

    /**
     * Сформировать файл по формату отчёта рекомендаций
     *
     * @param WildberriesRecommendationsExportEvent $event
     * @param string $tmpFilePath
     * @throws BindingResolutionException
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    protected function makeRecommendationsFormatFile(WildberriesRecommendationsExportEvent $event, string $tmpFilePath): void
    {
        $currentPage = 0;
        $isFullReport = $event->getExportFormat() === WildberriesRecommendationsExportEvent::EXPORT_FORMAT_EXCEL_FULL;
        /** @var WildberriesRecommendationsExporter $exporter */
        $exporter = $this->application->make(WildberriesRecommendationsExporter::class);
        $exporter->setIsFullReport($isFullReport)->openFile($tmpFilePath);

        do {
            $data = $this->getPageData($event, $currentPage);
            $hasNextPage = $data->getPage()->getHasNextPage();

            if ($currentPage === 0) {
                $exporter->writeHeaderData($data->getSummary());
            }

            foreach ($data->getItems() as $nomenclature) {
                /** @var NomenclatureRecommendations $nomenclature */
                $exporter->writeNomenclature($nomenclature);
            }

            $currentPage++;
        } while ($hasNextPage);

        $exporter->finish();
    }

    /**
     * Обработка события
     *
     * @param WildberriesRecommendationsExportEvent $event
     * @throws IOException
     * @throws BindingResolutionException
     * @throws Throwable
     */
    public function handle(WildberriesRecommendationsExportEvent $event): void
    {
        $tmpFilePath = $this->tmpPath . '/' . $event->getReportID() . '_' . microtime() . '.xlsx';

        $file = $event->getResultFile();

        try {
            $this->resultService->beginGenerate($file);

            switch ($event->getExportFormat()) {
                case WildberriesRecommendationsExportEvent::EXPORT_FORMAT_WB_INCOME:
                default:
                    $this->makeWildberriesFormatFile($event, $tmpFilePath);
                    break;
                case WildberriesRecommendationsExportEvent::EXPORT_FORMAT_EXCEL_APPLIED:
                case WildberriesRecommendationsExportEvent::EXPORT_FORMAT_EXCEL_FULL:
                    $this->makeRecommendationsFormatFile($event, $tmpFilePath);
                    break;
            }

            if ($this->resultService->uploadResultFile($file, $tmpFilePath)) {
                $this->resultService->endGenerate($file, false);
            } else {
                $this->resultService->endGenerate($file, true);
            }
        } catch (Throwable $ex) {
            $this->resultService->endGenerate($file, true);
            throw $ex;
        } finally {
            if (is_file($tmpFilePath)) {
                unlink($tmpFilePath);
            }
        }
    }

    /**
     * Подписка на события
     *
     * @return array
     */
    public function subscribe(): array
    {
        return [
            WildberriesRecommendationsExportEvent::class => [
                __CLASS__ . '@handle',
            ],
        ];
    }
}
