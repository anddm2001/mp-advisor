<?php

namespace App\Modules\Report\Listeners;

use App\Modules\Report\Contracts\ReportResultFileServiceContract;
use App\Modules\Report\Events\AbstractSalesExportEvent;
use App\Modules\Report\Events\WildberriesBrandsSalesExportEvent;
use App\Modules\Report\Events\WildberriesCategoriesSalesExportEvent;
use App\Modules\Report\Events\WildberriesNomenclaturesSalesExportEvent;
use App\Modules\Report\Exporter\WildberriesSalesExporter;
use App\Modules\Report\Requests\BrandsFilterRequest;
use App\Modules\Report\Requests\CategoriesFilterRequest;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\SalesPeriodRequest;
use App\Modules\Report\Requests\SalesSortRequest;
use App\Modules\Report\Services\WildberriesReportApi;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Queue\ShouldQueue;
use Report\GetBrandSalesResponse\BrandSalesReport;
use Report\GetCategorySalesResponse\CategorySalesReport;
use Report\GetNomenclatureSalesResponse\NomenclatureSalesReport;
use Throwable;

/**
 * Генерация файлов отчётов продаж по номенклатурам, брендам или категориям
 *
 * @package App\Modules\Report\Listeners
 */
class WildberriesSalesExportListener implements ShouldQueue
{
    /**
     * @var Application
     */
    protected Application $application;

    /**
     * @var WildberriesReportApi Подключение к API
     */
    protected WildberriesReportApi $api;

    /**
     * @var ReportResultFileServiceContract Сервис для обработки файлов
     */
    protected ReportResultFileServiceContract $resultService;

    /**
     * @var string Путь к временному хранилищу файлов на локальном жестком диске
     */
    protected string $tmpPath;

    /**
     * WildberriesSalesExportListener constructor.
     *
     * @param Application $application
     * @param WildberriesReportApi $api
     * @param ReportResultFileServiceContract $resultService
     * @param string $tmpPath
     */
    public function __construct(Application $application, WildberriesReportApi $api, ReportResultFileServiceContract $resultService, string $tmpPath)
    {
        $this->application = $application;
        $this->api = $api;
        $this->resultService = $resultService;
        $this->tmpPath = $tmpPath;
    }

    /**
     * Получить запрос с периодом для получения данных
     *
     * @param AbstractSalesExportEvent $event
     * @return SalesPeriodRequest
     * @throws Exception
     */
    protected function getPeriodRequest(AbstractSalesExportEvent $event): SalesPeriodRequest
    {
        $request = new SalesPeriodRequest($event->getPeriodData());
        $request->setContainer($this->application);
        return $request;
    }

    /**
     * Получить фильтр по номенклатурам
     *
     * @param WildberriesNomenclaturesSalesExportEvent $event
     * @return NomenclatureFilterRequest
     */
    protected function getNomenclatureFilter(WildberriesNomenclaturesSalesExportEvent $event): NomenclatureFilterRequest
    {
        $request = new NomenclatureFilterRequest($event->getFilterData());
        $request->setContainer($this->application);
        return $request;
    }

    /**
     * Получить фильтр по брендам
     *
     * @param WildberriesBrandsSalesExportEvent $event
     * @return BrandsFilterRequest
     */
    protected function getBrandFilter(WildberriesBrandsSalesExportEvent $event): BrandsFilterRequest
    {
        $request = new BrandsFilterRequest($event->getFilterData());
        $request->setContainer($this->application);
        return $request;
    }

    /**
     * Получить фильтр по категориям
     *
     * @param WildberriesCategoriesSalesExportEvent $event
     * @return CategoriesFilterRequest
     */
    protected function getCategoryFilter(WildberriesCategoriesSalesExportEvent $event): CategoriesFilterRequest
    {
        $request = new CategoriesFilterRequest($event->getFilterData());
        $request->setContainer($this->application);
        return $request;
    }

    /**
     * Запрос на сортировку
     *
     * @return SalesSortRequest
     */
    protected function getSortRequest(): SalesSortRequest
    {
        $sortRequest = new SalesSortRequest();
        $sortRequest->setContainer($this->application);
        return $sortRequest;
    }

    /**
     * Получить запрос на постраничную навигацию
     *
     * @param int $page
     * @return PageRequest
     */
    protected function getPageRequest(int $page): PageRequest
    {
        $pageRequest = new PageRequest(['page' => $page]);
        $pageRequest->setContainer($this->application);
        return $pageRequest;
    }

    /**
     * Записать отчёт по номенклатурам
     *
     * @param WildberriesNomenclaturesSalesExportEvent $event
     * @param WildberriesSalesExporter $exporter
     * @return void
     * @throws Exception
     */
    protected function makeNomenclaturesFormat(WildberriesNomenclaturesSalesExportEvent $event, WildberriesSalesExporter $exporter): void
    {
        $exporter->writeNomenclatureHeader();
        $page = 0;
        do {
            $response = $this->api->getSalesNomenclaturesReport(
                $event->getMerchantID(),
                $this->getPeriodRequest($event),
                $this->getNomenclatureFilter($event),
                $this->getSortRequest(),
                $this->getPageRequest($page++)
            );
            foreach ($response->getItems() as $item) {
                /** @var NomenclatureSalesReport $item */
                $exporter->writeNomenclature($item);
            }
        } while ($response->getPage()->getHasNextPage());
    }

    /**
     * Записать отчёт по категориям
     *
     * @param WildberriesBrandsSalesExportEvent $event
     * @param WildberriesSalesExporter $exporter
     * @return void
     * @throws Exception
     */
    protected function makeBrandsFormat(WildberriesBrandsSalesExportEvent $event, WildberriesSalesExporter $exporter): void
    {
        $exporter->writeBrandHeader();
        $page = 0;
        do {
            $response = $this->api->getSalesBrandsReport(
                $event->getMerchantID(),
                $this->getPeriodRequest($event),
                $this->getBrandFilter($event),
                $this->getSortRequest(),
                $this->getPageRequest($page++)
            );
            foreach ($response->getItems() as $item) {
                /** @var BrandSalesReport $item */
                $exporter->writeBrand($item);
            }
        } while ($response->getPage()->getHasNextPage());
    }

    /**
     * Записать отчёт по брендам
     *
     * @param WildberriesCategoriesSalesExportEvent $event
     * @param WildberriesSalesExporter $exporter
     * @return void
     * @throws Exception
     */
    protected function makeCategoriesFormat(WildberriesCategoriesSalesExportEvent $event, WildberriesSalesExporter $exporter): void
    {
        $exporter->writeCategoryHeader();
        $page = 0;
        do {
            $response = $this->api->getSalesCategoriesReport(
                $event->getMerchantID(),
                $this->getPeriodRequest($event),
                $this->getCategoryFilter($event),
                $this->getSortRequest(),
                $this->getPageRequest($page++)
            );
            foreach ($response->getItems() as $item) {
                /** @var CategorySalesReport $item */
                $exporter->writeCategory($item);
            }
        } while ($response->getPage()->getHasNextPage());
    }

    /**
     * Обработка запроса по номенклатурам, брендам или категориям
     *
     * @param AbstractSalesExportEvent $event
     * @return void
     * @throws IOException
     * @throws WriterNotOpenedException
     * @throws BindingResolutionException
     * @throws Throwable
     */
    public function handle(AbstractSalesExportEvent $event): void
    {
        $tmpFilePath = $this->tmpPath . '/' . md5($event->getMerchantID() . '_' . microtime()) . '.xlsx';

        $file = $event->getResultFile();

        try {
            $this->resultService->beginGenerate($file);

            /** @var WildberriesSalesExporter $exporter */
            $exporter = $this->application->make(WildberriesSalesExporter::class);

            $exporter->openFile($tmpFilePath);

            $exporter->writePeriodHeader($event->getPeriodData());

            if ($event instanceof WildberriesNomenclaturesSalesExportEvent) {
                $this->makeNomenclaturesFormat($event, $exporter);
            } elseif ($event instanceof WildberriesBrandsSalesExportEvent) {
                $this->makeBrandsFormat($event, $exporter);
            } elseif ($event instanceof WildberriesCategoriesSalesExportEvent) {
                $this->makeCategoriesFormat($event, $exporter);
            }

            $exporter->finish();

            if ($this->resultService->uploadResultFile($file, $tmpFilePath)) {
                $this->resultService->endGenerate($file, false);
            } else {
                $this->resultService->endGenerate($file, true);
            }
        } catch (Throwable $ex) {
            $this->resultService->endGenerate($file, true);
            throw $ex;
        } finally {
            if (is_file($tmpFilePath)) {
                unlink($tmpFilePath);
            }
        }
    }

    /**
     * Подписка на события
     *
     * @return array
     */
    public function subscribe(): array
    {
        return [
            WildberriesCategoriesSalesExportEvent::class => [
                __CLASS__ . '@handle',
            ],
            WildberriesNomenclaturesSalesExportEvent::class => [
                __CLASS__ . '@handle',
            ],
            WildberriesBrandsSalesExportEvent::class => [
                __CLASS__ . '@handle',
            ],
        ];
    }
}
