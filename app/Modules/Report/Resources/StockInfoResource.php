<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetStockInfoResponse;

/**
 * Информация о данных по складам
 */
class StockInfoResource extends JsonResource
{
    /**
     * @param GetStockInfoResponse $resource
     */
    public function __construct(GetStockInfoResponse $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        /** @var GetStockInfoResponse $stockInfo */
        $stockInfo = $this->resource;
        return [
            'marketplace_amount' => $stockInfo->getAmountMarketplace(),
            'my_stock_amount' => $stockInfo->getAmountMyStock(),
            'in_way' => $stockInfo->getInWay(),
            'lost_percent' => $stockInfo->getLostPercent() > 0 ? $stockInfo->getLostPercent() : 0,
            'lost_amount' => $stockInfo->getLostAmount(),
            'lost_price' => $stockInfo->getLostPrice(),
        ];
    }
}
