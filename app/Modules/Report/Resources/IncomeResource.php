<?php

namespace App\Modules\Report\Resources;

use App\Modules\Report\Helpers\IncomeStatusHelper;
use App\Modules\Report\Helpers\ProtobufTimestampHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Report\Income;

/**
 * Информация о поставке
 */
class IncomeResource extends JsonResource
{
    /**
     * @param Income $resource
     */
    public function __construct(Income $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var Income $income */
        $income = $this->resource;
        return [
            'id' => $income->getIncomeID(),
            'number' => $income->getNumber(),
            'date_create' => ProtobufTimestampHelper::toMilliseconds($income->getDateCreate()),
            'date_update' => ProtobufTimestampHelper::toMilliseconds($income->getDateUpdate()),
            'date_supply' => ProtobufTimestampHelper::toMilliseconds($income->getDateSupply()),
            'date_close' => ProtobufTimestampHelper::toMilliseconds($income->getDateClose()),
            'nomenclatures' => $income->getNomenclatures(),
            'status' => IncomeStatusHelper::toName($income->getStatus()),
        ];
    }
}
