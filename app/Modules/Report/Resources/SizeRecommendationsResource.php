<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetRecommendationsReportResponse\SizeRecommendations;

/**
 * Отчёт по конкретному размеру
 */
class SizeRecommendationsResource extends JsonResource
{
    /**
     * @param SizeRecommendations $resource
     */
    public function __construct(SizeRecommendations $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var SizeRecommendations $sizeRecommendations */
        $sizeRecommendations = $this->resource;
        return [
            'size' => new NomenclatureSizeDataResource($sizeRecommendations->getSize()),
            'in_stock' => $sizeRecommendations->getInStock(),
            'to_order' => $sizeRecommendations->getToOrder(),
            'from_order' => $sizeRecommendations->getFromOrder(),
            'in_income' => $sizeRecommendations->getInIncome(),
            'order_speed' => $sizeRecommendations->getOrderSpeed(),
            'ordered_count' => $sizeRecommendations->getOrderedCount(),
            'days' => $sizeRecommendations->getDays(),
            'need_stock' => $sizeRecommendations->getNeedStock(),
            'to_supply' => $sizeRecommendations->getToSupply(),
            'to_supply_sets' => $sizeRecommendations->getToSupplySets(),
            'apply' => $sizeRecommendations->getApply(),
        ];
    }
}
