<?php

namespace App\Modules\Report\Resources;

use App\Modules\Report\Helpers\IncomeStatusHelper;
use App\Modules\Report\Helpers\ProtobufTimestampHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Report\IncomeNomenclatureItem;

/**
 * Описание размера номенклатуры в поставке
 */
class IncomeNomenclatureItemResource extends JsonResource
{
    /**
     * @param IncomeNomenclatureItem $resource
     */
    public function __construct(IncomeNomenclatureItem $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var IncomeNomenclatureItem $incomeNomenclatureItem */
        $incomeNomenclatureItem = $this->resource;
        return [
            'size' => new NomenclatureSizeDataResource($incomeNomenclatureItem->getSize()),
            'quantity' => $incomeNomenclatureItem->getQuantity(),
            'date_update' => ProtobufTimestampHelper::toMilliseconds($incomeNomenclatureItem->getDateUpdate()),
            'date_close' => ProtobufTimestampHelper::toMilliseconds($incomeNomenclatureItem->getDateClose()),
            'status' => IncomeStatusHelper::toName($incomeNomenclatureItem->getStatus()),
            'warehouse_name' => $incomeNomenclatureItem->getWarehouseName(),
        ];
    }
}
