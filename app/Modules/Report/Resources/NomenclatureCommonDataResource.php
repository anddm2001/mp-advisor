<?php

namespace App\Modules\Report\Resources;

use App\Modules\Report\Helpers\WildberriesHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Report\NomenclatureCommonData;

/**
 * Описание номенклатур в ответе на запрос агрегированного отчёта
 */
class NomenclatureCommonDataResource extends JsonResource
{
    /**
     * @param NomenclatureCommonData|null $resource
     */
    public function __construct(?NomenclatureCommonData $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var NomenclatureCommonData|null $nomenclatureCommonData */
        $nomenclatureCommonData = $this->resource;
        return $nomenclatureCommonData ? [
            'name' => $nomenclatureCommonData->getName(),
            'brand' => $nomenclatureCommonData->getBrand(),
            'vendor_code' => $nomenclatureCommonData->getVendorCode(),
            'marketplace_nomenclature_code' => $nomenclatureCommonData->getMarketplaceNomenclatureCode(),
            'image_url' => WildberriesHelper::getNomenclatureImageUrl($nomenclatureCommonData->getMarketplaceNomenclatureCode()),
            'url' => WildberriesHelper::getNomenclatureUrl($nomenclatureCommonData->getMarketplaceNomenclatureCode()),
        ] : null;
    }
}
