<?php

namespace App\Modules\Report\Resources;

use App\Modules\Report\Helpers\ProtobufTimestampHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Report\SalesReportItem;

/**
 * Общий вывод результата в отчёте продаж с показателями продаж
 */
class SalesReportItemResource extends JsonResource
{
    /**
     * @param SalesReportItem $resource
     */
    public function __construct(SalesReportItem $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var SalesReportItem $salesReportItem */
        $salesReportItem = $this->resource;
        return [
            'date_from' => ProtobufTimestampHelper::toMilliseconds($salesReportItem->getDateFrom()),
            'date_to' => ProtobufTimestampHelper::toMilliseconds($salesReportItem->getDateTo()),
            'current' => new SalesReportItemDataResource($salesReportItem->getCurrentData()),
            'difference' => new SalesReportItemDataResource($salesReportItem->getIncData()),
            'percent_difference' => new SalesReportItemDataPercentDifferenceResource(
                $salesReportItem->getCurrentData(),
                $salesReportItem->getIncData()
            ),
        ];
    }
}
