<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\SalesReportItem\SalesReportItemData;

/**
 * Разница показателей продаж в процентах
 */
class SalesReportItemDataPercentDifferenceResource extends JsonResource
{
    /** @var SalesReportItemData */
    protected $difference;

    /**
     * @param SalesReportItemData $resource
     * @param SalesReportItemData $difference
     */
    public function __construct(SalesReportItemData $resource, SalesReportItemData $difference)
    {
        $this->difference = $difference;
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var SalesReportItemData $current */
        $current = $this->resource;
        $difference = $this->difference;
        return [
            'receipt_cost' => $this->calculateDiffPercent($current->getReceiptCost(), $difference->getReceiptCost()),
            'profit_cost' => $this->calculateDiffPercent($current->getProfitCost(), $difference->getProfitCost()),
            'marginality' => $this->calculateDiffPercent($current->getMarginality(), $difference->getMarginality()),
            'orders' => $this->calculateDiffPercent($current->getOrders(), $difference->getOrders()),
            'sales' => $this->calculateDiffPercent($current->getSales(), $difference->getSales()),
            'buyout' => $this->calculateDiffPercent($current->getBuyout(), $difference->getBuyout()),
            'discount' => $this->calculateDiffPercent($current->getDiscount(), $difference->getDiscount()),
            'returns' => $this->calculateDiffPercent($current->getReturns(), $difference->getReturns()),
            'orders_speed' => $this->calculateDiffPercent($current->getOrdersSpeed(), $difference->getOrdersSpeed()),
        ];
    }

    /**
     * Посчитать разницу в процентах между предыдущим показателем и текущим
     *
     * @param  integer|float  $current
     * @param  integer|float  $difference
     * @return float
     */
    protected function calculateDiffPercent($current, $difference): float
    {
        return floatval($current - $difference != 0 ? $difference * 100 / ($current - $difference) : 0);
    }
}
