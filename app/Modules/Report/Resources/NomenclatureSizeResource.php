<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\Nomenclature\Size;

/**
 * Детальная информация о размере номенклатуры
 */
class NomenclatureSizeResource extends JsonResource
{
    /**
     * @param Size $resource
     */
    public function __construct(Size $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var Size $nomenclatureSize */
        $nomenclatureSize = $this->resource;
        return [
            'size' => new NomenclatureSizeDataResource($nomenclatureSize->getSize()),
            'base_cost' => $nomenclatureSize->getBaseCost(),
            'sale_cost' => $nomenclatureSize->getSaleCost(),
            'sale_cost_with_discount' => $nomenclatureSize->getSaleCostWithDiscount(),
            'discount' => $nomenclatureSize->getDiscount(),
            'supply_set' => $nomenclatureSize->getSupplySet(),
            'our_stock' => $nomenclatureSize->getOurStock(),
            'removed' => $nomenclatureSize->getRemoved(),
        ];
    }
}
