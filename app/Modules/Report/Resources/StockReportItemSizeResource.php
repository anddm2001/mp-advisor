<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\StockItem\SizesItem;

/**
 * Данные размера элемента отчёта по складам
 */
class StockReportItemSizeResource extends JsonResource
{
    /**
     * @param SizesItem $resource
     */
    public function __construct(SizesItem $resource)
    {
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var SizesItem $size */
        $size = $this->resource;
        return [
            'size' => $size->getSize(),
            'my_stock_amount' => $size->getMyStockAmount(),
            'quantity_full' => $size->getQuantityFull(),
            'not_orders' => $size->getNotOrders(),
            'quantity' => $size->getQuantity(),
            'in_way' => $size->getInWay(),
            'lost_amount' => $size->getLostAmount(),
        ];
    }
}
