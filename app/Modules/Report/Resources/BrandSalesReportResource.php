<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetBrandSalesResponse\BrandSalesReport;

/**
 * Отчёт о продажах конкретного бренда
 */
class BrandSalesReportResource extends JsonResource
{
    /**
     * @param BrandSalesReport $resource
     */
    public function __construct(BrandSalesReport $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var BrandSalesReport $brandSalesReport */
        $brandSalesReport = $this->resource;
        return [
            'brand' => $brandSalesReport->getBrand(),
            'sales' => new SalesReportItemResource($brandSalesReport->getReport()),
        ];
    }
}
