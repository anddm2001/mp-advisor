<?php

namespace App\Modules\Report\Resources;

use App\Modules\Report\Helpers\ProtobufTimestampHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Report\RecommendationsReportSummary;

/**
 * Обобщённый результат отчёта о рекомендациях
 */
class RecommendationsReportSummaryResource extends JsonResource
{
    /**
     * @param RecommendationsReportSummary $resource
     */
    public function __construct(RecommendationsReportSummary $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var RecommendationsReportSummary $recommendationsReportSummary */
        $recommendationsReportSummary = $this->resource;
        return [
            'report_days' => $recommendationsReportSummary->getReportDays(),
            'warehouse_name' => $recommendationsReportSummary->getWarehouseName(),
            'delivery_date' => ProtobufTimestampHelper::toMilliseconds($recommendationsReportSummary->getDeliveryDate()),
            'calculate_from' => ProtobufTimestampHelper::toMilliseconds($recommendationsReportSummary->getCalculateFrom()),
            'calculate_to' => ProtobufTimestampHelper::toMilliseconds($recommendationsReportSummary->getCalculateTo()),
            'total_nomenclatures' => $recommendationsReportSummary->getTotalNomenclatures(),
            'recommendations_articles' => $recommendationsReportSummary->getRecommendationsArticles(),
            'income_nomenclatures' => $recommendationsReportSummary->getIncomeNomenclatures(),
            'income_articles' => $recommendationsReportSummary->getIncomeArticles(),
            'cost' => $recommendationsReportSummary->getCost(),
            'base_cost' => $recommendationsReportSummary->getBaseCost(),
        ];
    }
}
