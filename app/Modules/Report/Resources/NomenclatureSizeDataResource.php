<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\NomenclatureSizeData;

/**
 * Данные размера номенклатуры
 */
class NomenclatureSizeDataResource extends JsonResource
{
    /**
     * @param NomenclatureSizeData $resource
     */
    public function __construct(NomenclatureSizeData $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var NomenclatureSizeData $nomenclatureSizeData */
        $nomenclatureSizeData = $this->resource;
        return [
            'size' => $nomenclatureSizeData->getSize(),
            'barcode' => $nomenclatureSizeData->getBarcode(),
        ];
    }
}
