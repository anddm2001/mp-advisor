<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetNomenclatureSalesResponse\SizeSalesReport;

/**
 * Отчёт о продажах конкретного размера
 */
class SizeSalesReportResource extends JsonResource
{
    /**
     * @param SizeSalesReport $resource
     */
    public function __construct(SizeSalesReport $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var SizeSalesReport $sizeSalesReport */
        $sizeSalesReport = $this->resource;
        return [
            'size' => new NomenclatureSizeDataResource($sizeSalesReport->getSize()),
            'report' => new SalesReportItemResource($sizeSalesReport->getReport()),
            'in_stock' => $sizeSalesReport->getInStock(),
        ];
    }
}
