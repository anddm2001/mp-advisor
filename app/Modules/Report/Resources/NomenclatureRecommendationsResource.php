<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetRecommendationsReportResponse\NomenclatureRecommendations;

/**
 * Описание номенклатур в ответе на запрос агрегированного отчёта
 */
class NomenclatureRecommendationsResource extends JsonResource
{
    /**
     * @param NomenclatureRecommendations $resource
     */
    public function __construct(NomenclatureRecommendations $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var NomenclatureRecommendations $nomenclatureRecommendations */
        $nomenclatureRecommendations = $this->resource;
        return [
            'nomenclature' => new NomenclatureCommonDataResource($nomenclatureRecommendations->getNomenclature()),
            'sizes' => SizeRecommendationsResource::collection(collect($nomenclatureRecommendations->getSizes())),
        ];
    }
}
