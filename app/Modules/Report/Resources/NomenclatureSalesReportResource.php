<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetNomenclatureSalesResponse\NomenclatureSalesReport;

/**
 * Отчёт о продажах номенклатуры
 */
class NomenclatureSalesReportResource extends JsonResource
{
    /**
     * @param NomenclatureSalesReport $resource
     */
    public function __construct(NomenclatureSalesReport $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var NomenclatureSalesReport $nomenclatureSalesReport */
        $nomenclatureSalesReport = $this->resource;
        return [
            'nomenclature' => new NomenclatureCommonDataResource($nomenclatureSalesReport->getNomenclature()),
            'report' => new SalesReportItemResource($nomenclatureSalesReport->getReport()),
            'in_stock' => $nomenclatureSalesReport->getInStock(),
            'items' => SizeSalesReportResource::collection(collect($nomenclatureSalesReport->getSizes())),
        ];
    }
}
