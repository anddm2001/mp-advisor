<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetStatisticSalesByRangeResponse;

/**
 * Массив данных статистики заказов и продаж
 */
class SalesStatisticsByRangeResource extends JsonResource
{
    /**
     * @param GetStatisticSalesByRangeResponse $resource
     */
    public function __construct(GetStatisticSalesByRangeResponse $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var GetStatisticSalesByRangeResponse $response */
        $response = $this->resource;
        /** @var array $salesStatistics */
        $salesStatistics = SalesStatisticResource::collection(collect($response->getStatistics()));
        return $salesStatistics;
    }
}
