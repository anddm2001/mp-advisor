<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\IncomeNomenclature;

/**
 * Информация о номенклатуре в поставке
 */
class IncomeNomenclatureResource extends JsonResource
{
    /**
     * @param IncomeNomenclature $resource
     */
    public function __construct(IncomeNomenclature $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var IncomeNomenclature $incomeNomenclature */
        $incomeNomenclature = $this->resource;
        return [
            'nomenclature' => new NomenclatureCommonDataResource($incomeNomenclature->getNomenclature()),
            'items' => IncomeNomenclatureItemResource::collection(collect($incomeNomenclature->getItems())),
        ];
    }
}
