<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\StockItem;

/**
 * Элемент отчёта по складам
 */
class StockReportItemResource extends JsonResource
{
    /**
     * @param StockItem $resource
     */
    public function __construct(StockItem $resource)
    {
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var StockItem $stockItem */
        $stockItem = $this->resource;
        return [
            'nomenclature' => new NomenclatureCommonDataResource($stockItem->getCommonData()),
            'sizes' => StockReportItemSizeResource::collection(collect($stockItem->getSizes())),
        ];
    }
}
