<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetCategorySalesResponse\CategorySalesReport;

/**
 * Отчёт о продажах конкретной категории
 */
class CategorySalesReportResource extends JsonResource
{
    /**
     * @param CategorySalesReport $resource
     */
    public function __construct(CategorySalesReport $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var CategorySalesReport $categorySalesReport */
        $categorySalesReport = $this->resource;
        return [
            'category' => $categorySalesReport->getCategory(),
            'sales' => new SalesReportItemResource($categorySalesReport->getReport()),
        ];
    }
}
