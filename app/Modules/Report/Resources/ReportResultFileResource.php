<?php

namespace App\Modules\Report\Resources;

use App\Modules\Report\Models\ReportResultFile;
use App\Modules\Storage\Contracts\StorageServiceContract;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Результирующий файл
 */
class ReportResultFileResource extends JsonResource
{
    /** @var StorageServiceContract */
    protected StorageServiceContract $storageService;

    /**
     * @param ReportResultFile $resource
     * @param StorageServiceContract $storageService
     */
    public function __construct(ReportResultFile $resource, StorageServiceContract $storageService)
    {
        $this->storageService = $storageService;
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var ReportResultFile $reportResultFile */
        $reportResultFile = $this->resource;
        return [
            'file_id' => $reportResultFile->file_id,
            'status' => $reportResultFile->status_code,
            'download' => $this->when($reportResultFile->isDone(), function () use ($reportResultFile) {
                return $this->storageService->url($reportResultFile->file);
            }),
        ];
    }
}
