<?php

namespace App\Modules\Report\Resources;

use App\Modules\Report\Helpers\ProtobufTimestampHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetStocksReportResponse;

/**
 * Отчёт по складам
 */
class StockReportResource extends JsonResource
{
    /**
     * @param GetStocksReportResponse $resource
     */
    public function __construct(GetStocksReportResponse $resource)
    {
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var GetStocksReportResponse $response */
        $response = $this->resource;
        return [
            'items' => StockReportItemResource::collection(collect($response->getItems())),
            'page' => new PageResponseResource($response->getPage()),
            'last_import' => ProtobufTimestampHelper::toMilliseconds($response->getLastImport()),
        ];
    }
}
