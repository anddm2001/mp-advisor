<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\Nomenclature;

/**
 * Описание номенклатур в ответе на запрос каталога номенклатур мерчанта
 */
class NomenclatureResource extends JsonResource
{
    /**
     * @param Nomenclature $resource
     */
    public function __construct(Nomenclature $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var Nomenclature $nomenclature */
        $nomenclature = $this->resource;
        return [
            'id' => $nomenclature->getID(),
            'common_data' => new NomenclatureCommonDataResource($nomenclature->getCommonData()),
            'sizes' => NomenclatureSizeResource::collection(collect($nomenclature->getItems())),
        ];
    }
}
