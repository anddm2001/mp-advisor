<?php

namespace App\Modules\Report\Resources;

use App\Modules\Merchant\Http\Controllers\FileController;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Report\Models\ReportResultFile;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Экспорт данных в файл - ссылка для проверки статуса
 */
class ReportResultFileExportResource extends JsonResource
{
    /** @var Merchant  */
    protected Merchant $merchant;

    /**
     * @param Merchant $merchant
     * @param ReportResultFile $resource
     */
    public function __construct(Merchant $merchant, ReportResultFile $resource)
    {
        $this->merchant = $merchant;
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        $merchant = $this->merchant;
        /** @var ReportResultFile $file */
        $file = $this->resource;
        return [
            'file_id' => $file->id,
            'status_url' => action(FileController::class . '@status', compact('merchant', 'file')),
        ];
    }
}
