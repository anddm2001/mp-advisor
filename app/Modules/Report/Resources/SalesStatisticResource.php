<?php

namespace App\Modules\Report\Resources;

use App\Modules\Report\Helpers\ProtobufTimestampHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetStatisticSalesByRangeResponse\Statistic;

/**
 * Элемент статистики заказов и продаж
 */
class SalesStatisticResource extends JsonResource
{
    /**
     * @param Statistic $resource
     */
    public function __construct(Statistic $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        /** @var Statistic $statistic */
        $statistic = $this->resource;
        return [
            'date_from' => ProtobufTimestampHelper::toMilliseconds($statistic->getDateFrom()),
            'date_to' => ProtobufTimestampHelper::toMilliseconds($statistic->getDateTo()),
            'orders' => $statistic->getOrders(),
            'sales' => $statistic->getSales(),
        ];
    }
}
