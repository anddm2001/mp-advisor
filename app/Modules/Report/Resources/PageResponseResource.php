<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\PageResponse;

/**
 * Постраничная навигация
 */
class PageResponseResource extends JsonResource
{
    /**
     * @param PageResponse $resource
     */
    public function __construct(PageResponse $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var PageResponse $pageResponse */
        $pageResponse = $this->resource;
        return [
            'has_next_page' => $pageResponse->getHasNextPage(),
            'has_previous_page' => $pageResponse->getHasPreviousPage(),
        ];
    }
}
