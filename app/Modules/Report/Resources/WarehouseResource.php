<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\GetWarehousesResponse\Warehouse;

/**
 * Склад
 */
class WarehouseResource extends JsonResource
{
    /**
     * @param Warehouse $resource
     */
    public function __construct(Warehouse $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var Warehouse $warehouse */
        $warehouse = $this->resource;
        return [
            'id' => $warehouse->getId(),
            'name' => $warehouse->getName(),
        ];
    }
}
