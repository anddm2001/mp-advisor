<?php

namespace App\Modules\Report\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Report\SalesReportItem\SalesReportItemData;

/**
 * Показатели продаж
 */
class SalesReportItemDataResource extends JsonResource
{
    /**
     * @param SalesReportItemData $resource
     */
    public function __construct(SalesReportItemData $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        /** @var SalesReportItemData $salesReportItemData */
        $salesReportItemData = $this->resource;
        return [
            'receipt_cost' => $salesReportItemData->getReceiptCost(),
            'profit_cost' => $salesReportItemData->getProfitCost(),
            'marginality' => $salesReportItemData->getMarginality(),
            'orders' => $salesReportItemData->getOrders(),
            'sales' => $salesReportItemData->getSales(),
            'buyout' => $salesReportItemData->getBuyout(),
            'discount' => $salesReportItemData->getDiscount(),
            'returns' => $salesReportItemData->getReturns(),
            'orders_speed'=> $salesReportItemData->getOrdersSpeed(),
        ];
    }
}
