<?php

namespace App\Modules\Report\Contracts;

use App\Modules\Report\Models\Report;
use App\Modules\Report\Models\ReportResultFile;

/**
 * Интерфейс сервиса для создания и управления результирующего файла отчёта
 *
 * @package App\Modules\Report\Contracts
 */
interface ReportResultFileServiceContract
{
    /**
     * Запросить выгрузку нового файла отчёта
     *
     * @param int $merchantID
     *
     * @return ReportResultFile
     */
    public function requestExport(int $merchantID): ReportResultFile;

    /**
     * Загрузка файла в удалённое хранилище и сохранение файла внутрь БД
     *
     * @param ReportResultFile $file Модель запрошенного файла в БД
     * @param string $path Абсолютный путь до файла на локальном жёстком диске
     *
     * @return bool
     */
    public function uploadResultFile(ReportResultFile $file, string $path): bool;

    /**
     * Проставить файлу статус начала генерации
     *
     * @param ReportResultFile $file
     *
     * @return bool
     */
    public function beginGenerate(ReportResultFile $file): bool;

    /**
     * Проставить файлу статус окончания генерации
     *
     * @param ReportResultFile $file
     * @param bool $withErrors Были ли ошибки при генерации или нет
     *
     * @return bool
     */
    public function endGenerate(ReportResultFile $file, bool $withErrors): bool;
}
