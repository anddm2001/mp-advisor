<?php

namespace App\Modules\Report\Contracts;

/**
 * Интерфейс для описания маркетплейсов, используемых в сервисе
 *
 * @package App\Modules\Report\Contracts
 */
interface MarketPlaceContract
{
    /**
     * Тип маркетплейса - Wildberries
     */
    public const WILDBERRIES = 1;
}
