<?php

namespace App\Modules\Report\Exporter;

use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Report\GetRecommendationsReportResponse\NomenclatureRecommendations;
use Report\GetRecommendationsReportResponse\SizeRecommendations;

/**
 * Экспорт файла заказа поставки в Wildberries в XLSX
 *
 * @package App\Modules\Report\Exporter
 */
class WildberriesIncomeExporter extends AbstractXLSXExporter
{
    /**
     * Запись строки заголовка в таблицу
     *
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeHeaderData(): self
    {
        $data = ['Бренд', 'Артикул поставщика', 'Баркод', 'Количество'];
        $this->writeRowData($data);

        return $this;
    }

    /**
     * Запись номенклатуры в таблицу
     *
     * @param NomenclatureRecommendations $nomenclature
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeNomenclature(NomenclatureRecommendations $nomenclature): self
    {
        foreach ($nomenclature->getSizes() as $item) {
            /** @var SizeRecommendations $item */

            if (!$item->getApply()) {
                continue;
            }

            $data = [
                $nomenclature->getNomenclature()->getBrand(),
                $nomenclature->getNomenclature()->getVendorCode(),
                $item->getSize()->getBarcode(),
                $item->getToSupply(),
            ];

            $this->writeRowData($data);
        }

        return $this;
    }
}
