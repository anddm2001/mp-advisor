<?php

namespace App\Modules\Report\Exporter;

use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Report\Nomenclature;

/**
 * Экспорт данных номенклатур в XLSX
 */
class WildberriesNomenclaturesExporter extends AbstractXLSXExporter
{
    /**
     * Цвет фона заголовка (ARGB)
     */
    protected const STYLE_HEADER_BACKGROUND_COLOR = 'FFE7E6E6';

    /**
     * Записать строку заголовка со стилем
     *
     * @param array $data
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    protected function writeHeader(array $data): self
    {
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->setBackgroundColor(self::STYLE_HEADER_BACKGROUND_COLOR)
            ->build();

        $this->writeRowData($data, $headerStyle);

        return $this;
    }

    /**
     * Запись заголовка для файла данных номенклатур
     *
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeNomenclatureHeader(): self
    {
        $data = [
            'Бренд',
            'Предмет',
            'Артикул поставщика',
            'Артикул Wildberries',
            'Размер',
            'Баркод',
            'Розничная цена',
            'Скидка',
            'Себестоимость',
            'Квант поставки',
            'Поставляемое',
        ];

        return $this->writeHeader($data);
    }

    /**
     * Запись номенклатуры в таблицу
     *
     * @param Nomenclature $nomenclature
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeNomenclature(Nomenclature $nomenclature): self
    {
        $nomenclatureCommonData = $nomenclature->getCommonData();
        foreach ($nomenclature->getItems() as $item) {
            /** @var Nomenclature\Size $item */
            $data = [
                $nomenclatureCommonData->getBrand(),
                $nomenclatureCommonData->getName(),
                $nomenclatureCommonData->getVendorCode(),
                $nomenclatureCommonData->getMarketplaceNomenclatureCode(),
                $item->getSize()->getSize(),
                $item->getSize()->getBarcode(),
                $item->getSaleCost(),
                $item->getDiscount(),
                $item->getBaseCost(),
                $item->getSupplySet(),
                $item->getRemoved() ? 'Нет' : 'Да',
            ];

            $this->writeRowData($data);
        }

        return $this;
    }
}
