<?php

namespace App\Modules\Report\Exporter;

use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Report\NomenclatureCommonData;
use Report\StockItem;
use Report\StockItem\SizesItem;

/**
 * Экспорт данных складов в XLSX
 */
class WildberriesStocksExporter extends AbstractXLSXExporter
{
    /**
     * Цвет фона заголовка (ARGB)
     */
    protected const STYLE_HEADER_BACKGROUND_COLOR = 'FFE7E6E6';

    /**
     * Записать строку заголовка со стилем
     *
     * @param array $data
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    protected function writeHeader(array $data): self
    {
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->setBackgroundColor(self::STYLE_HEADER_BACKGROUND_COLOR)
            ->build();

        $this->writeRowData($data, $headerStyle);

        return $this;
    }

    /**
     * Запись заголовка для файла данных складов
     *
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeStocksHeader(): self
    {
        $data = [
            'Бренд',
            'Предмет',
            'Артикул поставщика',
            'Артикул Wildberries',
            'Размер',
            'Свой склад',
            'Полный остаток',
            'Кол-во не в заказе',
            'Доступно для заказов',
            'В пути',
            'Потери',
        ];

        return $this->writeHeader($data);
    }

    /**
     * Запись номенклатуры в таблицу
     *
     * @param StockItem $item
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeStockData(StockItem $item): self
    {
        $nomenclatureCommonData = $item->getCommonData() ?? new NomenclatureCommonData();
        foreach ($item->getSizes() as $size) {
            /** @var SizesItem $size */
            $data = [
                $nomenclatureCommonData->getBrand(),
                $nomenclatureCommonData->getName(),
                $nomenclatureCommonData->getVendorCode(),
                $nomenclatureCommonData->getMarketplaceNomenclatureCode(),
                $size->getSize(),
                $size->getMyStockAmount(),
                $size->getQuantityFull(),
                $size->getNotOrders(),
                $size->getQuantity(),
                $size->getInWay(),
                $size->getLostAmount(),
            ];

            $this->writeRowData($data);
        }

        return $this;
    }
}
