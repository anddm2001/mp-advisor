<?php

namespace App\Modules\Report\Exporter;

use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Report\GetBrandSalesResponse\BrandSalesReport;
use Report\GetCategorySalesResponse\CategorySalesReport;
use Report\GetNomenclatureSalesResponse\NomenclatureSalesReport;
use Report\GetNomenclatureSalesResponse\SizeSalesReport;

/**
 * Экспорт отчёта продаж в XLSX
 */
class WildberriesSalesExporter extends AbstractXLSXExporter
{
    /**
     * Цвет фона заголовка (ARGB)
     */
    protected const STYLE_HEADER_BACKGROUND_COLOR = 'FFE7E6E6';

    /**
     * Записать строку с данными периода выгрузки
     *
     * @param array $periodData
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writePeriodHeader(array $periodData): self
    {
        $boldStyle = (new StyleBuilder)->setFontBold()->build();

        $date_from = $periodData['date_from'] ?? '?';
        $date_to = $periodData['date_to'] ?? '?';
        $data = [
            WriterEntityFactory::createCell('Период', $boldStyle),
            $date_from . ' — ' . $date_to,
        ];

        $this->writeRowData($data);
        $this->writeRowData([]);

        return $this;
    }

    /**
     * Записать строку заголовка со стилем
     *
     * @param array $data
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    protected function writeHeader(array $data): self
    {
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->setBackgroundColor(self::STYLE_HEADER_BACKGROUND_COLOR)
            ->build();

        $this->writeRowData($data, $headerStyle);

        return $this;
    }

    /**
     * Запись заголовка для файла продаж номенклатур
     *
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeNomenclatureHeader(): self
    {
        $data = [
            'Бренд',
            'Предмет',
            'Артикул поставщика',
            'Артикул Wildberries',
            'Размер',
            'Заказы',
            'Продажи',
            'Возвраты',
            'Скорость заказов',
            '% выкупа',
            'Выручка',
            'Валовая прибыль',
            'Валовая маржа',
            'Скидка',
        ];

        return $this->writeHeader($data);
    }

    /**
     * Запись номенклатуры в таблицу
     *
     * @param NomenclatureSalesReport $salesNomenclature
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeNomenclature(NomenclatureSalesReport $salesNomenclature): self
    {
        $nomenclature = $salesNomenclature->getNomenclature();
        foreach ($salesNomenclature->getSizes() as $item) {
            /** @var SizeSalesReport $item */
            $currentData = $item->getReport()->getCurrentData();
            $data = [
                $nomenclature->getBrand(),
                $nomenclature->getName(),
                $nomenclature->getVendorCode(),
                $nomenclature->getMarketplaceNomenclatureCode(),
                $item->getSize()->getSize(),
                $currentData->getOrders(),
                $currentData->getSales(),
                $currentData->getReturns(),
                $currentData->getOrdersSpeed(),
                $currentData->getBuyout(),
                $currentData->getReceiptCost(),
                $currentData->getProfitCost(),
                $currentData->getMarginality(),
                $currentData->getDiscount(),
            ];

            $this->writeRowData($data);
        }

        return $this;
    }

    /**
     * Запись заголовка для файла продаж брендов
     *
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeBrandHeader(): self
    {
        $data = [
            'Бренд',
            'Заказы',
            'Продажи',
            'Возвраты',
            'Скорость заказов',
            '% выкупа',
            'Выручка',
            'Валовая прибыль',
            'Валовая маржа',
            'Скидка',
        ];

        return $this->writeHeader($data);
    }

    /**
     * Запись бренда в таблицу
     *
     * @param BrandSalesReport $salesBrand
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeBrand(BrandSalesReport $salesBrand): self
    {
        $currentData = $salesBrand->getReport()->getCurrentData();
        $data = [
            $salesBrand->getBrand(),
            $currentData->getOrders(),
            $currentData->getSales(),
            $currentData->getReturns(),
            $currentData->getOrdersSpeed(),
            $currentData->getBuyout(),
            $currentData->getReceiptCost(),
            $currentData->getProfitCost(),
            $currentData->getMarginality(),
            $currentData->getDiscount(),
        ];
        $this->writeRowData($data);

        return $this;
    }

    /**
     * Запись заголовка для файла продаж категорий
     *
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeCategoryHeader(): self
    {
        $data = [
            'Категория',
            'Заказы',
            'Продажи',
            'Возвраты',
            'Скорость заказов',
            '% выкупа',
            'Выручка',
            'Валовая прибыль',
            'Валовая маржа',
            'Скидка',
        ];

        return $this->writeHeader($data);
    }

    /**
     * Запись категории в таблицу
     *
     * @param CategorySalesReport $salesCategory
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeCategory(CategorySalesReport $salesCategory): self
    {
        $currentData = $salesCategory->getReport()->getCurrentData();
        $data = [
            $salesCategory->getCategory(),
            $currentData->getOrders(),
            $currentData->getSales(),
            $currentData->getReturns(),
            $currentData->getOrdersSpeed(),
            $currentData->getBuyout(),
            $currentData->getReceiptCost(),
            $currentData->getProfitCost(),
            $currentData->getMarginality(),
            $currentData->getDiscount(),
        ];
        $this->writeRowData($data);

        return $this;
    }
}
