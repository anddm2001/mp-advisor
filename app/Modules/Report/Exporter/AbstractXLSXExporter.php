<?php

namespace App\Modules\Report\Exporter;

use Box\Spout\Common\Entity\Cell;
use Box\Spout\Common\Entity\Style\Style;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Box\Spout\Writer\WriterAbstract;

/**
 * Абстрактный класс для экспортеров отчётов в XLSX
 *
 * @package App\Modules\Report\Exporter
 */
abstract class AbstractXLSXExporter
{
    /**
     * @var WriterAbstract Хендлер для сохранения файла
     */
    protected WriterAbstract $writer;

    /**
     * WildberriesApiReportExporter constructor.
     *
     * @param WriterAbstract $writer
     */
    public function __construct(WriterAbstract $writer)
    {
        $this->writer = $writer;
    }

    /**
     * Запись строки в таблицу
     *
     * @param array $data
     * @param Style|null $rowStyle
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    protected function writeRowData(array $data, ?Style $rowStyle = null): void
    {
        $row = WriterEntityFactory::createRow([], $rowStyle);

        foreach ($data as $cellData) {
            $row->addCell($cellData instanceof Cell ? $cellData : WriterEntityFactory::createCell($cellData));
        }

        $this->writer->addRow($row);
    }

    /**
     * Открыть файл для записи
     *
     * @param string $filePath
     * @return $this
     * @throws IOException
     */
    public function openFile(string $filePath): self
    {
        $this->writer->openToFile($filePath);

        return $this;
    }

    /**
     * Завершить обработку
     */
    public function finish(): void
    {
        $this->writer->close();
    }
}
