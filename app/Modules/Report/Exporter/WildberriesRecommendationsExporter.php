<?php

namespace App\Modules\Report\Exporter;

use App\Modules\Report\Helpers\ProtobufTimestampHelper;
use Box\Spout\Common\Entity\Cell;
use Box\Spout\Common\Entity\Style\Border;
use Box\Spout\Common\Entity\Style\CellAlignment;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Common\Exception\InvalidArgumentException;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\Style\BorderBuilder;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Report\GetRecommendationsReportResponse\NomenclatureRecommendations;
use Report\GetRecommendationsReportResponse\SizeRecommendations;
use Report\RecommendationsReportSummary;

/**
 * Экспорт файла рекомендаций к поставке в Wildberries в XLSX
 *
 * @package App\Modules\Report\Exporter
 */
class WildberriesRecommendationsExporter extends AbstractXLSXExporter
{
    /**
     * Цвет фона заголовка (ARGB)
     */
    protected const STYLE_HEADER_BACKGROUND_COLOR = 'FFE7E6E6';

    /**
     * @var bool Выгрузка полного отчёта
     */
    protected bool $isFullReport = false;

    /**
     * Выгружаем полный отчёт или только добавленные в поставку позиции
     *
     * @param bool $isFullReport
     * @return $this
     */
    public function setIsFullReport(bool $isFullReport): self
    {
        $this->isFullReport = $isFullReport;

        return $this;
    }

    /**
     * Запись строк заголовка в таблицу
     *
     * @param RecommendationsReportSummary $summary
     * @return $this
     * @throws IOException
     * @throws InvalidArgumentException
     * @throws WriterNotOpenedException
     */
    public function writeHeaderData(RecommendationsReportSummary $summary): self
    {
        $reportDays = (int) $summary->getReportDays();
        $calculateFrom = ProtobufTimestampHelper::toMilliseconds($summary->getCalculateFrom());
        $calculateTo = ProtobufTimestampHelper::toMilliseconds($summary->getCalculateTo());
        $calculateDays = (int) ceil(($calculateTo - $calculateFrom) / 1000 / 60 / 60 / 24);
        $daysDeclension = function (int $days): string {
            if ($days > 20) $days %= 10;
            if ($days == 1) return 'день';
            if ($days > 1 && $days < 5) return 'дня';
            return 'дней';
        };
        $rightBorderedCell = function (string $cellValue): Cell {
            $cellStyle = (new StyleBuilder())
                ->setBorder(
                    (new BorderBuilder())
                        ->setBorderRight(Color::BLACK, Border::WIDTH_THIN)
                        ->build()
                )
                ->build();
            return WriterEntityFactory::createCell($cellValue, $cellStyle);
        };
        $bottomBorderedCell = function (string $cellValue): Cell {
            $cellStyle = (new StyleBuilder())->setBorder((new BorderBuilder())->setBorderBottom()->build())->build();
            return WriterEntityFactory::createCell($cellValue, $cellStyle);
        };
        $rightAndBottomBorderedCell = function (string $cellValue): Cell {
            $cellStyle = (new StyleBuilder())
                ->setBorder(
                    (new BorderBuilder())
                        ->setBorderRight(Color::BLACK, Border::WIDTH_THIN)
                        ->setBorderBottom()
                        ->build()
                )
                ->build();
            return WriterEntityFactory::createCell($cellValue, $cellStyle);
        };

        $data = [
            [
                $rightBorderedCell('Бренд'),
                $rightBorderedCell('Предмет'),
                $rightBorderedCell('Артикул поставщика'),
                $rightBorderedCell('Артикул Wildberries'),
                $rightBorderedCell('Баркод'),
                $rightBorderedCell('Размер'),
                $rightBorderedCell('Остатки'),
                '',
                'В пути',
                $rightBorderedCell(''),
                'Заказы',
                $rightBorderedCell('за ' . $calculateDays . ' ' . $daysDeclension($calculateDays)),
                $rightBorderedCell('Доступен'),
                $rightBorderedCell('Обеспечить запас'),
                'К поставке',
                $rightBorderedCell(''),
            ],
            [
                $rightAndBottomBorderedCell(''), // Бренд
                $rightAndBottomBorderedCell(''), // Предмет
                $rightAndBottomBorderedCell(''), // Артикул поставщика
                $rightAndBottomBorderedCell(''), // Артикул WB
                $rightAndBottomBorderedCell(''), // Баркод
                $rightAndBottomBorderedCell(''), // Размер
                $rightAndBottomBorderedCell(''), // Остатки
                $bottomBorderedCell('К клиенту'), // В пути
                $bottomBorderedCell('От клиента'), // В пути
                $rightAndBottomBorderedCell('Едет в WB'), // В пути
                $bottomBorderedCell('Скорость'), // Заказы
                $rightAndBottomBorderedCell('Количество'), // Заказы
                $rightAndBottomBorderedCell(''), // Доступен
                $rightAndBottomBorderedCell('на ' . $reportDays . ' ' . $daysDeclension($reportDays)), // Обеспечить запас
                $bottomBorderedCell('Товара'), // К поставке
                $rightAndBottomBorderedCell('Коробов'), // К поставке
            ]
        ];

        $rowBackgroundStyle = (new StyleBuilder())
            ->setCellAlignment(CellAlignment::CENTER)
            ->setBackgroundColor(self::STYLE_HEADER_BACKGROUND_COLOR)
            ->build();
        $this->writeRowData($data[0], $rowBackgroundStyle);
        $this->writeRowData($data[1], $rowBackgroundStyle);

        return $this;
    }

    /**
     * Запись строки рекомендации в таблицу
     *
     * @param NomenclatureRecommendations $nomenclature
     * @return $this
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function writeNomenclature(NomenclatureRecommendations $nomenclature): self
    {
        foreach ($nomenclature->getSizes() as $item) {
            /** @var SizeRecommendations $item */
            $data = [
                $nomenclature->getNomenclature()->getBrand(),
                $nomenclature->getNomenclature()->getName(),
                $nomenclature->getNomenclature()->getVendorCode(),
                $nomenclature->getNomenclature()->getMarketplaceNomenclatureCode(),
                $item->getSize()->getBarcode(),
                $item->getSize()->getSize(),
                $item->getInStock(),
                $item->getToOrder(),
                $item->getFromOrder(),
                $item->getInIncome(),
                $item->getOrderSpeed(),
                $item->getOrderedCount(),
                $item->getDays() . ' дн.',
                $item->getNeedStock(),
                $item->getToSupply(),
                $item->getToSupplySets(),
            ];

            $this->writeRowData($data);
        }

        return $this;
    }
}
