<?php

namespace App\Modules\Report\Policies;

use App\Modules\Auth\Models\User;
use App\Modules\Report\Models\ReportResultFile;

/**
 * Проверка прав доступа к отчётам
 *
 * @package App\Modules\Report\Policies
 */
class ReportPolicy
{
    /**
     * Разрешено смотреть результат выгрузки файла
     *
     * @param User $user
     * @param ReportResultFile $file
     *
     * @return bool
     */
    public function allowViewResultFile(User $user, ReportResultFile $file): bool
    {
        return $user->merchants->where('id', $file->merchant_id)->isNotEmpty();
    }
}
