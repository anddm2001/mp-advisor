<?php

namespace App\Modules\Report\Events;

use App\Modules\Report\Models\ReportResultFile;

/**
 * Событие на экспорт продаж в XLSX
 */
abstract class AbstractSalesExportEvent
{
    /**
     * @var ReportResultFile Сгенерированная запись в БД с запросом
     */
    protected ReportResultFile $resultFile;

    /**
     * @var int Идентификатор мерчанта, для которого сгенерировать файл
     */
    protected int $merchantID;

    /**
     * @var array Данные периода для получения отчёта
     */
    protected array $periodData;

    /**
     * @var array Данные для фильтрации данных отчёта
     */
    protected array $filterData;

    /**
     * Определение параметров события
     *
     * @param ReportResultFile $resultFile
     * @param int $merchantID
     * @param array $periodData
     * @param array $filterData
     */
    public function __construct(
        ReportResultFile $resultFile,
        int $merchantID,
        array $periodData,
        array $filterData = []
    ) {
        $this->resultFile = $resultFile;
        $this->merchantID = $merchantID;
        $this->periodData = $periodData;
        $this->filterData = $filterData;
    }

    /**
     * @return ReportResultFile
     */
    public function getResultFile(): ReportResultFile
    {
        return $this->resultFile;
    }

    /**
     * @return int
     */
    public function getMerchantID(): int
    {
        return $this->merchantID;
    }

    /**
     * @return array
     */
    public function getPeriodData(): array
    {
        return $this->periodData;
    }

    /**
     * @return array
     */
    public function getFilterData(): array
    {
        return $this->filterData;
    }
}
