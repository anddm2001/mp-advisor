<?php

namespace App\Modules\Report\Events;

use App\Modules\Merchant\Models\MerchantConnection;
use DateTime;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

/**
 * Событие на сбор данных из Wildberries API по расписанию
 *
 * @package App\Modules\Report\Events
 */
class WildberriesApiScheduledDataSyncEvent
{
    use SerializesModels;

    /** @var string Данные за вчера */
    public const TYPE_YESTERDAY = 'yesterday';
    /** @var string Данные за прошлый месяц */
    public const TYPE_PREV_MONTH = 'prev-month';

    /**
     * @var MerchantConnection Данные подключения
     */
    protected MerchantConnection $connection;

    /**
     * @var string Ключ для подключения
     */
    protected string $base64Key;

    /**
     * @var string Тип добора данных (за вчера или за прошлый месяц)
     */
    protected string $type;

    /**
     * Определение параметров события
     *
     * @param MerchantConnection $connection
     * @param string $base64Key
     * @param string $type
     */
    public function __construct(MerchantConnection $connection, string $base64Key, string $type) {
        $this->connection = $connection;
        $this->base64Key = $base64Key;
        $this->type = $type;
    }

    /**
     * @return MerchantConnection
     */
    public function getConnection(): MerchantConnection
    {
        return $this->connection;
    }

    /**
     * @return string
     */
    public function getBase64Key(): string
    {
        return $this->base64Key;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Дата начала периода для сбора данных в зависимости от типа
     *
     * @return DateTime
     */
    public function getDateFrom(): DateTime
    {
        $dateFrom = Carbon::today();

        switch ($this->getType()) {
            case self::TYPE_YESTERDAY:
                $dateFrom = Carbon::yesterday();
                break;
            case self::TYPE_PREV_MONTH:
                $dateFrom = $dateFrom->subMonthNoOverflow()->startOfMonth();
                break;
        }

        return $dateFrom->toDateTime();
    }

    /**
     * Получить валидные типы добора данных
     *
     * @return string[]
     */
    public static function getValidTypes(): array
    {
        return [self::TYPE_YESTERDAY, self::TYPE_PREV_MONTH];
    }
}
