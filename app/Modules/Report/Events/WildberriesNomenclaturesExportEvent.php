<?php

namespace App\Modules\Report\Events;

use App\Modules\Report\Models\ReportResultFile;

/**
 * Событие для отложенной генерации результирующего файла c данными номерклатур из Wildberries API
 *
 * @package App\Modules\Report\Events
 */
class WildberriesNomenclaturesExportEvent
{
    /**
     * @var ReportResultFile Сгенерированная запись в БД с запросом
     */
    protected ReportResultFile $resultFile;

    /**
     * @var int Идентификатор мерчанта, для которого сгенерировать файл
     */
    protected int $merchantID;

    /**
     * @var array Данные для фильтрации номенклатур
     */
    protected array $filterData;

    /**
     * @var array Данные для фильтрации номенклатур по поставляемому / архиву
     */
    protected array $removedFilterData;

    /**
     * Определение параметров события
     *
     * @param ReportResultFile $resultFile
     * @param int $merchantID
     * @param array $filterData
     * @param array $removedFilterData
     */
    public function __construct(
        ReportResultFile $resultFile,
        int $merchantID,
        array $filterData = [],
        array $removedFilterData = []
    ) {
        $this->resultFile = $resultFile;
        $this->merchantID = $merchantID;
        $this->filterData = $filterData;
        $this->removedFilterData = $removedFilterData;
    }

    /**
     * @return ReportResultFile
     */
    public function getResultFile(): ReportResultFile
    {
        return $this->resultFile;
    }

    /**
     * @return int
     */
    public function getMerchantID(): int
    {
        return $this->merchantID;
    }

    /**
     * @return array
     */
    public function getFilterData(): array
    {
        return $this->filterData;
    }

    /**
     * @return array
     */
    public function getRemovedFilterData(): array
    {
        return $this->removedFilterData;
    }
}
