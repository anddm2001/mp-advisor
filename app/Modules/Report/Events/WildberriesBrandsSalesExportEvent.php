<?php

namespace App\Modules\Report\Events;

/**
 * Запрос на выгрузку продаж по брендам в XLSX
 */
class WildberriesBrandsSalesExportEvent extends AbstractSalesExportEvent
{
    //
}
