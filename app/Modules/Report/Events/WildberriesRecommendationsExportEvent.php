<?php

namespace App\Modules\Report\Events;

use App\Modules\Report\Models\ReportResultFile;

/**
 * Событие для отложенной генерации результирующего файла отчёта из Wildberries API
 *
 * @package App\Modules\Report\Events
 */
class WildberriesRecommendationsExportEvent
{
    /**
     * Форматы выгрузки
     */
    public const EXPORT_FORMAT_WB_INCOME = 'wb_income';
    public const EXPORT_FORMAT_EXCEL_APPLIED = 'excel_applied';
    public const EXPORT_FORMAT_EXCEL_FULL = 'excel_full';

    /**
     * @var ReportResultFile Сгенерированная запись в БД с запросом
     */
    protected ReportResultFile $resultFile;

    /**
     * @var int Идентификатор мерчанта, для которого сгенерировать файл
     */
    protected int $merchantID;

    /**
     * @var string Отчёт, для котрого сгенерировать файл
     */
    protected string $reportID;

    /**
     * @var string Формат выгрузки
     */
    protected string $exportFormat;

    /**
     * WildberriesApiExportEvent constructor.
     *
     * @param ReportResultFile $resultFile
     * @param int $merchantID
     * @param string $reportID
     * @param string $exportFormat
     */
    public function __construct(
        ReportResultFile $resultFile,
        int $merchantID,
        string $reportID,
        string $exportFormat
    )
    {
        $this->resultFile = $resultFile;
        $this->merchantID = $merchantID;
        $this->reportID = $reportID;
        $this->exportFormat = $exportFormat;
    }

    /**
     * @return int
     */
    public function getMerchantID(): int
    {
        return $this->merchantID;
    }

    /**
     * @return string
     */
    public function getReportID(): string
    {
        return $this->reportID;
    }

    /**
     * @return ReportResultFile
     */
    public function getResultFile(): ReportResultFile
    {
        return $this->resultFile;
    }

    /**
     * @return string
     */
    public function getExportFormat(): string
    {
        return $this->exportFormat;
    }
}
