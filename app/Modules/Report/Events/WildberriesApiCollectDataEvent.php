<?php

namespace App\Modules\Report\Events;

use App\Modules\Merchant\Models\MerchantConnection;
use Illuminate\Queue\SerializesModels;

/**
 * Событие на сбор данных из Wildberries API для мерчанта
 */
class WildberriesApiCollectDataEvent
{
    use SerializesModels;

    /** @var MerchantConnection Данные подключения */
    protected MerchantConnection $connection;

    /** @var string Ключ для подключения */
    protected string $base64Key;

    /** @var string Тип синхронизации */
    protected string $syncType;

    /** @var bool Игнорировать время последней синхронизации */
    protected bool $ignoreLastSync;

    /**
     * Определение параметров события
     *
     * @param MerchantConnection $connection
     * @param string $base64Key
     * @param string $syncType
     * @param bool $ignoreLastSync
     */
    public function __construct(
        MerchantConnection $connection,
        string $base64Key,
        string $syncType,
        bool $ignoreLastSync = false
    )
    {
        $this->connection = $connection;
        $this->base64Key = $base64Key;
        $this->syncType = $syncType;
        $this->ignoreLastSync = $ignoreLastSync;
    }

    /**
     * @return bool
     */
    public function isIgnoreLastSync(): bool
    {
        return $this->ignoreLastSync;
    }

    /**
     * @return MerchantConnection
     */
    public function getConnection(): MerchantConnection
    {
        return $this->connection;
    }

    /**
     * @return string
     */
    public function getBase64Key(): string
    {
        return $this->base64Key;
    }

    /**
     * @return string
     */
    public function getSyncType(): string
    {
        return $this->syncType;
    }
}
