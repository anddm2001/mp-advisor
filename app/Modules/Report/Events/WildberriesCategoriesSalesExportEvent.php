<?php

namespace App\Modules\Report\Events;

/**
 * Запрос на выгрузку продаж по категориям в XLSX
 */
class WildberriesCategoriesSalesExportEvent extends AbstractSalesExportEvent
{
    //
}
