<?php

namespace App\Modules\Report\Events;

/**
 * Событие на сбор данных о продажах мерчанта на каждую дату
 *
 * @package App\Modules\Report\Events
 */
class WildberriesApiCollectEachDaySalesEvent
{
    /**
     * @var int Идентификатор мерчанта
     */
    protected int $merchantID;

    /**
     * @var \DateTime Дата, с которой собирать продажи
     */
    protected \DateTime $since;

    /**
     * WildberriesApiCollectSalesEvent constructor.
     *
     * @param int $merchantID
     * @param \DateTime $since
     */
    public function __construct(int $merchantID, \DateTime $since)
    {
        $this->merchantID = $merchantID;
        $this->since = $since;
    }

    /**
     * @return int
     */
    public function getMerchantID(): int
    {
        return $this->merchantID;
    }

    /**
     * @return \DateTime
     */
    public function getSince(): \DateTime
    {
        return $this->since;
    }
}
