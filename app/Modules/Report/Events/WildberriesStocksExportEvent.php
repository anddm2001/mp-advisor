<?php

namespace App\Modules\Report\Events;

use App\Modules\Report\Models\ReportResultFile;

/**
 * Событие для отложенной генерации результирующего файла c данными складов
 */
class WildberriesStocksExportEvent
{
    /** @var ReportResultFile Сгенерированная запись в БД с запросом */
    protected ReportResultFile $resultFile;

    /** @var int Идентификатор мерчанта, для которого сгенерировать файл */
    protected int $merchantId;

    /** @var array Данные для фильтрации номенклатур */
    protected array $nomenclatureFilterData;

    /** @var array Данные для фильтрации складов */
    protected array $stockFilterData;

    /** @var array Данные для сортировки отчёта */
    protected array $sortData;

    /**
     * Определение параметров события
     *
     * @param ReportResultFile $resultFile
     * @param int $merchantId
     * @param array $nomenclatureFilterData
     * @param array $stockFilterData
     * @param array $sortData
     */
    public function __construct(
        ReportResultFile $resultFile,
        int $merchantId,
        array $nomenclatureFilterData = [],
        array $stockFilterData = [],
        array $sortData = []
    )
    {
        $this->resultFile = $resultFile;
        $this->merchantId = $merchantId;
        $this->nomenclatureFilterData = $nomenclatureFilterData;
        $this->stockFilterData = $stockFilterData;
        $this->sortData = $sortData;
    }

    /**
     * @return ReportResultFile
     */
    public function getResultFile(): ReportResultFile
    {
        return $this->resultFile;
    }

    /**
     * @return int
     */
    public function getMerchantId(): int
    {
        return $this->merchantId;
    }

    /**
     * @return array
     */
    public function getNomenclatureFilterData(): array
    {
        return $this->nomenclatureFilterData;
    }

    /**
     * @return array
     */
    public function getStockFilterData(): array
    {
        return $this->stockFilterData;
    }

    /**
     * @return array
     */
    public function getSortData(): array
    {
        return $this->sortData;
    }
}
