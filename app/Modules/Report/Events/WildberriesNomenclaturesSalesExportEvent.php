<?php

namespace App\Modules\Report\Events;

/**
 * Запрос на выгрузку продаж по номенклатурам в XLSX
 */
class WildberriesNomenclaturesSalesExportEvent extends AbstractSalesExportEvent
{
    //
}
