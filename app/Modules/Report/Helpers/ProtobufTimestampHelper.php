<?php

namespace App\Modules\Report\Helpers;

use DateTime;
use Google\Protobuf\Timestamp;
use Illuminate\Support\Carbon;

/**
 * Хелперы для работы с Google Protobuf Timestamp
 */
class ProtobufTimestampHelper
{
    /**
     * Метка времени из Protobuf в миллисекундах
     *
     * @param Timestamp|null $timestamp
     * @return int|null
     */
    public static function toMilliseconds(?Timestamp $timestamp): ?int
    {
        $timestamp = self::prepare($timestamp);

        return $timestamp ? (int) self::toFormat($timestamp, 'Uv') : null;
    }

    /**
     * Метка времени из Protobuf в указанном формате
     *
     * @param Timestamp|null $timestamp
     * @param string $format
     * @return string|null
     */
    public static function toFormat(?Timestamp $timestamp, string $format = Carbon::DEFAULT_TO_STRING_FORMAT): ?string
    {
        $timestamp = self::prepare($timestamp);

        return $timestamp ? $timestamp->toDateTime()->format($format) : null;
    }

    /**
     * Метка времени из Protobuf в DateTime
     *
     * @param Timestamp|null $timestamp
     * @return DateTime|null
     */
    public static function toDateTime(?Timestamp $timestamp): ?DateTime
    {
        $timestamp = self::prepare($timestamp);

        return $timestamp ? $timestamp->toDateTime() : null;
    }

    /**
     * Подготовка метки из Protobuf
     *
     * @param Timestamp|null $timestamp
     * @return Timestamp|null
     */
    protected static function prepare(?Timestamp $timestamp): ?Timestamp
    {
        if (is_null($timestamp) || $timestamp->getSeconds() < 0) {
            return null;
        }

        $nanos = $timestamp->getNanos();
        if ($nanos < 0 || $nanos > 999999999) {
            $timestamp->setNanos(0);
        }

        return $timestamp;
    }
}
