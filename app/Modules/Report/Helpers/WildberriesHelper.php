<?php

namespace App\Modules\Report\Helpers;

/**
 * Хелпер для работы с номенклатурами в Wildberries
 *
 * @package App\Modules\Report\Helpers
 */
class WildberriesHelper
{
    /**
     * Получить ссылку на картинку номенклатуры
     *
     * @param string $wildberriesCode
     *
     * @return string
     */
    public static function getNomenclatureImageUrl(string $wildberriesCode): string
    {
        $subPath = $wildberriesCode;

        if (strlen($subPath) > 4) {
            $subPath = substr($subPath, 0, strlen($subPath) - 4) . str_repeat('0', 4);
        }

        $pattern = 'https://images.wbstatic.net/tm/new/%s/%s-1.jpg';

        return sprintf($pattern, $subPath, $wildberriesCode);
    }

    /**
     * Получить ссылку на товар
     *
     * @param string $wildberriesCode
     *
     * @return string
     */
    public static function getNomenclatureUrl(string $wildberriesCode): string
    {
        return 'https://www.wildberries.ru/catalog/' . $wildberriesCode . '/detail.aspx';
    }
}
