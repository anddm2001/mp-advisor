<?php
namespace App\Modules\Report\Helpers;

use Report\IncomeStatus;

/**
 * Хелперы статусов поставок
 */
class IncomeStatusHelper
{
    /**
     * Идентификатор в строковое название
     *
     * @param int $status
     * @return string|string[]
     */
    public static function toName(int $status)
    {
        return str_replace('status_', '', strtolower(IncomeStatus::name($status)));
    }
}
