<?php

namespace App\Modules\Report\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на изменение номенклатуры
 *
 * @package App\Modules\Report\Requests
 */
class UpdateNomenclatureSizeRequest extends FormRequest
{
    /**
     * Доступ только для авторизованных пользователей, привязанных к мерчантам
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() instanceof User;
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nomenclature_id' => [
                'required',
                'string',
            ],
            'size' => [
                'required',
                'string',
            ],
            'supply_set' => [
                'nullable',
                'integer',
                'min:0',
            ],
            'base_cost' => [
                'required',
                'numeric',
            ],
            'removed' => [
                'boolean',
            ],
        ];
    }

    /**
     * Формирование запроса для API
     *
     * @param int $merchantID
     *
     * @return \Report\UpdateNomenclatureSizeRequest
     */
    public function getAPIRequest(int $merchantID): \Report\UpdateNomenclatureSizeRequest
    {
        return new \Report\UpdateNomenclatureSizeRequest([
            'merchantID' => $merchantID,
            'nomenclatureID' => $this->input('nomenclature_id'),
            'size' => $this->input('size'),
            'supplySet' => $this->input('supply_set'),
            'baseCost' => $this->input('base_cost'),
            'removed' => $this->input('removed') == 1,
        ]);
    }
}
