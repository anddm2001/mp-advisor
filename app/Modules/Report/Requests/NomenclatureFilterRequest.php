<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use Illuminate\Support\Collection;
use Report\NomenclatureFilter;

/**
 * Запрос на фильтрацию номенклатур
 *
 * @package App\Modules\Report\Requests
 */
class NomenclatureFilterRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'barcode.*' => [
                'nullable',
                'string',
                'max:100',
            ],
            'exclude_barcode.*' => [
                'nullable',
                'string',
                'max:100',
            ],
            'vendor_code.*' => [
                'nullable',
                'string',
                'max:100',
            ],
            'exclude_vendor_code.*' => [
                'nullable',
                'string',
                'max:100',
            ],
            'marketplace_nomenclature_code' => [
                'nullable',
                'integer',
            ],
            'brand' => [
                'nullable',
                'string',
                'max:100',
            ],
            'name' => [
                'nullable',
                'string',
                'max:100',
            ],
        ];
    }

    /**
     * Подписи атрибутов
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'vendor_code' => 'Артикул поставщика',
            'barcode' => 'Штрихкод',
            'marketplace_nomenclature_code' => 'ID Wildberries',
            'brand' => 'Бренд',
            'name' => 'Предмет',
        ];
    }

    /**
     * Отфильтровать данные запроса
     *
     * @return array
     */
    public function filterData(): array
    {
        $notEmptyFilter = function($value): bool {
            return is_scalar($value) && !empty($value);
        };

        $vendorCodes = (new Collection($this->input('vendor_code', [])))
            ->filter($notEmptyFilter)
            ->all();

        $excludeVendorCodes = (new Collection($this->input('exclude_vendor_code', [])))
            ->filter($notEmptyFilter)
            ->all();

        $barcodes = (new Collection($this->input('barcode', [])))
            ->filter($notEmptyFilter)
            ->all();

        $excludeBarcodes = (new Collection($this->input('exclude_barcode', [])))
            ->filter($notEmptyFilter)
            ->all();

        return [
            'vendor_code' => $vendorCodes,
            'exclude_vendor_code' => $excludeVendorCodes,
            'barcode' => $barcodes,
            'exclude_barcode' => $excludeBarcodes,
        ];
    }

    /**
     * Получить запрос для Wildberries API
     *
     * @return NomenclatureFilter
     */
    public function getAPIRequest(): NomenclatureFilter
    {
        $filteredData = $this->filterData();

        return new NomenclatureFilter([
            'name' => $this->input('name'),
            'brand' => $this->input('brand'),
            'vendorCode' => $filteredData['vendor_code'] ?? [],
            'excludeVendorCode' => $filteredData['exclude_vendor_code'] ?? [],
            'barcode' => $filteredData['barcode'] ?? [],
            'excludeBarcode' => $filteredData['exclude_barcode'] ?? [],
            'marketplaceNomenclatureCode' => (int) $this->input('marketplace_nomenclature_code', 0),
        ]);
    }
}
