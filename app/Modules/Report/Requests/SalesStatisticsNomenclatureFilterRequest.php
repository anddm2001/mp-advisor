<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на фильтрацию номенклатур при получении статистики заказов и продаж
 */
class SalesStatisticsNomenclatureFilterRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'vendor_code' => [
                'nullable',
                'string',
                'max:100',
            ],
            'marketplace_nomenclature_code' => [
                'nullable',
                'integer',
            ],
            'brand' => [
                'nullable',
                'string',
                'max:100',
            ],
            'name' => [
                'nullable',
                'string',
                'max:100',
            ],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'vendor_code' => 'Артикул поставщика',
            'marketplace_nomenclature_code' => 'ID Wildberries',
            'brand' => 'Бренд',
            'name' => 'Предмет',
        ];
    }

    /**
     * Получить артикул поставщика
     *
     * @return string
     */
    public function getVendorCode(): string
    {
        return (string) $this->input('vendor_code', '');
    }

    /**
     * Получить артикул маркетплейса
     *
     * @return int
     */
    public function getMarketplaceNomenclatureCode(): int
    {
        return (int) $this->input('marketplace_nomenclature_code', 0);
    }

    /**
     * Получить название бренда
     *
     * @return string
     */
    public function getBrand(): string
    {
        return (string) $this->input('brand', '');
    }

    /**
     * Получить название категории (предмета)
     *
     * @return string
     */
    public function getName(): string
    {
        return (string) $this->input('name', '');
    }
}
