<?php

namespace App\Modules\Report\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\CommonDataHelper;
use App\Modules\Common\Requests\FormRequest;
use Report\GetStockInfoRequest;

/**
 * Запрос на получение данных номенклатур по складам
 */
class StockInfoRequest extends FormRequest
{
    /**
     * Доступ только для авторизованных пользователей, привязанных к мерчантам
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user() instanceof User;
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'warehouse' => ['nullable', 'string'],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'warehouse' => 'Склад',
        ];
    }

    /**
     * Получить запрос для микросервиса
     * 
     * @param int $merchantId
     * @return GetStockInfoRequest
     */
    public function getAPIRequest(int $merchantId): GetStockInfoRequest
    {
        $warehouse = CommonDataHelper::prepareNameWithAllOption((string) $this->input('warehouse', ''));
        return new GetStockInfoRequest([
            'merchantID' => $merchantId,
            'warehouseName' => $warehouse,
        ]);
    }
}
