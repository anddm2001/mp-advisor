<?php


namespace App\Modules\Report\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Requests\FormRequest;
use Report\UpdateRecommendationsNomenclatureRequest;

/**
 * Запрос на обновление номенклатуры в рекомендации к поставке
 *
 * @package App\Modules\Report\Requests
 */
class RecommendationsNomenclatureReportUpdateRequest extends FormRequest
{
    /**
     * Только для авторизованных пользователей
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() instanceof User;
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor_code' => [
                'required',
                'string',
            ],
            'size' => [
                'required',
                'string',
            ],
            'need_count' => [
                'required',
                'integer',
                'min:0',
            ],
            'apply' => [
                'boolean',
            ],
        ];
    }

    /**
     * Получить запрос для отправки в API
     *
     * @param int $merchantID
     * @param string $reportID
     *
     * @return UpdateRecommendationsNomenclatureRequest
     */
    public function getAPIRequest(int $merchantID, string $reportID): UpdateRecommendationsNomenclatureRequest
    {
        return new UpdateRecommendationsNomenclatureRequest([
            'merchantID' => $merchantID,
            'reportID' => $reportID,
            'vendorCode' => $this->input('vendor_code'),
            'size' => $this->input('size'),
            'needCount' => (int) $this->input('need_count'),
            'apply' => $this->input('apply') == 1,
        ]);
    }
}
