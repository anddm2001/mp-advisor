<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;
use Google\Protobuf\Timestamp;
use Report\SalesReportItemRequest;

/**
 * Запрос с периодом для отчёта о продажах
 *
 * @package App\Modules\Report\Requests
 */
class SalesPeriodRequest extends FormRequest
{
    /** Формат дат */
    const DATE_FORMAT = 'd.m.Y';

    /** Строковое представление временной зоны */
    const TZ = 'UTC';

    /** @var DateTimeZone Временная зона */
    protected DateTimeZone $tz;

    /** @var DateTime Сегодня */
    protected DateTime $today;

    /**
     * Определение временной зоны и "сегодня"
     *
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null $content
     * @throws Exception
     */
    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->tz = new DateTimeZone(self::TZ);
        $this->today = (new DateTime('now', $this->tz))->setTime(0, 0);
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'date_from' => [
                'required',
                'date_format:' . self::DATE_FORMAT,
            ],
            'date_to' => [
                'required',
                'date_format:' . self::DATE_FORMAT,
                'after_or_equal:date_from',
            ],
            'compare_date_from' => [
                'nullable',
                'date_format:' . self::DATE_FORMAT,
            ],
            'compare_date_to' => [
                'nullable',
                'date_format:' . self::DATE_FORMAT,
                'after_or_equal:compare_date_from',
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages()
    {
        return [
            'date_from.required' => 'Необходимо указать дату начала периода.',
            'date_to.required' => 'Необходимо указать дату окончания периода.',
            'date_from.date_format' => 'Неверный формат даты начала периода.',
            'date_to.date_format' => 'Неверный формат даты окончания периода.',
            'date_to.after_or_equal' => 'Дата окончания периода не может быть раньше даты начала.',
            'compare_date_from.date_format' => 'Неверный формат даты начала периода для сравнения.',
            'compare_date_to.date_format' => 'Неверный формат даты окончания периода для сравнения.',
            'compare_date_to.after_or_equal' => 'Дата окончания периода для сравнения не может быть раньше даты начала.',
        ];
    }

    /**
     * Судя по поведению микросервиса, даты здесь надо создавать именно в UTC зоне
     * Если создавать в MSK, тогда 2020-05-15 00:00:00 MSK в микросервисе определяется по метке как 2020-05-14 21:00:00 UTC
     * При этом в микросервисе используется только дата, без времени, то есть получится 2020-05-14 вместо ожидаемой 2020-05-15
     */

    /**
     * Получает дату начала периода
     *
     * @return DateTime
     */
    public function getDateFrom(): DateTime
    {
        $dateInterval7Days = new DateInterval('P7D');
        return DateTime::createFromFormat(
            '!' . self::DATE_FORMAT,
            $this->input('date_from', $this->today->sub($dateInterval7Days)->format(self::DATE_FORMAT)),
            $this->tz
        );
    }

    /**
     * Получает дату окончания периода
     *
     * @return DateTime
     */
    public function getDateTo(): DateTime
    {
        return DateTime::createFromFormat(
            '!' . self::DATE_FORMAT,
            $this->input('date_to', $this->today->format(self::DATE_FORMAT)),
            $this->tz
        );
    }

    /**
     * Получить дату начала периода сравнения
     *
     * @return DateTime
     */
    public function getCompareDateFrom(): DateTime
    {
        $compareDate = $this->input('compare_date_from')
            ? DateTime::createFromFormat('!' . self::DATE_FORMAT, $this->input('compare_date_from'), $this->tz)
            : null;

        // если не задан период для сравнения - взять такой же период для отчёта
        if (!$compareDate) {
            $dateFrom = $this->getDateFrom();
            $dateTo = $this->getDateTo();

            $compareDate = (clone $dateTo)
                ->sub($dateFrom->diff($dateTo));
        }

        return $compareDate;

    }

    /**
     * Получить дату окончания периода сравнения
     *
     * @return DateTime
     */
    public function getCompareDateTo(): DateTime
    {
        // если задан кастомный период для сравнения - брать его
        // иначе брать левую границу дат отчёта
        return $this->input('compare_date_to')
            ? DateTime::createFromFormat('!' . self::DATE_FORMAT, $this->input('compare_date_to'), $this->tz)
            : $this->getDateFrom();
    }

    /**
     * Получить запрос для Wildberries API
     *
     * @param int $merchantId
     * @return SalesReportItemRequest
     */
    public function getAPIRequest(int $merchantId): SalesReportItemRequest
    {
        return new SalesReportItemRequest([
            'dateFrom' => new Timestamp(['seconds' => $this->getDateFrom()->getTimestamp()]),
            'dateTo' => new Timestamp(['seconds' => $this->getDateTo()->getTimestamp()]),
            'compareDateFrom' => new Timestamp(['seconds' => $this->getCompareDateFrom()->getTimestamp()]),
            'compareDateTo' => new Timestamp(['seconds' => $this->getCompareDateTo()->getTimestamp()]),
            'merchantID' => $merchantId,
        ]);
    }

    /**
     * Создать запрос с периодом, соответствующим периоду сравнения
     *
     * @return SalesPeriodRequest
     * @throws Exception
     */
    public function createForCompareDates(): SalesPeriodRequest
    {
        return self::createFromArray([
            'date_from' => $this->getCompareDateFrom()->format(self::DATE_FORMAT),
            'date_to' => $this->getCompareDateTo()->format(self::DATE_FORMAT),
        ]);
    }
}
