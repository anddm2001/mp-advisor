<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use Illuminate\Validation\Rule;
use Report\StockReportSort;

/**
 * Запрос на сортировку отчётов о складах
 */
class StockReportSortRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'sort_condition' => [
                'nullable',
                'string',
                Rule::in([
                    StockReportSort\SortCondition::name(StockReportSort\SortCondition::QUANTITY),
                    StockReportSort\SortCondition::name(StockReportSort\SortCondition::QUANTITY_FULL),
                    StockReportSort\SortCondition::name(StockReportSort\SortCondition::IN_WAY),
                    StockReportSort\SortCondition::name(StockReportSort\SortCondition::LOST),
                    StockReportSort\SortCondition::name(StockReportSort\SortCondition::NOT_IN_ORDER),
                    StockReportSort\SortCondition::name(StockReportSort\SortCondition::IN_MY),
                ]),
            ],
            'sort_direction' => [
                'nullable',
                'string',
                Rule::in([
                    StockReportSort\SortDirection::name(StockReportSort\SortDirection::ASC),
                    StockReportSort\SortDirection::name(StockReportSort\SortDirection::DESC),
                ]),
            ],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'sort_condition' => strtoupper($this->input('sort_condition', '')),
            'sort_direction' => strtoupper($this->input('sort_direction', '')),
        ]);
    }

    /**
     * Получить запрос для Wildberries API
     *
     * @return StockReportSort
     */
    public function getAPIRequest(): StockReportSort
    {
        $defaultCondition = StockReportSort\SortCondition::QUANTITY;
        $defaultDirection = StockReportSort\SortDirection::DESC;

        return new StockReportSort([
            'condition' => $this->input('sort_condition')
                ? StockReportSort\SortCondition::value($this->input('sort_condition'))
                : $defaultCondition,
            'direction' => $this->input('sort_direction')
                ? StockReportSort\SortDirection::value($this->input('sort_direction'))
                : $defaultDirection,
        ]);
    }
}
