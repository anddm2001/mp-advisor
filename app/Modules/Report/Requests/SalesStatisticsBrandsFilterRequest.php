<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на фильтрацию данных по бренду при получении статистики заказов и продаж
 */
class SalesStatisticsBrandsFilterRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'brand' => [
                'nullable',
                'string',
                'max:100',
            ],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'brand' => 'Бренд',
        ];
    }

    /**
     * Получить название бренда
     *
     * @return string
     */
    public function getBrand(): string
    {
        return (string) $this->input('brand', '');
    }
}
