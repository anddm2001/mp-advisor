<?php


namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use Illuminate\Validation\Rule;
use Report\GetNomenclatureRequest\Sort;
use Report\GetNomenclatureRequest\SortCondition;
use Report\GetNomenclatureRequest\SortDirection;

/**
 * Запрос на сортировку номенклатур
 *
 * @package App\Modules\Report\Requests
 */
class NomenclatureSortRequest extends FormRequest
{
    /**
     * Условие сортировки - выручка
     */
    const CONDITION_SALE_COST = 'sale_cost';

    /**
     * Условие сортировки - скидка
     */
    const CONDITION_DISCOUNT = 'discount';

    /**
     * Условие сортировки - себестоимость
     */
    const CONDITION_BASE_COST = 'base_cost';

    /**
     * Условие сортировки - удалённые номенклатуры
     */
    const CONDITION_REMOVED = 'removed';

    /**
     * Направление сортировки - от большего
     */
    const DIRECTION_DESC = 'desc';

    /**
     * Направление сортировки - от меньшего
     */
    const DIRECTION_ASC = 'asc';

    protected const DEFAULT_CONDITION = self::CONDITION_SALE_COST;

    protected const DEFAULT_DIRECTION = self::DIRECTION_DESC;

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'condition' => [
                'nullable',
                Rule::in([
                    self::CONDITION_SALE_COST,
                    self::CONDITION_DISCOUNT,
                    self::CONDITION_BASE_COST,
                    self::CONDITION_REMOVED,
                ]),
            ],
            'direction' => [
                'nullable',
                Rule::in([
                    self::DIRECTION_DESC,
                    self::DIRECTION_ASC,
                ]),
            ],
        ];
    }

    /**
     * Получить запрос для Wildberries API
     *
     * @return Sort
     */
    public function getAPIRequest(): Sort
    {
        $directionMapping = [
            self::DIRECTION_DESC => SortDirection::DESC,
            self::DIRECTION_ASC => SortDirection::ASC,
        ];

        $conditionMapping = [
            self::CONDITION_SALE_COST => SortCondition::SALE_DISCOUNT_COST,
            self::CONDITION_DISCOUNT => SortCondition::DISCOUNT,
            self::CONDITION_BASE_COST => SortCondition::BASE_COST,
            self::CONDITION_REMOVED => SortCondition::REMOVED,
        ];

        $reqDirection = $this->input('direction', self::DEFAULT_DIRECTION);
        $reqCondition = $this->input('condition', self::DEFAULT_CONDITION);

        return new Sort([
            'condition' => $conditionMapping[$reqCondition] ?? 0,
            'direction' => $directionMapping[$reqDirection] ?? 0,
        ]);
    }
}
