<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на фильтрацию данных по категории при получении статистики заказов и продаж
 */
class SalesStatisticsCategoriesFilterRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category' => [
                'nullable',
                'string',
                'max:100',
            ],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'category' => 'Категория',
        ];
    }

    /**
     * Получить название категории (предмета)
     *
     * @return string
     */
    public function getCategory(): string
    {
        return (string) $this->input('category', '');
    }
}
