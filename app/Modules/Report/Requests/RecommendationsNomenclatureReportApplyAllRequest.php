<?php

namespace App\Modules\Report\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Requests\FormRequest;
use Report\UpdateRecommendationsNomenclatureRequest;

/**
 * Запрос на добавление или удаление всех номенклатур с рекомендациями
 */
class RecommendationsNomenclatureReportApplyAllRequest extends FormRequest
{
    /**
     * Только для авторизованных пользователей
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user() instanceof User;
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'apply' => ['required', 'boolean'],
        ];
    }

    /**
     * Получить запрос для отправки в API
     *
     * @param int $merchantID
     * @param string $reportID
     *
     * @return UpdateRecommendationsNomenclatureRequest
     */
    public function getAPIRequest(int $merchantID, string $reportID): UpdateRecommendationsNomenclatureRequest
    {
        return new UpdateRecommendationsNomenclatureRequest([
            'merchantID' => $merchantID,
            'reportID' => $reportID,
            'apply' => (bool) $this->input('apply'),
        ]);
    }
}
