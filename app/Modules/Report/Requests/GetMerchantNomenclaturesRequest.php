<?php

namespace App\Modules\Report\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на получение номенклатур мерчанта
 *
 * @package App\Modules\Report\Requests
 */
class GetMerchantNomenclaturesRequest extends FormRequest
{
    /**
     * Доступ только для авторизованных пользователей, привязанных к мерчантам
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() instanceof User;
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor_code' => [
                'nullable',
                'string',
                'max:100',
            ],
            'marketplace_nomenclature_code' => [
                'nullable',
                'integer',
            ],
            'brand' => [
                'nullable',
                'string',
                'max:100',
            ],
            'name' => [
                'nullable',
                'string',
                'max:100',
            ],
        ];
    }

    /**
     * Подписи атрибутов
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'vendor_code' => 'Артикул поставщика',
            'marketplace_nomenclature_code' => 'ID Wildberries',
            'brand' => 'Бренд',
            'name' => 'Предмет',
        ];
    }
}
