<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use Illuminate\Support\Collection;
use Report\NomenclatureFilter;

/**
 * Запрос на фильтрацию номенклатур
 *
 * @package App\Modules\Report\Requests
 */
class NomenclatureRemovedFilterRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hide_removed' => [
                'boolean',
            ],
            'only_removed' => [
                'boolean',
            ],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'hide_removed' => (bool) $this->input('hide_removed'),
            'only_removed' => (bool) $this->input('only_removed'),
        ]);
    }

    /**
     * Подписи атрибутов
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'hide_removed' => 'Поставляемое',
            'only_removed' => 'Архив',
        ];
    }

    /**
     * Получить значение флага "Только поставляемое"
     *
     * @return bool
     */
    public function getHideRemoved(): bool
    {
        return (bool) $this->input('hide_removed');
    }

    /**
     * Получить значение флага "Только архив"
     *
     * @return bool
     */
    public function getOnlyRemoved(): bool
    {
        return (bool) $this->input('only_removed');
    }
}
