<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use Report\GetBrandSalesRequest;

/**
 * Запрос на фильтрацию данных по бренду
 *
 * @package App\Modules\Report\Requests
 */
class BrandsFilterRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => [
                'nullable',
                'string',
                'max:100',
            ],
            'brands.*' => [
                'nullable',
                'string',
                'max:100',
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'brand' => 'Бренд',
        ];
    }

    /**
     * Получить запрос для микросервиса
     *
     * @return GetBrandSalesRequest\Filter
     */
    public function getAPIRequest(): GetBrandSalesRequest\Filter
    {
        $brand = array_merge(
            [$this->input('brand')],
            $this->input('brands', []),
        );

        $brand = array_filter($brand, fn($b) => is_string($b) && !empty($b));

        return new GetBrandSalesRequest\Filter([
            'brand' => $brand
        ]);
    }
}
