<?php


namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use Illuminate\Validation\Rule;
use Report\GroupStatisticSalesFormat;

/**
 * Запрос с указанием формата агрегации статистики заказов и продаж
 */
class SalesStatisticsRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'group_format' => [
                'required',
                'string',
                Rule::in([
                    GroupStatisticSalesFormat::name(GroupStatisticSalesFormat::DAY),
                    GroupStatisticSalesFormat::name(GroupStatisticSalesFormat::WEEK),
                    GroupStatisticSalesFormat::name(GroupStatisticSalesFormat::MONTH),
                ]),
            ],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'group_format' => strtoupper($this->input('group_format', '')),
        ]);
    }

    /**
     * Получить значение формата агрегации данных статистики
     *
     * @return int
     */
    public function getGroupStatisticSalesFormatValue(): int
    {
        return GroupStatisticSalesFormat::value($this->input('group_format', ''));
    }
}
