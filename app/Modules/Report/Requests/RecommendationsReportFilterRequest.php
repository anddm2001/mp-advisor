<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use Report\GetRecommendationsReportRequest\Filter;

/**
 * Запрос на фильтрацию отчёта рекомендаций к поставке
 *
 * @package App\Modules\Report\Requests
 */
class RecommendationsReportFilterRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'only_apply' => [
                'boolean',
            ],
        ];
    }

    /**
     * Подписи атрибутов
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'only_apply' => 'Только из поставки',
        ];
    }

    /**
     * Получить запрос для Wildberries API
     *
     * @return Filter
     */
    public function getAPIRequest(): Filter
    {
        return new Filter([
            'onlyApply' => $this->input('only_apply') == 1,
        ]);
    }
}
