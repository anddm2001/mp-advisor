<?php

namespace App\Modules\Report\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Requests\FormRequest;
use Illuminate\Validation\Rule;
use Report\GetRecommendationsReportRequest;

/**
 * Сортировка товаров в поставке
 *
 * @package App\Modules\Report\Requests
 */
class RecommendationsReportSortRequest extends FormRequest
{
    /**
     * Условие сортировки - по наличию
     */
    protected const COND_STOCK = 'stock';

    /**
     * Условие сортировки - по товарам в пути
     */
    protected const COND_IN_WAY = 'in_way';

    /**
     * Условие сортировки - по скорости заказов
     */
    protected const COND_ORDER_SPEED = 'order_speed';

    /**
     * Условие сортировки - по общему заказанному количеству
     */
    public const COND_ORDERED_COUNT = 'ordered_count';

    /**
     * Условие сортировки - по обеспечению запаса
     */
    protected const COND_NEED_STOCK = 'need_stock';

    /**
     * Условие сортировки - по количеству в поставке
     */
    protected const COND_NEED_COUNT = 'need_count';

    /**
     * Направление сортировки - по возрастанию
     */
    protected const DIR_ASC = 'asc';

    /**
     * Направление сортировки - по убыванию
     */
    protected const DIR_DESC = 'desc';

    /**
     * Только для авторизованных пользователей
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() instanceof User;
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sort_condition' => [
                Rule::in(array_keys($this->getConditions())),
            ],
            'sort_direction' => [
                Rule::in(array_keys($this->getDirections())),
            ],
        ];
    }

    /**
     * Способы сортировки
     *
     * @return array
     */
    public function getConditions(): array
    {
        return [
            self::COND_NEED_COUNT => 'По количеству в поставке',
            self::COND_STOCK => 'По наличию',
            self::COND_IN_WAY => 'По товарам в пути',
            self::COND_ORDER_SPEED => 'По скорости заказов',
            self::COND_ORDERED_COUNT => 'По общему заказанному количеству',
            self::COND_NEED_STOCK => 'По обеспечению запаса',
        ];
    }

    /**
     * Направления сортировки
     *
     * @return array
     */
    public function getDirections(): array
    {
        return [
            self::DIR_DESC => 'По убыванию',
            self::DIR_ASC => 'По возрастанию',
        ];
    }

    /**
     * Получить условие сортировки для API
     *
     * @return int
     */
    public function getAPICondition(): int
    {
        $mapping = [
            self::COND_STOCK => GetRecommendationsReportRequest\SortCondition::STOCK,
            self::COND_IN_WAY => GetRecommendationsReportRequest\SortCondition::IN_WAY,
            self::COND_ORDER_SPEED => GetRecommendationsReportRequest\SortCondition::ORDER_SPEED,
            self::COND_ORDERED_COUNT => GetRecommendationsReportRequest\SortCondition::ORDERED_COUNT,
            self::COND_NEED_STOCK => GetRecommendationsReportRequest\SortCondition::NEED_STOCK,
            self::COND_NEED_COUNT => GetRecommendationsReportRequest\SortCondition::NEED_COUNT,
        ];

        $value = $this->input('sort_condition');
        $defaultValue = GetRecommendationsReportRequest\SortCondition::NEED_COUNT;

        return !empty($value) ? $mapping[$value] ?? $defaultValue : $defaultValue;
    }

    /**
     * Направление сортировки для API
     *
     * @return int
     */
    public function getAPIDirection(): int
    {
        $mapping = [
            self::DIR_ASC => GetRecommendationsReportRequest\SortDirection::ASC,
            self::DIR_DESC => GetRecommendationsReportRequest\SortDirection::DESC,
        ];

        $value = $this->input('sort_direction');
        $defaultValue = GetRecommendationsReportRequest\SortDirection::DESC;

        return !empty($value) ? $mapping[$value] ?? $defaultValue : $defaultValue;
    }
}
