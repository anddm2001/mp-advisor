<?php


namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use Illuminate\Validation\Rule;
use Report\SalesReportSort;

/**
 * Запрос на сортировку отчётов о продажах
 *
 * @package App\Modules\Report\Requests
 */
class SalesSortRequest extends FormRequest
{
    /**
     * Условие сортировки - выручка
     */
    const CONDITION_RECEIPT = 'receipt';

    /**
     * Условие сортировки - прибыль
     */
    const CONDITION_PROFIT = 'profit';

    /**
     * Условие сортировки - маржинальность
     */
    const CONDITION_MARGINALITY = 'marginality';

    /**
     * Условие сортировки - заказы
     */
    const CONDITION_ORDERS = 'orders';

    /**
     * Условие сортировки - продажи
     */
    const CONDITION_SALES = 'sales';

    /**
     * Условие сортировки - возвраты
     */
    const CONDITION_RETURNS = 'returns';

    /**
     * Условие сортировки - выкуп
     */
    const CONDITION_BUYOUT = 'buyout';

    /**
     * Условие сортировки - скорость заказов
     */
    const CONDITION_SPEED = 'speed';

    /**
     * Условие сортировки - скидка
     */
    const CONDITION_DISCOUNT = 'discount';

    /**
     * Направление сортировки - от большего
     */
    const DIRECTION_DESC = 'desc';

    /**
     * Направление сортировки - от меньшего
     */
    const DIRECTION_ASC = 'asc';

    protected const DEFAULT_CONDITION = self::CONDITION_RECEIPT;

    protected const DEFAULT_DIRECTION = self::DIRECTION_DESC;

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'condition' => [
                'nullable',
                Rule::in([
                    self::CONDITION_SALES,
                    self::CONDITION_ORDERS,
                    self::CONDITION_MARGINALITY,
                    self::CONDITION_PROFIT,
                    self::CONDITION_RECEIPT,
                    self::CONDITION_BUYOUT,
                    self::CONDITION_RETURNS,
                    self::CONDITION_SPEED,
                    self::CONDITION_DISCOUNT,
                ]),
            ],
            'direction' => [
                'nullable',
                Rule::in([
                    self::DIRECTION_DESC,
                    self::DIRECTION_ASC,
                ]),
            ],
        ];
    }

    /**
     * Получить запрос для Wildberries API
     *
     * @return SalesReportSort
     */
    public function getAPIRequest(): SalesReportSort
    {
        $directionMapping = [
            self::DIRECTION_DESC => SalesReportSort\SortDirection::DESC,
            self::DIRECTION_ASC => SalesReportSort\SortDirection::ASC,
        ];

        $conditionMapping = [
            self::CONDITION_RECEIPT => SalesReportSort\SortCondition::RECEIPT,
            self::CONDITION_PROFIT => SalesReportSort\SortCondition::PROFIT,
            self::CONDITION_MARGINALITY => SalesReportSort\SortCondition::MARGINALITY,
            self::CONDITION_ORDERS => SalesReportSort\SortCondition::ORDERS,
            self::CONDITION_SALES => SalesReportSort\SortCondition::SALES,
            self::CONDITION_DISCOUNT => SalesReportSort\SortCondition::DISCOUNT,
            self::CONDITION_BUYOUT => SalesReportSort\SortCondition::BUYOUT,
            self::CONDITION_RETURNS => SalesReportSort\SortCondition::RETURNS,
            self::CONDITION_SPEED => SalesReportSort\SortCondition::SPEED,
        ];

        $reqDirection = $this->input('direction', self::DEFAULT_DIRECTION);
        $reqCondition = $this->input('condition', self::DEFAULT_CONDITION);

        return new SalesReportSort([
            'condition' => $conditionMapping[$reqCondition] ?? 0,
            'direction' => $directionMapping[$reqDirection] ?? 0,
        ]);
    }
}
