<?php

namespace App\Modules\Report\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на редактирование даты поставки
 *
 * @package App\Modules\Report\Requests
 */
class UpdateApiIncomeRequest extends FormRequest
{
    protected const DATE_FORMAT = 'd.m.Y';

    /**
     * Доступ к запросу только для авторизованных пользователей
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() instanceof User;
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_supply' => [
                'required',
                'date_format:' . self::DATE_FORMAT,
            ],
        ];
    }

    /**
     * Подписи атрибутов
     *
     * @return string[]
     */
    public function attributes()
    {
        return [
            'date_supply' => 'Дата фактической отправки',
        ];
    }

    /**
     * Получить дату поставки в Wildberries
     *
     * @return \DateTime
     */
    public function getDateSupply(): \DateTime
    {
        $dateTime = \DateTime::createFromFormat(
            '!' . self::DATE_FORMAT,
            $this->input('date_supply'),
            new \DateTimeZone('UTC')
        );
        return $dateTime ?: new \DateTime();
    }
}
