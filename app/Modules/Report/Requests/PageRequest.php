<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос для постраничной навигации
 *
 * @package App\Modules\Report\Requests
 */
class PageRequest extends FormRequest
{
    protected const DEFAULT_LIMIT = 100;

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => [
                'integer',
                'min:0',
            ],
        ];
    }

    /**
     * Получить запрос для Wildberries API
     *
     * @param int|null $limit
     *
     * @return \Report\PageRequest
     */
    public function getAPIRequest(int $limit = null): \Report\PageRequest
    {
        if (is_null($limit)) {
            $limit = self::DEFAULT_LIMIT;
        }
        $page = (int) $this->input('page');

        return new \Report\PageRequest([
            'limit' => $limit,
            'offset' => $limit * $page,
        ]);
    }

    /**
     * Возвращает массив, в котором:
     *
     * - 1й-элемент - абсолютный путь к следующей странице (если существует);
     * - 2й-элемент - абсолютный путь к предыдущей странице (если существует);
     *
     * @param bool $hasNextPage
     * @param bool $hasPreviousPage
     *
     * @return array
     */
    public function getNextPreviousPageURL(bool $hasNextPage, bool $hasPreviousPage): array
    {
        $currentPage = $this->input('page', 0);
        $nextPageUrl = $previousPageUrl = null;

        $requestBuild = function(array $query) {
            $currentQuery = $this->query();
            return $this->url() . '?' . http_build_query(array_merge($currentQuery, $query));
        };

        if ($hasNextPage) {
            $nextPageUrl = $requestBuild(['page' => $currentPage + 1]);
        }

        if ($hasPreviousPage) {
            $previousPageUrl = $requestBuild(['page' => $currentPage - 1]);
        }

        return [$nextPageUrl, $previousPageUrl];
    }
}
