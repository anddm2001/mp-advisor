<?php


namespace App\Modules\Report\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на создание поставки в API
 *
 * @package App\Modules\Report\Requests
 */
class CreateApiIncomeRequest extends FormRequest
{
    protected const DATE_FORMAT = 'd.m.Y';

    /**
     * Доступ к запросу только для авторизованных пользователей
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() instanceof User;
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'income_id' => [
                'required',
                'integer',
                'min:1',
            ],
            'date_supply' => [
                'required',
                'date_format:' . self::DATE_FORMAT,
            ],
            'files' => [
                'required',
            ],
            'files.*' => [
                'required',
                'file',
                'mimes:xlsx',
            ],
        ];
    }

    /**
     * Подписи атрибутов
     *
     * @return string[]
     */
    public function attributes()
    {
        return [
            'income_id' => 'Номер поставки',
            'date_supply' => 'Дата фактической отправки',
            'files' => 'Файл(ы) заказа',
        ];
    }

    /**
     * Получить дату поставки в Wildberries
     *
     * @return \DateTime
     */
    public function getDateSupply(): \DateTime
    {
        $dateTime = \DateTime::createFromFormat(
            '!' . self::DATE_FORMAT,
            $this->input('date_supply'),
            new \DateTimeZone('UTC')
        );
        return $dateTime ?: new \DateTime();
    }
}
