<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use Report\GetCategorySalesRequest;

/**
 * Фильтр по категориям
 *
 * @package App\Modules\Report\Requests
 */
class CategoriesFilterRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => [
                'nullable',
                'string',
                'max:100',
            ],
            'categories.*' => [
                'nullable',
                'string',
                'max:100',
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'category' => 'Категория',
        ];
    }

    /**
     * Запрос в микросервис
     *
     * @return GetCategorySalesRequest\Filter
     */
    public function getAPIRequest(): GetCategorySalesRequest\Filter
    {
        $category = array_merge(
            [$this->input('category')],
            $this->input('categories', []),
        );

        $category = array_filter($category, fn($b) => is_string($b) && !empty($b));

        return new GetCategorySalesRequest\Filter([
            'category' => $category,
        ]);
    }
}
