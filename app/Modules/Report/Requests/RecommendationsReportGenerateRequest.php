<?php

namespace App\Modules\Report\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\CommonDataHelper;
use App\Modules\Common\Requests\FormRequest;
use App\Modules\Merchant\Models\Merchant;
use Google\Protobuf\Timestamp;
use Report\GenerateRecommendationsReportRequest;

/**
 * Запрос на генерацию отчёта рекомендаций
 *
 * @package App\Modules\Report\Requests
 */
class RecommendationsReportGenerateRequest extends FormRequest
{
    /**
     * Формат дат
     */
    public const DATE_FORMAT = 'd.m.Y';

    /**
     * Доступ только для авторизованных пользователей, привязанных к мерчантам
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() instanceof User;
    }

    /**
     * Получить мерчанта из роутинга
     *
     * @return Merchant|null
     */
    protected function getMerchant(): ?Merchant
    {
        /** @var Merchant|null $merchant */
        $merchant = $this->route('merchant');

        return $merchant;
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'warehouse' => [
                // склад
                'nullable',
                'string',
            ],
            'delivery_date' => [
                // дата поставки
                'required',
                'date_format:' . self::DATE_FORMAT,
            ],
            'calculate_from' => [
                // калькулировать отчёт "от"
                'required',
                'date_format:' . self::DATE_FORMAT,
            ],
            'calculate_to' => [
                // калькулировать отчёт "по"
                'required',
                'date_format:' . self::DATE_FORMAT,
            ],
            'report_days' => [
                // количество дней для расчёта зпаса
                'required',
                'integer',
                'min:1',
            ],
        ];
    }

    /**
     * Подписи атрибутов
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'warehouse' => 'Склад',
            'delivery_date' => 'Дата поставки',
            'calculate_from' => 'Даты для калькуляции скорости заказов',
            'calculate_to' => 'Даты для калькуляции скорости заказов',
            'report_days' => 'Количество дней для расчёта запаса',
        ];
    }

    /**
     * Получить дату поставки
     *
     * @return \DateTime
     */
    public function getDeliveryDateTime(): \DateTime
    {
        return \DateTime::createFromFormat(self::DATE_FORMAT, $this->input('delivery_date'));
    }

    /**
     * Получить левую границу даты для калькуляции
     *
     * @return \DateTime
     */
    public function getCalculateFromDateTime(): \DateTime
    {
        return \DateTime::createFromFormat(self::DATE_FORMAT, $this->input('calculate_from'));
    }

    /**
     * Получить правую границу даты для калькуляции
     *
     * @return \DateTime
     */
    public function getCalculateToDateTime(): \DateTime
    {
        return \DateTime::createFromFormat(self::DATE_FORMAT, $this->input('calculate_to'));
    }

    /**
     * Создать запрос для backend API
     *
     * @param int $merchantID
     * @param string $reportID
     *
     * @return GenerateRecommendationsReportRequest
     */
    public function getAPIRequest(int $merchantID, string $reportID = ''): GenerateRecommendationsReportRequest
    {
        $warehouse = CommonDataHelper::prepareNameWithAllOption((string) $this->input('warehouse', ''));
        return new GenerateRecommendationsReportRequest([
            'merchantID' => $merchantID,
            'reportID' => $reportID,
            'warehouseName' => $warehouse,
            'calculateFrom' => new Timestamp([
                'seconds' => $this->getCalculateFromDateTime()->getTimestamp(),
            ]),
            'calculateTo' => new Timestamp([
                'seconds' => $this->getCalculateToDateTime()->getTimestamp(),
            ]),
            'deliveryDate' => new Timestamp([
                'seconds' => $this->getDeliveryDateTime()->getTimestamp(),
            ]),
            'reportDays' => (int) $this->input('report_days'),
        ]);
    }
}
