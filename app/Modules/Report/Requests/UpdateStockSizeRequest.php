<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Requests\FormRequest;
use Report\UpdateStockSizeRequest as UpdateStockSizeAPIRequest;

/**
 * Запрос на изменение данных размера номенклатуры в складах
 */
class UpdateStockSizeRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor_code' => [
                'required',
                'string',
            ],
            'size' => [
                'string',
            ],
            'quantity' => [
                'nullable',
                'integer',
                'min:0',
            ],
        ];
    }

    /**
     * Формирование запроса для API
     *
     * @param int $merchantId
     * @return UpdateStockSizeAPIRequest
     */
    public function getAPIRequest(int $merchantId): UpdateStockSizeAPIRequest
    {
        return new UpdateStockSizeAPIRequest([
            'merchantID' => $merchantId,
            'articleSupply' => $this->input('vendor_code'),
            'size' => $this->input('size', ''),
            'quantity' => $this->input('quantity'),
        ]);
    }
}
