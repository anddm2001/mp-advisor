<?php

namespace App\Modules\Report\Requests;

use App\Modules\Common\Helpers\CommonDataHelper;
use App\Modules\Common\Requests\FormRequest;

class StockFilterRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'warehouse' => ['nullable', 'string'],
            'available_to_order_from' => ['nullable', 'integer'],
            'available_to_order_to' => ['nullable', 'integer'],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'warehouse' => 'Склад',
            'available_to_order_from' => 'Доступно для заказов от',
            'available_to_order_to' => 'Доступно для заказов до',
        ];
    }

    /**
     * Получить название склада
     *
     * @return string
     */
    public function getWarehouse(): string
    {
        return CommonDataHelper::prepareNameWithAllOption((string) $this->input('warehouse', ''));
    }

    /**
     * Получить минимальное доступное для заказов количество
     *
     * @return int
     */
    public function getAvailableToOrderFrom(): int
    {
        return (int) $this->input('available_to_order_from', 0);
    }

    /**
     * Получить максимальное доступное для заказов количество
     *
     * @return int
     */
    public function getAvailableToOrderTo(): int
    {
        return (int) $this->input('available_to_order_to', 0);
    }
}
