<?php

namespace App\Modules\Report\Importer;

use App\Modules\Merchant\Models\Merchant;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\NomenclatureSortRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\UpdateNomenclatureSizeRequest;
use App\Modules\Report\Services\WildberriesReportApi;
use Box\Spout\Common\Entity\Cell;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Report\Nomenclature;
use RuntimeException;

/**
 * Импорт данных складов
 */
abstract class WildberriesImporter
{
    public const TEMPLATE_FILE_NAME = '';
    public const TEMPLATE_VALUE_TEXT = '';

    /**
     * Отдаёт на скачивание файл с шаблоном для импорта
     *
     * @return void
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public static function getTemplateFile(): void
    {
        $writer = WriterEntityFactory::createXLSXWriter()->openToBrowser(static::TEMPLATE_FILE_NAME);
        $bold = (new StyleBuilder())->setFontBold()->build();
        $italic = (new StyleBuilder())->setFontItalic()->setShouldWrapText()->build();

        $writer->addRow(WriterEntityFactory::createRowFromArray([
            'Артикул поставщика',
            'Размер',
            'Значение',
        ], $bold));
        $writer->addRow(WriterEntityFactory::createRowFromArray([
            'Укажите ваш артикул поставщика, добавленный в карточку товара на маркетплейсе',
            'Если у товара есть размер, и он не указан, значение будет добавлено для всех размеров под этим артикулом',
            static::TEMPLATE_VALUE_TEXT,
        ], $italic));

        $writer->close();
    }

    /**
     * Импортирует данные из заполненного шаблона
     *
     * @param Merchant $merchant
     * @param array $options
     * @return array
     * @throws Exception
     */
    abstract public static function import(Merchant $merchant, array $options): array;

    /**
     * Собирает импортируемые данные из файла
     *
     * @param UploadedFile $file
     * @return array
     * @throws IOException
     * @throws ReaderNotOpenedException
     */
    protected static function gatherImportData(UploadedFile $file): array
    {
        $reader = ReaderEntityFactory::createXLSXReader();
        $reader->open($file->getPathname());

        $importData = [];
        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
                /**
                 * @var Cell $vendorCodeCell
                 * @var Cell $sizeCell
                 * @var Cell $valueCell
                 */
                [$vendorCodeCell, $sizeCell, $valueCell] = $row->getCells();

                // если значение не числовое, пропускаем такую строку
                if (!is_numeric($valueCell->getValue())) {
                    continue;
                }

                $importData[] = [
                    'vendor_code' => trim($vendorCodeCell->getValue()),
                    'size' => trim($sizeCell->getValue()),
                    'value' => (float) $valueCell->getValue(),
                ];
            }
        }

        $reader->close();

        return $importData;
    }
}
