<?php

namespace App\Modules\Report\Importer;

use App\Modules\Merchant\Models\Merchant;
use App\Modules\Report\Requests\UpdateStockSizeRequest;
use App\Modules\Report\Services\WildberriesReportApi;
use Illuminate\Http\UploadedFile;
use RuntimeException;

/**
 * Импорт данных складов
 */
class WildberriesStocksImporter extends WildberriesImporter
{
    /** @var string Имя файла шаблона для импорта */
    public const TEMPLATE_FILE_NAME = 'stocks-import.xlsx';

    /** @var string Текст-заголовок столбца со значением в шаблоне */
    public const TEMPLATE_VALUE_TEXT = 'Указанное будет добавлено как остаток товара на вашем складе';

    /** @inheritDoc */
    public static function import(Merchant $merchant, array $options): array
    {
        /** @var UploadedFile $file */
        $file = $options['file'] ?? null;
        if (!($file instanceof UploadedFile)) {
            throw new RuntimeException('Wrong importer params.');
        }

        $importData = static::gatherImportData($file);
        $result = ['positions' => 0, 'updated_positions' => 0];
        /** @var WildberriesReportApi $api */
        $api = resolve(WildberriesReportApi::class);

        foreach ($importData as $importItem) {
            $updateSizeData = [
                'vendor_code' => $importItem['vendor_code'] ?? '',
                'size' => $importItem['size'] ?? '',
                'quantity' => (int) ($importItem['value'] ?? -1),
            ];

            if ($updateSizeData['vendor_code'] === '' || $updateSizeData['quantity'] < 0) {
                $result['positions']++;
                continue;
            }
            $updateSizeRequest = UpdateStockSizeRequest::createFromArray($updateSizeData);

            try {
                $updateResponse = $api->updateStockSize($merchant->id, $updateSizeRequest);
                $result['positions'] += $updateResponse->getTotalSizes();
                $result['updated_positions'] += $updateResponse->getUpdatedSizes();
            } catch (RuntimeException $exception) {
                $result['positions']++;
            }
        }

        return $result;
    }
}
