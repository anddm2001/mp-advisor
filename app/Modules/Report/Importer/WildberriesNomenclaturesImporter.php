<?php

namespace App\Modules\Report\Importer;

use App\Modules\Merchant\Models\Merchant;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\NomenclatureRemovedFilterRequest;
use App\Modules\Report\Requests\NomenclatureSortRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\UpdateNomenclatureSizeRequest;
use App\Modules\Report\Services\WildberriesReportApi;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Report\Nomenclature;
use Report\PageResponse;
use RuntimeException;

/**
 * Импорт данных номенклатур
 */
class WildberriesNomenclaturesImporter extends WildberriesImporter
{
    /** @var string Имя файла шаблона для импорта */
    public const TEMPLATE_FILE_NAME = 'nomenclatures-import.xlsx';

    /** @var string Текст-заголовок столбца со значением в шаблоне */
    public const TEMPLATE_VALUE_TEXT = 'Указанное будет добавлено как себестоимость или квант поставки. Нужное выберите при загрузке';

    /** @var string Тип импорта - себестоимость */
    public const TYPE_BASE_COSTS = 'base_costs';
    /** @var string Тип импорта - квант поставки */
    public const TYPE_SUPPLY_SETS = 'supply_sets';

    /** @inheritDoc */
    public static function import(Merchant $merchant, array $options): array
    {
        /** @var string $type */
        $type = $options['type'] ?? null;
        /** @var UploadedFile $file */
        $file = $options['file'] ?? null;

        if (!$type || !($file instanceof UploadedFile)) {
            throw new RuntimeException('Wrong importer params.');
        }

        $importData = static::gatherImportData($file);
        $result = ['positions' => count($importData), 'updated_positions' => 0];

        if (!empty($importData)) {
            $importCollection = collect($importData);

            /** @var Nomenclature[] $nomenclatures */
            $nomenclatures = [];

            /** @var WildberriesReportApi $api */
            $api = resolve(WildberriesReportApi::class);
            $filterRequest = NomenclatureFilterRequest::createFromArray([
                'vendor_code' => $importCollection->pluck('vendor_code')->unique()->toArray(),
            ]);

            $removedFilterRequest = NomenclatureRemovedFilterRequest::createFromArray([]);
            $sortRequest = NomenclatureSortRequest::createFromArray([]);
            $page = 0;
            do {
                $pageRequest = PageRequest::createFromArray(['page' => $page++]);
                $nomenclaturesResponse = $api->getMerchantNomenclatures(
                    $merchant->id,
                    $filterRequest,
                    $removedFilterRequest,
                    $sortRequest,
                    $pageRequest,
                    true
                );
                foreach ($nomenclaturesResponse->getItems() as $nomenclature) {
                    $nomenclatures[] = $nomenclature;
                }
                /** @var PageResponse|null $pageResponse */
                $pageResponse = $nomenclaturesResponse->getPage();
            } while ($pageResponse && $pageResponse->getHasNextPage());

            $groupedImportCollection = $importCollection->groupBy('vendor_code');
            foreach ($nomenclatures as $nomenclature) {
                $nomenclatureCommonData = $nomenclature->getCommonData();
                if (!$nomenclatureCommonData) {
                    continue;
                }
                $vendorCode = $nomenclatureCommonData->getVendorCode();

                /** @var Collection $nomenclatureImportData */
                $nomenclatureImportData = $groupedImportCollection->get($vendorCode, new Collection());
                if ($nomenclatureImportData->isEmpty()) {
                    continue;
                }

                $importSizeData = $nomenclatureImportData->pluck('value', 'size')->toArray();
                $updatingPositionsCount = count($importSizeData);
                foreach ($nomenclature->getItems() as $nomenclatureSize) {
                    /** @var Nomenclature\Size $nomenclatureSize */
                    $nomenclatureSizeData = $nomenclatureSize->getSize();
                    if (!$nomenclatureSizeData) {
                        continue;
                    }
                    $size = $nomenclatureSizeData->getSize();

                    // пустая строка = применить ко всем размерам номенклатуры
                    if (!array_key_exists($size, $importSizeData) && !array_key_exists('', $importSizeData)) {
                        --$updatingPositionsCount;
                        continue;
                    }

                    $value = $importSizeData[$size] ?? $importSizeData[''];
                    $currentSizeData = $updateSizeData = [
                        'nomenclature_id' => $nomenclature->getID(),
                        'size' => $size,
                        'supply_set' => $nomenclatureSize->getSupplySet(),
                        'base_cost' => $nomenclatureSize->getBaseCost(),
                        'removed' => $nomenclatureSize->getRemoved(),
                    ];

                    switch ($type) {
                        case self::TYPE_BASE_COSTS:
                            $updateSizeData['base_cost'] = (float) $value;
                            break;
                        case self::TYPE_SUPPLY_SETS:
                            $updateSizeData['supply_set'] = (int) $value;
                            break;
                    }

                    // нет изменений = не надо ничего обновлять в микросервисе
                    if (empty(array_diff_assoc($updateSizeData, $currentSizeData))) {
                        continue;
                    }

                    $updateSizeRequest = UpdateNomenclatureSizeRequest::createFromArray($updateSizeData);
                    try {
                        $api->updateNomenclatureSize($merchant->id, $updateSizeRequest);
                    } catch (RuntimeException $exception) {
                        --$updatingPositionsCount;
                    }
                }
                $result['updated_positions'] += $updatingPositionsCount;
            }
        }

        return $result;
    }
}
