<?php

namespace App\Modules\Report\Services;

use App\Modules\Report\Contracts\ReportResultFileServiceContract;
use App\Modules\Report\Events\AbstractSalesExportEvent;
use App\Modules\Report\Events\WildberriesBrandsSalesExportEvent;
use App\Modules\Report\Events\WildberriesCategoriesSalesExportEvent;
use App\Modules\Report\Events\WildberriesNomenclaturesExportEvent;
use App\Modules\Report\Events\WildberriesNomenclaturesSalesExportEvent;
use App\Modules\Report\Events\WildberriesRecommendationsExportEvent;
use App\Modules\Report\Events\WildberriesStocksExportEvent;
use App\Modules\Report\Models\ReportResultFile;
use App\Modules\Report\Requests\BrandsFilterRequest;
use App\Modules\Report\Requests\CategoriesFilterRequest;
use App\Modules\Report\Requests\CreateApiIncomeRequest;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\NomenclatureRemovedFilterRequest;
use App\Modules\Report\Requests\NomenclatureSortRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\RecommendationsNomenclatureReportApplyAllRequest;
use App\Modules\Report\Requests\RecommendationsNomenclatureReportUpdateRequest;
use App\Modules\Report\Requests\RecommendationsReportFilterRequest;
use App\Modules\Report\Requests\RecommendationsReportGenerateRequest;
use App\Modules\Report\Requests\RecommendationsReportSortRequest;
use App\Modules\Report\Requests\SalesPeriodRequest;
use App\Modules\Report\Requests\SalesSortRequest;
use App\Modules\Report\Requests\StockFilterRequest;
use App\Modules\Report\Requests\SalesStatisticsBrandsFilterRequest;
use App\Modules\Report\Requests\SalesStatisticsCategoriesFilterRequest;
use App\Modules\Report\Requests\SalesStatisticsNomenclatureFilterRequest;
use App\Modules\Report\Requests\SalesStatisticsRequest;
use App\Modules\Report\Requests\StockReportSortRequest;
use App\Modules\Report\Requests\UpdateApiIncomeRequest;
use App\Modules\Report\Requests\UpdateNomenclatureSizeRequest;
use App\Modules\Report\Requests\UpdateStockSizeRequest;
use Google\Protobuf\GPBEmpty;
use Report\CreateNomenclatureSettingsRequest;
use Report\DeleteNomenclatureSettingsRequest;
use Report\GetNomenclatureSummaryRequest;
use Report\GetNomenclatureSummaryResponse;
use Report\GetRecommendationsReportSummaryRequest;
use Report\GetRecommendationsReportSummaryResponse;
use Report\GetStatisticSalesBrandByRangeRequest;
use Report\GetStatisticSalesByRangeResponse;
use Report\GetStatisticSalesCategoriesByRangeRequest;
use Report\GetStatisticSalesNomenclatureByRangeRequest;
use Report\GetStockInfoRequest;
use Report\GetStockInfoResponse;
use Report\GetStocksReportRequest;
use Report\GetStocksReportResponse;
use Report\GetWarehousesRequest;
use Report\GetWarehousesResponse;
use Report\StockFilter;
use Report\ListNomenclatureSettingsRequest;
use Report\ListNomenclatureSettingsResponse;
use Report\NomenclatureSettings;
use Report\UpdateNomenclatureSettingsRequest;
use Report\UpdateNomenclatureSizeRequest as UpdateNomenclatureSizeAPIRequest;
use Google\Protobuf\Timestamp;
use Illuminate\Contracts\Events\Dispatcher;
use Report\CollectDataRequest;
use Report\CollectDataResponse;
use Report\CollectSalesRequest;
use Report\CollectSalesResponse;
use Report\CreateIncomeRequest;
use Report\CreateIncomeResponse;
use Report\GenerateRecommendationsReportResponse;
use Report\GetBrandNamesRequest;
use Report\GetBrandNamesResponse;
use Report\GetBrandSalesRequest;
use Report\GetBrandSalesResponse;
use Report\GetCategorySalesRequest;
use Report\GetCategorySalesResponse;
use Report\GetNomenclatureRequest;
use Report\GetNomenclatureResponse;
use Report\GetNomenclatureSalesRequest;
use Report\GetNomenclatureSalesResponse;
use Report\GetRecommendationsReportRequest;
use Report\GetRecommendationsReportResponse;
use Report\GetSubjectNamesRequest;
use Report\GetSubjectNamesResponse;
use Report\Income;
use Report\IncomeDetails;
use Report\IncomeDetailsRequest;
use Report\IncomesListRequest;
use Report\IncomesListResponse;
use Report\IncomeUpdateRequest;
use Report\IncomeUpdateResponse;
use Report\SalesReportItem;
use Report\SalesReportItemRequest;
use Report\UpdateRecommendationsNomenclatureRequest;
use Report\UpdateStockSizeRequest as UpdateStockSizeAPIRequest;
use Report\WildberriesReportClient;

/**
 * Сервис для подключения к API микросервиса отчётов
 *
 * @package App\Modules\Report\Services
 */
class WildberriesReportApi
{
    /**
     * @var WildberriesReportClient Клиент для подключения к микросервису
     */
    protected WildberriesReportClient $client;

    /**
     * @var Dispatcher Event-manager
     */
    protected Dispatcher $events;

    /**
     * @var ReportResultFileServiceContract Сервис для работы с файлами выгрузок
     */
    protected ReportResultFileServiceContract $resultService;

    /**
     * WildberriesReportApi constructor.
     *
     * @param WildberriesReportClient $client
     * @param ReportResultFileServiceContract $resultService
     * @param Dispatcher $events
     */
    public function __construct(
        WildberriesReportClient $client,
        ReportResultFileServiceContract $resultService,
        Dispatcher $events
    )
    {
        $this->client = $client;
        $this->resultService = $resultService;
        $this->events = $events;
    }

    /**
     * Выполнить запрос и проверить ответ на соответствие классу и вывести ошибку, если не соответствует
     *
     * В случае успеха возвращает ответ с соответствующим классом.
     *
     * @param \Closure $requestProcessor
     * @param string $expectedClass
     *
     * @return mixed
     */
    protected function processRequest(\Closure $requestProcessor, string $expectedClass)
    {
        list ($response, $status) = $requestProcessor();

        if (!$response instanceof $expectedClass) {
            throw new \RuntimeException("Не удалось получить ответ: " . print_r($status, true));
        }

        return $response;
    }

    /**
     * Отправить запрос на сбор данных из API
     *
     * @param int $merchantId
     * @param string $base64Key
     * @param \DateTime $dateFrom Дата, с которой собирать данные
     *
     * @return array
     */
    public function collectData(int $merchantId, string $base64Key, \DateTime $dateFrom): array
    {
        $request = new CollectDataRequest([
            'merchantID' => $merchantId,
            'base64Key' => $base64Key,
            'dateFrom' => new Timestamp([
                'seconds' => $dateFrom->getTimestamp(),
            ]),
        ]);

        /** @var CollectDataResponse $response */
        list ($response, $status) = $this->client->CollectData($request)->wait();

        if (!$response instanceof CollectDataResponse) {
            return [false, print_r($status, true)];
        }

        return [$response->getRequestProcessing(), ''];
    }

    /**
     * Сбор продаж на указанную дату
     *
     * @param int $merchantId
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return bool
     */
    public function collectSales(int $merchantId, \DateTime $dateFrom, \DateTime $dateTo): bool
    {
        $request = new CollectSalesRequest([
            'merchantID' => $merchantId,
            'dateFrom' => new Timestamp([
                'seconds' => $dateFrom->getTimestamp(),
            ]),
            'dateTo' => new Timestamp([
                'seconds' => $dateTo->getTimestamp(),
            ]),
        ]);

        /** @var CollectSalesResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->CollectSales($request)->wait();
        }, CollectSalesResponse::class);

        return $response->getProcessed();
    }

    /**
     * Сгенерировать агрегированный отчёт.
     *
     * @param int $merchantId
     * @param string $reportID
     * @param RecommendationsReportGenerateRequest $generateRequest
     *
     * @return GenerateRecommendationsReportResponse
     */
    public function generateRecommendationsReport(
        int $merchantId,
        string $reportID,
        RecommendationsReportGenerateRequest $generateRequest
    ): GenerateRecommendationsReportResponse
    {
        /** @var GenerateRecommendationsReportResponse $response */
        $response = $this->processRequest(function() use ($generateRequest, $merchantId, $reportID) {
            return $this->client->GenerateRecommendationsReport(
                $generateRequest->getAPIRequest($merchantId, $reportID)
            )->wait();
        }, GenerateRecommendationsReportResponse::class);

        return $response;
    }

    /**
     * Получить сводную информацию об отчёте рекомендаций к поставке
     *
     * @param int $merchantID
     * @param string $reportID
     *
     * @return GetRecommendationsReportSummaryResponse
     */
    public function getRecommendationsReportSummary(
        int $merchantID,
        string $reportID
    ): GetRecommendationsReportSummaryResponse {
        $apiRequest = new GetRecommendationsReportSummaryRequest([
            'merchantID' => $merchantID,
            'reportID' => $reportID
        ]);

        /** @var GetRecommendationsReportSummaryResponse $response */
        $response = $this->processRequest(function() use ($apiRequest) {
            return $this->client->GetRecommendationsReportSummary($apiRequest)->wait();
        }, GetRecommendationsReportSummaryResponse::class);

        return $response;
    }

    /**
     * Перекалькуляция сводной информации по отчёту рекомендаций
     *
     * @param int $merchantID
     * @param string $reportID
     *
     * @return GetRecommendationsReportSummaryResponse
     */
    public function recalculateRecommendationsReportSummary(
        int $merchantID,
        string $reportID
    ): GetRecommendationsReportSummaryResponse {
        $apiRequest = new GetRecommendationsReportSummaryRequest([
            'merchantID' => $merchantID,
            'reportID' => $reportID
        ]);

        /** @var GetRecommendationsReportSummaryResponse $response */
        $response = $this->processRequest(function() use ($apiRequest) {
            return $this->client->RecalculateRecommendationsReportSummary($apiRequest)->wait();
        }, GetRecommendationsReportSummaryResponse::class);

        return $response;
    }

    /**
     * Генерация агрегированного отчёта
     *
     * @param int $merchantId
     * @param string $reportId
     * @param NomenclatureFilterRequest $nomenclatureFilter
     * @param RecommendationsReportFilterRequest $recommendationsReportFilter
     * @param RecommendationsReportSortRequest $sortRequest
     * @param PageRequest $pageRequest
     * @param int $limit Ограничение на одной странице
     *
     * @return GetRecommendationsReportResponse
     */
    public function getRecommendationsReport(
        int $merchantId,
        string $reportId,
        NomenclatureFilterRequest $nomenclatureFilter,
        RecommendationsReportFilterRequest $recommendationsReportFilter,
        RecommendationsReportSortRequest $sortRequest,
        PageRequest $pageRequest,
        int $limit = 30
    ): GetRecommendationsReportResponse
    {
        $apiRequest = new GetRecommendationsReportRequest([
            'merchantID' => $merchantId,
            'nomenclatureFilter' => $nomenclatureFilter->getAPIRequest(),
            'filter' => $recommendationsReportFilter->getAPIRequest(),
            'reportID' => $reportId,
            'sortCondition' => $sortRequest->getAPICondition(),
            'sortDirection' => $sortRequest->getAPIDirection(),
            'page' => $pageRequest->getAPIRequest($limit),
        ]);

        return $this->getInternalAggregateReport($apiRequest);
    }

    /**
     * Внутренний метод для запроса детальной информации по отчёту
     *
     * @param GetRecommendationsReportRequest $request
     *
     * @return GetRecommendationsReportResponse
     */
    protected function getInternalAggregateReport(GetRecommendationsReportRequest $request): GetRecommendationsReportResponse
    {
        /** @var GetRecommendationsReportResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetRecommendationsReport($request)->wait();
        }, GetRecommendationsReportResponse::class);

        return $response;
    }

    public function getNomenclatureSummary(int $merchantId): GetNomenclatureSummaryResponse
    {
        $request = new GetNomenclatureSummaryRequest([
            'merchantID' => $merchantId,
        ]);

        /** @var GetNomenclatureSummaryResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetNomenclatureSummary($request)->wait();
        }, GetNomenclatureSummaryResponse::class);

        return $response;
    }

    /**
     * Получить массив доступных брендов
     *
     * @param int $merchantId
     *
     * @return array
     */
    public function getBrandNames(int $merchantId): array
    {
        $request = new GetBrandNamesRequest([
            'merchantID' => $merchantId,
        ]);

        list ($response,) = $this->client->GetBrandNames($request)->wait();

        if ($response instanceof GetBrandNamesResponse) {
            $result = [];
            foreach ($response->getBrands() as $brandName) {
                $result[] = (string) $brandName;
            }
            sort($result);
            return $result;
        }

        return [];
    }

    /**
     * Получить массив доступных названий
     *
     * @param int $merchantId
     *
     * @return array
     */
    public function getSubjectNames(int $merchantId): array
    {
        $request = new GetSubjectNamesRequest([
            'merchantID' => $merchantId,
        ]);

        list ($response,) = $this->client->GetSubjectNames($request)->wait();

        if ($response instanceof GetSubjectNamesResponse) {
            $result = [];
            foreach ($response->getNames() as $name) {
                $result[] = (string) $name;
            }
            sort($result);
            return $result;
        }

        return [];
    }

    /**
     * Запросить выгрузку в XLSX по отчёту рекомендаций
     *
     * @param int $merchantId
     * @param string $reportId
     * @param string $exportFormat
     *
     * @return ReportResultFile
     */
    public function requestRecommendationsExport(
        int $merchantId,
        string $reportId,
        string $exportFormat
    ): ReportResultFile
    {
        // сгенерировать запись в БД
        $resultFile = $this->resultService->requestExport($merchantId);

        $event = new WildberriesRecommendationsExportEvent(
            $resultFile,
            $merchantId,
            $reportId,
            $exportFormat
        );

        $this->events->dispatch($event);

        return $resultFile;
    }

    /**
     * Получение номенклатур мерчанта
     *
     * @param int $merchantID
     * @param NomenclatureFilterRequest $filterRequest
     * @param NomenclatureRemovedFilterRequest $removedFilterRequest
     * @param NomenclatureSortRequest $sortRequest
     * @param PageRequest $pageRequest
     * @param bool $isImport
     * @return GetNomenclatureResponse
     */
    public function getMerchantNomenclatures(
        int $merchantID,
        NomenclatureFilterRequest $filterRequest,
        NomenclatureRemovedFilterRequest $removedFilterRequest,
        NomenclatureSortRequest $sortRequest,
        PageRequest $pageRequest,
        bool $isImport = false
    ): GetNomenclatureResponse
    {
        $apiRequest = new GetNomenclatureRequest([
            'merchantID' => $merchantID,
            'filter' => $filterRequest->getAPIRequest(),
            'HideRemoved' => $removedFilterRequest->getHideRemoved(),
            'OnlyRemoved' => $removedFilterRequest->getOnlyRemoved(),
            'IsImport' => $isImport,
            'sort' => $sortRequest->getAPIRequest(),
            'page' => $pageRequest->getAPIRequest(),
        ]);

        /** @var GetNomenclatureResponse $response */
        $response = $this->processRequest(function() use ($apiRequest) {
            return $this->client->GetNomenclatures($apiRequest)->wait();
        }, GetNomenclatureResponse::class);

        return $response;
    }

    /**
     * Запросить выгрузку данных номенклатур в XLSX
     *
     * @param int $merchantId
     * @param array $filterData
     * @return ReportResultFile
     */
    public function requestNomenclaturesExport(
        int $merchantId,
        array $filterData,
        array $removedFilterData
    ): ReportResultFile
    {
        $resultFile = $this->resultService->requestExport($merchantId);

        $event = new WildberriesNomenclaturesExportEvent($resultFile, $merchantId, $filterData, $removedFilterData);

        $this->events->dispatch($event);

        return $resultFile;
    }

    /**
     * Запрос на снятие номенклатуры с продажи
     *
     * @param int $merchantID
     * @param UpdateNomenclatureSizeRequest $request
     *
     * @return UpdateNomenclatureSizeAPIRequest
     */
    public function updateNomenclatureSize(int $merchantID, UpdateNomenclatureSizeRequest $request): UpdateNomenclatureSizeAPIRequest
    {
        /** @var UpdateNomenclatureSizeAPIRequest $response */
        $response = $this->processRequest(function() use ($request, $merchantID) {
            return $this->client->UpdateNomenclatureSize($request->getAPIRequest($merchantID))->wait();
        }, UpdateNomenclatureSizeAPIRequest::class);

        return $response;
    }

    /**
     * Создание поставки в Wildberries API
     *
     * @param int $merchantID
     * @param CreateApiIncomeRequest $createApiIncomeRequest
     * @param array $nomenclatures
     * @return CreateIncomeResponse
     */
    public function createIncome(
        int $merchantID,
        CreateApiIncomeRequest $createApiIncomeRequest,
        array $nomenclatures
    ): CreateIncomeResponse
    {
        $request = new CreateIncomeRequest([
            'incomeID' => $createApiIncomeRequest->input('income_id'),
            'dateSupply' => new Timestamp([
                'seconds' => $createApiIncomeRequest->getDateSupply()->getTimestamp()
            ]),
            'merchantID' => $merchantID,
            'nomenclatures' => $nomenclatures,
        ]);

        /** @var CreateIncomeResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->CreateIncome($request)->wait();
        }, CreateIncomeResponse::class);

        return $response;
    }

    /**
     * Получить список поставок мерчанта
     *
     * @param int $merchantID
     * @param int $status
     * @param PageRequest $pageRequest
     *
     * @return IncomesListResponse
     */
    public function getIncomesList(int $merchantID, int $status, PageRequest $pageRequest): IncomesListResponse
    {
        $request = new IncomesListRequest([
            'merchantID' => $merchantID,
            'status' => $status,
            'page' => $pageRequest->getAPIRequest(),
        ]);

        /** @var IncomesListResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetIncomesList($request)->wait();
        }, IncomesListResponse::class);

        return $response;
    }

    /**
     * Получить детальную информацию о поставке
     *
     * @param int $merchantID
     * @param int $incomeID
     *
     * @return IncomeDetails
     */
    public function getIncomeDetails(int $merchantID, int $incomeID): IncomeDetails
    {
        $request = new IncomeDetailsRequest([
            'merchantID' => $merchantID,
            'incomeID' => $incomeID,
        ]);

        /** @var IncomeDetails $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetIncomeDetails($request)->wait();
        }, IncomeDetails::class);

        return $response;
    }

    /**
     * Удаление поставки
     *
     * @param int $merchantID
     * @param int $incomeID
     *
     * @return IncomeUpdateResponse
     */
    public function removeIncome(int $merchantID, int $incomeID): IncomeUpdateResponse
    {
        $request = new IncomeDetailsRequest([
            'merchantID' => $merchantID,
            'incomeID' => $incomeID,
        ]);

        /** @var IncomeUpdateResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->RemoveIncome($request)->wait();
        }, IncomeUpdateResponse::class);

        return $response;
    }

    /**
     * Обновление поставки
     *
     * @param int $merchantID
     * @param int $incomeID
     * @param UpdateApiIncomeRequest $request
     *
     * @return IncomeUpdateResponse
     */
    public function updateIncome(int $merchantID, int $incomeID, UpdateApiIncomeRequest $request): IncomeUpdateResponse
    {
        $request = new IncomeUpdateRequest([
            'income' => new Income([
                'dateSupply' => new Timestamp([
                    'seconds' => $request->getDateSupply()->getTimestamp()
                ]),
            ]),
            'request' => new IncomeDetailsRequest([
                'merchantID' => $merchantID,
                'incomeID' => $incomeID,
            ]),
        ]);

        /** @var IncomeUpdateResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->UpdateIncome($request)->wait();
        }, IncomeUpdateResponse::class);

        return $response;
    }

    /**
     * Получить отчёт о продажах по всем номенклатурам мерчанта
     *
     * @param int $merchantID Идентификатор мерчанта
     * @param \DateTime $dateFrom Дата, с которой собирать продажи
     * @param \DateTime $dateTo Дата, по которую собирать продажи
     *
     * @return SalesReportItem
     */
    public function getSalesAllReport(int $merchantID, \DateTime $dateFrom, \DateTime $dateTo): SalesReportItem
    {
        $request = new SalesReportItemRequest([
            'dateFrom' => new Timestamp([
                'seconds' => $dateFrom->getTimestamp(),
            ]),
            'dateTo' => new Timestamp([
                'seconds' => $dateTo->getTimestamp()
            ]),
            'merchantID' => $merchantID,
        ]);

        /** @var SalesReportItem $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetSalesAllReport($request)->wait();
        }, SalesReportItem::class);

        return $response;
    }

    /**
     * Получить страницу продаж для номенклатур
     *
     * @param int $merchantID
     * @param SalesPeriodRequest $periodRequest
     * @param NomenclatureFilterRequest $filterRequest
     * @param SalesSortRequest $sortRequest
     * @param PageRequest $pageRequest
     *
     * @return GetNomenclatureSalesResponse
     */
    public function getSalesNomenclaturesReport(
        int $merchantID,
        SalesPeriodRequest $periodRequest,
        NomenclatureFilterRequest $filterRequest,
        SalesSortRequest $sortRequest,
        PageRequest $pageRequest
    ): GetNomenclatureSalesResponse {
        $request = new GetNomenclatureSalesRequest([
            'request' => $periodRequest->getAPIRequest($merchantID),
            'filter' => $filterRequest->getAPIRequest(),
            'sort' => $sortRequest->getAPIRequest(),
            'page' => $pageRequest->getAPIRequest(10), // TODO: большое ограничение, для предотвращения нагрузки
        ]);

        /** @var GetNomenclatureSalesResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetNomenclaturesSales($request)->wait();
        }, GetNomenclatureSalesResponse::class);

        return $response;
    }

    /**
     * Получить страницу продаж для брендов
     *
     * @param int $merchantID
     * @param SalesPeriodRequest $periodRequest
     * @param BrandsFilterRequest $filterRequest
     * @param SalesSortRequest $sortRequest
     * @param PageRequest $pageRequest
     *
     * @return GetBrandSalesResponse
     */
    public function getSalesBrandsReport(
        int $merchantID,
        SalesPeriodRequest $periodRequest,
        BrandsFilterRequest $filterRequest,
        SalesSortRequest $sortRequest,
        PageRequest $pageRequest
    ): GetBrandSalesResponse {
        $request = new GetBrandSalesRequest([
            'request' => $periodRequest->getAPIRequest($merchantID),
            'filter' => $filterRequest->getAPIRequest(),
            'sort' => $sortRequest->getAPIRequest(),
            'page' => $pageRequest->getAPIRequest(),
        ]);

        /** @var GetBrandSalesResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetBrandsSales($request)->wait();
        }, GetBrandSalesResponse::class);

        return $response;
    }

    /**
     * Получить страницу продаж для категорий
     *
     * @param int $merchantID
     * @param SalesPeriodRequest $periodRequest
     * @param CategoriesFilterRequest $filterRequest
     * @param SalesSortRequest $sortRequest
     * @param PageRequest $pageRequest
     *
     * @return GetCategorySalesResponse
     */
    public function getSalesCategoriesReport(
        int $merchantID,
        SalesPeriodRequest $periodRequest,
        CategoriesFilterRequest $filterRequest,
        SalesSortRequest $sortRequest,
        PageRequest $pageRequest
    ): GetCategorySalesResponse {
        $request = new GetCategorySalesRequest([
            'request' => $periodRequest->getAPIRequest($merchantID),
            'filter' => $filterRequest->getAPIRequest(),
            'sort' => $sortRequest->getAPIRequest(),
            'page' => $pageRequest->getAPIRequest(),
        ]);

        /** @var GetCategorySalesResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetCategoriesSales($request)->wait();
        }, GetCategorySalesResponse::class);

        return $response;
    }

    /**
     * Получить статистические данные о заказах и продажах номенклатур
     *
     * @param int $merchantID
     * @param SalesPeriodRequest $periodRequest
     * @param SalesStatisticsNomenclatureFilterRequest $filterRequest
     * @param SalesStatisticsRequest $salesStatisticsRequest
     * @return GetStatisticSalesByRangeResponse
     */
    public function getSalesStatisticsNomenclaturesReport(
        int $merchantID,
        SalesPeriodRequest $periodRequest,
        SalesStatisticsNomenclatureFilterRequest $filterRequest,
        SalesStatisticsRequest $salesStatisticsRequest
    ): GetStatisticSalesByRangeResponse
    {
        $request = new GetStatisticSalesNomenclatureByRangeRequest([
            'brand' => $filterRequest->getBrand(),
            'subject' => $filterRequest->getName(),
            'dateFrom' => $periodRequest->getAPIRequest($merchantID)->getDateFrom(),
            'dateTo' => $periodRequest->getAPIRequest($merchantID)->getDateTo(),
            'marketplaceNomenclatureCode' => $filterRequest->getMarketplaceNomenclatureCode(),
            'vendorCode' => $filterRequest->getVendorCode(),
            'format' => $salesStatisticsRequest->getGroupStatisticSalesFormatValue(),
            'merchantID' => $merchantID,
        ]);

        /** @var GetStatisticSalesByRangeResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetStatisticNomenclatureByRange($request)->wait();
        }, GetStatisticSalesByRangeResponse::class);

        return $response;
    }

    /**
     * Получить статистические данные о заказах и продажах брендов
     *
     * @param int $merchantID
     * @param SalesPeriodRequest $periodRequest
     * @param SalesStatisticsBrandsFilterRequest $filterRequest
     * @param SalesStatisticsRequest $salesStatisticsRequest
     * @return GetStatisticSalesByRangeResponse
     */
    public function getSalesStatisticsBrandsReport(
        int $merchantID,
        SalesPeriodRequest $periodRequest,
        SalesStatisticsBrandsFilterRequest $filterRequest,
        SalesStatisticsRequest $salesStatisticsRequest
    ): GetStatisticSalesByRangeResponse
    {
        $request = new GetStatisticSalesBrandByRangeRequest([
            'brand' => $filterRequest->getBrand(),
            'dateFrom' => $periodRequest->getAPIRequest($merchantID)->getDateFrom(),
            'dateTo' => $periodRequest->getAPIRequest($merchantID)->getDateTo(),
            'format' => $salesStatisticsRequest->getGroupStatisticSalesFormatValue(),
            'merchantID' => $merchantID,
        ]);

        /** @var GetStatisticSalesByRangeResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetStatisticBrandByRange($request)->wait();
        }, GetStatisticSalesByRangeResponse::class);

        return $response;
    }

    /**
     * Получить статистические данные о заказах и продажах категорий
     *
     * @param int $merchantID
     * @param SalesPeriodRequest $periodRequest
     * @param SalesStatisticsCategoriesFilterRequest $filterRequest
     * @param SalesStatisticsRequest $salesStatisticsRequest
     * @return GetStatisticSalesByRangeResponse
     */
    public function getSalesStatisticsCategoriesReport(
        int $merchantID,
        SalesPeriodRequest $periodRequest,
        SalesStatisticsCategoriesFilterRequest $filterRequest,
        SalesStatisticsRequest $salesStatisticsRequest
    ): GetStatisticSalesByRangeResponse
    {
        $request = new GetStatisticSalesCategoriesByRangeRequest([
            'category' => $filterRequest->getCategory(),
            'dateFrom' => $periodRequest->getAPIRequest($merchantID)->getDateFrom(),
            'dateTo' => $periodRequest->getAPIRequest($merchantID)->getDateTo(),
            'format' => $salesStatisticsRequest->getGroupStatisticSalesFormatValue(),
            'merchantID' => $merchantID,
        ]);

        /** @var GetStatisticSalesByRangeResponse $response */
        $response = $this->processRequest(function() use ($request) {
            return $this->client->GetStatisticCategoriesByRange($request)->wait();
        }, GetStatisticSalesByRangeResponse::class);

        return $response;
    }

    /**
     * Запросить выгрузку в XLSX по отчёту продаж
     *
     * @param int $merchantId
     * @param array $periodData
     * @param array $filterData
     * @param string $eventClass Название класса для генерации события
     * @return ReportResultFile
     * @see AbstractSalesExportEvent
     * @see WildberriesNomenclaturesSalesExportEvent
     * @see WildberriesBrandsSalesExportEvent
     * @see WildberriesCategoriesSalesExportEvent
     */
    public function requestSalesExport(
        int $merchantId,
        array $periodData,
        array $filterData,
        string $eventClass
    ): ReportResultFile
    {
        // сгенерировать запись в БД
        $resultFile = $this->resultService->requestExport($merchantId);

        if (!in_array($eventClass, [
            WildberriesNomenclaturesSalesExportEvent::class,
            WildberriesBrandsSalesExportEvent::class,
            WildberriesCategoriesSalesExportEvent::class,
        ])) {
            throw new \RuntimeException();
        }

        /** @var AbstractSalesExportEvent $event */
        $event = new $eventClass($resultFile, $merchantId, $periodData, $filterData);
        $this->events->dispatch($event);

        return $resultFile;
    }

    /**
     * Получение всех складов
     *
     * @return GetWarehousesResponse
     */
    public function getWarehouses(): GetWarehousesResponse
    {
        $request = new GetWarehousesRequest();

        list ($response, $status) = $this->client->GetWarehouses($request)->wait();

        if (!$response instanceof GetWarehousesResponse) {
            throw new \RuntimeException("Не удалось получить ответ, " . print_r($status, true));
        }

        return $response;
    }

    /**
     * Редактирование номенклатуры в рекоммендации к поставке
     *
     * @param int $merchantID
     * @param string $reportID
     * @param RecommendationsNomenclatureReportUpdateRequest $request
     *
     * @return UpdateRecommendationsNomenclatureRequest
     */
    public function updateRecommendationsNomenclature(int $merchantID, string $reportID, RecommendationsNomenclatureReportUpdateRequest $request): UpdateRecommendationsNomenclatureRequest
    {
        /** @var UpdateRecommendationsNomenclatureRequest $response */
        $response = $this->processRequest(function() use ($merchantID, $reportID, $request) {
            return $this->client->UpdateRecommendationsNomenclature($request->getAPIRequest($merchantID, $reportID))->wait();
        }, UpdateRecommendationsNomenclatureRequest::class);

        return $response;
    }

    /**
     * Добавить в поставку / убрать из поставки все номенклатуры с рекомендациями
     *
     * @param int $merchantId
     * @param string $reportId
     * @param RecommendationsNomenclatureReportApplyAllRequest $request
     * @return UpdateRecommendationsNomenclatureRequest
     */
    public function updateRecommendationsNomenclatureApplyAll(
        int $merchantId,
        string $reportId,
        RecommendationsNomenclatureReportApplyAllRequest $request
    ): UpdateRecommendationsNomenclatureRequest
    {
        return $this->processRequest(function () use ($merchantId, $reportId, $request) {
            return $this->client->UpdateRecommendationsNomenclatureAll($request->getAPIRequest($merchantId, $reportId))->wait();
        }, UpdateRecommendationsNomenclatureRequest::class);
    }

    /**
     * Получить список настроек номенклатур
     *
     * @param string $subject
     * @param PageRequest $pageRequest
     * @return ListNomenclatureSettingsResponse
     */
    public function listNomenclatureSettings(string $subject, PageRequest $pageRequest): ListNomenclatureSettingsResponse
    {
        $request = new ListNomenclatureSettingsRequest([
            'subject' => $subject,
            'page' => $pageRequest->getAPIRequest(50),
        ]);
        return $this->processRequest(function () use ($request) {
            return $this->client->ListNomenclatureSettings($request)->wait();
        }, ListNomenclatureSettingsResponse::class);
    }

    /**
     * Создание настроек номенклатур
     *
     * @param CreateNomenclatureSettingsRequest $request
     * @return NomenclatureSettings
     */
    public function createNomenclatureSettings(CreateNomenclatureSettingsRequest $request): NomenclatureSettings
    {
        return $this->processRequest(function () use ($request) {
            return $this->client->CreatePresetQuantumOfDelivery($request)->wait();
        }, NomenclatureSettings::class);
    }

    /**
     * Обновление настроек номенклатур
     *
     * @param UpdateNomenclatureSettingsRequest $request
     * @return NomenclatureSettings
     */
    public function updateNomenclatureSettings(UpdateNomenclatureSettingsRequest $request): NomenclatureSettings
    {
        return $this->processRequest(function () use ($request) {
            return $this->client->UpdateNomenclatureSettings($request)->wait();
        }, NomenclatureSettings::class);
    }

    /**
     * Удаление настроек номенклатур
     *
     * @param DeleteNomenclatureSettingsRequest $request
     * @return GPBEmpty
     */
    public function deleteNomenclatureSettings(DeleteNomenclatureSettingsRequest $request): GPBEmpty
    {
        return $this->processRequest(function () use ($request) {
            return $this->client->DeleteNomenclatureSettings($request)->wait();
        }, GPBEmpty::class);
    }

    /**
     * Получить сводную информацию с данными по складам
     *
     * @param int $merchantId
     * @param NomenclatureFilterRequest $nomenclatureFilterRequest
     * @param StockFilterRequest $stockFilterRequest
     * @return GetStockInfoResponse
     */
    public function getStockInfo(
        int $merchantId,
        NomenclatureFilterRequest $nomenclatureFilterRequest,
        StockFilterRequest $stockFilterRequest
    ): GetStockInfoResponse
    {
        $request = new GetStockInfoRequest([
            'merchantID' => $merchantId,
            'filter' => new StockFilter([
                'filter' => $nomenclatureFilterRequest->getAPIRequest(),
                'warehouse' => $stockFilterRequest->getWarehouse(),
                'availableToOrderFrom' => $stockFilterRequest->getAvailableToOrderFrom(),
                'availableToOrderTo' => $stockFilterRequest->getAvailableToOrderTo(),
            ]),
        ]);

        return $this->processRequest(function () use ($request) {
            return $this->client->GetInfoStocks($request)->wait();
        }, GetStockInfoResponse::class);
    }

    /**
     * Получить отчёт по складам
     *
     * @param int $merchantId
     * @param NomenclatureFilterRequest $nomenclatureFilterRequest
     * @param StockFilterRequest $stockFilterRequest
     * @param StockReportSortRequest $sortRequest
     * @param PageRequest $pageRequest
     * @param bool $unlimited
     * @return GetStocksReportResponse
     */
    public function getStockReport(
        int $merchantId,
        NomenclatureFilterRequest $nomenclatureFilterRequest,
        StockFilterRequest $stockFilterRequest,
        StockReportSortRequest $sortRequest,
        PageRequest $pageRequest,
        bool $unlimited = false
    ): GetStocksReportResponse
    {
        $request = new GetStocksReportRequest([
            'merchantID' => $merchantId,
            'filter' => new StockFilter([
                'filter' => $nomenclatureFilterRequest->getAPIRequest(),
                'warehouse' => $stockFilterRequest->getWarehouse(),
                'availableToOrderFrom' => $stockFilterRequest->getAvailableToOrderFrom(),
                'availableToOrderTo' => $stockFilterRequest->getAvailableToOrderTo(),
            ]),
            'sort' => $sortRequest->getAPIRequest(),
            'page' => $pageRequest->getAPIRequest($unlimited ? 999999999 : 30),
        ]);

        return $this->processRequest(function () use ($request) {
            return $this->client->GetStockReport($request)->wait();
        }, GetStocksReportResponse::class);
    }

    /**
     * Обновление данных размера номенклатуры в складах
     *
     * @param int $merchantId
     * @param UpdateStockSizeRequest $request
     * @return UpdateStockSizeAPIRequest
     */
    public function updateStockSize(int $merchantId, UpdateStockSizeRequest $request): UpdateStockSizeAPIRequest
    {
        return $this->processRequest(function () use ($merchantId, $request) {
            return $this->client->UpdateStockSize($request->getAPIRequest($merchantId))->wait();
        }, UpdateStockSizeAPIRequest::class);
    }

    /**
     * Запросить выгрузку данных складов в XLSX
     *
     * @param int $merchantId
     * @param array $nomenclatureFilterData
     * @param array $stockFilterData
     * @param array $sortData
     * @return ReportResultFile
     */
    public function requestStocksExport(
        int $merchantId,
        array $nomenclatureFilterData,
        array $stockFilterData,
        array $sortData
    ): ReportResultFile
    {
        $resultFile = $this->resultService->requestExport($merchantId);

        $event = new WildberriesStocksExportEvent(
            $resultFile,
            $merchantId,
            $nomenclatureFilterData,
            $stockFilterData,
            $sortData
        );

        $this->events->dispatch($event);

        return $resultFile;
    }
}
