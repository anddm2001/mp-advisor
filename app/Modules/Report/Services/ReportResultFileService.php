<?php

namespace App\Modules\Report\Services;

use App\Modules\Report\Contracts\ReportResultFileServiceContract;
use App\Modules\Report\Models\ReportResultFile;
use App\Modules\Storage\Contracts\StorageServiceContract;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Testing\MimeType;
use Illuminate\Http\UploadedFile;

/**
 * Сервиса для создания и управления результирующего файла отчёта
 *
 * @package App\Modules\Report\Services
 */
class ReportResultFileService implements ReportResultFileServiceContract
{
    /**
     * @var StorageServiceContract Сервис для управления файлами
     */
    protected StorageServiceContract $storageService;

    /**
     * @var ReportResultFile Модель пустого файла для создания новых записей
     */
    protected ReportResultFile $model;

    /**
     * @var Dispatcher Менеджер событий
     */
    protected Dispatcher $events;

    /**
     * ReportResultFileService constructor.
     *
     * @param StorageServiceContract $storageService
     * @param ReportResultFile $model
     * @param Dispatcher $events
     */
    public function __construct(StorageServiceContract $storageService, ReportResultFile $model, Dispatcher $events)
    {
        $this->storageService = $storageService;
        $this->model = $model;
        $this->events = $events;
    }

    /**
     * @inheritDoc
     */
    public function requestExport(int $merchantID): ReportResultFile
    {
        $model = clone $this->model;

        $model->merchant_id = $merchantID;
        $model->generate_status = ReportResultFile::STATUS_NEW;

        if ($model->save()) {
            return $model;
        }

        throw new \RuntimeException();
    }

    /**
     * @inheritDoc
     */
    public function uploadResultFile(ReportResultFile $file, string $path): bool
    {
        $tmpFile = new UploadedFile(
            $path,
            basename($path),
            MimeType::from(basename($path)),
            null,
            true
        );

        $storageFile = $this->storageService->upload($tmpFile);
        $file->file_id = $storageFile->id;
        return $file->save();
    }

    /**
     * @inheritDoc
     */
    public function beginGenerate(ReportResultFile $file): bool
    {
        $file->generate_status = ReportResultFile::STATUS_PROCESSING;
        return $file->save();
    }

    /**
     * @inheritDoc
     */
    public function endGenerate(ReportResultFile $file, bool $withErrors): bool
    {
        $file->generate_status = $withErrors ? ReportResultFile::STATUS_ERROR : ReportResultFile::STATUS_DONE;
        return $file->save();
    }
}
