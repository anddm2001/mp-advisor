<?php

namespace App\Modules\Report\Services;

use Box\Spout\Common\Entity\Row;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Reader\ReaderInterface;
use Box\Spout\Reader\SheetInterface;
use Report\CreateIncomeRequest\CreateIncomeNomenclature;

/**
 * Парсер Excel-файла для загрузки новой поставки в Wildberries
 *
 * @package App\Modules\Report\Services
 */
class WildberriesAPIIncomeXLSParser
{
    /**
     * @var string Путь до файла XLS
     */
    protected string $xlsFilePath;

    /**
     * @var array Массив количества товаров в поставке, проиндексированный по штрихкодам
     */
    protected array $qtyByBarcode = [];

    /**
     * @var ReaderInterface Ридер XLSX-файла
     */
    protected ReaderInterface $reader;

    /**
     * WildberriesIncomeXLSParser constructor.
     *
     * @param string $xlsFilePath
     */
    public function __construct(string $xlsFilePath)
    {
        $this->xlsFilePath = $xlsFilePath;
        $this->reader = ReaderEntityFactory::createXLSXReader();
    }

    /**
     * Распарсить файл и запомнить количество товаров к поставке
     *
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     */
    public function parseFile(): self
    {
        try {
            $this->reader->open($this->xlsFilePath);
            foreach ($this->reader->getSheetIterator() as $sheet) {
                /** @var SheetInterface $sheet */
                /** @var Row $row */
                foreach ($sheet->getRowIterator() as $row) {
                    $barcodeCell = $row->getCellAtIndex(0);
                    $quantityCell = $row->getCellAtIndex(1);

                    if (empty($barcodeCell) || empty($quantityCell)) {
                        continue;
                    }

                    $barcode = (int) $barcodeCell->getValue();
                    $quantity = (int) $quantityCell->getValue();

                    if ($barcode < 1 || $quantity < 1) {
                        // работаем только со строками, где штрихкод - цифры и количество - цифры
                        continue;
                    }

                    if (!isset($this->qtyByBarcode[$barcode])) {
                        $this->qtyByBarcode[$barcode] = 0;
                    }

                    $this->qtyByBarcode[$barcode] += $quantity;
                }
            }
        } finally {
            $this->reader->close();
        }

        return $this;
    }

    /**
     * Создать массив номенклатур для вставки в API
     *
     * @return CreateIncomeNomenclature[]
     */
    public function fillAPINomenclatures(): array
    {
        $result = [];

        foreach ($this->qtyByBarcode as $barcode => $quantity) {
            $result[] = new CreateIncomeNomenclature([
                'barcode' => (string) $barcode,
                'quantity' => $quantity,
            ]);
        }

        return $result;
    }
}
