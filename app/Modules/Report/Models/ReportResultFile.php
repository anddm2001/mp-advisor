<?php

namespace App\Modules\Report\Models;

use App\Modules\Storage\Models\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Модель результирующего файла для скачивания пользователем
 *
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property int $merchant_id Идентификатор мерчанта
 * @property int $file_id Идентификатор файла
 * @property int $generate_status Статус генерации файла
 *
 * @property File|null $file Модель файла для скачивания
 *
 * @property string $status_code Код статуса генерации файла
 *
 * @package App\Modules\Report\Models
 */
class ReportResultFile extends Model
{
    /**
     * Статус генерации - новый
     */
    const STATUS_NEW = 0;

    /**
     * Статус генерации - в процессе
     */
    const STATUS_PROCESSING = 1;

    /**
     * Статус генерации - завершена генерации
     */
    const STATUS_DONE = 2;

    /**
     * Статус генерации - ошибка генерации
     */
    const STATUS_ERROR = -1;

    /**
     * @var string
     */
    protected $table = 'report_result_file';

    /**
     * Привязка к файлу
     *
     * @return BelongsTo
     */
    public function file(): BelongsTo
    {
        return $this->belongsTo(File::class, 'file_id', 'id');
    }

    /**
     * Аттрибут кода статуса генерации файла
     *
     * @return string
     */
    public function getStatusCodeAttribute(): string
    {
        $codes = self::getStatusCodes();
        return $codes[$this->generate_status] ?? (string) $this->generate_status;
    }

    /**
     * Коды статусов генерации файла
     *
     * @return string[]
     */
    public static function getStatusCodes(): array
    {
        return [
            self::STATUS_NEW => 'new',
            self::STATUS_PROCESSING => 'processing',
            self::STATUS_DONE => 'done',
            self::STATUS_ERROR => 'error',
        ];
    }

    /**
     * Обработка не была начала
     *
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->generate_status == self::STATUS_NEW;
    }

    /**
     * Обработка в процессе
     *
     * @return bool
     */
    public function isProcessing(): bool
    {
        return $this->generate_status == self::STATUS_PROCESSING;
    }

    /**
     * Обработка завершена
     *
     * @return bool
     */
    public function isDone(): bool
    {
        return $this->generate_status == self::STATUS_DONE;
    }

    /**
     * Обработка завершена с ошибками
     *
     * @return bool
     */
    public function isError(): bool
    {
        return $this->generate_status == self::STATUS_ERROR;
    }
}
