<?php

namespace App\Modules\Report\Models;

use App\Modules\Merchant\Models\Merchant;
use App\Modules\Report\Requests\RecommendationsReportGenerateRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Отчёт рекомендаций к поставке
 *
 * @property int $id
 * @property int $merchant_id
 * @property string $report_id Идентификатор отчёта
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Merchant $merchant
 */
class RecommendationReport extends Model
{
    /** @var int Кол-во дней, добавляемых к текущей дате для формирования даты поставки */
    private const DEFAULT_DELIVERY_DATE_ADD_DAYS = 1;
    /** @var int Кол-во дней, вычитаемых из текущей даты для формирования начала периода расчёта */
    private const DEFAULT_CALCULATE_FROM_SUB_DAYS = 14;
    /** @var int Кол-во дней для расчёта запаса */
    private const DEFAULT_REPORT_DAYS = 30;

    /** @inheritdoc  */
    protected $table = 'recommendation_report';

    /** @inheritdoc  */
    protected $fillable = [
        'merchant_id',
        'report_id',
    ];

    // Relationships ================================================================================================ //

    /**
     * Мерчант
     *
     * @return BelongsTo
     */
    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id', 'merchant');
    }

    // Stuff ======================================================================================================== //

    /**
     * Получить запрос с параметрами по умолчанию для генерации отчёта в микросервисе
     *
     * @return RecommendationsReportGenerateRequest
     */
    public static function getGenerationRequestForWildberriesApi(): RecommendationsReportGenerateRequest
    {
        $now = Carbon::now('UTC');
        return RecommendationsReportGenerateRequest::createFromArray([
            'delivery_date' => (clone $now)
                ->addDays(self::DEFAULT_DELIVERY_DATE_ADD_DAYS)
                ->format(RecommendationsReportGenerateRequest::DATE_FORMAT),
            'calculate_from' => (clone $now)
                ->subDays(self::DEFAULT_CALCULATE_FROM_SUB_DAYS)
                ->format(RecommendationsReportGenerateRequest::DATE_FORMAT),
            'calculate_to' => (clone $now)->format(RecommendationsReportGenerateRequest::DATE_FORMAT),
            'report_days' => self::DEFAULT_REPORT_DAYS,
        ]);
    }
}
