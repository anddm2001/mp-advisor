<?php

namespace App\Modules\Report\Providers;

use App\Modules\Report\Contracts\ReportResultFileServiceContract;
use App\Modules\Report\Exporter\WildberriesIncomeExporter;
use App\Modules\Report\Exporter\WildberriesNomenclaturesExporter;
use App\Modules\Report\Exporter\WildberriesRecommendationsExporter;
use App\Modules\Report\Exporter\WildberriesSalesExporter;
use App\Modules\Report\Exporter\WildberriesStocksExporter;
use App\Modules\Report\Services\ReportResultFileService;
use App\Modules\Report\Services\WildberriesReportApi;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Grpc\ChannelCredentials;
use Illuminate\Support\ServiceProvider;
use Report\WildberriesReportClient;

/**
 * Провайдер сервисов для модуля отчётов
 *
 * @package App\Modules\Report\Providers
 */
class ReportServiceProvider extends ServiceProvider
{
    public function register()
    {
        // сервис для работы с файлами выгрузки
        $this->app->singleton(ReportResultFileServiceContract::class, ReportResultFileService::class);

        // микросервис для работы с API отчётов
        $this->app->singleton(WildberriesReportApi::class, function($app) {
            $host = $app->make('config')['report.wildberries_api_report_host'];

            $client = new WildberriesReportClient($host, [
                'credentials' => ChannelCredentials::createInsecure(),
            ]);

            return new WildberriesReportApi(
                $client,
                $app->make(ReportResultFileServiceContract::class),
                $app->make('events')
            );
        });

        // сервис для выгрузки файлов поставок в Wildberries в XLSX
        $this->app->bind(WildberriesIncomeExporter::class, function($app) {
            $writer = WriterEntityFactory::createXLSXWriter();

            return new WildberriesIncomeExporter($writer);
        });

        // сервис для выгрузки рекомендация к поставкам в Wildberries в XLSX
        $this->app->bind(WildberriesRecommendationsExporter::class, function($app) {
            $writer = WriterEntityFactory::createXLSXWriter();

            return new WildberriesRecommendationsExporter($writer);
        });

        // сервис для выгрузки файлов отчётов продаж в Wildberries в XLSX
        $this->app->bind(WildberriesSalesExporter::class, function($app) {
            $writer = WriterEntityFactory::createXLSXWriter();

            return new WildberriesSalesExporter($writer);
        });

        // сервис для выгрузки файлов с данными номенклатур Wildberries в XLSX
        $this->app->bind(WildberriesNomenclaturesExporter::class, function($app) {
            $writer = WriterEntityFactory::createXLSXWriter();

            return new WildberriesNomenclaturesExporter($writer);
        });

        // сервис для выгрузки файлов с данными складов
        $this->app->bind(WildberriesStocksExporter::class, function($app) {
            $writer = WriterEntityFactory::createXLSXWriter();

            return new WildberriesStocksExporter($writer);
        });
    }
}
