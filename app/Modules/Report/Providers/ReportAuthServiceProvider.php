<?php

namespace App\Modules\Report\Providers;

use App\Modules\Common\Providers\BaseAuthServiceProvider;
use App\Modules\Report\Policies\ReportPolicy;

/**
 * Регистрация политик прав доступа
 *
 * @package App\Modules\Report\Providers
 */
class ReportAuthServiceProvider extends BaseAuthServiceProvider
{
    protected function policies(): array
    {
        return [
            'view-report-result-file' => ReportPolicy::class . '@allowViewResultFile',
        ];
    }
}
