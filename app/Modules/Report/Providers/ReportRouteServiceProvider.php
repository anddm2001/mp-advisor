<?php

namespace App\Modules\Report\Providers;

use App\Modules\Report\Models\ReportResultFile;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Report\IncomeStatus;

/**
 * Объявление переменных в роутах
 *
 * @package App\Modules\Report\Providers
 */
class ReportRouteServiceProvider extends ServiceProvider
{
    /**
     * Биндинг моделей внутрь роутинга
     */
    protected function bindRouteModels()
    {
        /** @var Router $router */
        $router = $this->app->make('router');

        // отчёты из API
        $router->pattern('reportID', '^[a-f\d]{24}$');

        // результирующие файлы из API
        $router->pattern('reportResultFile', '[0-9]+');
        $router->model('reportResultFile', ReportResultFile::class);

        // идентификатор поставки
        $router->pattern('incomeID', '[0-9]+');

        // статусы поставок в API
        $router->pattern('incomeStatus', '[' . implode('|', [
            IncomeStatus::STATUS_NEW,
            IncomeStatus::STATUS_TRANSIT,
            IncomeStatus::STATUS_DELIVERED
        ]) . ']');
    }

    public function boot()
    {
        $this->bindRouteModels();
    }
}
