<?php

namespace App\Modules\Report\Providers;

use App\Modules\Report\Contracts\ReportResultFileServiceContract;
use App\Modules\Report\Listeners\WildberriesApiCollectDataListener;
use App\Modules\Report\Listeners\WildberriesNomenclaturesExportListener;
use App\Modules\Report\Listeners\WildberriesRecommendationsExportListener;
use App\Modules\Report\Listeners\WildberriesSalesExportListener;
use App\Modules\Report\Listeners\WildberriesStocksExportListener;
use App\Modules\Report\Services\WildberriesReportApi;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;

/**
 * Регистрация обработчиков событий
 *
 * @package App\Modules\Report\Providers
 */
class ReportEventServiceProvider extends EventServiceProvider
{
    /**
     * @var string[]
     */
    protected $subscribe = [
        WildberriesApiCollectDataListener::class,
        WildberriesNomenclaturesExportListener::class,
        WildberriesRecommendationsExportListener::class,
        WildberriesSalesExportListener::class,
        WildberriesStocksExportListener::class,
    ];

    public function register()
    {
        $this->app->bind(WildberriesNomenclaturesExportListener::class, function($app) {
            return new WildberriesNomenclaturesExportListener(
                $app,
                $app->make(WildberriesReportApi::class),
                $app->make(ReportResultFileServiceContract::class),
                storage_path('app')
            );
        });

        $this->app->bind(WildberriesRecommendationsExportListener::class, function($app) {
            return new WildberriesRecommendationsExportListener(
                $app,
                $app->make(WildberriesReportApi::class),
                $app->make(ReportResultFileServiceContract::class),
                storage_path('app')
            );
        });

        $this->app->bind(WildberriesSalesExportListener::class, function($app) {
            return new WildberriesSalesExportListener(
                $app,
                $app->make(WildberriesReportApi::class),
                $app->make(ReportResultFileServiceContract::class),
                storage_path('app')
            );
        });

        $this->app->bind(WildberriesStocksExportListener::class, function($app) {
            return new WildberriesStocksExportListener(
                $app,
                $app->make(WildberriesReportApi::class),
                $app->make(ReportResultFileServiceContract::class),
                storage_path('app')
            );
        });

        parent::register();
    }
}
