<?php
namespace App\Modules\ControlPanel\Services;

use App\Modules\Report\Services\WildberriesReportApi;
use Box\Spout\Common\Entity\Row;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Reader\ReaderInterface;
use Box\Spout\Reader\SheetInterface;
use Box\Spout\Reader\XLSX\Reader;
use Exception;
use Report\CreateNomenclatureSettingsRequest;

/**
 * Сервис для импорта настроек номенклатур (квантов поставок категорий)
 */
class SupplyImportNomenclatureSettings
{
    /** @var WildberriesReportApi */
    protected WildberriesReportApi $api;

    /** @var ReaderInterface|Reader */
    protected ReaderInterface $reader;

    /**
     * Определение сервиса API и ридера XLSX
     */
    public function __construct(WildberriesReportApi $api)
    {
        $this->api = $api;
        $this->reader = ReaderEntityFactory::createXLSXReader();
    }

    /**
     * Импорт данных из файла в микросервис
     *
     * @param string $filePath
     * @throws Exception
     */
    public function import(string $filePath): void
    {
        try {
            $this->reader->open($filePath);
            /** @var SheetInterface $sheet */
            foreach ($this->reader->getSheetIterator() as $sheet) {
                /** @var Row $row */
                foreach ($sheet->getRowIterator() as $row) {
                    $subjectCell = $row->getCellAtIndex(0);
                    $quantumCell = $row->getCellAtIndex(1);
                    if (empty($subjectCell) || empty($quantumCell)) {
                        continue;
                    }

                    $subject = (string) $subjectCell->getValue();
                    $defaultQuantum = (int) $quantumCell->getValue();
                    if ($subject === '' || $subject === 'Предмет' || $defaultQuantum <= 0) {
                        continue;
                    }

                    $this->api->createNomenclatureSettings(
                        new CreateNomenclatureSettingsRequest(compact('subject', 'defaultQuantum'))
                    );
                }
            }
        } finally {
            $this->reader->close();
        }
    }
}
