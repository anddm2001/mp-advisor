<?php

namespace App\Modules\ControlPanel\Services;

use App\Modules\Auth\Models\PersonalAccessToken;
use App\Modules\Auth\Models\User;
use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Common\Models\EventLog;
use App\Modules\ControlPanel\Http\Requests\DashboardExportDetails;
use App\Modules\ControlPanel\Http\Requests\DashboardIndex;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Report\Contracts\MarketPlaceContract;
use Box\Spout\Common\Entity\Cell;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class StatisticService
{
    /** @var string Формат даты из лога */
    public const LOG_DATE_FORMAT = 'Y-m-d';
    /** @var string Формат даты для графика */
    public const CHART_DATE_FORMAT = 'd.m';
    /** @var string Формат даты для экспорта */
    public const EXPORT_DATE_FORMAT = 'd.m.Y';
    /** @var string Формат даты для имени файла экспорта */
    public const EXPORT_FILE_DATE_FORMAT = 'y-m-d';

    /**
     * Получить данные для графиков
     *
     * @param DashboardIndex $request
     * @return array|array[]
     */
    public static function getChartsData(DashboardIndex $request): array
    {
        $charts = EventLog::getTypeNames();
        $dates = self::getDates($request);
        $logData = self::getData($request);
        $result = [];

        foreach ($charts as $kebabCode => $name) {
            $code = Str::camel($kebabCode);
            $labels = [];
            $data = ['main' => [], 'compare' => []];

            foreach ($dates as $mainDateStr => $compareDateStr) {
                $mainDate = Carbon::createFromFormat(self::LOG_DATE_FORMAT, $mainDateStr);
                $compareDate = Carbon::createFromFormat(self::LOG_DATE_FORMAT, $compareDateStr);

                $labels[] = $mainDate->format(self::CHART_DATE_FORMAT) .
                    ' (' . $compareDate->format(self::CHART_DATE_FORMAT) . ')';

                $data['main'][] = $logData['main'][$kebabCode][$mainDateStr] ?? 0;
                $data['compare'][] = $logData['compare'][$kebabCode][$compareDateStr] ?? 0;
            }

            $result[] = compact('name', 'code', 'labels', 'data');
        }

        return $result;
    }

    /**
     * Экспорт данных графиков
     *
     * @param DashboardIndex $request
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public static function exportChartsData(DashboardIndex $request): void
    {
        $fileName = 'stats_' .
            $request->getDateFrom()->format(self::EXPORT_FILE_DATE_FORMAT) . '_' .
            $request->getDateTo()->format(self::EXPORT_FILE_DATE_FORMAT);
        $writer = WriterEntityFactory::createXLSXWriter()->openToBrowser($fileName . '.xlsx');
        $bold = (new StyleBuilder())->setFontBold()->build();

        $charts = array_flip(EventLog::getTypeNames());
        $dates = array_flip(self::getDates($request));
        $logData = self::getData($request, true);

        $writer->addRow(WriterEntityFactory::createRowFromArray(collect(array_keys($charts))->prepend('')->toArray(), $bold));

        foreach ($dates as $dateStr) {
            $rowData = [Carbon::createFromFormat(self::LOG_DATE_FORMAT, $dateStr)->format(self::EXPORT_DATE_FORMAT)];
            foreach ($charts as $code) {
                $rowData[] = $logData['main'][$code][$dateStr] ?? 0;
            }
            $writer->addRow(WriterEntityFactory::createRowFromArray($rowData));
        }

        $writer->close();
    }

    /**
     * Экспорт данных по пользователям
     *
     * @param DashboardExportDetails $request
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public static function exportUsersData(DashboardExportDetails $request): void
    {
        $fileName = 'user_stats_' .
            $request->getDateFrom()->format(self::EXPORT_FILE_DATE_FORMAT) . '_' .
            $request->getDateTo()->format(self::EXPORT_FILE_DATE_FORMAT);
        $writer = WriterEntityFactory::createXLSXWriter()->openToBrowser($fileName . '.xlsx');
        $bold = (new StyleBuilder())->setFontBold()->build();

        $writer->addRow(WriterEntityFactory::createRowFromArray([
            'ID пользователя',
            'ID мерчантов',
            'E-mail',
            'Последняя авторизация',
            'Активные SKU WB',
            'Регистрация',
            'Активация',
            'Ключ WB',
            'Триал',
            '1 оплата',
            '1 сумма',
            '2 оплата',
            '2 сумма',
            '3 оплата',
            '3 сумма',
        ], $bold));

        /** @var Collection|User[] $users */
        $users = User::with([
            'tokens' => function (MorphMany $query) {
                $query->oldest();
            },
            'merchants' => function (HasManyThrough $query) {
                $query->oldest();
            },
            'merchants.marketplaceConnections' => function (HasMany $query) {
                $query->where('marketplace', MarketPlaceContract::WILDBERRIES)
                    ->where('credentials', '!=', '')
                    ->whereNotNull('created_at');
            },
            'merchants.billingOperations' => function (HasMany $query) {
                $query->where([
                    'type' => BillingOperation::TYPE_PAYMENT,
                    'status' => BillingOperation::STATUS_SUCCESS,
                    'payment_system' => BillingOperation::PAYMENT_SYSTEM_YOO_KASSA,
                ])->whereNotNull('captured_at')->oldest('captured_at');
            },
        ])->whereBetween('created_at', [$request->getDateFrom(), $request->getDateTo()])->oldest()->get();

        foreach ($users as $user) {
            /** @var PersonalAccessToken|null $token */
            $token = $user->tokens->first();
            /** @var Carbon|null $credentialsCreatedAt */
            $credentialsCreatedAt = $user->merchants->max(function (Merchant $item) {
                return $item->marketplaceConnections->max('created_at');
            });
            /** @var Carbon|null $trialPeriodFinish */
            $trialPeriodFinish = $user->merchants->max('trial_period_finish');
            $rowData = [
                $user->id,
                $user->merchants->implode('id', ', '),
                $user->email,
                $token ? $token->created_at->format(self::EXPORT_DATE_FORMAT) : '—',
                $user->merchants->sum(function (Merchant $item) {
                    return $item->marketplaceConnections->sum('item_active_count');
                }),
                $user->created_at->format(self::EXPORT_DATE_FORMAT),
                $user->email_verified_at ? $user->email_verified_at->format(self::EXPORT_DATE_FORMAT) : '—',
                $credentialsCreatedAt ? $credentialsCreatedAt->format(self::EXPORT_DATE_FORMAT) : '—',
                $trialPeriodFinish ? $trialPeriodFinish->format(self::EXPORT_DATE_FORMAT) : '—',
            ];

            foreach ($user->merchants as $merchant) {
                $n = 0;
                foreach ($merchant->billingOperations as $operation) {
                    if (++$n > 3) {
                        break;
                    }

                    $rowData[] = $operation->captured_at
                        ? $operation->captured_at->format(self::EXPORT_DATE_FORMAT)
                        : '?';
                    $rowData[] = $operation->amount;
                }
            }

            $writer->addRow(WriterEntityFactory::createRowFromArray($rowData));
        }

        $writer->close();
    }

    /**
     * Получить даты для графиков
     *
     * @param DashboardIndex $request
     * @return array
     */
    protected static function getDates(DashboardIndex $request): array
    {
        $dates = [];
        $date = new Carbon($request->getDateFrom());
        $compareDate = new Carbon($request->getComparePeriod()[0]);
        while ($date <= $request->getDateTo()) {
            $dates[$date->format(self::LOG_DATE_FORMAT)] = $compareDate->format(self::LOG_DATE_FORMAT);
            $date->addDay();
            $compareDate->addDay();
        }
        return $dates;
    }

    /**
     * Получить массив данных из лога событий
     *
     * @param DashboardIndex $request
     * @param bool $mainOnly
     * @return array
     */
    protected static function getData(DashboardIndex $request, bool $mainOnly = false): array
    {
        $data = [
            'main' => EventLog::query()
                ->selectRaw('DATE("triggered_at") AS "triggered_date", COUNT(DISTINCT "user_id") AS "user_count", "type"')
                ->whereBetween('triggered_at', $request->getPeriod())
                ->groupBy(['type', 'triggered_date'])
                ->orderBy('triggered_date')
                ->get()
                ->mapToGroups(function ($item) {
                    return [$item->type => $item];
                })
                ->transform(function (Collection $itemGroup){
                    return $itemGroup->pluck('user_count', 'triggered_date');
                })
                ->toArray(),
        ];
        if (!$mainOnly) {
            $data['compare'] = EventLog::query()
                ->selectRaw('DATE("triggered_at") AS "triggered_date", COUNT(DISTINCT "user_id") AS "user_count", "type"')
                ->whereBetween('triggered_at', $request->getComparePeriod())
                ->groupBy(['type', 'triggered_date'])
                ->orderBy('triggered_date')
                ->get()
                ->mapToGroups(function ($item) {
                    return [$item->type => $item];
                })
                ->transform(function (Collection $itemGroup){
                    return $itemGroup->pluck('user_count', 'triggered_date');
                })
                ->toArray();
        }

        return $data;
    }
}
