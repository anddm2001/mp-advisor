<?php

namespace App\Modules\ControlPanel\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Проверка отсутствия пересечений в диапазонах
 */
class RangesNotIntersecting implements Rule
{
    /** @var Builder Запрос */
    protected Builder $query;

    /** @var string|null Имя атрибута со значением начала диапазона */
    protected ?string $fromAttribute;

    /** @var string|null Имя атрибута со значением окончания диапазона */
    protected ?string $toAttribute;

    /**
     * Определение параметров
     *
     * @param string $class
     * @param string|null $primaryKey
     * @param array|null $scope
     * @param string|null $fromAttribute
     * @param string|null $toAttribute
     */
    public function __construct(
        string $class,
        ?string $primaryKey = null,
        ?array $scope = null,
        ?string $fromAttribute = null,
        ?string $toAttribute = null
    )
    {
        /** @var Model $instance */
        $instance = new $class();
        $this->query = $instance->newModelQuery();

        if ($primaryKey) {
            $this->query->whereKeyNot($primaryKey);
        }

        if ($scope) {
            $this->query->where($scope);
        }

        $this->fromAttribute = $fromAttribute;
        $this->toAttribute = $toAttribute;
    }

    /** @inheritDoc */
    public function passes($attribute, $value): bool
    {
        $fromAttribute = $this->fromAttribute ?? $attribute . '_from';
        $toAttribute = $this->toAttribute ?? $attribute . '_to';

        $fromValue = floatval($value['from']) ?? null;
        $toValue = floatval($value['to']) ?? null;

        if (!$fromValue && !$toValue) {
            return true;
        }

        $this->query->where(function (Builder $query) use ($fromAttribute, $toAttribute, $fromValue, $toValue) {
            $query->when($fromValue, function (Builder $query, float $fromValue) use ($toAttribute): Builder {
                return $query->where(function (Builder $query) use ($toAttribute, $fromValue) {
                    $query->where($toAttribute, '>=', $fromValue)->orWhereNull($toAttribute);
                });
            });
            $query->when($toValue, function (Builder $query, float $toValue) use ($fromAttribute): Builder {
                return $query->where(function (Builder $query) use ($fromAttribute, $toValue) {
                    $query->where($fromAttribute, '<=', $toValue)->orWhereNull($fromAttribute);
                });
            });
        });

        return $this->query->doesntExist();
    }

    /** @inheritDoc */
    public function message()
    {
        return 'Диапазоны (:attribute) не должны пересекаться с другими записями.';
    }
}