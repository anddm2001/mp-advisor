<?php

namespace App\Modules\ControlPanel\Models;

use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Billing\Models\BillingOperationInitiatorContract;
use Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswordTrait;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContact;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

/**
 * Модель админа — пользователя контрольной панели
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property string $first_name
 * @property string $last_name
 * @property bool $is_active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|BillingOperation[] $billingOperations
 * @property-read int|null $billing_operations_count
 * @property-read string $avatar_url
 * @property-read string $full_name
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 */
class Admin extends Authenticatable implements CanResetPasswordContact, BillingOperationInitiatorContract
{
    use CanResetPasswordTrait;
    use Notifiable;

    /** @inheritdoc */
    protected $table = 'admin';

    /** @inheritdoc */
    protected $fillable = [
        'email',
        'password',
        'remember_token',
        'first_name',
        'last_name',
        'is_active',
    ];

    /** @inheritdoc */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    // Relationships ================================================================================================ //

    /**
     * Операции биллинга, инициированные этим пользователем
     *
     * @return MorphMany
     */
    public function billingOperations(): MorphMany
    {
        return $this->morphMany(BillingOperation::class, 'initiator');
    }

    // Accessors & Mutators ========================================================================================= //

    /**
     * Получить полное имя администратора
     *
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return trim($this->first_name . ' ' . $this->last_name);
    }

    /**
     * Получить ссылку на аватарку пользователя
     *
     * @return string
     */
    public function getAvatarUrlAttribute(): string
    {
        return asset('img/avatar.jpg');
    }

    /** @inheritDoc */
    public function getBillingOperationInitiatorName(): string
    {
        return $this->full_name;
    }
}
