<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\Common\Models\MailLog;
use App\Modules\ControlPanel\Http\Requests\MailLogFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Illuminate\View\View;

/**
 * Логи писем
 */
class MailLogController extends Controller
{
    /** @var int Кол-во элементов на страницу */
    protected const PER_PAGE = 50;

    /** @var Builder */
    protected Builder $query;

    /**
     * Определяется билдер запросов для контроллера
     */
    public function __construct()
    {
        parent::__construct();
        $this->query = with(new MailLog())->newQuery()->with(['user', 'merchant']);
    }

    /**
     * Список записей логов
     *
     * @param MailLogFilter $request
     * @return View
     */
    public function index(MailLogFilter $request): View
    {
        $filter = $request->validated();
        $logs = $this->filter($request)->orderByDesc('id')->paginate(self::PER_PAGE);
        return view('cp.mail-logs.index', compact('filter', 'logs'));
    }

    /**
     * Фильтрация логов
     *
     * @param MailLogFilter $request
     * @return Builder
     */
    protected function filter(MailLogFilter $request): Builder
    {
        if ($request->input('email')) {
            $this->query->where('email', 'ilike', '%' . $request->input('email') . '%');
        }
        if ($request->input('user')) {
            $this->query->where('user_id', $request->input('user'));
        }
        if ($request->input('merchant')) {
            $this->query->where('merchant_id', $request->input('merchant'));
        }
        if ($request->input('date')) {
            $this->query->when($request->input('date.from'), function (Builder $query, $from): Builder {
                $date = Carbon::createFromFormat(MailLogFilter::DATE_FORMAT, $from);
                if ($date) {
                    $query->where('created_at', '>=', $date->setTime(0, 0));
                }
                return $query;
            });
            $this->query->when($request->input('date.to'), function (Builder $query, $to): Builder {
                $date = Carbon::createFromFormat(MailLogFilter::DATE_FORMAT, $to);
                if ($date) {
                    $query->where('created_at', '<=', $date->setTime(23, 59, 59));
                }
                return $query;
            });
        }
        if ($request->input('type')) {
            $this->query->where('type', $request->input('type'));
        }
        return $this->query;
    }
}
