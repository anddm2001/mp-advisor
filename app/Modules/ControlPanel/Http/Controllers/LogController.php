<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\Common\Models\Log;
use App\Modules\ControlPanel\Http\Requests\LogFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\View;

/**
 * Логи
 */
class LogController extends Controller
{
    /** @var int Кол-во элементов на страницу */
    protected const PER_PAGE = 100;

    /** @var Builder */
    protected Builder $query;

    /**
     * Определяется билдер запросов для контроллера
     */
    public function __construct()
    {
        parent::__construct();
        $this->query = with(new Log())->newQuery();
    }

    /**
     * Список записей логов
     *
     * @param LogFilter $request
     * @return View
     */
    public function index(LogFilter $request): View
    {
        $filter = $request->validated();
        $logs = $this->filter($request)->orderByDesc('id')->paginate(self::PER_PAGE);
        return view('cp.logs.index', compact('filter', 'logs'));
    }

    /**
     * Фильтрация логов
     *
     * @param LogFilter $request
     * @return Builder
     */
    protected function filter(LogFilter $request): Builder
    {
        if ($request->input('category')) {
            $this->query->where('category', $request->input('category'));
        }
        return $this->query;
    }
}
