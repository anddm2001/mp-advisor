<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\Auth\Models\User;
use App\Modules\ControlPanel\Http\Requests\DashboardExportDetails;
use App\Modules\ControlPanel\Http\Requests\DashboardIndex;
use App\Modules\ControlPanel\Services\StatisticService;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Illuminate\Support\Carbon;
use Illuminate\View\View;

/**
 * Стартовая страница - обзор
 */
class DashboardController extends Controller
{
    /**
     * Отображение стартовой страницы
     *
     * @param DashboardIndex $request
     * @return View
     */
    public function index(DashboardIndex $request): View
    {
        return view('cp.dashboard.index', [
            'periodsParams' => $request->getPeriodsParams(),
            'charts' => StatisticService::getChartsData($request),
        ]);
    }

    /**
     * Экспорт данных
     *
     * @param DashboardIndex $request
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function export(DashboardIndex $request): void
    {
        StatisticService::exportChartsData($request);
    }

    /**
     * Экспорт данных пользователей за период
     *
     * @param DashboardExportDetails $request
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function exportDetails(DashboardExportDetails $request): void
    {
        StatisticService::exportUsersData($request);
    }
}
