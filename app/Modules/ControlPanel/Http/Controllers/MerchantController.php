<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Billing\Models\BillingOperationInitiatorContract;
use App\Modules\ControlPanel\Http\Requests\MerchantChargeOn;
use App\Modules\ControlPanel\Http\Requests\MerchantConnectionUpdate;
use App\Modules\ControlPanel\Http\Requests\MerchantFilter;
use App\Modules\ControlPanel\Http\Requests\MerchantStartSync;
use App\Modules\ControlPanel\Http\Requests\MerchantStore;
use App\Modules\ControlPanel\Http\Requests\MerchantTopUp;
use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Models\MerchantConnectionSync;
use App\Modules\Merchant\Models\MerchantUser;
use App\Modules\Merchant\Requests\StoreMerchantConnectionRequest;
use App\Modules\Report\Contracts\MarketPlaceContract;
use App\Modules\Report\Events\WildberriesApiCollectDataEvent;
use App\Modules\Report\Events\WildberriesApiScheduledDataSyncEvent;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

/**
 * Управление мерчантами
 */
class MerchantController extends Controller
{
    /** @var int Кол-во элементов на страницу */
    protected const PER_PAGE = 100;

    /** @var Builder */
    protected Builder $query;

    /**
     * Определяется билдер запросов для контроллера
     */
    public function __construct()
    {
        parent::__construct();
        $this->query = with(new Merchant())->newQuery();
    }

    /**
     * Список пользователей
     *
     * @param MerchantFilter $request
     * @return View
     */
    public function index(MerchantFilter $request): View
    {
        $filter = $request->validated();
        $query = $this->filter($request)->with(['marketplaceConnections', 'users', 'users.tokens' => function (MorphMany $query) {
            $query->orderByDesc('last_used_at');
        }])->orderByDesc('id');
        $merchantsCount = $query->count();
        $merchants = $query->paginate(self::PER_PAGE);
        return view('cp.merchants.index', compact('filter', 'merchants', 'merchantsCount'));
    }

    /**
     * Фильтрация списка пользователей
     *
     * @param MerchantFilter $request
     * @return Builder
     */
    protected function filter(MerchantFilter $request): Builder
    {
        if ($request->input('merchant')) {
            $this->query->where('id', $request->input('merchant'));
        }
        if ($request->input('user')) {
            $this->query->whereHas('owner', function (Builder $query) use ($request) {
                $query->where('id', $request->input('user'));
            });
        }
        if ($request->input('items')) {
            $this->query->when($request->input('items.from'), function (Builder $query, $from): Builder {
                return $query->whereHas('marketplaceConnections', function (Builder $query) use ($from) {
                    $query->where('item_active_count', '>=', $from);
                });
            });
            $this->query->when($request->input('items.to'), function (Builder $query, $to): Builder {
                return $query->whereHas('marketplaceConnections', function (Builder $query) use ($to) {
                    $query->where('item_active_count', '<=', $to);
                });
            });
        }
        if ($request->input('status')) {
            $this->query->whereHas('marketplaceConnections', function (Builder $query) use ($request) {
                switch ($request->input('status')) {
                    case 'success':
                        $query->whereNotNull('last_sync')->where(function (Builder $query) {
                            $query->whereNull('error')->orWhere('error', '');
                        });
                        break;
                    case 'error':
                        $query->whereNotNull('last_sync')->whereNotNull('error')->where('error', '!=', '');
                        break;
                    case 'none':
                        $query->whereNull('last_sync');
                        break;
                }
            })->when($request->input('status') === 'none', function (Builder $query, $isNoneStatus): Builder {
                return $query->orWhereDoesntHave('marketplaceConnections');
            });
        }
        return $this->query;
    }

    /**
     * Форма добавления мерчанта
     *
     * @param Request $request
     * @return View
     */
    public function create(Request $request): View
    {
        $merchant = $this->query->newModelInstance($request->only('owner_id'));
        return view('cp.merchants.create', compact('merchant'));
    }

    /**
     * Добавление мерчанта
     *
     * @param MerchantStore $request
     * @return RedirectResponse
     */
    public function store(MerchantStore $request): RedirectResponse
    {
        $validatedData = $request->validated();
        $users = Arr::pull($validatedData, 'users', []);
        if (!in_array($validatedData['owner_id'], $users)) {
            $users = Arr::prepend($users, $validatedData['owner_id']);
        }

        /** @var Merchant $merchant */
        $merchant = $this->query->create($validatedData);

        // Wildberries
        $merchant->marketplaceConnections()->create([
            'marketplace' => MarketPlaceContract::WILDBERRIES,
            'credentials' => '',
        ]);

        if (!empty($users)) {
            foreach ($users as $userId) {
                with(new MerchantUser())->newModelQuery()->create([
                    'merchant_id' => $merchant->id,
                    'user_id' => $userId,
                ]);
            }
        }
        return Redirect::route('cp.merchants.edit', compact('merchant'))->withFragment('success');
    }

    /**
     * Форма редактирования мерчанта
     *
     * @param Merchant $merchant
     * @param MerchantConnectionManagerContract $service
     * @return View
     */
    public function edit(Merchant $merchant, MerchantConnectionManagerContract $service): View
    {
        return view('cp.merchants.edit', compact('service', 'merchant'));
    }

    /**
     * Обновление параметров доступа к маркетплейсу
     *
     * @param Merchant $merchant
     * @param MerchantConnection $connection
     * @param Request $request
     * @param MerchantConnectionManagerContract $service
     * @return RedirectResponse
     */
    public function updateConnection(
        Merchant $merchant,
        MerchantConnection $connection,
        Request $request,
        MerchantConnectionManagerContract $service
    ): RedirectResponse
    {
        $marketplace = $connection->marketplace;
        $credentialsKeys = array_keys($service->getCredentialsNames($marketplace));
        $requestData = array_merge($request->only($credentialsKeys), compact('marketplace'));
        $storeConnectionRequest = StoreMerchantConnectionRequest::createFromArray($requestData, $merchant->owner);

        if ($service->store($storeConnectionRequest, $merchant, $connection)) {
            return back()->withFragment('success');
        }

        return back()->withFragment('failed');
    }

    /**
     * Данные о синхронизации
     *
     * @param Merchant $merchant
     * @return View
     */
    public function sync(Merchant $merchant): View
    {
        $syncs = $merchant->marketplaceConnectionSyncs()
            ->with('connection')
            ->orderByDesc('id')
            ->paginate(self::PER_PAGE);
        return view('cp.merchants.sync', compact('merchant', 'syncs'));
    }

    /**
     * Запустить синхронизацию данных мерчанта
     *
     * @param Merchant $merchant
     * @param MerchantStartSync $request
     * @param MerchantConnectionManagerContract $service
     * @param Dispatcher $events
     * @return RedirectResponse
     */
    public function startSync(
        Merchant $merchant,
        MerchantStartSync $request,
        MerchantConnectionManagerContract $service,
        Dispatcher $events
    ): RedirectResponse
    {
        $marketplace = $request->input('marketplace', 0);
        /** @var ?MerchantConnection $connection */
        $connection = $merchant->marketplaceConnections()->where('marketplace', $marketplace)->first();
        if (!$connection) {
            return back()->withFragment('failed');
        }

        $credentials = $service->getCredentialsValues($connection);

        $type = $request->input('type');
        switch ($marketplace) {
            case MarketPlaceContract::WILDBERRIES:
                $key = $credentials['key'] ?? null;
                if (!$key) {
                    return back()->withFragment('failed');
                }

                switch ($type) {
                    case MerchantStartSync::TYPE_WB_REGULAR:
                        $syncType = MerchantConnectionSync::TYPE_MANUAL_CP;
                        $events->dispatch(new WildberriesApiCollectDataEvent($connection, $key, $syncType, true));
                        break;
                    case MerchantStartSync::TYPE_WB_SCHEDULED_YESTERDAY:
                        $syncType = WildberriesApiScheduledDataSyncEvent::TYPE_YESTERDAY;
                        $events->dispatch(new WildberriesApiScheduledDataSyncEvent($connection, $key, $syncType));
                        break;
                    case MerchantStartSync::TYPE_WB_SCHEDULED_PREV_MONTH:
                        $syncType = WildberriesApiScheduledDataSyncEvent::TYPE_PREV_MONTH;
                        $events->dispatch(new WildberriesApiScheduledDataSyncEvent($connection, $key, $syncType));
                        break;
                    case MerchantStartSync::TYPE_WB_MAXIMUM_EFFORT:
                        $syncType = MerchantConnectionSync::TYPE_MAXIMUM_CP;
                        $events->dispatch(new WildberriesApiCollectDataEvent($connection, $key, $syncType, true));
                        break;
                }
                break;
        }

        return back()->withFragment('queued');
    }

    /**
     * Данные биллинга
     *
     * @param Merchant $merchant
     * @param Request $request
     * @return View
     */
    public function billing(Merchant $merchant, Request $request): View
    {
        $operations = $merchant->billingOperations()->orderByDesc('id')->paginate(self::PER_PAGE);
        return view('cp.merchants.billing', compact('merchant', 'operations'));
    }

    /**
     * Ручное пополнение баланса мерчанта
     *
     * @param Merchant $merchant
     * @param MerchantTopUp $request
     * @return RedirectResponse
     */
    public function topUp(Merchant $merchant, MerchantTopUp $request): RedirectResponse
    {
        /** @var BillingOperationInitiatorContract $adminUser */
        $adminUser = $this->adminUser;

        $merchant->billingOperations()->create([
            'initiator_type' => $this->adminUser->getMorphClass(),
            'initiator_id' => $this->adminUser->getKey(),
            'billing_rate_id' => $merchant->billing_rate_id,
            'billing_rate_name' => $merchant->billingRate ? $merchant->billingRate->name : null,
            'type' => BillingOperation::TYPE_PAYMENT,
            'status' => BillingOperation::STATUS_SUCCESS,
            'amount' => $request->input('amount'),
            'description' => 'Пополнение баланса для мерчанта №' . $merchant->id,
            'payment_system' => BillingOperation::PAYMENT_SYSTEM_MPADVISOR,
            'captured_at' => Carbon::now(),
        ]);

        $merchant->updateBalance(true);

        return back()->withFragment('success');
    }

    /**
     * Изменение даты следующего списания у мерчанта
     *
     * @param Merchant $merchant
     * @param MerchantChargeOn $request
     * @return RedirectResponse
     */
    public function chargeOn(Merchant $merchant, MerchantChargeOn $request): RedirectResponse
    {
        $merchant->update([
            'charge_on' => $request->input('date'),
        ]);

        return back()->withFragment('success');
    }
}
