<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\ControlPanel\Http\Requests\AuthLogin;
use App\Modules\ControlPanel\Http\Requests\AuthPasswordSend;
use App\Modules\ControlPanel\Http\Requests\AuthPasswordUpdate;
use App\Modules\ControlPanel\Models\Admin;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Contracts\Auth\PasswordBroker as PasswordBrokerContact;
use Illuminate\Contracts\Auth\PasswordBrokerFactory;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

/**
 * Аутентификация, восстановление пароля
 */
class AuthController extends Controller
{
    /** @var PasswordBrokerContact|PasswordBroker */
    protected PasswordBrokerContact $passwordBroker;

    /** @var Hasher */
    protected Hasher $hasher;

    /**
     * Определение брокера и хэшера паролей
     *
     * @param PasswordBrokerFactory $passwordBrokerManager
     * @param Hasher $hasher
     */
    public function __construct(PasswordBrokerFactory $passwordBrokerManager, Hasher $hasher)
    {
        $this->passwordBroker = $passwordBrokerManager->broker('admins');
        $this->hasher = $hasher;
        parent::__construct();
    }

    /**
     * Вход по логину и паролю
     *
     * @param AuthLogin $request
     * @return RedirectResponse
     */
    public function login(AuthLogin $request): RedirectResponse
    {
        $credentials = $request->only(['email', 'password']);
        $credentials['is_active'] = true;
        $remember = $request->input('remember') ?? false;
        /** @var StatefulGuard $guard */
        $guard = Auth::guard('cp');

        if ($guard->attempt($credentials, $remember)) {
            $request->session()->regenerate();
            return redirect()->intended(route('cp.dashboard', [], false));
        }

        return Redirect::back()->withErrors(['password' => 'Пользователь не найден'])->withInput($request->except(['password']));
    }

    /**
     * Выход
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::route('cp.dashboard');
    }

    /**
     * Страница запроса сброса пароля
     *
     * @return View
     */
    public function passwordRequest(): View
    {
        return view('cp.auth.password-request');
    }

    /**
     * Отправка уведомления со ссылкой для сброса пароля
     *
     * @param AuthPasswordSend $request
     * @return RedirectResponse
     */
    public function passwordSend(AuthPasswordSend $request): RedirectResponse
    {
        ResetPassword::createUrlUsing(function ($user, string $token) {
            return route('cp.auth.password.reset', compact('token'));
        });

        $email = Arr::get($request->validated(), 'email');

        $status = $this->passwordBroker->sendResetLink(compact('email'));

        return $status === PasswordBrokerContact::RESET_LINK_SENT
            ? Redirect::back()->with(['status' => __($status)])
            : Redirect::back()->withErrors(['email' => __($status)]);
    }

    /**
     * Страница для ввода нового пароля
     *
     * @param string $token
     * @return View
     */
    public function passwordReset(string $token): View
    {
        return view('cp.auth.password-reset', compact('token'));
    }

    /**
     * Смена пароля
     *
     * @param string $token
     * @param AuthPasswordUpdate $request
     * @return RedirectResponse
     */
    public function passwordUpdate(string $token, AuthPasswordUpdate $request): RedirectResponse
    {
        $credentials = $request->only(['email', 'password', 'password_confirmation']);
        Arr::set($credentials, 'token', $token);

        $status = $this->passwordBroker->reset($credentials, function (Admin $administrator, string $password): void {
            $password = $this->hasher->make($password);
            $remember_token = null;
            $administrator->update(compact('password', 'remember_token'));
        });

        return $status === PasswordBrokerContact::PASSWORD_RESET
            ? Redirect::route('cp.auth.login')->with('status', __($status))
            : Redirect::back()->withErrors(['email' => __($status)]);
    }
}
