<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\Auth\Models\User;
use App\Modules\ControlPanel\Http\Requests\UserFilter;
use App\Modules\ControlPanel\Http\Requests\UserPassword;
use App\Modules\ControlPanel\Http\Requests\UserStore;
use App\Modules\ControlPanel\Http\Requests\UserUpdate;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

/**
 * Управление пользователями
 */
class UserController extends Controller
{
    /** @var int Кол-во пользователей на страницу */
    protected const PER_PAGE = 50;

    /** @var Builder */
    protected Builder $query;

    /**
     * Определяется билдер запросов для контроллера
     */
    public function __construct()
    {
        parent::__construct();
        $this->query = with(new User())->newQuery();
    }

    /**
     * Список пользователей
     *
     * @param UserFilter $request
     * @return View
     */
    public function index(UserFilter $request): View
    {
        $filter = $request->validated();
        $query = $this->filter($request)->with(['merchants', 'tokens' => function (MorphMany $query) {
            $query->orderByDesc('last_used_at');
        }])->orderByDesc('id');
        $usersCount = $query->count();
        $users = $query->paginate(self::PER_PAGE);
        return view('cp.users.index', compact('filter', 'users', 'usersCount'));
    }

    /**
     * Фильтрация списка пользователей
     *
     * @param UserFilter $request
     * @return Builder
     */
    protected function filter(UserFilter $request): Builder
    {
        if ($request->input('email')) {
            $this->query->where('email', 'ilike', '%' . $request->input('email') . '%');
        }
        if ($request->input('name')) {
            $this->query->where('name', 'ilike', '%' . $request->input('name') . '%');
        }
        if ($request->input('merchant')) {
            $this->query->whereHas('merchants', function (Builder $query) use ($request) {
                $query->where('id', $request->input('merchant'));
            });
        }
        if ($request->input('user')) {
            $this->query->where('id', $request->input('user'));
        }
        if ($request->input('company')) {
            $this->query->where('company_name', 'ilike', '%' . $request->input('company') . '%');
        }
        return $this->query;
    }

    /**
     * Форма добавления пользователя
     *
     * @return View
     */
    public function create(): View
    {
        $user = $this->query->newModelInstance();
        return view('cp.users.create', compact('user'));
    }

    /**
     * Добавление пользователя
     *
     * @param UserStore $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function store(UserStore $request): RedirectResponse
    {
        $validatedData = $request->validated();
        $status = Arr::pull($validatedData, 'status');
        $validatedData['password'] = Hash::make($validatedData['password']);
        /** @var User $user */
        $user = $this->query->create($validatedData);
        if ($status) {
            $this->updateUserStatus($user, $status, true);
        }
        return Redirect::route('cp.users.index')->withFragment('success');
    }

    /**
     * Форма редактирования пользователя
     *
     * @param User $user
     * @return View
     */
    public function edit(User $user): View
    {
        $lastUsedToken = $user->tokens()->orderByDesc('last_used_at')->first();
        return view('cp.users.edit', compact('user', 'lastUsedToken'));
    }

    /**
     * Сохранение данных пользователя
     *
     * @param User $user
     * @param UserUpdate $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function update(User $user, UserUpdate $request): RedirectResponse
    {
        $validatedData = $request->validated();
        $status = Arr::pull($validatedData, 'status');
        if ($user->update($validatedData) && $status) {
            $this->updateUserStatus($user, $status);
        }
        return Redirect::route('cp.users.edit', compact('user'))->withFragment('success');
    }

    /**
     * Обновить статус пользователя с соответствующими уведомлениями
     *
     * @param User $user
     * @param string $status
     * @param bool $forceUpdate
     * @throws Exception
     */
    protected function updateUserStatus(User $user, string $status, bool $forceUpdate = false): void
    {
        if ($status !== $user->status || $forceUpdate) {
            switch ($status) {
                case User::STATUS_PENDING:
                    // сброс активации
                    if ($user->forceFill(['email_verified_at' => null])->save()) {
                        // письмо со ссылкой на активацию
                        $user->sendEmailVerificationNotification();
                    }
                    break;
                case User::STATUS_ACTIVE:
                    // если у пользователя ещё нет мерчантов
                    if ($user->merchants()->doesntExist()) {
                        // создаём и связываем мерчанта
                        $user->createMerchant();
                    }
                    // автивация - пометка проверенным
                    if ($user->markEmailAsVerified()) {
                        // приветственное письмо
                        $user->sendUserWelcomeNotification();
                    }
                    break;
            }
        }
    }

    /**
     * Смена пароля пользователя
     *
     * @param User $user
     * @param UserPassword $request
     * @return RedirectResponse
     */
    public function password(User $user, UserPassword $request): RedirectResponse
    {
        $password = Hash::make(Arr::get($request->validated(), 'password'));
        $user->update(compact('password'));
        $user->tokens()->delete();
        return Redirect::route('cp.users.edit', compact('user'))->withFragment('success');
    }
}
