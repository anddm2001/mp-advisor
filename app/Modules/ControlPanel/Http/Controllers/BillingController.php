<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\Billing\Models\BillingOperation;
use App\Modules\ControlPanel\Http\Requests\BillingFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Illuminate\View\View;

/**
 * Операции биллинга
 */
class BillingController extends Controller
{
    /** @var string Формат дат */
    public const DATE_FORMAT = 'd.m.Y';

    /** @var int Кол-во элементов на страницу */
    protected const PER_PAGE = 100;

    /** @var Builder */
    protected Builder $operationQuery;

    /**
     * Определяются билдеры запросов для контроллера
     */
    public function __construct()
    {
        parent::__construct();
        $this->operationQuery = with(new BillingOperation())->newQuery()
            ->with(['merchant', 'marketplaceConnection'])
            ->orderByDesc('id');
    }

    /**
     * История операций
     *
     * @param BillingFilter $request
     * @return View
     */
    public function index(BillingFilter $request): View
    {
        $filter = $request->validated();

        $operations = $this->operationsFilter($request)->paginate(self::PER_PAGE);

        return view('cp.billing.index', compact('filter', 'operations'));
    }

    /**
     * Фильтрация операций
     *
     * @param BillingFilter $request
     * @return Builder
     */
    protected function operationsFilter(BillingFilter $request): Builder
    {
        if ($request->input('id')) {
            $this->operationQuery->where('id', 'like', '%' . $request->input('id') . '%');
        }
        if ($request->input('merchant')) {
            $this->operationQuery->where('merchant_id', $request->input('merchant'));
        }
        if ($request->input('amount')) {
            $this->operationQuery->when($request->input('amount.from'), function (Builder $query, $from): Builder {
                return $query->where('amount', '>=', $from);
            });
            $this->operationQuery->when($request->input('amount.to'), function (Builder $query, $to): Builder {
                return $query->where('amount', '<=', $to);
            });
        }
        if ($request->input('date')) {
            $this->operationQuery->when($request->input('date.from'), function (Builder $query, $from): Builder {
                $date = Carbon::createFromFormat(self::DATE_FORMAT, $from);
                if ($date) {
                    $query->where('created_at', '>=', $date->setTime(0, 0));
                }
                return $query;
            });
            $this->operationQuery->when($request->input('date.to'), function (Builder $query, $to): Builder {
                $date = Carbon::createFromFormat(self::DATE_FORMAT, $to);
                if ($date) {
                    $query->where('created_at', '<=', $date->setTime(23, 59, 59));
                }
                return $query;
            });
        }
        if ($request->input('type')) {
            $this->operationQuery->where('type', $request->input('type'));
        }

        return $this->operationQuery;
    }
}
