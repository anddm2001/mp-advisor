<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\ControlPanel\Http\Requests\SupplyDestroy;
use App\Modules\ControlPanel\Http\Requests\SupplyImport;
use App\Modules\ControlPanel\Http\Requests\SupplyIndex;
use App\Modules\ControlPanel\Http\Requests\SupplySave;
use App\Modules\ControlPanel\Services\SupplyImportNomenclatureSettings;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Services\WildberriesReportApi;
use Box\Spout\Common\Exception\SpoutException;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Report\CreateNomenclatureSettingsRequest;
use Report\ListNomenclatureSettingsResponse;
use Report\NomenclatureSettings;
use Report\PageResponse;

/**
 * Управление настройками поставок
 */
class SupplyController extends Controller
{
    /** @var WildberriesReportApi */
    protected WildberriesReportApi $api;

    /**
     * Определяется билдер запросов для контроллера
     */
    public function __construct(WildberriesReportApi $api)
    {
        parent::__construct();
        $this->api = $api;
    }

    /**
     * Таблица квантов поставок по умолчанию
     *
     * @param SupplyIndex $request
     * @param PageRequest $pageRequest
     * @return View
     */
    public function index(SupplyIndex $request, PageRequest $pageRequest): View
    {
        $filter = $request->getFilter();
        $apiResponse = $this->api->listNomenclatureSettings($request->getSubject(), $pageRequest);
        $items = collect($apiResponse->getItems());
        /** @var PageResponse|null $pageResponse */
        $pageResponse = $apiResponse->getPage();
        $pagination = $pageRequest->getNextPreviousPageURL(
            $pageResponse && $pageResponse->getHasNextPage(),
            $pageResponse && $pageResponse->getHasPreviousPage()
        );

        return view('cp.supply.index', compact('filter', 'items', 'pagination'));
    }

    /**
     * Импорт квантов поставок из файла
     *
     * @param SupplyImport $request
     * @param SupplyImportNomenclatureSettings $service
     * @return RedirectResponse
     * @throws Exception
     */
    public function import(SupplyImport $request, SupplyImportNomenclatureSettings $service): RedirectResponse
    {
        if ($request->hasFile('file')) {
            try {
                $service->import($request->file('file')->getRealPath());
            } catch (SpoutException $exception) {
                return back()->withFragment('failed');
            }
        }
        return back()->withFragment('imported');
    }

    /**
     * Добавить квант поставки для категории
     *
     * @param SupplySave $request
     * @return RedirectResponse
     */
    public function store(SupplySave $request): RedirectResponse
    {
        $this->api->createNomenclatureSettings($request->getAPICreateRequest());
        return back()->withFragment('success');
    }

    /**
     * Обновить квант поставки для категории
     *
     * @param SupplySave $request
     * @return JsonResponse
     */
    public function update(SupplySave $request): JsonResponse
    {
        $apiResponse = $this->api->updateNomenclatureSettings($request->getAPIUpdateRequest());
        return new JsonResponse($apiResponse->getDefaultQuantum() === $request->getQuantum() ? 'ok' : 'failed');
    }

    /**
     * Удалить квант поставки по умолчанию для категории
     *
     * @param SupplyDestroy $request
     * @return RedirectResponse
     */
    public function destroy(SupplyDestroy $request): RedirectResponse
    {
        $this->api->deleteNomenclatureSettings($request->getAPIRequest());
        return back()->withFragment('deleted');
    }
}
