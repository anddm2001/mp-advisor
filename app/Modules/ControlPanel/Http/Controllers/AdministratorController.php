<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\ControlPanel\Http\Requests\AdministratorPassword;
use App\Modules\ControlPanel\Http\Requests\AdministratorStore;
use App\Modules\ControlPanel\Http\Requests\AdministratorUpdate;
use App\Modules\ControlPanel\Models\Admin;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

/**
 * Управление администраторами
 */
class AdministratorController extends Controller
{
    /** @var Builder */
    protected Builder $query;

    /**
     * Определяется билдер запросов для контроллера
     */
    public function __construct()
    {
        parent::__construct();
        $this->query = with(new Admin())->newQuery();
    }

    /**
     * Список администраторов
     *
     * @return View
     */
    public function index(): View
    {
        $administrators = $this->query->orderBy('last_name')->orderBy('first_name')->paginate();
        return view('cp.administrators.index', compact('administrators'));
    }

    /**
     * Форма добавления администратора
     *
     * @return View
     */
    public function create(): View
    {
        $administrator = $this->query->newModelInstance();
        return view('cp.administrators.create', compact('administrator'));
    }

    /**
     * Добавление администратора
     *
     * @param AdministratorStore $request
     * @return RedirectResponse
     */
    public function store(AdministratorStore $request): RedirectResponse
    {
        $administratorData = $request->validated();
        $administratorData['password'] = Hash::make($administratorData['password']);
        $this->query->create($administratorData);
        return Redirect::route('cp.administrators.index')->withFragment('success');
    }

    /**
     * Форма редактирования администратора
     *
     * @param Admin $administrator
     * @return View
     */
    public function edit(Admin $administrator): View
    {
        return view('cp.administrators.edit', compact('administrator'));
    }

    /**
     * Сохранение данных администратора
     *
     * @param Admin $administrator
     * @param AdministratorUpdate $request
     * @return RedirectResponse
     */
    public function update(Admin $administrator, AdministratorUpdate $request): RedirectResponse
    {
        $administrator->update($request->validated());
        return Redirect::route('cp.administrators.index')->withFragment('success');
    }

    /**
     * Смена пароля администратора
     *
     * @param Admin $administrator
     * @param AdministratorPassword $request
     * @return RedirectResponse
     */
    public function password(Admin $administrator, AdministratorPassword $request): RedirectResponse
    {
        $password = Hash::make(Arr::get($request->validated(), 'password'));
        $remember_token = null;
        $administrator->update(compact('password', 'remember_token'));
        DB::table('sessions')->where('user_id', $administrator->id)->delete();
        return Redirect::route('cp.administrators.edit', compact('administrator'))->withFragment('success');
    }
}
