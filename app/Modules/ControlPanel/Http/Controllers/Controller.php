<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\ControlPanel\Models\Admin;
use Closure;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

/**
 * Базовый контроллер панели управления
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /** @var Admin Аутентифицированный пользователь */
    protected Admin $adminUser;

    /**
     * Определяется аутентифицированный пользователь и шарится на контролеры и вьюхи панели управления
     */
    public function __construct()
    {
        $this->middleware(function (Request $request, Closure $next) {
            $this->adminUser = $request->user('cp') ?? new Admin();
            View::share('adminUser', $this->adminUser);

            return $next($request);
        });

        // паджинация на бутстрапе
        Paginator::useBootstrap();

        // название текущего роута
        View::share('currentRouteName', Route::currentRouteName());
    }
}
