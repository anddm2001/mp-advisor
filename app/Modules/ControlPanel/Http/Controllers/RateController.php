<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\Billing\Models\BillingRate;
use App\Modules\ControlPanel\Http\Requests\RateSave;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

/**
 * Управление тарифами
 */
class RateController extends Controller
{
    /** @var Builder */
    protected Builder $query;

    /**
     * Определяется билдер запросов для контроллера
     */
    public function __construct()
    {
        parent::__construct();
        $this->query = with(new BillingRate())->newQuery();
    }

    /**
     * Список тарифов
     *
     * @return View
     */
    public function index(): View
    {
        $rates = $this->query->orderByDesc('is_active')->orderBy('min_item_count')->get();
        return view('cp.rates.index', compact('rates'));
    }

    /**
     * Форма добавления тарифа
     *
     * @return View
     */
    public function create(): View
    {
        $rate = $this->query->newModelInstance();
        return view('cp.rates.create', compact('rate'));
    }

    /**
     * Добавление тарифа
     *
     * @param RateSave $request
     * @return RedirectResponse
     */
    public function store(RateSave $request): RedirectResponse
    {
        $validatedData = $request->validated();
        $itemCount = Arr::pull($validatedData, 'item_count');
        /** @var BillingRate $rate */
        $rate = $this->query->newModelInstance($validatedData);
        $rate->item_count = $itemCount;
        $rate->save();

        return Redirect::route('cp.rates.index')->withFragment('success');
    }

    /**
     * Форма редактирования тарифа
     *
     * @param BillingRate $rate
     * @return View
     */
    public function edit(BillingRate $rate): View
    {
        return view('cp.rates.edit', compact('rate'));
    }

    /**
     * Сохранение данных тарифа
     *
     * @param BillingRate $rate
     * @param RateSave $request
     * @return RedirectResponse
     */
    public function update(BillingRate $rate, RateSave $request): RedirectResponse
    {
        $validatedData = $request->validated();
        $itemCount = Arr::pull($validatedData, 'item_count');
        $rate->fill($validatedData);
        $rate->item_count = $itemCount;
        $rate->save();

        return Redirect::route('cp.rates.index')->withFragment('success');
    }

    /**
     * Удаление тарифа
     *
     * @param BillingRate $rate
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(BillingRate $rate): RedirectResponse
    {
        $rate->delete();
        return Redirect::route('cp.rates.index')->withFragment('deleted');
    }
}
