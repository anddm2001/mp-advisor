<?php

namespace App\Modules\ControlPanel\Http\Controllers;

use App\Modules\Common\Models\LinkHandbook;
use App\Modules\ControlPanel\Http\Requests\LinkHandbookStore;
use App\Modules\ControlPanel\Http\Requests\LinkHandbookUpdate;
use App\Modules\ControlPanel\Http\Requests\LinkHandbookUpload;
use App\Modules\Storage\Contracts\StorageServiceContract;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

/**
 * Управление справочником ссылок
 */
class LinkHandbookController extends Controller
{
    /**
     * Таблица ссылок
     *
     * @return View
     */
    public function index(): View
    {
        if (Session::has('errors')) {
            dd(Session::get('errors'));
        }
        $links = LinkHandbook::all();
        return view('cp.links.index', compact('links'));
    }

    /**
     * Добавить ссылку
     *
     * @param LinkHandbookStore $request
     * @return RedirectResponse
     */
    public function store(LinkHandbookStore $request): RedirectResponse
    {
        LinkHandbook::query()->create($request->validated());
        return back()->withFragment('success');;
    }

    /**
     * Обновить ссылку
     *
     * @param LinkHandbook $link
     * @param LinkHandbookUpdate $request
     * @return RedirectResponse
     */
    public function update(LinkHandbook $link, LinkHandbookUpdate $request): RedirectResponse
    {
        $validatedData = $request->validated();
        $bagName = 'update' . $link->id;
        $validatedData = collect($validatedData)->flip()->map(function (string $item) use ($bagName): string {
            return str_replace($bagName . '-', '', $item);
        })->flip()->toArray();
        if ($validatedData) {
            $link->update($validatedData);
        }
        return back()->withFragment('success');;
    }

    /**
     * Загрузка файла в хранилище для справочника
     *
     * @param LinkHandbookUpload $request
     * @param StorageServiceContract $storageService
     * @return RedirectResponse
     */
    public function upload(LinkHandbookUpload $request, StorageServiceContract $storageService): RedirectResponse
    {
        return back()->with('uploaded_file_url', $storageService->simpleUpload($request->file('file'), 'links'));
    }
}
