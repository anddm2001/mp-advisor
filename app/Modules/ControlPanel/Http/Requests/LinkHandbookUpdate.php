<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\Common\Models\LinkHandbook;

/**
 * Запрос на обновление ссылки в справочник
 */
class LinkHandbookUpdate extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            $this->errorBag . '-name' => ['required', 'string', 'max:255'],
            $this->errorBag . '-link' => ['required', 'string', 'max:1000', 'url'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation(): void
    {
        /** @var LinkHandbook $link */
        $link = $this->route('link', new LinkHandbook());
        $this->errorBag = 'update' . ($link->id ?? '0');
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            $this->errorBag . '-name' => 'Название',
            $this->errorBag . '-link' => 'Ссылка',
        ];
    }
}
