<?php

namespace App\Modules\ControlPanel\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as BaseFormRequest;

/**
 * Обработка запроса в рамках панели управления
 */
abstract class FormRequest extends BaseFormRequest
{
    //
}
