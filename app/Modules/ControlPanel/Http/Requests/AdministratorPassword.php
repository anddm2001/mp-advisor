<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Запрос на изменение пароля администратора
 */
class AdministratorPassword extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'password' => ['required', 'string', 'confirmed'],
            'admin_password' => ['required', 'string', 'password:cp'],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'password' => 'Новый пароль',
            'admin_password' => 'Пароль текущего администратора',
        ];
    }
}
