<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Название даты списания у мерчанта
 */
class MerchantChargeOn extends FormRequest
{
    public const DATE_FORMAT = 'd.m.Y H:i:s';

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'date' => ['required', 'date_format:' . self::DATE_FORMAT, 'after_or_equal:now'],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'date' => 'Дата следующего списания',
        ];
    }
}
