<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\Billing\Models\BillingRate;
use App\Modules\ControlPanel\Rules\RangesNotIntersecting;

/**
 * Запрос на сохранение тарифа
 */
class RateSave extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        /** @var BillingRate|null $rate */
        $rate = $this->route('rate');

        return [
            'name' => ['required', 'string'],
            'amount' => ['required', 'numeric'],
            'min_amount' => ['nullable', 'numeric'],
            'item_count' => ['required', 'array', new RangesNotIntersecting(
                BillingRate::class,
                $rate ? $rate->getKey() : null,
                ['is_active' => true],
                'min_item_count',
                'max_item_count'
            )],
            'item_count.from' => ['required', 'integer', 'min:0'],
            'item_count.to' => ['nullable', 'integer', 'min:1', 'gte:item_count.from'],
            'trial_period_days' => ['nullable', 'integer', 'min:1'],
            'is_active' => ['required', 'boolean'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation()
    {
        $this->merge([
            'is_active' => !empty($this->is_active),
        ]);
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'name' => 'Название',
            'amount' => 'Сумма списания в день',
            'min_amount' => 'Минимальная сумма пополнения',
            'item_count' => 'Кол-во активных SKU',
            'item_count.from' => 'Кол-во активных SKU от',
            'item_count.to' => 'Кол-во активных SKU до',
            'trial_period_days' => 'Пробный период в днях',
            'is_active' => 'Активен',
        ];
    }
}
