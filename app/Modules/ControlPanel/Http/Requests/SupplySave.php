<?php

namespace App\Modules\ControlPanel\Http\Requests;

use Report\CreateNomenclatureSettingsRequest;
use Report\UpdateNomenclatureSettingsRequest;

/**
 * Запрос на сохранение кванта поставки
 */
class SupplySave extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'subject' => ['required', 'string'],
            'quantum' => ['required', 'integer', 'min:0'],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'subject' => 'Название категории',
            'quantum' => 'Квант поставки',
        ];
    }

    /**
     * Получить название категории
     *
     * @return string
     */
    public function getSubject(): string
    {
        return (string) $this->input('subject', '');
    }

    /**
     * Получить значение кванта поставки
     *
     * @return int
     */
    public function getQuantum(): int
    {
        return (int) $this->input('quantum', 0);
    }

    /**
     * Получить запрос на создание настроек для микросервиса
     *
     * @return CreateNomenclatureSettingsRequest
     */
    public function getAPICreateRequest(): CreateNomenclatureSettingsRequest
    {
        return new CreateNomenclatureSettingsRequest([
            'subject' => $this->getSubject(),
            'defaultQuantum' => $this->getQuantum(),
        ]);
    }

    /**
     * Получить запрос на обновление настроек для микросервиса
     *
     * @return UpdateNomenclatureSettingsRequest
     */
    public function getAPIUpdateRequest(): UpdateNomenclatureSettingsRequest
    {
        return new UpdateNomenclatureSettingsRequest([
            'subject' => $this->getSubject(),
            'defaultQuantum' => $this->getQuantum(),
        ]);
    }
}
