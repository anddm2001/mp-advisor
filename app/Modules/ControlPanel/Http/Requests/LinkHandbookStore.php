<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\Common\Models\LinkHandbook;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

/**
 * Запрос на добавление ссылки в справочник
 */
class LinkHandbookStore extends FormRequest
{
    /** @inheritdoc */
    protected $errorBag = 'store';

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'code' => ['required', 'string', 'max:255', Rule::unique(LinkHandbook::class, 'code')],
            'name' => ['required', 'string', 'max:255'],
            'link' => ['required', 'string', 'max:1000', 'url'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation(): void
    {
        $code = (string) $this->input('code', '');
        $this->merge([
            'code' => $code === '' ? Str::slug((string) $this->input('name', '')) : $code,
        ]);
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'code' => 'Уникальный код',
            'name' => 'Название',
            'link' => 'Ссылка',
        ];
    }
}
