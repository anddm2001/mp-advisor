<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\Report\Contracts\MarketPlaceContract;

/**
 * Запрос на запуск синхронизации данных по мерчанту
 */
class MerchantStartSync extends FormRequest
{
    /** @var string Тип синхронизации для WB — обычный */
    public const TYPE_WB_REGULAR = 'regular';
    /** @var string Тип синхронизации для WB — добор за вчера */
    public const TYPE_WB_SCHEDULED_YESTERDAY = 'scheduled_yesterday';
    /** @var string Тип синхронизации для WB — добор за пред. месяц */
    public const TYPE_WB_SCHEDULED_PREV_MONTH = 'scheduled_prev_month';
    /** @var string Тип синхронизации для WB — максимальный период */
    public const TYPE_WB_MAXIMUM_EFFORT = 'maximum_effort';

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        $marketplace = $this->input('marketplace');

        return [
            'marketplace' => ['required', 'integer', 'in:' . MarketPlaceContract::WILDBERRIES],
            'type' => ['required', 'string', 'in:' . implode(',', array_keys(self::getTypes($marketplace)))],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'marketplace' => 'Маркетплейс',
            'type' => 'Тип синхронизации',
        ];
    }

    /**
     * Список кодов типов синхронизации для маркетплейса
     *
     * @param int $marketplace
     * @return string[]
     */
    public static function getTypes(int $marketplace): array
    {
        switch ($marketplace) {
            case MarketPlaceContract::WILDBERRIES:
                return [
                    self::TYPE_WB_REGULAR => 'Обычная (сутки)',
                    self::TYPE_WB_SCHEDULED_YESTERDAY => 'Добор (вчера)',
                    self::TYPE_WB_SCHEDULED_PREV_MONTH => 'Добор (пред. месяц)',
                    self::TYPE_WB_MAXIMUM_EFFORT => 'Максимальная синхронизация (90 дней)',
                ];
            default:
                return [];
        }
    }
}
