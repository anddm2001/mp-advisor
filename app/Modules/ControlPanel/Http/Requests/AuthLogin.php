<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Запрос на аутентификацию
 */
class AuthLogin extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
            'remember' => ['nullable', 'boolean'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation()
    {
        $this->merge([
            'remember' => !empty($this->remember)
        ]);
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'email' => 'E-mail адрес',
            'password' => 'Пароль',
        ];
    }
}
