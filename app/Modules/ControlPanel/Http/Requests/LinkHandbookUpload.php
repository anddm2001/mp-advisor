<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Запрос на загрузку файла для справочника в хранилище
 */
class LinkHandbookUpload extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => ['required', 'file'],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'file' => 'Ссылка',
        ];
    }
}
