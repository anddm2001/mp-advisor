<?php

namespace App\Modules\ControlPanel\Http\Requests;

use Report\DeleteNomenclatureSettingsRequest;

/**
 * Запрос на удаление кванта поставки
 */
class SupplyDestroy extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'subject' => ['required', 'string'],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'subject' => 'Название категории',
        ];
    }

    /**
     * Получить название категории
     *
     * @return string
     */
    public function getSubject(): string
    {
        return (string) $this->input('subject', '');
    }

    /**
     * Запрос для микросервиса
     *
     * @return DeleteNomenclatureSettingsRequest
     */
    public function getAPIRequest(): DeleteNomenclatureSettingsRequest
    {
        return new DeleteNomenclatureSettingsRequest([
            'subject' => $this->getSubject(),
        ]);
    }
}
