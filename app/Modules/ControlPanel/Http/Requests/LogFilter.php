<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Запрос на фильтрацию логов
 */
class LogFilter extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category' => ['nullable', 'string'],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'category' => 'Категория',
        ];
    }
}
