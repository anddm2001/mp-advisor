<?php

namespace App\Modules\ControlPanel\Http\Requests;

use DateTime;
use Illuminate\Support\Carbon;

/**
 * Запрос для отображения графиков в панели управления
 */
class DashboardIndex extends FormRequest
{
    /** @var string Формат входящих дат */
    public const DATE_FORMAT = 'd.m.Y';

    /** @var int Кол-во дней периода (без текущего: 6 = период в 7 дней) */
    public const DEFAULT_DATE_FROM_SUB_DAYS = 6;

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'date' => ['nullable', 'array'],
            'date.from' => ['nullable', 'date_format:' . self::DATE_FORMAT],
            'date.to' => ['nullable', 'date_format:' . self::DATE_FORMAT, 'after_or_equal:date.from'],
            'compare_date' => ['nullable', 'array'],
            'compare_date.from' => ['nullable', 'date_format:' . self::DATE_FORMAT],
            'compare_date.to' => ['nullable', 'date_format:' . self::DATE_FORMAT, 'after_or_equal:compare_date.from'],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'date.from' => 'Начало периода',
            'date.to' => 'Окончание периода',
            'compare_date.from' => 'Начало периода сравнения',
            'compare_date.to' => 'Окончание периода сравнения',
        ];
    }

    /**
     * Получить начало периода
     *
     * @return DateTime
     */
    public function getDateFrom(): DateTime
    {
        $date = $this->input('date.from')
            ? Carbon::createFromFormat('!' . self::DATE_FORMAT, $this->input('date.from'))
            : Carbon::today()->subDays(self::DEFAULT_DATE_FROM_SUB_DAYS);
        return $date->toDateTime();
    }

    /**
     * Получить окончание периода
     *
     * @return DateTime
     */
    public function getDateTo(): DateTime
    {
        $date = $this->input('date.to')
            ? Carbon::createFromFormat('!' . self::DATE_FORMAT, $this->input('date.to'))
            : Carbon::today();
        return $date->setTime(23, 59,59, 999999)->toDateTime();
    }

    /**
     * Получить массив дат периода
     *
     * @return array
     */
    public function getPeriod(): array
    {
        return [
            $this->getDateFrom(),
            $this->getDateTo(),
        ];
    }

    /**
     * Получить кол-во дней в периоде
     *
     * @param bool $inclusive
     * @return int
     */
    public function getPeriodDays(bool $inclusive = true): int
    {
        $periodDays = (int) $this->getDateTo()->diff($this->getDateFrom(), true)->days;
        return $inclusive ? ++$periodDays : $periodDays;
    }

    /**
     * Получить начало периода сравнения
     *
     * @param bool $forceDefault
     * @return DateTime
     */
    public function getCompareDateFrom(bool $forceDefault = false): DateTime
    {
        $date = $this->input('compare_date.from') && !$forceDefault
            ? Carbon::createFromFormat('!' . self::DATE_FORMAT, $this->input('compare_date.from'))
            : Carbon::createFromFormat('!' . self::DATE_FORMAT, $this->getDateFrom()->format(self::DATE_FORMAT))
                ->subDays($this->getPeriodDays());
        return $date->toDateTime();
    }

    /**
     * Получить окончание периода сравнения
     *
     * @param bool $forceDefault
     * @return DateTime
     */
    public function getCompareDateTo(bool $forceDefault = false): DateTime
    {
        $date = $this->input('compare_date.to') && !$forceDefault
            ? Carbon::createFromFormat('!' . self::DATE_FORMAT, $this->input('compare_date.to'))
            : Carbon::createFromFormat('!' . self::DATE_FORMAT, $this->getDateTo()->format(self::DATE_FORMAT))
                ->subDays($this->getPeriodDays());
        return $date->setTime(23, 59,59, 999999)->toDateTime();
    }

    /**
     * Получить массив дат периода сравнения
     *
     * @return array
     */
    public function getComparePeriod(): array
    {
        $comparePeriodDays = (int) $this->getCompareDateTo()->diff($this->getCompareDateFrom(), true)->days;
        $forceDefault = $comparePeriodDays !== $this->getPeriodDays(false);
        return [
            $this->getCompareDateFrom($forceDefault),
            $this->getCompareDateTo($forceDefault),
        ];
    }

    /**
     * Получиьт параметры периодов в виде массива
     *
     * @return array
     */
    public function getPeriodsParams(): array
    {
        $comparePeriod = $this->getComparePeriod();
        return [
            'date' => [
                'from' => $this->getDateFrom()->format(self::DATE_FORMAT),
                'to' => $this->getDateTo()->format(self::DATE_FORMAT),
            ],
            'compare_date' => [
                'from' => $comparePeriod[0]->format(self::DATE_FORMAT),
                'to' => $comparePeriod[1]->format(self::DATE_FORMAT),
            ],
        ];
    }
}
