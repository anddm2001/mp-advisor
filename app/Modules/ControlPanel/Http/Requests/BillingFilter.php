<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\Billing\Models\BillingOperation;
use App\Modules\ControlPanel\Http\Controllers\BillingController;
use Illuminate\Validation\Rule;

/**
 * Запрос на фильтрацию истории операций по биллингу
 */
class BillingFilter extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => ['nullable', 'integer'],
            'merchant' => ['nullable', 'integer'],
            'amount' => ['nullable', 'array'],
            'amount.*' => ['nullable', 'numeric', 'min:0.01'],
            'date' => ['nullable', 'array'],
            'date.*' => ['nullable', 'date_format:' . BillingController::DATE_FORMAT],
            'type' => ['nullable', 'string', Rule::in(array_keys(BillingOperation::getTypes()))],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'id' => 'ID операции',
            'merchant' => 'ID мерчанта',
            'amount' => 'Сумма',
            'amount.from' => 'Сумма от',
            'amount.to' => 'Сумма до',
            'date' => 'Дата',
            'date.from' => 'Дата от',
            'date.to' => 'Дата до',
            'type' => 'Тип действия',
        ];
    }
}
