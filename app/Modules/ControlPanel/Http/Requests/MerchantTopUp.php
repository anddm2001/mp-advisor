<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Пополнение баланса мерчанта
 */
class MerchantTopUp extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'amount' => ['required', 'numeric', 'min:1'],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'amount' => 'Сумма пополнения',
        ];
    }
}
