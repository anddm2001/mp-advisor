<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\ControlPanel\Models\Admin;
use Illuminate\Validation\Rule;

/**
 * Запрос на отправку уведомления для сброса пароля администратора
 */
class AuthPasswordSend extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email', Rule::exists(Admin::class, 'email')],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'email' => 'E-mail адрес',
        ];
    }
}
