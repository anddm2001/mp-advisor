<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\ControlPanel\Models\Admin;
use Illuminate\Validation\Rule;

/**
 * Запрос на обновление данных администратора
 */
class AdministratorUpdate extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'string',
                'email',
                Rule::unique(Admin::class, 'email')->ignore($this->route('administrator')),
            ],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'is_active' => ['required', 'boolean'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation()
    {
        $this->merge([
            'is_active' => !empty($this->is_active),
        ]);
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'email' => 'E-mail адрес',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'is_active' => 'Доступ в панель управления',
        ];
    }
}
