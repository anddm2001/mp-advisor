<?php

namespace App\Modules\ControlPanel\Http\Requests;

use DateTime;
use Illuminate\Support\Carbon;

/**
 * Запрос для экспорта данных пользователей
 */
class DashboardExportDetails extends FormRequest
{
    /** @var string Формат входящих дат */
    public const DATE_FORMAT = 'd.m.Y';

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'export_date' => ['required', 'array'],
            'export_date.from' => ['required', 'date_format:' . self::DATE_FORMAT],
            'export_date.to' => ['required', 'date_format:' . self::DATE_FORMAT, 'after_or_equal:export_date.from'],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'export_date.from' => 'Начало периода',
            'export_date.to' => 'Окончание периода',
        ];
    }

    /**
     * Получить начало периода
     *
     * @return DateTime
     */
    public function getDateFrom(): DateTime
    {
        return Carbon::createFromFormat('!' . self::DATE_FORMAT, $this->input('export_date.from'))->toDateTime();
    }

    /**
     * Получить окончание периода
     *
     * @return DateTime
     */
    public function getDateTo(): DateTime
    {
        return Carbon::createFromFormat('!' . self::DATE_FORMAT, $this->input('export_date.to'))
            ->setTime(23, 59,59, 999999)
            ->toDateTime();
    }
}
