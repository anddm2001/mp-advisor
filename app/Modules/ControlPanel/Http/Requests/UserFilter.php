<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Запрос на фильтрацию списка пользователей
 */
class UserFilter extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['nullable', 'string'],
            'name' => ['nullable', 'string'],
            'merchant' => ['nullable', 'integer'],
            'user' => ['nullable', 'integer'],
            'company' => ['nullable', 'string'],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'email' => 'Почта',
            'name' => 'Имя',
            'merchant' => 'ID мерчанта',
            'user' => 'ID пользователя',
            'company' => 'Название компании',
        ];
    }
}
