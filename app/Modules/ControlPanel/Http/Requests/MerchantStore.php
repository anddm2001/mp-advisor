<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\Auth\Models\User;
use Illuminate\Validation\Rule;

/**
 * Запрос на создание пользователя
 */
class MerchantStore extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'owner_id' => ['required', 'integer', Rule::exists(User::class, 'id')],
            'users' => ['nullable', 'array'],
            'users.*' => ['nullable', 'integer', Rule::exists(User::class, 'id')],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'name' => 'Название',
            'owner_id' => 'Владелец',
            'users' => 'Подключённые пользователи',
        ];
    }
}
