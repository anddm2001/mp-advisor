<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Запрос на получение списка настроек номенклатур с фильтрацией
 */
class SupplyIndex extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'filter.subject' => ['nullable', 'string'],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'filter.subject' => 'Название категории',
        ];
    }

    /**
     * Получить массив фильтров для списка настроек номенклатур
     *
     * @return array
     */
    public function getFilter(): array
    {
        return (array) $this->input('filter', []);
    }

    /**
     * Получить название категории
     *
     * @return string
     */
    public function getSubject(): string
    {
        return (string) $this->input('filter.subject', '');
    }
}
