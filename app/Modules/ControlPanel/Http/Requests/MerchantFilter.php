<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Запрос на фильтрацию списка мерчантов
 */
class MerchantFilter extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'merchant' => ['nullable', 'integer'],
            'user' => ['nullable', 'integer'],
            'status' => ['nullable', 'string'],
            'items' => ['nullable', 'array'],
            'items.*' => ['nullable', 'integer', 'min:1'],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'merchant' => 'ID мерчанта',
            'user' => 'ID пользователя',
            'status' => 'Статус синхронизации',
            'items' => 'Кол-во SKU',
            'items.from' => 'Кол-во SKU от',
            'items.to' => 'Кол-во SKU до',
        ];
    }
}
