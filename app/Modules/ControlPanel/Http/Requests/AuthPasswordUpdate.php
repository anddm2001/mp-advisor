<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Запрос на смену пароля администратора при сбросе
 */
class AuthPasswordUpdate extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string', 'confirmed'],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'email' => 'E-mail адрес',
            'password' => 'Новый пароль',
        ];
    }
}
