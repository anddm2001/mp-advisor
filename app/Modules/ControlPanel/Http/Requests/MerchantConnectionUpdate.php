<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Models\MerchantConnection;
use Exception;

/**
 * Запрос на обновление параметров доступа к маркетплейсам
 */
class MerchantConnectionUpdate extends FormRequest
{
    /**
     * Правила валидации
     *
     * @param MerchantConnectionManagerContract $service
     * @return array
     */
    public function rules(MerchantConnectionManagerContract $service): array
    {
        return $service->getCredentialsValidation($this->getMarketplace());
    }

    /** @inheritDoc */
    public function attributes()
    {
        /** @var MerchantConnectionManagerContract $service */
        $service = resolve(MerchantConnectionManagerContract::class);

        return $service->getCredentialsNames($this->getMarketplace());
    }

    /**
     * Получить идентификатор маркетплейса
     *
     * @return int
     * @throws Exception
     */
    protected function getMarketplace(): int
    {
        /** @var MerchantConnection $connection */
        $connection = $this->route('connection');

        if (empty($connection)) {
            throw new Exception('Can not determine connection to update.');
        }

        return $connection->marketplace;
    }
}
