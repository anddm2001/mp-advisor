<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Запрос на импорт данных настроек номенклатур из файла
 */
class SupplyImport extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'mimes:xlsx'],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'file' => 'Файл XLSX',
        ];
    }
}
