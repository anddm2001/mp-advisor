<?php

namespace App\Modules\ControlPanel\Http\Requests;

/**
 * Запрос на изменение пароля пользователя
 */
class UserPassword extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'password' => ['required', 'string', 'confirmed'],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'password' => 'Новый пароль',
        ];
    }
}
