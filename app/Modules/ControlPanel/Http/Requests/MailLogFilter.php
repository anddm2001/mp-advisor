<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\Common\Models\MailLog;
use Illuminate\Validation\Rule;

/**
 * Запрос на фильтрацию логов писем
 */
class MailLogFilter extends FormRequest
{
    /** @var string Формат дат для фильтра */
    public const DATE_FORMAT = 'd.m.Y';

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['nullable', 'string'],
            'user' => ['nullable', 'integer'],
            'merchant' => ['nullable', 'integer'],
            'date' => ['nullable', 'array'],
            'date.*' => ['nullable', 'date_format:' . self::DATE_FORMAT],
            'type' => ['nullable', 'string', Rule::in(array_keys(MailLog::getTypeNames()))],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'email' => 'E-mail',
            'user' => 'ID пользователя',
            'merchant' => 'ID мерчанта',
            'date' => 'Дата',
            'date.from' => 'Дата от',
            'date.to' => 'Дата до',
            'type' => 'Тип письма',
        ];
    }
}
