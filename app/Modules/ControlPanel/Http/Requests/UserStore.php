<?php

namespace App\Modules\ControlPanel\Http\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\CommonDataHelper;
use Illuminate\Validation\Rule;

/**
 * Запрос на создание пользователя
 */
class UserStore extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'status' => ['required', 'string', Rule::in(array_keys(User::getStatusNames()))],
            'email' => ['required', 'string', 'max:255', 'email', Rule::unique(User::class, 'email')],
            'password' => ['required', 'string', 'confirmed'],
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['nullable', 'string', 'max:50'],
            'company_name' => ['nullable', 'string', 'max:50'],
            'company_post' => ['nullable', 'string', 'max:50'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation()
    {
        $email = $this->input('email', '');
        $this->merge([
            'email' => CommonDataHelper::prepareEmail($email ?? ''),
        ]);
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'status' => 'Статус',
            'email' => 'Почта',
            'password' => 'Пароль',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'company_name' => 'Название компании',
            'company_post' => 'Должность',
        ];
    }
}
