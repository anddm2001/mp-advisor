<?php

namespace App\Modules\ControlPanel\Http\Middleware;

use App\Modules\ControlPanel\Models\Admin;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LogoutIfNotActive
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var ?Admin $adminUser */
        $adminUser = $request->user('cp');
        if ($adminUser && !$adminUser->is_active) {
            Auth::logout();
            $session = $request->session();
            $session->invalidate();
            $session->regenerateToken();
            return Redirect::route('cp.dashboard');
        }

        return $next($request);
    }
}
