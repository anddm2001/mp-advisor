<?php

namespace App\Modules\Storage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Модель файла в файловой системе
 *
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $path Путь к файлу, относительно хранилища
 * @property string $name Название файла в хранилище
 * @property string $original_name Оригинальное название файла
 * @property int $size Размер файла в байтах
 * @property string $mime Mime-тип файла
 *
 * @package App\Modules\Storage\Models
 */
class File extends Model
{
    /**
     * @var string
     */
    protected $table = 'file';

    /**
     * @var array
     */
    protected $fillable = [
        'path',
        'name',
        'original_name',
        'size',
        'mime',
    ];
}
