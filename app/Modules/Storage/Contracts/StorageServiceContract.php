<?php

namespace App\Modules\Storage\Contracts;

use App\Modules\Storage\Models\File;
use Illuminate\Http\UploadedFile;
use Psr\Http\Message\StreamInterface;

/**
 * Интерфейс сервиса для работы с файловой системой.
 *
 * Загружает файлы в файловую систему и сохраняет их в БД.
 *
 * @package App\Modules\Storage\Contracts
 */
interface StorageServiceContract
{
    /**
     * Загрузка файла в хранилище.
     *
     * Сохраняет файл в БД и загружает его на хранилище.
     *
     * @param UploadedFile $uploadedFile Временно загруженный файл
     *
     * @return File Результирующий файл в БД
     */
    public function upload(UploadedFile $uploadedFile): File;

    /**
     * Простая загрузка файла в хранилище (без БД, моделей и прочего).
     *
     * @param UploadedFile $uploadedFile Загруженный файл
     * @param string|null $pathPrefix Опциональный префикс пути в хранилище
     * @return string Ссылка на файл
     */
    public function simpleUpload(UploadedFile $uploadedFile, ?string $pathPrefix = null): string;

    /**
     * Получение абсолютного URL к файлу
     *
     * @param File $file
     *
     * @return string
     */
    public function url(File $file): string;

    /**
     * Удаление файла
     *
     * @param File $file
     *
     * @return bool
     */
    public function remove(File $file): bool;

    /**
     * Получить содержимое файла для последующего сохранения на локальный диск
     *
     * @param File $file Файл для скачивания
     *
     * @return string
     */
    public function getContents(File $file): string;
}
