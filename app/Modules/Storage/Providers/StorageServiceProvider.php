<?php

namespace App\Modules\Storage\Providers;

use App\Modules\Storage\Contracts\StorageServiceContract;
use App\Modules\Storage\Models\File;
use App\Modules\Storage\Services\StorageService;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Support\ServiceProvider;

/**
 * Провайдер для сервисов файлового хранилища
 *
 * @package App\Modules\Storage\Providers
 */
class StorageServiceProvider extends ServiceProvider
{
    public function register()
    {
        // сервис для работы с файлами пользовательских данных
        $this->app->singleton(StorageServiceContract::class, function($app) {
            // куда сохранять пользовательские данные
            /** @var FilesystemManager $fileManager */
            $fileManager = $app->make('filesystem');

            /** @var FilesystemAdapter $driver */
            $driver = $fileManager->drive($app->make('config')['storage.user_data_file_system']);

            return new StorageService($driver, $app->make(File::class));
        });
    }
}
