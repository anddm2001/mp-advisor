<?php

namespace App\Modules\Storage\Services;

use App\Modules\Storage\Contracts\StorageServiceContract;
use App\Modules\Storage\Models\File;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Psr\Http\Message\StreamInterface;

/**
 * Сервис для работы с удалённой файловой системой
 *
 * @package App\Modules\Storage\Services
 */
class StorageService implements StorageServiceContract
{
    /**
     * Файловая система, в которую загружать файлы
     */
    protected FileSystemAdapter $fileSystem;

    /**
     * Пустая модель файла для создания новых записей
     */
    protected File $fileModel;

    /**
     * StorageService constructor.
     *
     * @param FilesystemAdapter $fileSystem Файловая система, в которую загружать файлы
     * @param File $fileModel Пустая модель файла для создания новых записей
     */
    public function __construct(FilesystemAdapter $fileSystem, File $fileModel)
    {
        $this->fileSystem = $fileSystem;
        $this->fileModel = $fileModel;
    }

    /**
     * @inheritdoc
     */
    public function upload(UploadedFile $uploadedFile): File
    {
        // новое название файла
        $fileName = md5(uniqid() . time() . $uploadedFile->getPathname());
        $ext = $uploadedFile->getClientOriginalExtension();

        if ($ext) {
            $fileName .= '.' . $ext;
        }

        $path = substr($fileName, 0, 2) . '/' . substr($fileName, 1, 1);

        $file = clone $this->fileModel;

        $file->path = $path;
        $file->original_name = $uploadedFile->getClientOriginalName();
        $file->size = $uploadedFile->getSize();
        $file->name = $fileName;
        $file->mime = $uploadedFile->getClientMimeType();

        $done = $file->getConnection()->transaction(function() use ($file, $uploadedFile, $path, $fileName) {
            if ($file->save()) {
                return !!$this->fileSystem->putFileAs($path, $uploadedFile, $fileName);
            }

            return false;
        });

        if (!$done) {
            throw new \RuntimeException();
        }

        return $file;
    }

    /**
     * @inheritDoc
     */
    public function simpleUpload(UploadedFile $uploadedFile, ?string $pathPrefix = null): string
    {
        $path = 'files' . ($pathPrefix ? '/' . $pathPrefix : '');
        $fileInfo = pathinfo($uploadedFile->getClientOriginalName());
        $fileName = Str::slug($fileInfo['filename'] ?? '');
        if ($fileName === '') {
            $fileName = md5(uniqid());
        }
        $fileName .= ($fileInfo['extension'] ?? null) ? '.' . strtolower($fileInfo['extension']) : '';

        $storedPath = $this->fileSystem->putFileAs($path, $uploadedFile, $fileName);
        if (!$storedPath) {
            throw new \RuntimeException();
        }

        return $this->fileSystem->url($storedPath);
    }

    /**
     * @inheritDoc
     */
    public function url(File $file): string
    {
        return $this->fileSystem->url($file->path . '/' . $file->name);
    }

    /**
     * @inheritDoc
     */
    public function remove(File $file): bool
    {
        if ($file->delete()) {
            $this->fileSystem->delete($file->path . '/' . $file->name);

            return true;
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getContents(File $file): string
    {
        return $this->fileSystem->get($file->path . '/' . $file->name);
    }
}
