<?php

namespace App\Modules\Merchant\Providers;

use App\Modules\Common\Providers\BaseAuthServiceProvider;
use App\Modules\Merchant\Policies\MerchantPolicy;

/**
 * Права доступа к модулю мерчантов
 *
 * @package App\Modules\Merchant\Providers
 */
class MerchantAuthServiceProvider extends BaseAuthServiceProvider
{
    protected function policies(): array
    {
        return [
            'view-merchant' => MerchantPolicy::class . '@allowViewMerchant',
            'update-merchant' => MerchantPolicy::class . '@allowUpdateMerchant',
            'view-merchant-connection' => MerchantPolicy::class . '@allowViewMerchantConnection',
        ];
    }
}
