<?php

namespace App\Modules\Merchant\Providers;

use App\Modules\Merchant\Models\Merchant;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

/**
 * Объявление переменных в роутинге
 *
 * @package App\Modules\Merchant\Providers
 */
class MerchantRouteServiceProvider extends ServiceProvider
{
    /**
     * Биндинг моделей внутрь роутинга
     */
    protected function bindRouteModels()
    {
        /** @var Router $router */
        $router = $this->app->make('router');

        // мерчанты
        $router->pattern('merchant', '[0-9]+');
        $router->model('merchant', Merchant::class);
    }

    public function boot()
    {
        $this->bindRouteModels();
    }
}
