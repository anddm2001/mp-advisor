<?php

namespace App\Modules\Merchant\Providers;

use App\Modules\Merchant\Console\Commands\BillingChargeCommand;
use App\Modules\Merchant\Console\Commands\BillingSendNotificationsCommand;
use App\Modules\Merchant\Console\Commands\WildberriesApiCollectDataCommand;
use App\Modules\Merchant\Console\Commands\WildberriesApiScheduledDataSyncCommand;
use App\Modules\Merchant\Console\Commands\WildberriesCollectEachDaySalesCommand;
use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Services\MerchantConnectionManagerService;
use Illuminate\Support\ServiceProvider;

/**
 * Сервис провайдер для модуля мерчантов
 *
 * @package App\Modules\Merchant\Providers
 */
class MerchantServiceProvider extends ServiceProvider
{
    public function register()
    {
        // сервис для управления подключениями к API маркетплейсов
        $this->app->singleton(MerchantConnectionManagerContract::class, function($app) {
            $config = $app->make('config')['merchant.connections_config'];

            return new MerchantConnectionManagerService($config, $app->make('encrypter'));
        });
    }

    public function boot()
    {
        $this->commands([
            BillingChargeCommand::class,
            BillingSendNotificationsCommand::class,
            WildberriesApiCollectDataCommand::class,
            WildberriesApiScheduledDataSyncCommand::class,
            WildberriesCollectEachDaySalesCommand::class,
        ]);
    }
}
