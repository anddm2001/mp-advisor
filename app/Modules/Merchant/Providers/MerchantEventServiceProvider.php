<?php

namespace App\Modules\Merchant\Providers;

use App\Modules\Common\Listeners\LogEventListener;
use App\Modules\Merchant\Events\MerchantConnectionCredentialsCreatedEvent;
use App\Modules\Merchant\Listeners\MerchantConnectionFixCreatedAtListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;

/**
 * Регистрация обработчиков событий мерчанта
 */
class MerchantEventServiceProvider extends EventServiceProvider
{
    /** @inheritdoc */
    protected $listen = [
        MerchantConnectionCredentialsCreatedEvent::class => [
            MerchantConnectionFixCreatedAtListener::class,
            LogEventListener::class,
        ],
    ];
}
