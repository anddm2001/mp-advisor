<?php

namespace App\Modules\Merchant\Notifications;

use App\Modules\Merchant\Models\Feedback;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Уведомление управляющему с данными обратной связи
 */
class ManagerFeedbackNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /** @var Feedback Запись формы обратной связи */
    protected Feedback $feedback;

    /** @var string|null Данные загруженного файла */
    protected ?string $uploadedFileData;

    /**
     * Определение параметров для уведомления
     *
     * @param Feedback $feedback
     * @param string|null $uploadedFileData
     */
    public function __construct(Feedback $feedback, ?string $uploadedFileData)
    {
        $this->feedback = $feedback;
        $this->uploadedFileData = $uploadedFileData;
    }

    /**
     * Каналы отправки уведомления
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Сборка письма для отправки уведомления по e-mail
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        $mailMessage = new MailMessage();
        $mailMessage->from($this->feedback->email, $this->feedback->user->name)
            ->replyTo($this->feedback->email, $this->feedback->user->name);
        $mailMessage->subject('mpAdvisor — Поддержка пользователей');
        $mailMessage->greeting(' ');
        $mailMessage->line('**ID мерчанта** — ' . $this->feedback->merchant_id);
        $mailMessage->line('**ID пользователя** — ' . $this->feedback->user_id);
        $mailMessage->line('**E-mail пользователя** — ' . $this->feedback->email);
        $mailMessage->line('**Что случилось** — ' . $this->feedback->category);
        $mailMessage->line('**Точнее** — ' . $this->feedback->subcategory);
        $mailMessage->line('**Текст сообщения:**');
        foreach ($this->feedback->message_lines as $message) {
            $mailMessage->line($message);
        }
        if ($this->uploadedFileData) {
            $fileName = basename($this->feedback->file);
            $mailMessage->line('**Файл** — ' . $fileName);
            $mailMessage->attachData(base64_decode($this->uploadedFileData), $fileName);
        }
        $mailMessage->salutation(' ');
        return $mailMessage;
    }
}
