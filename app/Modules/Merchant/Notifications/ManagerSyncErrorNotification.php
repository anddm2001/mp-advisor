<?php

namespace App\Modules\Merchant\Notifications;

use App\Modules\Marketplace\Entities\Marketplace;
use App\Modules\Merchant\Models\MerchantConnectionSync;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Уведомление управляющему об ошибке синхронизации данных
 */
class ManagerSyncErrorNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /** @var MerchantConnectionSync Запись истории синхронизации */
    protected MerchantConnectionSync $sync;

    /**
     * Определение параметров для уведомления
     *
     * @param MerchantConnectionSync $sync
     */
    public function __construct(MerchantConnectionSync $sync)
    {
        $this->sync = $sync;
    }

    /**
     * Каналы отправки уведомления
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Сборка письма для отправки уведомления по e-mail
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        $mailMessage = new MailMessage();
        $mailMessage->subject('Ошибки синхронизации. Мерчант ID — ' . $this->sync->connection->merchant_id);
        $mailMessage->greeting(' ');
        $mailMessage->line('Маркетплейс — ' . Marketplace::getNameById($this->sync->connection->marketplace));
        $mailMessage->line('Тип синхронизации — ' . $this->sync->type_name);
        $mailMessage->line('Дата — ' . $this->sync->date->format('d.m.Y H:i:s'));
        $mailMessage->line('Ошибка — ' . $this->sync->data);
        $mailMessage->line('[Карточка мерчанта](' . route('cp.merchants.sync', ['merchant' => $this->sync->connection->merchant]) . ')');
        $mailMessage->salutation(' ');
        return $mailMessage;
    }
}
