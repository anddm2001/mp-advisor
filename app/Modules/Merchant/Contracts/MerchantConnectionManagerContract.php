<?php

namespace App\Modules\Merchant\Contracts;

use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Requests\StoreMerchantConnectionRequest;
use Illuminate\Contracts\Validation\Rule;

/**
 * Интерфейс сервиса для управления подключениями к API маркетплейсов
 *
 * @package App\Modules\Merchant\Contracts
 */
interface MerchantConnectionManagerContract
{
    /**
     * Получить доступные поля для подключения и их правила валидации.
     *
     * Возвращает массив, в котором:
     *
     * 1. ключ - название поля
     * 2. значение - массив правил валидации (string, Rule)
     *
     * @param int $marketplace
     * @return string[]|Rule[]
     */
    public function getCredentialsValidation(int $marketplace): array;

    /**
     * Получить названия полей для подключения к маркетплейсу
     *
     * Возвращает массив, в котором:
     *
     * 1. ключ - название поля
     * 2. значение - заголовок поля
     *
     * @param int $marketplace
     * @return string[]
     */
    public function getCredentialsNames(int $marketplace): array;

    /**
     * Получить креденшенелы для подключения к API
     *
     * @param MerchantConnection $connection
     *
     * @return array
     */
    public function getCredentialsValues(MerchantConnection $connection): array;

    /**
     * Определить, изменились ли указанные параметры подключения в сравнении с имеющимися
     *
     * @param MerchantConnection $connection
     * @param array $newCredentials
     * @return bool
     */
    public function credentialsHaveChanged(MerchantConnection $connection, array $newCredentials): bool;

    /**
     * Установить массив событий, которые должны быть запущены, если параметры подключения изменились
     *
     * @param array $events
     */
    public function setCredentialsChangedEvents(array $events): void;

    /**
     * Сохранить (обновить или создать) поключение к API маркетплейса
     *
     * @param StoreMerchantConnectionRequest $request
     * @param Merchant $merchant
     * @param MerchantConnection $model
     *
     * @return bool
     */
    public function store(StoreMerchantConnectionRequest $request, Merchant $merchant, MerchantConnection $model): bool;
}
