<?php

namespace App\Modules\Merchant\Contracts;

/**
 * Интерфейс для фильтрации данных подключения к API
 *
 * @package App\Modules\Merchant\Contracts
 */
interface CredentialsFieldFilterContract
{
    /**
     * Отфильтровать данные и вернуть данные, пригодные для вставки в БД
     *
     * @param string $value
     *
     * @return string
     */
    public function filter(string $value): string;
}
