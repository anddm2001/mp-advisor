<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Modules\Common\Http\Middleware\ApiTransformDate;
use App\Modules\Common\Responses\SuccessResponse;
use App\Modules\Marketplace\Http\Controllers\MarketplaceController;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Requests\IndexIncomeRequest;
use App\Modules\Merchant\Responses\Income\IndexResponse;
use App\Modules\Merchant\Responses\Income\ShowResponse;
use App\Modules\Merchant\Responses\Income\StoreResponse;
use App\Modules\Merchant\Responses\Income\UpdateResponse;
use App\Modules\Report\Requests\CreateApiIncomeRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\UpdateApiIncomeRequest;
use App\Modules\Report\Services\WildberriesAPIIncomeXLSParser;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
use Report\IncomeStatus;

/**
 * Работа с поставками
 */
class IncomeController extends MarketplaceController
{
    protected $middleware = [
        [
            'middleware' => ApiTransformDate::class . ':date_supply',
            'options' => ['only' => ['store', 'update']],
        ],
    ];

    /**
     * Список поставок
     *
     * @param Merchant $merchant
     * @param IndexIncomeRequest $request
     * @param PageRequest $pageRequest
     * @return IndexResponse
     */
    public function index(Merchant $merchant, IndexIncomeRequest $request, PageRequest $pageRequest): IndexResponse
    {
        $incomeStatus = IncomeStatus::value('status_' . $request->input('status', ''));
        $response = $this->api->getIncomesList($merchant->id, $incomeStatus, $pageRequest);
        return new IndexResponse($response->getItems(), $response->getPage());
    }

    /**
     * Детали поставки
     *
     * @param Merchant $merchant
     * @param int $incomeId
     * @return ShowResponse
     */
    public function show(Merchant $merchant, int $incomeId): ShowResponse
    {
        $response = $this->api->getIncomeDetails($merchant->id, $incomeId);
        return new ShowResponse($response->getIncome(), $response->getItems());
    }

    /**
     * Создание поставки
     *
     * @param Merchant $merchant
     * @param CreateApiIncomeRequest $request
     * @return StoreResponse
     * @throws IOException
     * @throws ReaderNotOpenedException
     */
    public function store(Merchant $merchant, CreateApiIncomeRequest $request): StoreResponse
    {
        $files = $request->file('files');
        $nomenclatures = [];
        foreach ($files as $file) {
            $parser = new WildberriesAPIIncomeXLSParser($file->getPathname());
            $nomenclatures = array_merge($nomenclatures, $parser->parseFile()->fillAPINomenclatures());
        }

        $response = $this->api->createIncome($merchant->id, $request, $nomenclatures);
        return new StoreResponse($response);
    }

    /**
     * Обновление поставки
     *
     * @param Merchant $merchant
     * @param int $incomeId
     * @param UpdateApiIncomeRequest $request
     * @return UpdateResponse
     */
    public function update(Merchant $merchant, int $incomeId, UpdateApiIncomeRequest $request): UpdateResponse
    {
        $this->api->updateIncome($merchant->id, $incomeId, $request);
        return new UpdateResponse($this->api->getIncomeDetails($merchant->id, $incomeId)->getIncome());
    }

    /**
     * Удаление поставки
     *
     * @param Merchant $merchant
     * @param int $incomeId
     * @return SuccessResponse
     */
    public function destroy(Merchant $merchant, int $incomeId): SuccessResponse
    {
        return new SuccessResponse($this->api->removeIncome($merchant->id, $incomeId)->getRemoved());
    }
}
