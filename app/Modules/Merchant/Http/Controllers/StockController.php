<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Modules\Common\Errors\Error;
use App\Modules\Common\Responses\ErrorResponse;
use App\Modules\Marketplace\Http\Controllers\MarketplaceController;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Requests\ImportStockRequest;
use App\Modules\Report\Contracts\MarketPlaceContract;
use App\Modules\Report\Importer\WildberriesStocksImporter;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\StockFilterRequest;
use App\Modules\Report\Requests\StockInfoRequest;
use App\Modules\Report\Requests\StockReportSortRequest;
use App\Modules\Report\Resources\ReportResultFileExportResource;
use App\Modules\Report\Resources\StockInfoResource;
use App\Modules\Report\Resources\StockReportResource;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * Работа со складами
 */
class StockController extends MarketplaceController
{
    /**
     * Обобщённая информация с данными по складам
     *
     * @param Merchant $merchant
     * @param NomenclatureFilterRequest $nomenclatureFilterRequest
     * @param StockFilterRequest $stockFilterRequest
     * @return StockInfoResource
     */
    public function index(
        Merchant $merchant,
        NomenclatureFilterRequest $nomenclatureFilterRequest,
        StockFilterRequest $stockFilterRequest
    ): StockInfoResource
    {
        return new StockInfoResource($this->api->getStockInfo(
            $merchant->id,
            $nomenclatureFilterRequest,
            $stockFilterRequest
        ));
    }

    /**
     * Получить отчёт по складам
     *
     * @param Merchant $merchant
     * @param NomenclatureFilterRequest $nomenclatureFilterRequest
     * @param StockFilterRequest $stockFilterRequest
     * @param StockReportSortRequest $sortRequest
     * @param PageRequest $pageRequest
     * @return StockReportResource
     */
    public function report(
        Merchant $merchant,
        NomenclatureFilterRequest $nomenclatureFilterRequest,
        StockFilterRequest $stockFilterRequest,
        StockReportSortRequest $sortRequest,
        PageRequest $pageRequest
    ): StockReportResource
    {
        return new StockReportResource($this->api->getStockReport(
            $merchant->id,
            $nomenclatureFilterRequest,
            $stockFilterRequest,
            $sortRequest,
            $pageRequest
        ));
    }

    /**
     * Запрос на экспорт данных складов в XLSX
     *
     * @param Merchant $merchant
     * @param NomenclatureFilterRequest $nomenclatureFilterRequest
     * @param StockFilterRequest $stockFilterRequest
     * @param StockReportSortRequest $sortRequest
     * @return ReportResultFileExportResource
     */
    public function export(
        Merchant $merchant,
        NomenclatureFilterRequest $nomenclatureFilterRequest,
        StockFilterRequest $stockFilterRequest,
        StockReportSortRequest $sortRequest
    ): ReportResultFileExportResource
    {
        $reportResultFile = $this->api->requestStocksExport(
            $merchant->id,
            array_merge($nomenclatureFilterRequest->validated(), $nomenclatureFilterRequest->filterData()),
            $stockFilterRequest->validated(),
            $sortRequest->validated()
        );
        return new ReportResultFileExportResource($merchant, $reportResultFile);
    }

    /**
     * Отображает шаблон для импорта номенклатур
     *
     * @param Merchant $merchant
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function importTemplate(Merchant $merchant): void
    {
        // для Wildberries генерируем шаблон для импорта по формату импортера
        if ($this->marketplace->getId() === MarketPlaceContract::WILDBERRIES) {
            WildberriesStocksImporter::getTemplateFile();
        }
    }

    /**
     * Импортирует данные из загруженного файла
     *
     * @param Merchant $merchant
     * @param ImportStockRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function import(Merchant $merchant, ImportStockRequest $request): JsonResponse
    {
        $result = ['positions' => 0, 'updated_positions' => 0];

        // для Wildberries импортируем данные по шаблону
        if ($this->marketplace->getId() === MarketPlaceContract::WILDBERRIES) {
            $result = WildberriesStocksImporter::import($merchant, [
                'file' => $request->file('file'),
            ]);
        }

        // вообще ни одной позиции не удалось прочитать
        if ($result['positions'] === 0) {
            $error = new Error(422, 'Формат не верный, проверьте таблицу на соответствие шаблону.');
            return new ErrorResponse($error);
        }

        // все позиции из файла импортировны
        if ($result['positions'] === $result['updated_positions']) {
            return new JsonResponse(['updated' => $result['updated_positions']]);
        }

        // не все позиции импортированы
        $positionsDeclension = static function (int $positions): string {
            if ($positions > 20) {
                $positions %= 10;
            }
            if ($positions === 1) {
                return 'позиции';
            }
            return 'позиций';
        };
        $error = new Error(422, sprintf(
            'Добавлено %d из %d %s. %d не удалось найти.',
            $result['updated_positions'],
            $result['positions'],
            $positionsDeclension($result['positions']),
            $result['positions'] - $result['updated_positions']
        ));
        return new ErrorResponse($error);
    }
}
