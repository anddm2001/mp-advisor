<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Modules\Billing\Http\Resources\MerchantBillingResource;
use App\Modules\Marketplace\Http\Controllers\MarketplaceController;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Requests\IndexBillingRequest;
use Illuminate\Database\Eloquent\Builder;

/**
 * Общая биллинг-информация по мерчанту
 */
class BillingController extends MarketplaceController
{
    /** @var int Кол-во операций на страницу */
    protected const PER_PAGE = 35;

    /**
     * Отображение сводной биллинг-информации
     *
     * @param Merchant $merchant
     * @param IndexBillingRequest $request
     * @return MerchantBillingResource
     */
    public function index(Merchant $merchant, IndexBillingRequest $request): MerchantBillingResource
    {
        $type = $request->input('type');
        $operations = $merchant->billingOperations()->when($type, function (Builder $query, $type): Builder {
            return $query->where('type', $type);
        })->orderByDesc('id')->paginate(self::PER_PAGE);

        return new MerchantBillingResource($merchant, $this->getCurrentConnection($merchant), $operations);
    }

    /**
     * Получить текущее подключение для маркетплейса
     *
     * @param Merchant $merchant
     * @return MerchantConnection|null
     */
    protected function getCurrentConnection(Merchant $merchant): ?MerchantConnection
    {
        return $merchant->marketplaceConnections->where('marketplace', $this->marketplace->getId())->first();
    }
}
