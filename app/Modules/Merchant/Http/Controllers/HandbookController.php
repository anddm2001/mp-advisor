<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Modules\Marketplace\Http\Controllers\MarketplaceController;
use App\Modules\Merchant\Models\Merchant;
use Illuminate\Http\JsonResponse;

/**
 * Работа с брендами
 */
class HandbookController extends MarketplaceController
{
    /**
     * Список названий брендов
     *
     * @param Merchant $merchant
     * @return JsonResponse
     */
    public function brandNames(Merchant $merchant): JsonResponse
    {
        return new JsonResponse($this->api->getBrandNames($merchant->id));
    }

    /**
     * Список названий предметов
     *
     * @param Merchant $merchant
     * @return JsonResponse
     */
    public function subjectNames(Merchant $merchant): JsonResponse
    {
        return new JsonResponse($this->api->getSubjectNames($merchant->id));
    }
}
