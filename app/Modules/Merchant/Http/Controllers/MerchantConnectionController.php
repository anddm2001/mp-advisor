<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Modules\Marketplace\Http\Controllers\MarketplaceController;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Resources\MerchantConnectionLastSyncResource;

/**
 * Контроллер данных синхронизации с маркетплейсом
 */
class MerchantConnectionController extends MarketplaceController
{
    /**
     * Получить текущее подключение для маркетплейса
     *
     * @param Merchant $merchant
     * @return MerchantConnection|null
     */
    protected function getCurrentConnection(Merchant $merchant): ?MerchantConnection
    {
        return $merchant->marketplaceConnections->where('marketplace', $this->marketplace->getId())->first();
    }

    /**
     * Получить данные последней синхронизации с маркетплейсом
     *
     * @param Merchant $merchant
     * @return MerchantConnectionLastSyncResource
     */
    public function lastSync(Merchant $merchant): MerchantConnectionLastSyncResource
    {
        $connection = $this->getCurrentConnection($merchant) ?? new MerchantConnection();
        return new MerchantConnectionLastSyncResource($connection);
    }
}
