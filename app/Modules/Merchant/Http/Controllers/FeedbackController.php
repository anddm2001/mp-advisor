<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Auth\Models\User;
use App\Modules\Common\Responses\SuccessResponse;
use App\Modules\Merchant\Models\Feedback;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Notifications\ManagerFeedbackNotification;
use App\Modules\Merchant\Requests\StoreFeedbackRequest;
use App\Modules\Storage\Contracts\StorageServiceContract;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Notification;

/**
 * Обратная связь
 */
class FeedbackController extends ApiController
{
    /**
     * Отправка данных формы обратной связи админу
     *
     * @param Merchant $merchant
     * @param StoreFeedbackRequest $request
     * @param StorageServiceContract $storageService
     * @return SuccessResponse
     * @throws AuthenticationException
     */
    public function store(
        Merchant $merchant,
        StoreFeedbackRequest $request,
        StorageServiceContract $storageService
    ): SuccessResponse
    {
        /** @var User|null $user */
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException();
        }

        $feedbackData = $request->validated();
        $feedbackData['user_id'] = $user->id;
        $feedbackData['email'] = $user->email;
        $fileData = null;
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $randPrefix = base_convert(md5(uniqid('', true)), 16, 36);
            $feedbackData['file'] = $storageService->simpleUpload($file, 'feedback/' . $randPrefix);
            $fileData = $file->getRealPath() ? base64_encode(file_get_contents($file->getRealPath())) : null;
        }

        /** @var Feedback $feedback */
        $feedback = $merchant->feedback()->create($feedbackData);

        Notification::route('mail', 'info@mpadvisor.ru')
            ->notify(new ManagerFeedbackNotification($feedback, $fileData));

        return new SuccessResponse();
    }
}
