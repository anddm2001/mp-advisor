<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Modules\Common\Errors\Error;
use App\Modules\Common\Responses\ErrorResponse;
use App\Modules\Marketplace\Http\Controllers\MarketplaceController;
use App\Modules\Merchant\Http\Middleware\PrepareNomenclaturesSortRequest;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Requests\ImportNomenclatureRequest;
use App\Modules\Merchant\Responses\Nomenclature\IndexResponse;
use App\Modules\Merchant\Responses\Nomenclature\UpdateResponse;
use App\Modules\Report\Contracts\MarketPlaceContract;
use App\Modules\Report\Importer\WildberriesNomenclaturesImporter;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\NomenclatureRemovedFilterRequest;
use App\Modules\Report\Requests\NomenclatureSortRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\UpdateNomenclatureSizeRequest;
use App\Modules\Report\Resources\ReportResultFileExportResource;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Writer\Exception\WriterNotOpenedException;
use Exception;
use Illuminate\Http\JsonResponse;

/**
 * Работа с номенклатурами мерчанта
 */
class NomenclatureController extends MarketplaceController
{
    protected $middleware = [
        ['middleware' => PrepareNomenclaturesSortRequest::class, 'options' => ['only' => 'index']],
    ];

    /**
     * Получить список всех номенклатур мерчанта
     *
     * @param Merchant $merchant
     * @param NomenclatureFilterRequest $filterRequest
     * @param NomenclatureRemovedFilterRequest $removedFilterRequest
     * @param NomenclatureSortRequest $sortRequest
     * @param PageRequest $pageRequest
     * @return IndexResponse
     */
    public function index(
        Merchant $merchant,
        NomenclatureFilterRequest $filterRequest,
        NomenclatureRemovedFilterRequest $removedFilterRequest,
        NomenclatureSortRequest $sortRequest,
        PageRequest $pageRequest
    ): IndexResponse
    {
        $response = $this->api->getMerchantNomenclatures(
            $merchant->id,
            $filterRequest,
            $removedFilterRequest,
            $sortRequest,
            $pageRequest
        );
        return new IndexResponse($response->getItems(), $response->getPage());
    }

    /**
     * Запрос на изменение номенклатуры
     *
     * @param Merchant $merchant
     * @param UpdateNomenclatureSizeRequest $request
     * @return UpdateResponse
     */
    public function update(Merchant $merchant, UpdateNomenclatureSizeRequest $request): UpdateResponse
    {
        return new UpdateResponse($this->api->updateNomenclatureSize($merchant->id, $request));
    }

    /**
     * Запрос на экспорт данных номенклатур в XLSX
     *
     * @param Merchant $merchant
     * @param NomenclatureFilterRequest $filterRequest
     * @param NomenclatureRemovedFilterRequest $removedFilterRequest
     * @return ReportResultFileExportResource
     */
    public function export(
        Merchant $merchant,
        NomenclatureFilterRequest $filterRequest,
        NomenclatureRemovedFilterRequest $removedFilterRequest
    ): ReportResultFileExportResource
    {
        $reportResultFile = $this->api->requestNomenclaturesExport(
            $merchant->id,
            array_merge($filterRequest->validated(), $filterRequest->filterData()),
            $removedFilterRequest->validated()
        );
        return new ReportResultFileExportResource($merchant, $reportResultFile);
    }

    /**
     * Получает шаблон для импорта номенклатур
     *
     * @param Merchant $merchant
     * @return void
     * @throws IOException
     * @throws WriterNotOpenedException
     */
    public function importTemplate(Merchant $merchant): void
    {
        // для Wildberries генерируем шаблон для импорта по формату импортера
        if ($this->marketplace->getId() === MarketPlaceContract::WILDBERRIES) {
            WildberriesNomenclaturesImporter::getTemplateFile();
        }
    }

    /**
     * Импортирует данные из загруженного файла
     *
     * @param Merchant $merchant
     * @param ImportNomenclatureRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function import(Merchant $merchant, ImportNomenclatureRequest $request): JsonResponse
    {
        $result = ['positions' => 0, 'updated_positions' => 0];

        // для Wildberries импортируем данные по шаблону
        if ($this->marketplace->getId() === MarketPlaceContract::WILDBERRIES) {
            $result = WildberriesNomenclaturesImporter::import($merchant, [
                'type' => $request->input('type'),
                'file' => $request->file('file'),
            ]);
        }

        // вообще ни одной позиции не удалось прочитать
        if ($result['positions'] === 0) {
            $error = new Error(422, 'Формат не верный, проверьте таблицу на соответствие шаблону.');
            return new ErrorResponse($error);
        }

        // все позиции из файла импортировны
        if ($result['positions'] === $result['updated_positions']) {
            return new JsonResponse(['updated' => $result['updated_positions']]);
        }

        // не все позиции импортированы
        $positionsDeclension = static function (int $positions): string {
            if ($positions > 20) {
                $positions %= 10;
            }
            if ($positions === 1) {
                return 'позиции';
            }
            return 'позиций';
        };
        $error = new Error(422, sprintf(
            'Добавлено %d из %d %s. %d не удалось найти.',
            $result['updated_positions'],
            $result['positions'],
            $positionsDeclension($result['positions']),
            $result['positions'] - $result['updated_positions']
        ));
        return new ErrorResponse($error);
    }
}
