<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Report\Models\ReportResultFile;
use App\Modules\Report\Resources\ReportResultFileResource;
use App\Modules\Storage\Contracts\StorageServiceContract;

/**
 * Работа с файлами
 */
class FileController extends ApiController
{
    /** @var StorageServiceContract Сервис для работы с хранилищем */
    protected StorageServiceContract $service;

    /**
     * Инициализация сервиса
     *
     * @param StorageServiceContract $service
     */
    public function __construct(StorageServiceContract $service)
    {
        $this->service = $service;
    }

    /**
     * Статус генерации файла
     *
     * @param Merchant $merchant
     * @param ReportResultFile $file
     * @return ReportResultFileResource
     */
    public function status(Merchant $merchant, ReportResultFile $file): ReportResultFileResource
    {
        return new ReportResultFileResource($file, $this->service);
    }
}
