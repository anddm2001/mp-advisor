<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Modules\Common\Http\Middleware\ApiTransformDate;
use App\Modules\Common\Responses\SuccessResponse;
use App\Modules\Marketplace\Http\Controllers\MarketplaceController;
use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Http\Middleware\PrepareSalesSortRequest;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Models\MerchantConnectionSync;
use App\Modules\Merchant\Responses\Sale\BrandsResponse;
use App\Modules\Merchant\Responses\Sale\CategoriesResponse;
use App\Modules\Merchant\Responses\Sale\NomenclaturesResponse;
use App\Modules\Report\Contracts\MarketPlaceContract;
use App\Modules\Report\Events\WildberriesApiCollectDataEvent;
use App\Modules\Report\Events\WildberriesBrandsSalesExportEvent;
use App\Modules\Report\Events\WildberriesCategoriesSalesExportEvent;
use App\Modules\Report\Events\WildberriesNomenclaturesSalesExportEvent;
use App\Modules\Report\Requests\BrandsFilterRequest;
use App\Modules\Report\Requests\CategoriesFilterRequest;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\SalesPeriodRequest;
use App\Modules\Report\Requests\SalesSortRequest;
use App\Modules\Report\Requests\SalesStatisticsBrandsFilterRequest;
use App\Modules\Report\Requests\SalesStatisticsCategoriesFilterRequest;
use App\Modules\Report\Requests\SalesStatisticsNomenclatureFilterRequest;
use App\Modules\Report\Requests\SalesStatisticsRequest;
use App\Modules\Report\Resources\ReportResultFileExportResource;
use App\Modules\Report\Resources\SalesReportItemResource;
use App\Modules\Report\Resources\SalesStatisticsByRangeResource;
use Exception;
use Illuminate\Contracts\Events\Dispatcher;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Работа с поставками
 */
class SaleController extends MarketplaceController
{
    /** @inheritdoc */
    protected $middleware = [
        [
            'middleware' => ApiTransformDate::class . ':date_from,date_to,compare_date_from,compare_date_to',
            'options' => ['except' => 'sync'],
        ],
        [
            'middleware' => PrepareSalesSortRequest::class,
            'options' => ['only' => ['nomenclatures', 'brands', 'categories']],
        ],
    ];

    /**
     * Общие отчёты о продажах
     *
     * @param Merchant $merchant
     * @param SalesPeriodRequest $periodRequest
     * @return SalesReportItemResource
     */
    public function index(Merchant $merchant, SalesPeriodRequest $periodRequest): SalesReportItemResource
    {
        $response = $this->api->getSalesAllReport(
            $merchant->id,
            $periodRequest->getDateFrom(),
            $periodRequest->getDateTo()
        );
        return new SalesReportItemResource($response);
    }

    /**
     * Синхронизация данных
     *
     * @param Merchant $merchant
     * @param MerchantConnectionManagerContract $service
     * @param Dispatcher $events
     * @return SuccessResponse
     */
    public function sync(
        Merchant $merchant,
        MerchantConnectionManagerContract $service,
        Dispatcher $events
    ): SuccessResponse
    {
        // для Wildberries получаем ключ и запускаем обновление
        if ($this->marketplace->getId() === MarketPlaceContract::WILDBERRIES) {
            /** @var MerchantConnection|null $connection */
            $connection = $merchant
                ->marketplaceConnections
                ->where('marketplace', MarketPlaceContract::WILDBERRIES)
                ->first();
            if (!$connection) {
                throw new NotFoundHttpException();
            }

            $credentials = $service->getCredentialsValues($connection);
            $base64Key = $credentials['key'] ?? null;
            if (empty($base64Key)) {
                throw new NotFoundHttpException();
            }

            $events->dispatch(new WildberriesApiCollectDataEvent(
                $connection,
                $base64Key,
                MerchantConnectionSync::TYPE_MANUAL_SALES
            ));
        }

        return new SuccessResponse();
    }

    /**
     * Отчёты о продажах по номенклатурам
     *
     * @param Merchant $merchant
     * @param SalesPeriodRequest $periodRequest
     * @param NomenclatureFilterRequest $filterRequest
     * @param SalesSortRequest $sortRequest
     * @param PageRequest $pageRequest
     * @return NomenclaturesResponse
     * @throws Exception
     */
    public function nomenclatures(
        Merchant $merchant,
        SalesPeriodRequest $periodRequest,
        NomenclatureFilterRequest $filterRequest,
        SalesSortRequest $sortRequest,
        PageRequest $pageRequest
    ): NomenclaturesResponse
    {
        $response = $this->api->getSalesNomenclaturesReport(
            $merchant->id,
            $periodRequest,
            $filterRequest,
            $sortRequest,
            $pageRequest
        );
        return new NomenclaturesResponse($response->getItems(), $response->getPage());
    }

    /**
     * Статистические данные заказов и продаж номенклатур
     *
     * @param Merchant $merchant
     * @param SalesPeriodRequest $periodRequest
     * @param SalesStatisticsNomenclatureFilterRequest $filterRequest
     * @param SalesStatisticsRequest $salesStatisticsRequest
     * @return SalesStatisticsByRangeResource
     */
    public function statisticNomenclatures(
        Merchant $merchant,
        SalesPeriodRequest $periodRequest,
        SalesStatisticsNomenclatureFilterRequest $filterRequest,
        SalesStatisticsRequest $salesStatisticsRequest
    ): SalesStatisticsByRangeResource
    {
        $response = $this->api->getSalesStatisticsNomenclaturesReport(
            $merchant->id,
            $periodRequest,
            $filterRequest,
            $salesStatisticsRequest
        );
        return new SalesStatisticsByRangeResource($response);
    }

    /**
     * Экспортирует данные о продажах товаров в XLSX
     *
     * @param Merchant $merchant
     * @param SalesPeriodRequest $periodRequest
     * @param NomenclatureFilterRequest $filterRequest
     * @return ReportResultFileExportResource
     */
    public function exportNomenclatures(
        Merchant $merchant,
        SalesPeriodRequest $periodRequest,
        NomenclatureFilterRequest $filterRequest
    ): ReportResultFileExportResource
    {
        $reportResultFile = $this->api->requestSalesExport(
            $merchant->id,
            $periodRequest->validated(),
            $filterRequest->validated(),
            WildberriesNomenclaturesSalesExportEvent::class
        );
        return new ReportResultFileExportResource($merchant, $reportResultFile);
    }

    /**
     * Отчёты о продажах по брендам
     *
     * @param Merchant $merchant
     * @param SalesPeriodRequest $periodRequest
     * @param BrandsFilterRequest $filterRequest
     * @param SalesSortRequest $sortRequest
     * @param PageRequest $pageRequest
     * @return BrandsResponse
     * @throws Exception
     */
    public function brands(
        Merchant $merchant,
        SalesPeriodRequest $periodRequest,
        BrandsFilterRequest $filterRequest,
        SalesSortRequest $sortRequest,
        PageRequest $pageRequest
    ): BrandsResponse
    {
        $response = $this->api->getSalesBrandsReport(
            $merchant->id,
            $periodRequest,
            $filterRequest,
            $sortRequest,
            $pageRequest
        );
        return new BrandsResponse($response->getItems(), $response->getPage());
    }

    /**
     * Статистические данные заказов и продаж брендов
     *
     * @param Merchant $merchant
     * @param SalesPeriodRequest $periodRequest
     * @param SalesStatisticsBrandsFilterRequest $filterRequest
     * @param SalesStatisticsRequest $salesStatisticsRequest
     * @return SalesStatisticsByRangeResource
     */
    public function statisticBrands(
        Merchant $merchant,
        SalesPeriodRequest $periodRequest,
        SalesStatisticsBrandsFilterRequest $filterRequest,
        SalesStatisticsRequest $salesStatisticsRequest
    ): SalesStatisticsByRangeResource
    {
        $response = $this->api->getSalesStatisticsBrandsReport(
            $merchant->id,
            $periodRequest,
            $filterRequest,
            $salesStatisticsRequest
        );
        return new SalesStatisticsByRangeResource($response);
    }

    /**
     * Экспортирует данные о продажах брендов в XLSX
     *
     * @param Merchant $merchant
     * @param SalesPeriodRequest $periodRequest
     * @param BrandsFilterRequest $filterRequest
     * @return ReportResultFileExportResource
     */
    public function exportBrands(
        Merchant $merchant,
        SalesPeriodRequest $periodRequest,
        BrandsFilterRequest $filterRequest
    ): ReportResultFileExportResource
    {
        $reportResultFile = $this->api->requestSalesExport(
            $merchant->id,
            $periodRequest->validated(),
            $filterRequest->validated(),
            WildberriesBrandsSalesExportEvent::class
        );
        return new ReportResultFileExportResource($merchant, $reportResultFile);
    }

    /**
     * Отчёты о продажах по категориям товаров
     *
     * @param Merchant $merchant
     * @param SalesPeriodRequest $periodRequest
     * @param CategoriesFilterRequest $filterRequest
     * @param SalesSortRequest $sortRequest
     * @param PageRequest $pageRequest
     * @return CategoriesResponse
     * @throws Exception
     */
    public function categories(
        Merchant $merchant,
        SalesPeriodRequest $periodRequest,
        CategoriesFilterRequest $filterRequest,
        SalesSortRequest $sortRequest,
        PageRequest $pageRequest
    ): CategoriesResponse
    {
        $response = $this->api->getSalesCategoriesReport(
            $merchant->id,
            $periodRequest,
            $filterRequest,
            $sortRequest,
            $pageRequest
        );
        return new CategoriesResponse($response->getItems(), $response->getPage());
    }

    /**
     * Статистические данные заказов и продаж категорий
     *
     * @param Merchant $merchant
     * @param SalesPeriodRequest $periodRequest
     * @param SalesStatisticsCategoriesFilterRequest $filterRequest
     * @param SalesStatisticsRequest $salesStatisticsRequest
     * @return SalesStatisticsByRangeResource
     */
    public function statisticCategories(
        Merchant $merchant,
        SalesPeriodRequest $periodRequest,
        SalesStatisticsCategoriesFilterRequest $filterRequest,
        SalesStatisticsRequest $salesStatisticsRequest
    ): SalesStatisticsByRangeResource
    {
        $response = $this->api->getSalesStatisticsCategoriesReport(
            $merchant->id,
            $periodRequest,
            $filterRequest,
            $salesStatisticsRequest
        );
        return new SalesStatisticsByRangeResource($response);
    }

    /**
     * Экспортирует данные о продажах категорий в XLSX
     *
     * @param Merchant $merchant
     * @param SalesPeriodRequest $periodRequest
     * @param CategoriesFilterRequest $filterRequest
     * @return ReportResultFileExportResource
     */
    public function exportCategories(
        Merchant $merchant,
        SalesPeriodRequest $periodRequest,
        CategoriesFilterRequest $filterRequest
    ): ReportResultFileExportResource
    {
        $reportResultFile = $this->api->requestSalesExport(
            $merchant->id,
            $periodRequest->validated(),
            $filterRequest->validated(),
            WildberriesCategoriesSalesExportEvent::class
        );
        return new ReportResultFileExportResource($merchant, $reportResultFile);
    }
}
