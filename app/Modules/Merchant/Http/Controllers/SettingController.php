<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Modules\Auth\Models\User;
use App\Modules\Marketplace\Http\Controllers\MarketplaceController;
use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Events\MerchantConnectionCredentialsCreatedEvent;
use App\Modules\Merchant\Http\Middleware\PrepareStoreMerchantConnectionRequest;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Models\MerchantConnectionSync;
use App\Modules\Merchant\Requests\StoreMerchantConnectionRequest;
use App\Modules\Merchant\Responses\Setting\SettingResponse;
use App\Modules\Merchant\Validation\Filters\WildberriesKeyFilter;
use App\Modules\Report\Contracts\MarketPlaceContract;
use App\Modules\Report\Events\WildberriesApiCollectDataEvent;
use Illuminate\Http\Request;

/**
 * Контроллер настроек
 */
class SettingController extends MarketplaceController
{
    protected $middleware = [
        ['middleware' => PrepareStoreMerchantConnectionRequest::class, 'options' => ['only' => 'update']],
    ];

    /** @var MerchantConnectionManagerContract */
    protected MerchantConnectionManagerContract $service;

    /**
     * Инициализация сервиса
     *
     * @param MerchantConnectionManagerContract $service
     */
    public function __construct(MerchantConnectionManagerContract $service)
    {
        parent::__construct(null);
        $this->service = $service;
    }

    /**
     * Получить текущее подключение для маркетплейса
     *
     * @param Merchant $merchant
     * @return MerchantConnection
     */
    protected function getCurrentConnection(Merchant $merchant): MerchantConnection
    {
        /** @var MerchantConnection $merchantConnection */
        $merchantConnection = $merchant->marketplaceConnections()->firstOrCreate([
            'marketplace' => $this->marketplace->getId(),
        ], [
            'credentials' => '',
        ]);
        return $merchantConnection;
    }

    /**
     * Получение значений настроек доступов мерчанта к маркетплейсу
     *
     * @param Merchant $merchant
     * @return SettingResponse
     */
    public function index(Merchant $merchant): SettingResponse
    {
        $connection = $this->getCurrentConnection($merchant);
        return new SettingResponse($this->service->getCredentialsValues($connection));
    }

    /**
     * Сохранение значений настроек доступов мерчанта к маркетплейсу
     *
     * @param Merchant $merchant
     * @param Request $request
     * @return SettingResponse
     */
    public function update(Merchant $merchant, Request $request): SettingResponse
    {
        $connection = $this->getCurrentConnection($merchant);
        $marketplace = $this->marketplace->getId();

        // для Wildberries передаём в сервис события для запуска обновлений
        if ($marketplace === MarketPlaceContract::WILDBERRIES && $newKey = $request->input('key')) {
            // сбрасываем дату последнего обновления, чтобы запустить полную загрузку данных за 90 дней
            $newConnection = clone $connection;
            $newConnection->last_sync = null;
            $newConnection->error = null;

            $newKey = with(new WildberriesKeyFilter())->filter($newKey);

            $credentialsChangedEvents = [
                new WildberriesApiCollectDataEvent($newConnection, $newKey, MerchantConnectionSync::TYPE_INITIAL, true),
            ];

            // для первичного добавления ключа
            if (empty($connection->credentials)) {
                // вызывается соответствующее событие
                /** @var User|null $user */
                $user = $request->user('api') ?? $merchant->owner;
                if ($user && $user instanceof User) {
                    $credentialsChangedEvents[] = new MerchantConnectionCredentialsCreatedEvent($user, $connection);
                }
            }

            $this->service->setCredentialsChangedEvents($credentialsChangedEvents);
        }

        // ключи полей параметров подключения к маркетплейсу
        $credentialsKeys = array_keys($this->service->getCredentialsNames($marketplace));
        // данные из запроса по ключам + идентификатор маркетплейса
        $requestData = array_merge($request->only($credentialsKeys), compact('marketplace'));
        // запрос для сервиса управления подключениями к маркетплейсам
        $serviceRequest = StoreMerchantConnectionRequest::createFromArray($requestData, $request->user());
        // сохранение параметров
        if ($this->service->store($serviceRequest, $merchant, $connection)) {
            return new SettingResponse($this->service->getCredentialsValues($connection));
        }

        throw new \RuntimeException("Не удалось сохранить подключение");
    }
}
