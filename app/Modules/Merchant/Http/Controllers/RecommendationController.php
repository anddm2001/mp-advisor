<?php

namespace App\Modules\Merchant\Http\Controllers;

use App\Modules\Common\Http\Middleware\ApiTransformDate;
use App\Modules\Common\Responses\SuccessResponse;
use App\Modules\Marketplace\Http\Controllers\MarketplaceController;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Requests\ExportRecommendationRequest;
use App\Modules\Merchant\Responses\Recommendation\NomenclatureResponse;
use App\Modules\Merchant\Responses\Recommendation\ShowResponse;
use App\Modules\Report\Helpers\ProtobufTimestampHelper;
use App\Modules\Report\Models\RecommendationReport;
use App\Modules\Report\Requests\NomenclatureFilterRequest;
use App\Modules\Report\Requests\PageRequest;
use App\Modules\Report\Requests\RecommendationsNomenclatureReportApplyAllRequest;
use App\Modules\Report\Requests\RecommendationsNomenclatureReportUpdateRequest;
use App\Modules\Report\Requests\RecommendationsReportFilterRequest;
use App\Modules\Report\Requests\RecommendationsReportGenerateRequest;
use App\Modules\Report\Requests\RecommendationsReportSortRequest;
use App\Modules\Report\Resources\RecommendationsReportSummaryResource;
use App\Modules\Report\Resources\ReportResultFileExportResource;
use Illuminate\Support\Carbon;
use Report\RecommendationsReportSummary;

/**
 * Работа с расчётом поставки
 */
class RecommendationController extends MarketplaceController
{
    protected $middleware = [
        [
            'middleware' => ApiTransformDate::class . ':delivery_date,calculate_from,calculate_to',
            'options' => ['only' => 'update']
        ],
    ];

    /**
     * Получить идентификатор отчёта
     *
     * @param Merchant $merchant
     * @return string
     */
    protected function getReportId(Merchant $merchant): string
    {
        /** @var RecommendationReport $recommendationReport */
        $recommendationReport = $merchant->recommendationReport()->firstOrNew();
        if (empty($recommendationReport->report_id)) {
            $generateResponse = $this->api->generateRecommendationsReport(
                $merchant->id,
                '',
                RecommendationReport::getGenerationRequestForWildberriesApi()
            );
            $recommendationReport->report_id = $generateResponse->getReportID();
            $recommendationReport->save();
        }

        abort_if(empty($recommendationReport->report_id), 500, 'Не удалось получить отчёт рекомендаций к поставке.');

        $this->checkRegenerateReport($merchant->id, $recommendationReport);

        return $recommendationReport->report_id;
    }

    /**
     * Проверить и перегенерировать отчёт по необходимости.
     *
     * @param int $merchantId
     * @param RecommendationReport $recommendationReport
     */
    protected function checkRegenerateReport(int $merchantId, RecommendationReport $recommendationReport): void
    {
        $now = Carbon::now();
        if ($now->diffInRealHours($recommendationReport->updated_at) >= 24) {
            $summaryResponse = $this->api->getRecommendationsReportSummary($merchantId, $recommendationReport->report_id);
            /** @var RecommendationsReportSummary|null $summary */
            $summary = $summaryResponse->getSummary();

            if ($summary) {
                $today = Carbon::today('UTC');
                $deliveryDate = ProtobufTimestampHelper::toDateTime($summary->getDeliveryDate());
                if ($deliveryDate < $today) {
                    $deliveryDate = $today->copy()->addDay();
                }
                $format = RecommendationsReportGenerateRequest::DATE_FORMAT;

                $generateRequest = RecommendationsReportGenerateRequest::createFromArray([
                    'warehouse' => $summary->getWarehouseName(),
                    'delivery_date' => $deliveryDate->format($format),
                    'calculate_from' => ProtobufTimestampHelper::toFormat($summary->getCalculateFrom(), $format),
                    'calculate_to' => ProtobufTimestampHelper::toFormat( $summary->getCalculateTo(), $format),
                    'report_days' => $summary->getReportDays(),
                ]);
            } else {
                $generateRequest = RecommendationReport::getGenerationRequestForWildberriesApi();
            }

            $this->api->generateRecommendationsReport($merchantId, $recommendationReport->report_id, $generateRequest);

            $recommendationReport->touch();
        }
    }

    /**
     * Отображение отчёта
     *
     * @param Merchant $merchant
     * @param NomenclatureFilterRequest $nomenclatureFilter
     * @param RecommendationsReportFilterRequest $recommendationsReportFilter
     * @param RecommendationsReportSortRequest $sortRequest
     * @param PageRequest $pageRequest
     * @return ShowResponse
     */
    public function show(
        Merchant $merchant,
        NomenclatureFilterRequest $nomenclatureFilter,
        RecommendationsReportFilterRequest $recommendationsReportFilter,
        RecommendationsReportSortRequest $sortRequest,
        PageRequest $pageRequest
    ): ShowResponse
    {
        $reportId = $this->getReportId($merchant);
        $response = $this->api->getRecommendationsReport(
            $merchant->id,
            $reportId,
            $nomenclatureFilter,
            $recommendationsReportFilter,
            $sortRequest,
            $pageRequest
        );
        /** @var RecommendationReport|null $recommendationReportParams */
        $recommendationReportParams = RecommendationReport::query()->where('report_id', $reportId)->first();
        return new ShowResponse(
            $response->getItems(),
            $response->getSummary(),
            $response->getPage(),
            $recommendationReportParams);
    }

    /**
     * Перегенерация отчёта с новыми параметрами
     *
     * @param Merchant $merchant
     * @param RecommendationsReportGenerateRequest $request
     * @return SuccessResponse
     */
    public function update(Merchant $merchant, RecommendationsReportGenerateRequest $request): SuccessResponse
    {
        $this->api->generateRecommendationsReport($merchant->id, $this->getReportId($merchant), $request);
        return new SuccessResponse();
    }

    /**
     * Обобщённый результат отчёта о рекомендациях
     *
     * @param Merchant $merchant
     * @return RecommendationsReportSummaryResource
     */
    public function summary(Merchant $merchant): RecommendationsReportSummaryResource
    {
        $response = $this->api->getRecommendationsReportSummary($merchant->id, $this->getReportId($merchant));
        return new RecommendationsReportSummaryResource($response->getSummary());
    }

    /**
     * Обновление обобщённого результата отчёта о рекомендациях
     *
     * @param Merchant $merchant
     *
     * @return RecommendationsReportSummaryResource
     */
    public function updateSummary(Merchant $merchant): RecommendationsReportSummaryResource
    {
        $response = $this->api->recalculateRecommendationsReportSummary($merchant->id, $this->getReportId($merchant));
        return new RecommendationsReportSummaryResource($response->getSummary());
    }

    /**
     * Обновление номенклатуры в поставке
     *
     * @param Merchant $merchant
     * @param RecommendationsNomenclatureReportUpdateRequest $request
     * @return NomenclatureResponse
     */
    public function nomenclature(
        Merchant $merchant,
        RecommendationsNomenclatureReportUpdateRequest $request
    ): NomenclatureResponse
    {
        $response = $this->api->updateRecommendationsNomenclature(
            $merchant->id,
            $this->getReportId($merchant),
            $request
        );
        return new NomenclatureResponse($response);
    }

    /**
     * Добавить в поставку / убрать из поставки все номенклатуры с рекомендациями
     *
     * @param Merchant $merchant
     * @param RecommendationsNomenclatureReportApplyAllRequest $request
     * @return SuccessResponse
     */
    public function nomenclatureApplyAll(
        Merchant $merchant,
        RecommendationsNomenclatureReportApplyAllRequest $request
    ): SuccessResponse
    {
        $response = $this->api->updateRecommendationsNomenclatureApplyAll(
            $merchant->id,
            $this->getReportId($merchant),
            $request
        );
        return new SuccessResponse($response->getApply() === (bool) $request->input('apply'));
    }

    /**
     * Запрос на экспорт файла в XLSX
     *
     * @param Merchant $merchant
     * @param ExportRecommendationRequest $request
     * @return ReportResultFileExportResource
     */
    public function export(Merchant $merchant, ExportRecommendationRequest $request): ReportResultFileExportResource
    {
        $file = $this->api->requestRecommendationsExport(
            $merchant->id,
            $this->getReportId($merchant),
            $request->input('format')
        );
        return new ReportResultFileExportResource($merchant, $file);
    }
}
