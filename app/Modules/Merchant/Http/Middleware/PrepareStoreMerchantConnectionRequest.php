<?php

namespace App\Modules\Merchant\Http\Middleware;

use App\Modules\Marketplace\Entities\Marketplace;
use Illuminate\Http\Request;

/**
 * Подготовка данных запроса для сохранения настроек мерчанта
 */
class PrepareStoreMerchantConnectionRequest
{
    /**
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, \Closure $next)
    {
        $request->request->add([
            'marketplace' => app(Marketplace::class)->getId(),
            'key' => $request->input('base64_key'),
        ]);
        $request->request->remove('base64_key');

        return $next($request);
    }
}
