<?php

namespace App\Modules\Merchant\Http\Middleware;

use App\Modules\Billing\Exceptions\PaymentRequiredException;
use App\Modules\Merchant\Models\Merchant;
use Closure;
use Illuminate\Http\Request;

/**
 * Проверить, что мерчант не заблокирован из-за нехватки средств
 */
class MerchantIsNotBalanceBlocked
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws PaymentRequiredException
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var Merchant|null $merchant */
        $merchant = $request->route('merchant');
        if ($merchant && $merchant->is_balance_blocked) {
            throw new PaymentRequiredException([
                'balance' => $merchant->balance,
                'lack_amount' => $merchant->lack_amount,
                'billing_rate_min_amount' => $merchant->billingRate ? $merchant->billingRate->min_amount : null,
            ]);
        }

        return $next($request);
    }
}
