<?php

namespace App\Modules\Merchant\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Подготовка данных запроса для сортировки номенклатур
 */
class PrepareNomenclaturesSortRequest
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->has('sort_condition')) {
            $request->request->add(['condition' => $request->input('sort_condition')]);
            $request->request->remove('sort_condition');
        }
        if ($request->has('sort_direction')) {
            $request->request->add(['direction' => $request->input('sort_direction')]);
            $request->request->remove('sort_direction');
        }

        return $next($request);
    }
}
