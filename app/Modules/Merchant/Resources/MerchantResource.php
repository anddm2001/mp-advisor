<?php

namespace App\Modules\Merchant\Resources;

use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

/**
 * Данные мерчанта
 */
class MerchantResource extends JsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        $today = Carbon::today();
        /** @var Merchant $merchant */
        $merchant = $this->resource;
        return [
            'id' => $merchant->id,
            'name' => $merchant->name,
            'balance' => $merchant->balance,
            'balance_days' => $merchant->balance_days,
            'is_balance_blocked' => $merchant->is_balance_blocked,
            'lack_amount' => $merchant->lack_amount,
            'billing_rate_min_amount' => $merchant->billingRate ? $merchant->billingRate->min_amount : null,
            'trial_period_days' => $merchant->trial_period_finish && $merchant->trial_period_finish > $today
                ? $merchant->trial_period_finish->diffInDays($today)
                : null,
            'has_nonempty_settings' => $merchant->marketplaceConnections
                ->reject(function (MerchantConnection $connection, $key): bool {
                    return empty($connection->credentials);
                })
                ->isNotEmpty(),
        ];
    }
}
