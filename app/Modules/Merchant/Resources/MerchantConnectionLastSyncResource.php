<?php

namespace App\Modules\Merchant\Resources;

use App\Modules\Merchant\Models\MerchantConnection;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Данные последней синхронизации с маркетплейсом
 */
class MerchantConnectionLastSyncResource extends JsonResource
{
    /**
     * @param MerchantConnection $resource
     */
    public function __construct(MerchantConnection $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        /** @var MerchantConnection $merchantConnection */
        $merchantConnection = $this->resource;
        return [
            'last_sync' => $merchantConnection->last_sync ? (int) $merchantConnection->last_sync->format('Uv') : null,
            'last_sync_error' => $merchantConnection->error ?: null,
        ];
    }
}
