<?php

namespace App\Modules\Merchant\Events;

use App\Modules\Auth\Models\User;
use App\Modules\Merchant\Models\MerchantConnection;

/**
 * Событие первичного добавления параметров доступа к маркетплейсу
 */
class MerchantConnectionCredentialsCreatedEvent
{
    /** @var User */
    protected User $user;

    /** @var MerchantConnection */
    protected MerchantConnection $merchantConnection;

    /**
     * @param User $user
     * @param MerchantConnection $merchantConnection
     */
    public function __construct(User $user, MerchantConnection $merchantConnection)
    {
        $this->user = $user;
        $this->merchantConnection = $merchantConnection;
    }

    /**
     * Получить пользователя, инициировавшего событие
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * Получить модель соединения с маркетплейсом
     *
     * @return MerchantConnection
     */
    public function getMerchantConnection(): MerchantConnection
    {
        return $this->merchantConnection;
    }
}
