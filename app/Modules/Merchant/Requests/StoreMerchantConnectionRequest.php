<?php

namespace App\Modules\Merchant\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Models\Log;
use App\Modules\Common\Requests\FormRequest;
use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Services\MerchantConnectionLogger;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

/**
 * Запрос на сохранение ключей подключения
 *
 * @package App\Modules\Merchant\Requests
 */
class StoreMerchantConnectionRequest extends FormRequest
{
    /**
     * Доступ только для авторизованных пользователей
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user() instanceof User;
    }

    /**
     * Получить из роутинга идентификатор маркетплейса
     *
     * @return int
     */
    public function getMarketplace(): int
    {
        $marketplace = $this->route('marketplace');

        if (empty($marketplace)) {
            $marketplace = $this->input('marketplace');
        }

        if (!is_scalar($marketplace) || empty($marketplace)) {
            throw new \RuntimeException();
        }

        return (int) $marketplace;
    }

    /**
     * Правила валидации
     *
     * @param MerchantConnectionManagerContract $service
     * @return Rule[]|string[]
     */
    public function rules(MerchantConnectionManagerContract $service): array
    {
        return $service->getCredentialsValidation($this->getMarketplace());
    }

    /**
     * Подписи атрибутов
     *
     * @return array
     */
    public function attributes(): array
    {
        /** @var MerchantConnectionManagerContract $service */
        $service = app(MerchantConnectionManagerContract::class);

        return $service->getCredentialsNames($this->getMarketplace());
    }

    /**
     * @inheritDoc
     */
    protected function failedValidation(Validator $validator): void
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }

    /**
     * Обработка действий после валидации (логирование при фейлах)
     *
     * @param Validator $validator
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator) {
            // если валидация не прошла
            if ($validator->failed()) {
                /** @var MerchantConnectionLogger $logger */
                $logger = with(new MerchantConnectionLogger(MerchantConnectionLogger::CATEGORY_VALIDATION_FAILED));
                /** @var MerchantConnectionManagerContract $service */
                $service = resolve(MerchantConnectionManagerContract::class);
                /** @var Encrypter $encrypter */
                $encrypter = resolve(Encrypter::class);

                $fields = collect($service->getCredentialsNames($this->getMarketplace()));

                $message = 'Введённые данные: ';
                $message .= Log::ENCRYPTED_DATA_BEGIN_TAG; // метка начала зашифрованных данных
                // из запроса собираются параметры подключения и зашифровываются
                $message .= $encrypter->encrypt($fields->keys()->mapWithKeys(function (string $key) use ($fields) {
                    return [$fields->get($key, $key) => $this->input($key, '') ?: '(пусто)'];
                })->prepend($this->user()->id ?? '?', 'Пользователь')->toArray());
                $message .= Log::ENCRYPTED_DATA_END_TAG; // метка конца зашифрованных данных

                // собираются ошибки валидации
                $errors = $validator->errors();
                if ($errors->isNotEmpty()) {
                    $message .= ' / Ошибки: ';
                    foreach ($errors->keys() as $fKey) {
                        $message .= $fields->get($fKey, $fKey) . ' — ' . implode(', ', $errors->get($fKey)) . '; ';
                    }
                    $message = rtrim($message, '; ');
                }

                $logger->error($message);
            }
        });
    }
}
