<?php

namespace App\Modules\Merchant\Requests;

use App\Modules\Common\Requests\FormRequest;
use App\Modules\Report\Importer\WildberriesNomenclaturesImporter;
use Illuminate\Validation\Rule;

/**
 * Запрос на импорт данных складов
 */
class ImportStockRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'mimes:xlsx'],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'type' => 'тип импорта',
            'file' => 'XLSX-файл с данными для импортирования',
        ];
    }
}
