<?php

namespace App\Modules\Merchant\Requests;

use App\Modules\Common\Requests\FormRequest;
use App\Modules\Report\Events\WildberriesRecommendationsExportEvent;
use Illuminate\Validation\Rule;

/**
 * Запрос на экспорт отчёта рекомендаций
 */
class ExportRecommendationRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'format' => ['required', 'string', Rule::in([
                WildberriesRecommendationsExportEvent::EXPORT_FORMAT_WB_INCOME,
                WildberriesRecommendationsExportEvent::EXPORT_FORMAT_EXCEL_APPLIED,
                WildberriesRecommendationsExportEvent::EXPORT_FORMAT_EXCEL_FULL,
            ])],
        ];
    }
}
