<?php

namespace App\Modules\Merchant\Requests;

use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на вывод списка поставок
 */
class IndexIncomeRequest extends FormRequest
{
    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            'status' => ['required', 'string'],
        ];
    }
}
