<?php

namespace App\Modules\Merchant\Requests;

use App\Modules\Common\Requests\FormRequest;
use App\Modules\Report\Importer\WildberriesNomenclaturesImporter;
use Illuminate\Validation\Rule;

/**
 * Запрос на импорт данных номенклатур
 */
class ImportNomenclatureRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'type' => ['required', 'string', Rule::in([
                WildberriesNomenclaturesImporter::TYPE_BASE_COSTS,
                WildberriesNomenclaturesImporter::TYPE_SUPPLY_SETS,
            ])],
            'file' => ['required', 'file', 'mimes:xlsx'],
        ];
    }

    /** @inheritDoc */
    public function attributes()
    {
        return [
            'type' => 'тип импорта',
            'file' => 'XLSX-файл с данными для импортирования',
        ];
    }
}
