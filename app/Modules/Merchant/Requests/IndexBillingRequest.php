<?php

namespace App\Modules\Merchant\Requests;

use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Common\Requests\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Запрос для вывода сводной биллинг-информации по мерчанту
 */
class IndexBillingRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array[]
     */
    public function rules(): array
    {
        return [
            'type' => ['nullable', 'string', Rule::in(array_keys(BillingOperation::getTypes()))],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'type' => 'Действие',
        ];
    }
}
