<?php

namespace App\Modules\Merchant\Requests;

use App\Modules\Common\Requests\FormRequest;
use Illuminate\Http\UploadedFile;

/**
 * Запрос с данными формы обратной связи
 */
class StoreFeedbackRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array[]
     */
    public function rules(): array
    {
        return [
            'category' => ['required', 'string'],
            'subcategory' => ['required', 'string'],
            'message' => ['required', 'string'],
            'file' => ['nullable', 'file', 'max:5120', 'mimes:jpeg,gif,bmp,png,doc,rtf,txt,zip,rar'],
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'category' => 'Что случилось',
            'subcategory' => 'Точнее',
            'message' => 'Текст сообщения',
            'file' => 'Файл',
        ];
    }

    /**
     * Получить текст поля "Что случилось"
     *
     * @return string
     */
    public function getCategory(): string
    {
        return $this->input('category', '');
    }

    /**
     * Получить текст поля "Точнее"
     *
     * @return string
     */
    public function getSubCategory(): string
    {
        return $this->input('subcategory', '');
    }

    /**
     * Получить текст поля "Текст сообщения"
     *
     * @return string
     */
    public function getMessage(): string
    {
        return nl2br($this->input('message', ''));
    }
}
