<?php

namespace App\Modules\Merchant\Responses\Sale;

use App\Modules\Report\Resources\NomenclatureSalesReportResource;
use App\Modules\Report\Resources\PageResponseResource;
use Google\Protobuf\Internal\RepeatedField;
use Illuminate\Http\JsonResponse;
use Report\GetNomenclatureSalesResponse\NomenclatureSalesReport;
use Report\PageResponse;

class NomenclaturesResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param RepeatedField|NomenclatureSalesReport[] $reports
     * @param PageResponse $page
     */
    public function __construct(RepeatedField $reports, PageResponse $page)
    {
        parent::__construct([
            'items' => NomenclatureSalesReportResource::collection(collect($reports)),
            'page' => new PageResponseResource($page),
        ]);
    }
}
