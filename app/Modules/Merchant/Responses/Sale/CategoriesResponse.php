<?php

namespace App\Modules\Merchant\Responses\Sale;

use App\Modules\Report\Resources\CategorySalesReportResource;
use App\Modules\Report\Resources\PageResponseResource;
use Google\Protobuf\Internal\RepeatedField;
use Illuminate\Http\JsonResponse;
use Report\GetCategorySalesResponse\CategorySalesReport;
use Report\PageResponse;

class CategoriesResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param RepeatedField|CategorySalesReport[] $reports
     * @param PageResponse $page
     */
    public function __construct(RepeatedField $reports, PageResponse $page)
    {
        parent::__construct([
            'items' => CategorySalesReportResource::collection(collect($reports)),
            'page' => new PageResponseResource($page),
        ]);
    }
}
