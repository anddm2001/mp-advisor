<?php

namespace App\Modules\Merchant\Responses\Sale;

use App\Modules\Report\Resources\BrandSalesReportResource;
use App\Modules\Report\Resources\PageResponseResource;
use Google\Protobuf\Internal\RepeatedField;
use Illuminate\Http\JsonResponse;
use Report\GetBrandSalesResponse\BrandSalesReport;
use Report\PageResponse;

class BrandsResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param RepeatedField|BrandSalesReport[] $reports
     * @param PageResponse $page
     */
    public function __construct(RepeatedField $reports, PageResponse $page)
    {
        parent::__construct([
            'items' => BrandSalesReportResource::collection(collect($reports)),
            'page' => new PageResponseResource($page),
        ]);
    }
}
