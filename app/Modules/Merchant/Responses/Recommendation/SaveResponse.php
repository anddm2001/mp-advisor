<?php

namespace App\Modules\Merchant\Responses\Recommendation;

use Illuminate\Http\JsonResponse;

class SaveResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param string $reportId
     */
    public function __construct(string $reportId)
    {
        parent::__construct(['report_id' => $reportId]);
    }
}
