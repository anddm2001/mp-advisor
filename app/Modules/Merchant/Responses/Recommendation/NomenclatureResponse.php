<?php

namespace App\Modules\Merchant\Responses\Recommendation;

use Illuminate\Http\JsonResponse;
use Report\UpdateRecommendationsNomenclatureRequest;

class NomenclatureResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param UpdateRecommendationsNomenclatureRequest $response
     */
    public function __construct(UpdateRecommendationsNomenclatureRequest $response)
    {
        parent::__construct([
            'vendor_code' => $response->getVendorCode(),
            'size' => $response->getSize(),
            'need_count' => $response->getNeedCount(),
            'apply' => $response->getApply(),
        ]);
    }
}
