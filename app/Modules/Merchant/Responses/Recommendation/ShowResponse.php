<?php

namespace App\Modules\Merchant\Responses\Recommendation;

use App\Modules\Report\Models\RecommendationReport;
use App\Modules\Report\Resources\NomenclatureRecommendationsResource;
use App\Modules\Report\Resources\PageResponseResource;
use App\Modules\Report\Resources\RecommendationsReportSummaryResource;
use Google\Protobuf\Internal\RepeatedField;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Report\GetRecommendationsReportResponse\NomenclatureRecommendations;
use Report\PageResponse;
use Report\RecommendationsReportSummary;

class ShowResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param RepeatedField|NomenclatureRecommendations[] $nomenclatures
     * @param RecommendationsReportSummary $summary
     * @param PageResponse $page
     * @param RecommendationReport|null $reportParams
     */
    public function __construct(
        RepeatedField $nomenclatures,
        RecommendationsReportSummary $summary,
        PageResponse $page,
        ?RecommendationReport $reportParams
    ) {
        parent::__construct([
            'items' => NomenclatureRecommendationsResource::collection(collect($nomenclatures)),
            'summary' => new RecommendationsReportSummaryResource($summary),
            'page' => new PageResponseResource($page),
            'internal_report_updated_at' => $reportParams
                ? $reportParams->updated_at->format(Carbon::DEFAULT_TO_STRING_FORMAT)
                : null,
        ]);
    }
}
