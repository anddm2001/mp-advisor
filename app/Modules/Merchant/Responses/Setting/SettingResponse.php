<?php

namespace App\Modules\Merchant\Responses\Setting;

use Illuminate\Http\JsonResponse;

/**
 * Настройки доступов мерчанта к маркетплейсу
 */
class SettingResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        parent::__construct([
            'base64_key' => $settings['key'] ?? '',
        ]);
    }
}
