<?php

namespace App\Modules\Merchant\Responses\Nomenclature;

use Illuminate\Http\JsonResponse;
use Report\UpdateNomenclatureSizeRequest;

/**
 * Изменение номенклатуры
 */
class UpdateResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param UpdateNomenclatureSizeRequest $updateNomenclatureSizeRequest
     */
    public function __construct(UpdateNomenclatureSizeRequest $updateNomenclatureSizeRequest)
    {
        parent::__construct([
            'nomenclature_id' => $updateNomenclatureSizeRequest->getNomenclatureID(),
            'size' => $updateNomenclatureSizeRequest->getSize(),
            'base_cost' => $updateNomenclatureSizeRequest->getBaseCost(),
            'supply_set' => $updateNomenclatureSizeRequest->getSupplySet(),
            'removed' => $updateNomenclatureSizeRequest->getRemoved(),
        ]);
    }
}
