<?php

namespace App\Modules\Merchant\Responses\Nomenclature;

use App\Modules\Report\Resources\NomenclatureResource;
use App\Modules\Report\Resources\PageResponseResource;
use Google\Protobuf\Internal\RepeatedField;
use Illuminate\Http\JsonResponse;
use Report\Nomenclature;
use Report\PageResponse;

/**
 * Список всех номенклатур мерчанта
 */
class IndexResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param RepeatedField|Nomenclature[] $nomenclatures
     * @param PageResponse $page
     */
    public function __construct(RepeatedField $nomenclatures, PageResponse $page)
    {
        parent::__construct([
            'items' => NomenclatureResource::collection(collect($nomenclatures)),
            'page' => new PageResponseResource($page),
        ]);
    }
}
