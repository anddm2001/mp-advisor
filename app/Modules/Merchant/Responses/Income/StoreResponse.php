<?php

namespace App\Modules\Merchant\Responses\Income;

use Illuminate\Http\JsonResponse;
use Report\CreateIncomeResponse;

/**
 * Обновлённая поставка
 */
class StoreResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param CreateIncomeResponse $response
     */
    public function __construct(CreateIncomeResponse $response)
    {
        parent::__construct([
            'nomenclatures' => $response->getNomenclatures(),
            'quantity' => $response->getQuantity(),
            'unknown_nomenclatures' => $response->getUnknownNomenclatures(),
        ]);
    }
}
