<?php

namespace App\Modules\Merchant\Responses\Income;

use App\Modules\Report\Resources\IncomeNomenclatureResource;
use App\Modules\Report\Resources\IncomeResource;
use Google\Protobuf\Internal\RepeatedField;
use Illuminate\Http\JsonResponse;
use Report\Income;
use Report\IncomeNomenclature;

/**
 * Детали поставки
 */
class ShowResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param Income $income
     * @param RepeatedField|IncomeNomenclature[] $nomenclatures
     */
    public function __construct(Income $income, RepeatedField $nomenclatures)
    {
        parent::__construct([
            'income' => new IncomeResource($income),
            'items' => IncomeNomenclatureResource::collection(collect($nomenclatures)),
        ]);
    }
}
