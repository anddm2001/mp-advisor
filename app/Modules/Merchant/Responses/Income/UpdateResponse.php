<?php

namespace App\Modules\Merchant\Responses\Income;

use App\Modules\Report\Resources\IncomeResource;
use Illuminate\Http\JsonResponse;
use Report\Income;

/**
 * Обновлённая поставка
 */
class UpdateResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param Income $income
     */
    public function __construct(Income $income)
    {
        parent::__construct(new IncomeResource($income));
    }
}
