<?php

namespace App\Modules\Merchant\Responses\Income;

use App\Modules\Report\Resources\IncomeResource;
use App\Modules\Report\Resources\PageResponseResource;
use Google\Protobuf\Internal\RepeatedField;
use Illuminate\Http\JsonResponse;
use Report\Income;
use Report\PageResponse;

/**
 * Список поставок
 */
class IndexResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param RepeatedField|Income[] $incomes
     * @param PageResponse $page
     */
    public function __construct(RepeatedField $incomes, PageResponse $page)
    {
        parent::__construct([
            'items' => IncomeResource::collection(collect($incomes)),
            'page' => new PageResponseResource($page),
        ]);
    }
}
