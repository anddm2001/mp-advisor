<?php

namespace App\Modules\Merchant\Listeners;

use App\Modules\Merchant\Events\MerchantConnectionCredentialsCreatedEvent;

class MerchantConnectionFixCreatedAtListener
{
    /**
     * Указание правильной даты "создания"
     *
     * @param MerchantConnectionCredentialsCreatedEvent $event
     */
    public function handle(MerchantConnectionCredentialsCreatedEvent $event): void
    {
        $merchantConnection = $event->getMerchantConnection();
        $merchantConnection->setCreatedAt($merchantConnection->{$merchantConnection->getUpdatedAtColumn()})->save();
    }
}
