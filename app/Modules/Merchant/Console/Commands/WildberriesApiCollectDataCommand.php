<?php

namespace App\Modules\Merchant\Console\Commands;

use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Models\MerchantConnectionSync;
use App\Modules\Report\Contracts\MarketPlaceContract;
use App\Modules\Report\Events\WildberriesApiCollectDataEvent;
use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Collection;

/**
 * Команда для запуска сбора данных из Wildberries API по всем мерчантам
 */
class WildberriesApiCollectDataCommand extends Command
{
    /** @inheritdoc */
    protected $signature = 'merchant:wildberries-api-collect-data {--merchant= : Идентификаторы мерчантов, для которых получить данные (по умолчанию - для всех)}';

    /** @inheritdoc */
    protected $description = 'Сбор данных из Wildberries API по всем мерчантам';

    /**
     * Запуск команды
     *
     * @param Dispatcher $events
     * @param MerchantConnectionManagerContract $service
     * @param MerchantConnection $model
     * @return int
     */
    public function handle(Dispatcher $events, MerchantConnectionManagerContract $service, MerchantConnection $model): int
    {
        $merchantIds = $this->option('merchant');

        if (!empty($merchantIds)) {
            $merchantIds = (new Collection(explode(',', $merchantIds)))
                ->map(fn(string $merchantId): int => (int) $merchantId)
                ->filter(fn(int $merchantId): bool => $merchantId > 0)
                ->all();
        }

        $connections = $model->newQuery()->where('marketplace', MarketPlaceContract::WILDBERRIES);

        if (!empty($merchantIds)) {
            $connections->whereIn('merchant_id', $merchantIds);
        }

        $connections->each(function(MerchantConnection $connection) use ($service, $events) {
            $credentials = $service->getCredentialsValues($connection);
            $merchantID = $connection->merchant_id;
            $base64Key = $credentials['key'] ?? null;

            if (!empty($base64Key)) {
                $events->dispatch(new WildberriesApiCollectDataEvent(
                    $connection,
                    $base64Key,
                    MerchantConnectionSync::TYPE_REGULAR,
                    true
                ));
                $this->info("Запрос поставлен в очередь для мерчанта: {$merchantID}");
            } else {
                $this->error("Для мерчанта {$merchantID} не указан ключ");
            }
        });

        return 0;
    }
}
