<?php

namespace App\Modules\Merchant\Console\Commands;

use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Report\Contracts\MarketPlaceContract;
use App\Modules\Report\Events\WildberriesApiCollectEachDaySalesEvent;
use Illuminate\Console\Command;
use Illuminate\Events\Dispatcher;

/**
 * Команда для сбора продаж всех мерчантов на каждый день, начиная с какой-то даты, заканчивая сегодняшним днём
 *
 * @package App\Modules\Merchant\Console\Commands
 */
class WildberriesCollectEachDaySalesCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'merchant:wildberries-collect-each-day-sales {--since= : Дата, с которой начинать собирать данные} {--merchant= : Идентификатор мерчанта, для которого собирать данные}';

    /**
     * @var string
     */
    protected $description = 'Сбор продаж всех мерчантов с указанной даты на каждый день.';

    public function handle(Dispatcher $dispatcher, MerchantConnection $model): int
    {
        $sinceStr = $this->option('since');
        $merchantId = (int) $this->option('merchant');

        if (empty($sinceStr)) {
            throw new \RuntimeException("Не указана дата");
        }

        $since = new \DateTime($sinceStr);

        $connections = $model->newQuery()->where('marketplace', MarketPlaceContract::WILDBERRIES);

        if ($merchantId > 0) {
            $connections->where('merchant_id', $merchantId);
        }

        $connections->each(function(MerchantConnection $connection) use ($since, $dispatcher) {
            $merchantId = $connection->merchant_id;

            $event = new WildberriesApiCollectEachDaySalesEvent($merchantId, $since);
            $dispatcher->dispatch($event);

            $this->info("Для мерчанта {$merchantId} запрос поставлен в очередь");
        });

        return 0;
    }
}
