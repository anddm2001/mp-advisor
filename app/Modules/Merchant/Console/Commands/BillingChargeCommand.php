<?php

namespace App\Modules\Merchant\Console\Commands;

use App\Modules\Billing\Events\BillingChargeEvent;
use App\Modules\Merchant\Models\Merchant;
use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Carbon;

/**
 * Команда для запуска списания средств по тарифу
 */
class BillingChargeCommand extends Command
{
    /** @inheritdoc  */
    protected $signature = 'merchant:billing-charge {merchant?* : Идентификаторы мерчантов, для которых произвести списания (по умолчанию — для всех)}';

    /** @inheritdoc */
    protected $description = 'Списание средств по тарифу';

    /**
     * Обработка события
     *
     * @param Dispatcher $events
     * @param Merchant $model
     * @return int
     */
    public function handle(Dispatcher $events, Merchant $model): int
    {
        $merchantQuery = $model->newQuery()->where('charge_on', '<=', Carbon::now());

        $merchantIds = $this->argument('merchant');
        if (!empty($merchantIds)) {
            $merchantQuery->whereIn('id', $merchantIds);
        }

        $merchantQuery->each(function (Merchant $merchant) use ($events) {
            $merchantDisplayName = '#' . $merchant->id . ' ' . ($merchant->name ?: '');
            $events->dispatch(new BillingChargeEvent($merchant));
            $this->info('Запрос на списание средств по тарифу поставлен в очередь для мерчанта ' . $merchantDisplayName);
        });

        return 0;
    }
}
