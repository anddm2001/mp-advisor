<?php

namespace App\Modules\Merchant\Console\Commands;

use App\Modules\Billing\Models\BillingNotification;
use App\Modules\Merchant\Models\Merchant;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

/**
 * Команда для отправки уведомлений по событиям баллинга
 */
class BillingSendNotificationsCommand extends Command
{
    /** @inheritdoc  */
    protected $signature = 'merchant:billing-send-notifications {merchant?* : Идентификаторы мерчантов, для которых произвести списания (по умолчанию — для всех)}';

    /** @inheritdoc */
    protected $description = 'Отправка уведомлений по событиям биллинга';

    /**
     * Обработка события
     *
     * @param Merchant $model
     * @return int
     */
    public function handle(Merchant $model): int
    {
        $merchantQuery = $model->newQuery()->where(function (Builder $query) {
            $query->whereBetween('trial_period_finish', [
                Carbon::today(),
                Carbon::today()->addDays(BillingNotification::TRIAL_PERIOD_BEFORE_FINISH_DAYS),
            ])->where(function (Builder $query) {
                $query
                    ->doesntHave('users.billingNotification')
                    ->orWhereHas('users.billingNotification', function (Builder $query) {
                        $query
                            ->whereNull('is_trial_period_finish_sent')
                            ->orWhere('is_trial_period_finish_sent', false);
                    });
            });
        })->orWhere(function (Builder $query) {
            $query->whereNotNull('lack_amount')->where(function (Builder $query) {
                $query
                    ->doesntHave('users.billingNotification')
                    ->orWhereHas('users.billingNotification', function (Builder $query) {
                        $query
                            ->whereNull('blocked_sent_count')
                            ->orWhere(function (Builder $query) {
                                $query
                                    ->where('blocked_sent_count', '<', BillingNotification::MAX_BLOCKED_SENT_COUNT)
                                    ->where(function (Builder $query) {
                                        $query->whereNull('blocked_last_sent_date')
                                            ->orWhere(
                                                'blocked_last_sent_date',
                                                '<=',
                                                Carbon::now()->subDays(BillingNotification::BLOCKED_SENDING_DAYS)
                                            );
                                    });
                            });
                    });
            });
        });

        $merchantIds = $this->argument('merchant');
        if (!empty($merchantIds)) {
            $merchantQuery->whereIn('id', $merchantIds);
        }

        $merchantQuery->with(['users']);

        $merchantQuery->each(function (Merchant $merchant) {
            foreach ($merchant->users as $user) {
                $user->sendBillingNotifications($merchant);
                $userDisplayName = '#' . $user->id . ' ' . ($user->name ?: '');
                $this->info('Проверка и отправка уведомлений по событиям биллинга запущены для пользователя ' . $userDisplayName);
            }
        });

        return 0;
    }
}
