<?php

namespace App\Modules\Merchant\Console\Commands;

use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Report\Contracts\MarketPlaceContract;
use App\Modules\Report\Events\WildberriesApiScheduledDataSyncEvent;
use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;

/**
 * Команда для запуска добора данных из Wildberries API
 */
class WildberriesApiScheduledDataSyncCommand extends Command
{
    /** @inheritdoc  */
    protected $signature = 'merchant:wildberries-api-scheduled-data-sync' .
        ' {type : Тип добора данных (' .
        WildberriesApiScheduledDataSyncEvent::TYPE_YESTERDAY . ' — за вчера, ' .
        WildberriesApiScheduledDataSyncEvent::TYPE_PREV_MONTH . ' — за предыдущий месяц' .
        ')}';

    /** @inheritdoc */
    protected $description = 'Добор данных из Wildberries API по всем мерчантам за определенный период';

    /**
     * Запуск событий добора данных
     *
     * @param Dispatcher $events
     * @param MerchantConnectionManagerContract $service
     * @param MerchantConnection $model
     * @return int
     */
    public function handle(Dispatcher $events, MerchantConnectionManagerContract $service, MerchantConnection $model): int
    {
        $type = $this->argument('type');
        if (!in_array($type, WildberriesApiScheduledDataSyncEvent::getValidTypes())) {
            $this->error('Указан неверный тип добора данных из Wildberries');
            return -1;
        }

        $model
            ->newQuery()
            ->where('marketplace', MarketPlaceContract::WILDBERRIES)
            ->each(function(MerchantConnection $connection) use ($type, $service, $events) {
                $credentials = $service->getCredentialsValues($connection);
                $base64Key = $credentials['key'] ?? null;
                $merchantDisplayName = '#' . $connection->merchant->id . ' ' . ($connection->merchant->name ?: '');
                if (!empty($base64Key)) {
                    $events->dispatch(new WildberriesApiScheduledDataSyncEvent($connection, $base64Key, $type));
                    $this->info('Запрос на добор данных поставлен в очередь для мерчанта ' . $merchantDisplayName);
                } else {
                    $this->error('Для мерчанта ' . $merchantDisplayName . ' не указан ключ WB');
                }
            });

        return 0;
    }
}
