<?php


namespace App\Modules\Merchant\Validation\Filters;

use App\Modules\Merchant\Contracts\CredentialsFieldFilterContract;
use Illuminate\Support\Str;

/**
 * Фильтрация ключа подключения к Wildberries API.
 *
 * Если был введён GUID, то преобразуем его в base64.
 *
 * @package App\Modules\Merchant\Validation\Filters
 */
class WildberriesKeyFilter implements CredentialsFieldFilterContract
{
    /**
     * Если пользователь ввёл GUID в поле ключа подключения к Wildberries API - преобразуем его в base64
     *
     * @param string $value
     *
     * @return string
     */
    public function filter(string $value): string
    {
        if (Str::isUuid($value)) {
            return base64_encode($value);
        }

        return $value;
    }
}
