<?php

namespace App\Modules\Merchant\Validation\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

/**
 * Правила для валидации ключа подключения к Wildberries API.
 *
 * Пользователь может указать в двух видах:
 *
 * 1. в виде guid: ae822260-3333-5c76-888c-f729aa1239e8
 * 2. в виде base64: YWU4MjIyNjAtMzMzMy01Yzc2LTg4OGMtZjcyOWFhMTIzOWU4
 *
 * @package App\Modules\Merchant\Validation\Rules
 */
class WildberriesKeyRule implements Rule
{
    /**
     * @var string Текст ошибки
     */
    protected string $message = 'Неверный формат ключа';

    /**
     * Проверка введённого кода на валидность.
     *
     * Пользователь может указать в двух видах:
     *
     * 1. в виде guid: ae822260-3333-5c76-888c-f729aa1239e8
     * 2. в виде base64: YWU4MjIyNjAtMzMzMy01Yzc2LTg4OGMtZjcyOWFhMTIzOWU4
     *
     * @param string $attribute
     * @param mixed $value
     *
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if (!is_string($value)) {
            return false;
        }

        $base64Length = 48;

        if (strlen($value) === $base64Length) {
            // был введён guid в формате base64
            // декодируем и далее проверяем на валидность guid
            $value = base64_decode($value);
        }

        // после декодирования из base64 проверяем на правильность ввода guid
        return Str::isUuid($value);
    }

    /**
     * Текст сообщения об ошибке
     *
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }
}
