<?php

namespace App\Modules\Merchant\Validation\Rules;

use App\Modules\Merchant\Services\MerchantConnectionLogger;
use App\Modules\Merchant\Validation\Filters\WildberriesKeyFilter;
use Exception;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;
use function Sentry\captureMessage;

/**
 * Валидаций ключа путём отправки тестового запроса к API Wildberries
 */
class WildberriesKeyCheckRule implements Rule
{
    /** @var string Базовый URI для тестового запроса к API */
    protected const WB_API_BASE_URI = 'https://suppliers-stats.wildberries.ru/api/v1/supplier/';

    /** @var string Формат даты для API */
    protected const WB_DATE_FORMAT = 'Y-m-d';

    /** @var string Формат даты со временем для API */
    protected const WB_DATETIME_FORMAT = 'Y-m-d\TH:i:s.v\Z';

    /** @var array Параметры для разных эндпоинтов */
    protected array $apiEndpointParams = [
//        'incomes' => ['date' => 'rand'],
//        'stocks' => ['date' => 'today'],
//        'orders' => ['date' => ['0' => 'rand', '1' => 'yesterday']],
//        'sales' => ['date' => ['0' => 'rand', '1' => 'yesterday']],
        'reportDetailMart' => ['date' => 'startOfMonth', 'rrdid' => '0', 'limit' => '1'],
    ];

    // ^^^ На данный момент эндпоинт reportDetailMart ни разу не ответил ошибкой 429 Too Many Requests
    // Остальные эндпоинты пока закомментированы, если reportDetailMart начнёт сбоить, вернёмся к рандому

    /**
     * @var string Текст ошибки
     */
    protected string $message = 'Не удалось получить данные по указанному ключу. Проверьте его корректность и попробуйте снова';

    /**
     * Получить "случайных" URI для проверки из списка
     *
     * @param string $base64Key
     * @return string
     */
    protected function getRandomRequestUri(string $base64Key): string
    {
        $endpoint = array_rand($this->apiEndpointParams);
        $endpointParams = $this->apiEndpointParams[$endpoint];
        $query = ['key' => $base64Key];

        $dateParam = $endpointParams['date'] ?? 'rand';
        if (is_array($dateParam)) {
            $query['flag'] = array_rand($dateParam);
            $dateParam = $dateParam[$query['flag']];
        }

        switch ($dateParam) {
            case 'rand':
            default:
                $now = Carbon::now('UTC');
                try {
                    $query['dateFrom'] = Carbon::today('UTC')
                        ->setHour(random_int(0, $now->hour))
                        ->setMinute(random_int(0, $now->minute))
                        ->setSecond(random_int(0, $now->second))
                        ->format(self::WB_DATETIME_FORMAT);
                } catch (Exception $exception) {
                    $query['dateFrom'] = Carbon::today('UTC')
                        ->setHour(0)
                        ->setMinute(0)
                        ->setSecond(0)
                        ->format(self::WB_DATETIME_FORMAT);
                }
                break;
            case 'yesterday':
                $query['dateFrom'] = Carbon::yesterday('UTC')->format(self::WB_DATE_FORMAT);
                break;
            case 'today':
                $query['dateFrom'] = Carbon::today('UTC')->format(self::WB_DATETIME_FORMAT);
                break;
            case 'startOfMonth':
                $query['dateFrom'] = Carbon::today('UTC')->startOfMonth()->format(self::WB_DATE_FORMAT);
                break;
        }

        if (array_key_exists('date', $endpointParams)) {
            unset($endpointParams['date']);
        }
        if (count($endpointParams) > 0) {
            $query = array_merge($query, $endpointParams);
        }

        return self::WB_API_BASE_URI . $endpoint . '?' . http_build_query($query);
    }

    /**
     * Запрос к Wildberries API
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        /** @var MerchantConnectionLogger $logger */
        $logger = with(new MerchantConnectionLogger(MerchantConnectionLogger::CATEGORY_CREDENTIALS_CHECK));
        $base64Key = with(new WildberriesKeyFilter())->filter($value);
        $requestUri = $this->getRandomRequestUri($base64Key);
        $logger->info('Wildberries. Параметры подключения проверяются запросом: ' . parse_url($requestUri, PHP_URL_PATH));
        try {
            $headers = get_headers($requestUri, 1);
        } catch (Exception $exception) {
            $logger->error('Не удалось выполнить проверку: ' . $exception->getMessage());
            $this->message = 'Не удалось подключиться к Wildberries API. Пожалуйста, попробуйте позже.';
            captureMessage('Wildberries failed to check key validity (exception -- internal error).');
            return false;
        }
        if (!empty($headers)) {
            $status = $headers[0] ?? '';
            if (strpos($status, '200') !== false) {
                $logger->info('Проверка прошла успешно, параметры подключения валидны');
                return true;
            }

            $logger->error('При проверке произошла ошибка. Ответ сервера: ' . $status ?: '(пусто)');
            if (strpos($status, '429') !== false) {
                $this->message = 'Прямо сейчас не удалось проверить ключ в Wildberries. Пожалуйста, попробуйте ещё раз буквально через минуту.';
            }
            captureMessage('Wildberries replied: ' . $status . ' -- when trying to check key validity.');
        } else {
            $logger->error('При проверке произошла ошибка. Сервер ничего не ответил');
            $this->message = 'Не удалось подключиться к Wildberries API. Пожалуйста, попробуйте позже.';
            captureMessage('Wildberries failed to check key validity (no reply -- connection error).');
        }

        return false;
    }

    /**
     * Текст сообщения об ошибке
     *
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }
}
