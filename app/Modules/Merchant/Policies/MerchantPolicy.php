<?php

namespace App\Modules\Merchant\Policies;

use App\Modules\Auth\Models\User;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;

/**
 * Проверка прав доступа к мерчанту
 *
 * @package App\Modules\Merchant\Policies
 */
class MerchantPolicy
{
    /**
     * Разрешён просмотр мерчанта
     *
     * @param User $user
     * @param Merchant $merchant
     *
     * @return bool
     */
    public function allowViewMerchant(User $user, Merchant $merchant): bool
    {
        return $user->merchants->where('id', $merchant->id)->isNotEmpty();
    }

    /**
     * Разрешено редактирование мерчанта
     *
     * @param User $user
     * @param Merchant $merchant
     *
     * @return bool
     */
    public function allowUpdateMerchant(User $user, Merchant $merchant): bool
    {
        return $user->id == $merchant->owner_id && $merchant->owner_id;
    }

    /**
     * Разрешено просмотр и редактирование подключения
     *
     * @param User $user
     * @param Merchant $merchant
     * @param MerchantConnection $connection
     *
     * @return bool
     */
    public function allowViewMerchantConnection(User $user, Merchant $merchant, MerchantConnection $connection): bool
    {
        return $this->allowUpdateMerchant($user, $merchant) && $merchant->id == $connection->merchant_id && $connection->merchant_id;
    }
}
