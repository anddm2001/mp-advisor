<?php

namespace App\Modules\Merchant\Services;

use App\Modules\Common\Services\Logger;

/**
 * Логгер для параметров подключения к маркетплейсам
 */
class MerchantConnectionLogger extends Logger
{
    /** @var string Префикс кодов категорий */
    public const PREFIX_FOR_CATEGORY = 'merchant-connection.';
    /** @var string Префикс для читаемых названий категорий */
    public const PREFIX_FOR_NAME_OF_CATEGORY = 'Параметры подключения к МП — ';

    /** @var string Логи проверок параметров подключения к маркетплейсу */
    public const CATEGORY_CREDENTIALS_CHECK = 'credentials-check';
    /** @var string Логи проверок параметров подключения к маркетплейсу */
    public const NAME_OF_CATEGORY_CREDENTIALS_CHECK = 'Проверка параметров';

    /** @var string Логи невалидных параметров подключения к маркетплейсу */
    public const CATEGORY_VALIDATION_FAILED = 'validation-failed';
    /** @var string Логи невалидных параметров подключения к маркетплейсу */
    public const NAME_OF_CATEGORY_VALIDATION_FAILED = 'Ошибка валидации';

    /** @var string */
    protected string $category;

    /**
     * Определение категории логирования
     *
     * @param string $category
     */
    public function __construct(string $category)
    {
        $this->category = $category;
    }

    /** @inheritDoc */
    public function getCategory(): string
    {
        return self::PREFIX_FOR_CATEGORY . $this->category;
    }

    /** @inheritDoc */
    public static function getCategoryName(string $category = ''): string
    {
        $category = str_replace(self::PREFIX_FOR_CATEGORY, '', $category);
        $categoryNameMap = self::getCategoryNameMap();
        return self::PREFIX_FOR_NAME_OF_CATEGORY . ($categoryNameMap[$category] ?? '?');
    }

    /**
     * Названия категорий по коду
     *
     * @return string[]
     */
    public static function getCategoryNameMap(): array
    {
        return [
            self::CATEGORY_CREDENTIALS_CHECK => self::NAME_OF_CATEGORY_CREDENTIALS_CHECK,
            self::CATEGORY_VALIDATION_FAILED => self::NAME_OF_CATEGORY_VALIDATION_FAILED,
        ];
    }
}
