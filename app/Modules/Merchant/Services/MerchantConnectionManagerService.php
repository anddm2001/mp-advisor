<?php

namespace App\Modules\Merchant\Services;

use App\Modules\Merchant\Contracts\CredentialsFieldFilterContract;
use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Requests\StoreMerchantConnectionRequest;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Collection;

/**
 * Сервис для управления подключениями к API маркетплейсов
 *
 * @package App\Modules\Merchant\Services
 */
class MerchantConnectionManagerService implements MerchantConnectionManagerContract
{
    /**
     * @var Collection Массив конфигураций полей подключений в зависимости от маркетплейса
     */
    protected Collection $connectionsConfig;

    /**
     * @var Encrypter Сервис для кодирования данных
     */
    protected Encrypter $encrypter;

    /**
     * @var array Массив событий, которые должны быть запущены, если параметры подключения изменились
     */
    protected array $credentialsChangedEvents = [];

    /**
     * MerchantConnectionManagerService constructor.
     *
     * @param array $connectionsConfig
     * @param Encrypter $encrypter
     */
    public function __construct(array $connectionsConfig, Encrypter $encrypter)
    {
        $this->connectionsConfig = new Collection($connectionsConfig);
        $this->encrypter = $encrypter;
    }

    /**
     * Получить конфигурацию полей подключения
     *
     * @param int $marketplace
     *
     * @return Collection
     */
    protected function getFieldsConfig(int $marketplace): Collection
    {
        $fieldsConfig = $this->connectionsConfig
            ->where('marketplace', $marketplace)
            ->map(fn(array $config): array => $config['credentials'])
            ->first();

        return new Collection($fieldsConfig);
    }

    /**
     * @inheritDoc
     */
    public function getCredentialsValidation(int $marketplace): array
    {
        $fieldsConfig = $this->getFieldsConfig($marketplace);

        return $fieldsConfig
            ->map(fn(array $field): array => $field['validation'])
            ->all();
    }

    /**
     * @inheritDoc
     */
    public function getCredentialsNames(int $marketplace): array
    {
        $fieldsConfig = $this->getFieldsConfig($marketplace);

        return $fieldsConfig
            ->map(fn(array $field): string => $field['title'])
            ->all();
    }

    /**
     * @inheritDoc
     */
    public function getCredentialsValues(MerchantConnection $connection): array
    {
        $result = [];

        // если параметры пустые (ещё не сохранялись)
        if (empty($connection->credentials)) {
            return $result;
        }

        $values = $this->encrypter->decrypt($connection->credentials, true);
        $fields = $this->getFieldsConfig($connection->marketplace)->keys()->all();

        foreach ($fields as $field) {
            $result[$field] = $values[$field] ?? null;
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function credentialsHaveChanged(MerchantConnection $connection, array $newCredentials): bool
    {
        $oldCredentials = $this->getCredentialsValues($connection);
        if (empty($oldCredentials)) {
            return true;
        }
        return count(array_diff_assoc($newCredentials, $oldCredentials)) > 0;
    }

    /**
     * @inheritDoc
     */
    public function setCredentialsChangedEvents(array $events): void
    {
        $this->credentialsChangedEvents = $events;
    }

    /**
     * @inheritDoc
     */
    public function store(StoreMerchantConnectionRequest $request, Merchant $merchant, MerchantConnection $model): bool
    {
        $request->validateResolved();

        // ключи подключения в зависимости от конфига маркетплейса
        $credentials = $this->getFieldsConfig($request->getMarketplace())
            ->map(function(array $fieldConfig, string $field) use ($request) {
                $filter = $fieldConfig['filter'] ?? null;
                $value = $request->input($field);
                if ($filter instanceof CredentialsFieldFilterContract) {
                    $value = $filter->filter($value);
                }
                return $value;
            })
            ->all();

        // параметры подключения изменились
        $credentialsHaveChanged = false;
        if (!empty($this->credentialsChangedEvents)) {
            $credentialsHaveChanged = $this->credentialsHaveChanged($model, $credentials);
        }

        $model->merchant_id = $merchant->id;
        $model->marketplace = $request->getMarketplace();
        $model->credentials = $this->encrypter->encrypt($credentials, true);

        $saveResult = $model->save();

        // запуск событий, если параметры подключения изменились
        if ($saveResult && $credentialsHaveChanged && !empty($this->credentialsChangedEvents)) {
            /** @var Dispatcher $events */
            $events = resolve(Dispatcher::class);
            foreach ($this->credentialsChangedEvents as $credentialsChangedEvent) {
                $events->dispatch($credentialsChangedEvent);
            }
        }

        return $saveResult;
    }
}
