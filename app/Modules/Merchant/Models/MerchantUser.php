<?php

namespace App\Modules\Merchant\Models;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Models\HasCompositePrimaryKeyTrait;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Модель связи мерчанта с пользователем
 *
 * @property int $merchant_id Идентификатор мерчанта
 * @property int $user_id Идентификатор пользователя
 *
 * @property Merchant $merchant Модель мерчанта
 * @property User $user Модель пользователя
 *
 * @package App\Modules\Merchant\Models
 */
class MerchantUser extends Model
{
    use HasCompositePrimaryKeyTrait,
        Compoships;

    /**
     * @var string
     */
    protected $table = 'merchant_user';

    /**
     * @var string[]
     */
    protected $primaryKey = ['user_id', 'merchant_id'];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /** @inheritdoc  */
    protected $fillable = [
        'merchant_id',
        'user_id',
    ];

    /**
     * Получить привязку к пользователю
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Получить привязку к мерчанту
     *
     * @return BelongsTo
     */
    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id');
    }
}
