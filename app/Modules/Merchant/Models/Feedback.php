<?php

namespace App\Modules\Merchant\Models;

use App\Modules\Auth\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Данные из формы обратной связи
 *
 * @property int $id
 * @property int $user_id
 * @property int $merchant_id
 * @property string $email Почта пользователя
 * @property string $category Что случилось
 * @property string $subcategory Точнее
 * @property string $message Текст сообщения
 * @property string|null $file Путь к файлу
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read bool $has_file
 * @property-read array $message_lines
 * @property-read User $user
 */
class Feedback extends Model
{
    /** @inheritdoc  */
    protected $table = 'feedback';

    /** @inheritdoc  */
    protected $fillable = [
        'user_id',
        'merchant_id',
        'email',
        'category',
        'subcategory',
        'message',
        'file',
    ];

    // Relationships ================================================================================================ //

    /**
     * Пользователь
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id', 'user');
    }

    // Accessors & Mutators ========================================================================================= //

    /**
     * Сообщение в виде массива по строкам
     *
     * @return array
     */
    public function getMessageLinesAttribute(): array
    {
        return explode("\n", str_replace(["\r\n", "\n\r", "\r"], "\n", $this->message));
    }

    /**
     * Флаг наличия файла в записи
     *
     * @return bool
     */
    public function getHasFileAttribute(): bool
    {
        return !is_null($this->file);
    }
}
