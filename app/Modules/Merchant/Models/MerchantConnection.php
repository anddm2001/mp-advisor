<?php

namespace App\Modules\Merchant\Models;

use App\Modules\Billing\Models\BillingRate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Модель подключения к API маркетплейса
 *
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $merchant_id Идентификатор мерчанта
 * @property int $marketplace Идентификатор маркетплейса
 * @property string $credentials Закодированные данные подключения
 * @property Carbon|null $last_sync Дата последней синхронизации
 * @property string|null $error Ошибка последней синхронизации
 * @property int|null $item_total_count Общее кол-во позиций
 * @property int|null $item_active_count Кол-во активных позиций
 * @property-read Merchant $merchant
 */
class MerchantConnection extends Model
{
    /** @inheritdoc  */
    protected $table = 'merchant_connection';

    /** @inheritdoc  */
    protected $fillable = [
        'merchant_id',
        'marketplace',
        'credentials',
        'last_sync',
        'error',
        'item_total_count',
        'item_active_count',
    ];

    /** @inheritdoc  */
    protected $casts = [
        'last_sync' => 'datetime',
    ];

    // Relationships ================================================================================================ //

    /**
     * Получить привязку к мерчанту
     *
     * @return BelongsTo
     */
    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id');
    }

    /**
     * Данные о результатах синхронизаций
     *
     * @return HasMany
     */
    public function syncs(): HasMany
    {
        return $this->hasMany(MerchantConnectionSync::class, 'merchant_connection_id', 'id');
    }

    /**
     * Подобрать подходящий тариф
     *
     * @return BillingRate|null
     */
    public function getSuitableBillingRate(): ?BillingRate
    {
        if (is_null($this->item_active_count)) {
            return null;
        }

        /** @var BillingRate|null $billingRate */
        $billingRate = BillingRate::query()->where('is_active', true)->where(function (Builder $query) {
            $query->where('min_item_count', '<=', $this->item_active_count)->where(function (Builder $query) {
                $query->where('max_item_count', '>=', $this->item_active_count)->orWhereNull('max_item_count');
            });
        })->orderByDesc('amount')->first();

        return $billingRate;
    }
}
