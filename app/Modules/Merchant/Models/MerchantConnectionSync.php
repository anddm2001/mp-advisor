<?php

namespace App\Modules\Merchant\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Данные по результатам синхронизации с маркетплейсами
 *
 * @property int $id
 * @property int $merchant_connection_id
 * @property Carbon $date
 * @property string $type Тип синхронизации
 * @property string $status Статус синхронизации
 * @property string|null $data Данные по результатам синхронизации
 * @property-read MerchantConnection $connection
 * @property-read string $status_name
 * @property-read string $type_name
 */
class MerchantConnectionSync extends Model
{
    /** @var string Первичное получение данных за 90 дней */
    public const TYPE_INITIAL = 'initial';
    /** @var string Регулярное обновление по расписанию */
    public const TYPE_REGULAR = 'regular';
    /** @var string Добор данных по расписанию */
    public const TYPE_SCHEDULED = 'scheduled';
    /** @var string Вручную по кнопке из раздела "Продажи" */
    public const TYPE_MANUAL_SALES = 'manual_sales';
    /** @var string Вручную по кнопке из панели управления */
    public const TYPE_MANUAL_CP = 'manual_cp';
    /** @var string Вручную по кнопке из панели управления */
    public const TYPE_MAXIMUM_CP = 'maximum_cp';

    /** @var string Успешная синхронизация */
    public const STATUS_SUCCESS = 'success';
    /** @var string Ошибка при синхронизации */
    public const STATUS_ERROR = 'error';

    /** @inheritdoc  */
    protected $table = 'merchant_connection_sync';

    /** @inheritdoc */
    public $timestamps = false;

    /** @inheritdoc  */
    protected $fillable = [
        'date',
        'type',
        'status',
        'data',
    ];

    /** @inheritdoc  */
    protected $casts = [
        'date' => 'datetime',
    ];

    // Relationships ================================================================================================ //

    /**
     * Получить привязку к подключению
     *
     * @return BelongsTo
     */
    public function connection(): BelongsTo
    {
        return $this->belongsTo(MerchantConnection::class, 'merchant_connection_id', 'id', 'connection');
    }

    // Accessors & Mutators ========================================================================================= //

    /**
     * Название статуса синхронизации
     *
     * @return string
     */
    public function getStatusNameAttribute(): string
    {
        $statuses = self::getStatuses();
        return $statuses[$this->status] ?? '?';
    }

    /**
     * Название типа синхронизации
     *
     * @return string
     */
    public function getTypeNameAttribute(): string
    {
        $types = self::getTypes();
        return $types[$this->type] ?? '?';
    }

    // Stuff ======================================================================================================== //

    /**
     * Типы синхронизации
     *
     * @return string[]
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_INITIAL => 'Обновление за 90 дней',
            self::TYPE_REGULAR => 'Регулярное обновление',
            self::TYPE_SCHEDULED => 'Добор данных по расписанию',
            self::TYPE_MANUAL_SALES => 'Вручную (раздел продаж)',
            self::TYPE_MANUAL_CP => 'Вручную (панель управления)',
            self::TYPE_MAXIMUM_CP => 'Максимальный период (панель управления)',
        ];
    }

    /**
     * Статусы синхронизации
     *
     * @return string[]
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_SUCCESS => 'Успешно',
            self::STATUS_ERROR => 'Ошибка',
        ];
    }
}
