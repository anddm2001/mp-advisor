<?php

namespace App\Modules\Merchant\Models;

use App\Modules\Auth\Models\User;
use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Billing\Models\BillingRate;
use App\Modules\Billing\Services\BillingLogger;
use App\Modules\Report\Models\RecommendationReport;
use App\Modules\Report\Models\ReportResultFile;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use YooKassa\Model\Receipt;
use YooKassa\Model\ReceiptCustomer;
use YooKassa\Model\ReceiptItem;

/**
 * Модель мерчанта
 *
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $name Название мерчанта
 * @property int|null $owner_id Идентификатор владельца мерчанта
 * @property float $balance Баланс мерчанта
 * @property Carbon|null $charge_on Дата следующего списания
 * @property int|null $billing_rate_id Применённый тариф
 * @property float|null $lack_amount Нехватка средств для списания по тарифу
 * @property Carbon|null $trial_period_finish Завершение пробного периода
 * @property-read Collection|BillingOperation[] $billingOperations
 * @property-read int|null $billing_operations_count
 * @property-read BillingRate|null $billingRate
 * @property-read Collection|ReportResultFile[] $files
 * @property-read int|null $files_count
 * @property-read int|null $balance_days
 * @property-read bool $is_balance_blocked
 * @property-read Collection|MerchantConnectionSync[] $marketplaceConnectionSyncs
 * @property-read int|null $marketplace_connection_syncs_count
 * @property-read Collection|MerchantConnection[] $marketplaceConnections
 * @property-read int|null $marketplace_connections_count
 * @property-read User|null $owner
 * @property-read RecommendationReport|null $recommendationReport
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 */
class Merchant extends Model
{
    /** @inheritdoc  */
    protected $table = 'merchant';

    /** @inheritdoc  */
    protected $fillable = [
        'name',
        'owner_id',
        'billing_rate_id',
        'balance',
        'lack_amount',
        'charge_on',
        'trial_period_finish',
    ];

    /** @inheritdoc  */
    protected $casts = [
        'balance' => 'float',
        'lack_amount' => 'float',
        'charge_on' => 'datetime',
        'trial_period_finish' => 'date',
    ];

    // Relationships ================================================================================================ //

    /**
     * Получить привязку к владельцу
     *
     * @return HasOne
     */
    public function owner(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'owner_id');
    }

    /**
     * Получить применённый к мерчанту тариф
     *
     * @return BelongsTo
     */
    public function billingRate(): BelongsTo
    {
        return $this->belongsTo(BillingRate::class, 'billing_rate_id', 'id', 'billingRate');
    }

    /**
     * Пользователи, которым принадлежит мерчант
     *
     * @return HasManyThrough
     */
    public function users(): HasManyThrough
    {
        return $this->hasManyThrough(
            User::class,
            MerchantUser::class,
            'merchant_id',
            'id',
            'id',
            'user_id'
        );
    }

    /**
     * Подключения к маркетплейсам
     *
     * @return HasMany
     */
    public function marketplaceConnections(): HasMany
    {
        return $this->hasMany(MerchantConnection::class, 'merchant_id', 'id');
    }

    /**
     * Данные о результатах синхронизаций по подключениям
     *
     * @return HasManyThrough
     */
    public function marketplaceConnectionSyncs(): HasManyThrough
    {
        return $this->hasManyThrough(
            MerchantConnectionSync::class,
            MerchantConnection::class,
            'merchant_id',
            'merchant_connection_id',
            'id',
            'id'
        );
    }

    /**
     * Результирующие файлы для скачивания
     *
     * @return HasMany
     */
    public function files(): HasMany
    {
        return $this->hasMany(ReportResultFile::class);
    }

    /**
     * Биллинг-операции мерчанта
     *
     * @return HasMany
     */
    public function billingOperations(): HasMany
    {
        return $this->hasMany(BillingOperation::class, 'merchant_id', 'id');
    }

    /**
     * Отчёт рекомендаций к поставке
     *
     * @return HasOne
     */
    public function recommendationReport(): HasOne
    {
        return $this->hasOne(RecommendationReport::class, 'merchant_id', 'id');
    }

    /**
     * Записи формы обратной связи
     *
     * @return HasMany
     */
    public function feedback(): HasMany
    {
        return $this->hasMany(Feedback::class, 'merchant_id', 'id');
    }

    // Accessors & Mutators ========================================================================================= //

    /**
     * Флаг блокировки по недостаче средств
     *
     * @return bool
     */
    public function getIsBalanceBlockedAttribute(): bool
    {
        return !empty($this->lack_amount);
    }

    /**
     * Получить кол-во дней, на сколько хватит баланса
     *
     * @return int|null
     */
    public function getBalanceDaysAttribute(): ?int
    {
        if (!$this->billingRate || !$this->charge_on) {
            return null;
        }

        $today = Carbon::today();
        $balanceDays = $this->charge_on > $today ? $this->charge_on->diffInDays($today) : 0;
        $balanceDays += $this->billingRate->amount > 0 ? (int) floor($this->balance / $this->billingRate->amount) : 0;
        return $balanceDays;
    }

    // Stuff ======================================================================================================== //

    /**
     * Обновить баланс мерчанта на основе платежей и расходов
     *
     * @param bool $isPaymentUpdate
     * @return void
     */
    public function updateBalance(bool $isPaymentUpdate = false): void
    {
        $balance = (float) $this->billingOperations()
            ->where('status', BillingOperation::STATUS_SUCCESS)
            ->sum('amount');

        if (round($balance, 2) !== round($this->balance, 2)) {
            $this->forceFill(compact('balance'))->save();
            // если мерчант заблокирован по балансу, пробуем произвести списание для разблокировки
            if ($isPaymentUpdate && $this->is_balance_blocked) {
                $this->charge(true);
            }
        }
    }

    /**
     * Списание средств с баланса
     *
     * @param bool $forceUnblockingCharge
     * @return void
     */
    public function charge(bool $forceUnblockingCharge = false): void
    {
        $now = Carbon::now();
        $forceChargeToday = $forceUnblockingCharge && $this->is_balance_blocked;

        // пробный период ещё не прошёл
        if (is_null($this->charge_on) || ($this->charge_on > $now && !$forceChargeToday)) {
            return;
        }

        // логгер смены тарифа
        $billingRateChangeLogger = with(new BillingLogger(BillingLogger::CATEGORY_RATE_CHANGE));

        $balance = $this->balance;
        // при разблокировке производится списание сегодня (даже если уже было неудачная попытка списания)
        $chargeOn = $forceChargeToday && $this->charge_on >= $now ? $now->copy() : $this->charge_on->copy();
        $lackAmount = null;
        while ($chargeOn <= $now) {
            foreach ($this->marketplaceConnections as $connection) {
                $billingRate = $connection->getSuitableBillingRate();
                // если тариф не удалось подобрать, используем предыдущий
                if (!$billingRate && $connection->merchant->billingRate) {
                    $billingRate = $connection->merchant->billingRate;
                    $billingRateChangeLogger->notice(
                        'При попытке списания для мерчанта #' . $connection->merchant->id .
                        ' не нашлось подходящего тарифа и использовался предыдущий тариф "' . $billingRate->name . '"'
                    );
                }

                if ($billingRate) {
                    $expense = new BillingOperation([
                        'merchant_id' => $connection->merchant_id,
                        'merchant_connection_id' => $connection->id,
                        'billing_rate_id' => $billingRate->id,
                        'billing_rate_name' => $billingRate->name,
                        'item_total_count' => $connection->item_total_count,
                        'item_active_count' => $connection->item_active_count,
                        'type' => BillingOperation::TYPE_EXPENSE,
                        'amount' => 0 - $billingRate->amount,
                        'description' => 'Списание средств по тарифу ' . $billingRate->name . ' за ' . $chargeOn->format('d.m.Y'),
                        'captured_at' => $chargeOn,
                    ]);

                    // проверка наличия достаточного количества средств на балансе
                    if ($balance >= abs($expense->amount)) {
                        $expense->status = BillingOperation::STATUS_SUCCESS;
                        $balance += $expense->amount;
                    } else {
                        $expense->fill([
                            'status' => BillingOperation::STATUS_FAILED,
                            'error_title' => 'Недостаточно средств на балансе',
                            'error_description' => 'Не удалось списать оплату по тарифу. Пожалуйста, пополните баланс.',
                        ]);
                        // недостача
                        $lackAmount = abs($expense->amount);
                    }

                    // если списание не удалось сохранить, цикл по дням задолжености прерывается
                    // дата следующего списания при этом сохраняется на дне, когда списание не удалось
                    if (!$expense->save()) {
                        break 2;
                    }
                } else {
                    $billingRateChangeLogger->alert(
                        'При попытке списания для мерчанта #' . $connection->merchant->id .
                        ' не нашлось подходящего тарифа (SKU: ' .
                        ($connection->item_active_count ?? '?') . ' / ' . ($connection->item_total_count ?? '?') .
                        ')'
                    );
                }
            }

            // переход к следующему дню
            $chargeOn->addDay();
        }

        // сохранение новой даты списания и недостачи на балансе
        // в случае успеха это будет "завтра", в случае провала - день, за который списание не удалось
        if (!is_null($lackAmount)) {
            $lackAmount -= $balance;

            // проверка и отправка письма о блокировке по балансу
            foreach ($this->users as $user) {
                $user->sendBillingNotifications($this);
            }
        }
        $this->update([
            'charge_on' => $chargeOn,
            'lack_amount' => $lackAmount,
        ]);

        // если есть успешные списания - обновить баланс мерчанта
        if ($balance !== $this->balance) {
            $this->updateBalance();
        }
    }

    /**
     * Получить объект данных для чека для ЮKassa
     *
     * @param string $description
     * @param float $amount
     * @return Receipt
     */
    public function getYooKassaReceipt(string $description, float $amount): Receipt
    {
        $receipt = new Receipt();

        $receipt->setCustomer(new ReceiptCustomer([
            'phone' => $this->owner->phone_formatted,
            'email' => $this->owner->email,
        ]));

        $receipt->setItems([
            new ReceiptItem([
                'description' => $description,
                'quantity' => 1,
                'amount' => [
                    'value' => number_format($amount, 2, '.', ''),
                    'currency' => config('yookassa.currency', 'RUB'),
                ],
                'vat_code' => 1, // Без НДС
            ]),
        ]);

        return $receipt;
    }
}
