<?php

namespace App\Modules\Marketplace\Responses\Warehouse;

use App\Modules\Report\Resources\WarehouseResource;
use Google\Protobuf\Internal\RepeatedField;
use Illuminate\Http\JsonResponse;
use Report\GetWarehousesResponse\Warehouse;

/**
 * Список складов
 */
class IndexResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * WarehousesResponse constructor.
     * @param RepeatedField|Warehouse[] $warehouses
     */
    public function __construct(RepeatedField $warehouses)
    {
        parent::__construct([
            'items' => WarehouseResource::collection(collect($warehouses)),
        ]);
    }
}
