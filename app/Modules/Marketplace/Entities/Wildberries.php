<?php

namespace App\Modules\Marketplace\Entities;

/**
 * Wildberries
 */
class Wildberries extends Marketplace
{
    /** @inheritdoc */
    public int $id = 1;

    /** @inheritdoc  */
    public static string $name = 'Wildberries';

    /** @inheritdoc  */
    public static string $shortName = 'WB';
}
