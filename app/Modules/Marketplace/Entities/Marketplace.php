<?php

namespace App\Modules\Marketplace\Entities;

use App\Modules\Report\Contracts\MarketPlaceContract;

/**
 * Общий класс сущности маркетплейса
 */
abstract class Marketplace implements MarketplaceInterface
{
    /** @var integer Идентификатор маркетплейса */
    public int $id;

    /** @var string Название маркетплейса */
    public static string $name;

    /** @var string Короткое название маркетплейса */
    public static string $shortName;

    /** @inheritDoc */
    public function getId(): int
    {
        return $this->id;
    }

    /** @inheritDoc */
    public static function getName(bool $short = false): string
    {
        return $short ? static::$shortName : static::$name;
    }

    /**
     * Получить название маркетплейса по ID
     *
     * @param int $id
     * @param bool $short
     * @return string
     */
    public static function getNameById(int $id, bool $short = false): string
    {
        $marketplaces = [
            MarketPlaceContract::WILDBERRIES => Wildberries::getName($short),
        ];

        return $marketplaces[$id] ?? '?';
    }
}
