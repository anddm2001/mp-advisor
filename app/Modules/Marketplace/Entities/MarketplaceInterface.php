<?php

namespace App\Modules\Marketplace\Entities;

/**
 * Интерфейс сущности маркетплейса
 */
interface MarketplaceInterface
{
    /**
     * Получение идентификатора маркетплейса
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Получить название маркетплейса
     *
     * @param bool $short
     * @return string
     */
    public static function getName(bool $short = false): string;
}
