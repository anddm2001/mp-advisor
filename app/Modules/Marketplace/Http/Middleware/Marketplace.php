<?php

namespace App\Modules\Marketplace\Http\Middleware;

use App\Modules\Marketplace\Entities\Marketplace as AbstractMarketplace;
use App\Modules\Marketplace\Entities\Wildberries;
use Closure;
use Illuminate\Http\Request;

class Marketplace
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string $marketplaceCode
     * @return mixed
     * @throws \ErrorException
     */
    public function handle(Request $request, Closure $next, string $marketplaceCode)
    {
        $marketplaceClass = null;
        switch ($marketplaceCode) {
            case 'wildberries':
                $marketplaceClass = Wildberries::class;
                break;
        }

        if (is_null($marketplaceClass)) {
            throw new \ErrorException('Невозможно определить маркетплейс.');
        }

        app()->instance(AbstractMarketplace::class, new $marketplaceClass);

        return $next($request);
    }
}
