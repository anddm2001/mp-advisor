<?php

namespace App\Modules\Marketplace\Http\Controllers;

use App\Modules\Marketplace\Responses\Warehouse\IndexResponse;

/**
 * Работа со складами
 */
class WarehouseController extends MarketplaceController
{
    /**
     * Список складов
     *
     * @return IndexResponse
     */
    public function index(): IndexResponse
    {
        $response = $this->api->getWarehouses();
        return new IndexResponse($response->getList());
    }
}
