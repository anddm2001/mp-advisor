<?php

namespace App\Modules\Marketplace\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Marketplace\Entities\Marketplace;
use App\Modules\Report\Services\WildberriesReportApi;
use Closure;

/**
 * Контроллеры, работающие с маркетплейсом
 */
abstract class MarketplaceController extends ApiController
{
    /** @var Marketplace */
    protected Marketplace $marketplace;

    /** @var WildberriesReportApi */
    protected WildberriesReportApi $api;

    /**
     * Резолв маркетплейса из мидлвары и инициализация сервиса API
     *
     * @param WildberriesReportApi|null $api
     */
    public function __construct(?WildberriesReportApi $api)
    {
        $this->middleware(function ($request, Closure $next) {
            $this->marketplace = app(Marketplace::class);

            return $next($request);
        });

        if (!is_null($api)) {
            $this->api = $api;
        }
    }
}
