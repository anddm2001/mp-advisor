<?php

namespace App\Modules\Billing\Console\Commands;

use App\Modules\Billing\Events\BillingChargeEvent;
use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Billing\Services\BillingService;
use App\Modules\Merchant\Models\Merchant;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

/**
 * Команда для синхронизации данных платежей "в ожидании" более часа
 */
class BillingSyncPendingCommand extends Command
{
    /** @inheritdoc  */
    protected $signature = 'billing:sync-pending';

    /** @inheritdoc */
    protected $description = 'Синхронизация данных платежей "в ожидании" более часа';

    /**
     * Обработка события
     *
     * @param BillingService $billingService
     * @return int
     */
    public function handle(BillingService $billingService): int
    {
        $anHourAgo = Carbon::now()->subHour();
        /** @var Collection|BillingOperation[] $pendingPaymentOperations */
        $pendingPaymentOperations = BillingOperation::query()
            ->where('status', BillingOperation::STATUS_PENDING)
            ->where('type', BillingOperation::TYPE_PAYMENT)
            ->where('updated_at', '<', $anHourAgo)
            ->whereNotNull('payment_reference')
            ->where('payment_reference', '!=', '')
            ->pluck('payment_reference', 'id');

        foreach ($pendingPaymentOperations as $paymentOperationId => $paymentReference) {
            try {
                $paymentResponse = $billingService->getPayment($paymentReference);
                $billingService->updateBillingPayment($paymentResponse, true);
                $this->info(
                    'Данные платежа по операции #' . $paymentOperationId . ' успешно обновлены. ' .
                    'Новый статус платежа: ' . $paymentResponse->getStatus() .
                    ' (' . $paymentResponse->getCommonStatus() . ')'
                );
            } catch (Exception $exception) {
                $this->error(
                    'При обновлении данных платежа по операции #' . $paymentOperationId .
                    ' произошла ошибка: ' . $exception->getMessage()
                );
            }

        }

        return 0;
    }
}
