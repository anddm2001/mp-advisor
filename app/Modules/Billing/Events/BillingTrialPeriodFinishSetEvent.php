<?php

namespace App\Modules\Billing\Events;

use App\Modules\Merchant\Models\Merchant;
use Illuminate\Support\Carbon;

/**
 * Событие назначения даты окончания пробного периода
 */
class BillingTrialPeriodFinishSetEvent
{
    /** @var Merchant */
    protected Merchant $merchant;

    /** @var Carbon Дата завершения пробного периода */
    protected Carbon $trialPeriodFinishDate;

    /**
     * Определение параметров события
     *
     * @param Merchant $merchant
     * @param Carbon $trialPeriodFinishDate
     */
    public function __construct(Merchant $merchant, Carbon $trialPeriodFinishDate)
    {
        $this->merchant = $merchant;
        $this->trialPeriodFinishDate = $trialPeriodFinishDate;
    }

    /**
     * @return Merchant
     */
    public function getMerchant(): Merchant
    {
        return $this->merchant;
    }

    /**
     * @return Carbon
     */
    public function getTrialPeriodFinishDate(): Carbon
    {
        return $this->trialPeriodFinishDate;
    }
}
