<?php

namespace App\Modules\Billing\Events;

use App\Modules\Merchant\Models\Merchant;
use Illuminate\Queue\SerializesModels;

/**
 * Событие списания средств с баланса мерчанта
 */
class BillingChargeEvent
{
    use SerializesModels;

    /** @var Merchant */
    protected Merchant $merchant;

    /**
     * Определение параметров события
     *
     * @param Merchant $merchant
     */
    public function __construct(Merchant $merchant)
    {
        $this->merchant = $merchant;
    }

    /**
     * @return Merchant
     */
    public function getMerchant(): Merchant
    {
        return $this->merchant;
    }
}
