<?php

namespace App\Modules\Billing\Events;

use App\Modules\Billing\Models\BillingOperation;

/**
 * Событие платежа для сохранения в лог событий пользователя
 */
class BillingPaymentToEventLogEvent
{
    /** @var BillingOperation */
    protected BillingOperation $operation;

    /**
     * Определение параметров события
     *
     * @param BillingOperation $operation
     */
    public function __construct(BillingOperation $operation)
    {
        $this->operation = $operation;
    }

    /**
     * @return BillingOperation
     */
    public function getBillingOperation(): BillingOperation
    {
        return $this->operation;
    }
}
