<?php

namespace App\Modules\Billing\Omnipay\YooKassa\Message;

use Omnipay\Common\Message\ResponseInterface;
use YooKassa\Common\Exceptions\ExtensionNotFoundException;

/**
 * Запрос на получение данных платежа из ЮKassa
 */
class GetPaymentInfoRequest extends AbstractRequest
{
    /** @inheritDoc */
    public function getData(): array
    {
        $this->validate('transactionReference');

        return ['paymentId' => $this->getTransactionReference()];
    }

    /**
     * @inheritDoc
     * @throws ExtensionNotFoundException
     */
    public function sendData($data): ResponseInterface
    {
        return $this->response = new PaymentResponse($this, $this->getSDKClient()->getPaymentInfo($data['paymentId']));
    }
}
