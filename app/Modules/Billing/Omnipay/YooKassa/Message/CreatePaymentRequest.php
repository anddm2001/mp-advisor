<?php

namespace App\Modules\Billing\Omnipay\YooKassa\Message;

use Omnipay\Common\Message\ResponseInterface;

/**
 * Запрос на создание нового платежа
 */
class CreatePaymentRequest extends AbstractRequest
{
    /** @inheritDoc */
    public function getData(): array
    {
        $this->validate('amount', 'currency', 'description', 'receipt', 'returnUrl', 'clientIp');

        return [
            'amount' => [
                'value' => $this->getAmount(),
                'currency' => $this->getCurrency(),
            ],
            'description' => $this->getDescription(),
            'receipt' => $this->getReceipt(),
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => $this->getReturnUrl(),
            ],
            'capture' => true,
            'client_ip' => $this->getClientIp(),
        ];
    }

    /** @inheritDoc */
    public function sendData($data): ResponseInterface
    {
        return $this->response = new PaymentResponse($this, $this->getSDKClient()->createPayment($data));
    }
}
