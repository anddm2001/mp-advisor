<?php

namespace App\Modules\Billing\Omnipay\YooKassa\Message;

use App\Modules\Billing\Omnipay\Common\Message\Notification as BaseNotification;
use YooKassa\Common\Exceptions\InvalidPropertyValueException;
use YooKassa\Model\Notification\AbstractNotification;
use YooKassa\Model\Notification\NotificationCanceled;
use YooKassa\Model\Notification\NotificationRefundSucceeded;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\Notification\NotificationWaitingForCapture;
use YooKassa\Model\NotificationEventType;
use YooKassa\Model\PaymentInterface;
use YooKassa\Model\RefundInterface;

/**
 * Уведомление ЮKassa
 */
class Notification extends BaseNotification
{
    /** @var AbstractNotification Уведомление ЮKassa */
    protected AbstractNotification $notification;

    /**
     * Определение уведомление в зависимости от типа события
     *
     * @param array $options
     */
    public function __construct(array $options)
    {
        $eventType = $options['event'] ?? null;
        switch ($eventType) {
            case NotificationEventType::PAYMENT_SUCCEEDED:
                $notificationClass = NotificationSucceeded::class;
                break;
            case NotificationEventType::PAYMENT_CANCELED:
                $notificationClass = NotificationCanceled::class;
                break;
            case NotificationEventType::PAYMENT_WAITING_FOR_CAPTURE:
                $notificationClass = NotificationWaitingForCapture::class;
                break;
            case NotificationEventType::REFUND_SUCCEEDED:
                $notificationClass = NotificationRefundSucceeded::class;
                break;
            default:
                throw new InvalidPropertyValueException('Unknown notification event type');
        }

        $this->setNotification(new $notificationClass($options));
    }

    /**
     * @return AbstractNotification
     */
    public function getNotification(): AbstractNotification
    {
        return $this->notification;
    }

    /**
     * @param AbstractNotification $notification
     */
    public function setNotification(AbstractNotification $notification): void
    {
        $this->notification = $notification;
    }

    /**
     * Получить данные платежа из уведомления
     *
     * @return PaymentInterface|RefundInterface|null
     */
    public function getData()
    {
        $notification = $this->getNotification();
        if (method_exists($notification, 'getObject')) {
            return $notification->getObject();
        }

        return null;
    }

    /**
     * Получить тип объекта (платёж или возврат)
     *
     * @return string
     */
    public function getDataType(): string
    {
        return $this->getData() instanceof PaymentInterface ? self::DATA_TYPE_PAYMENT : self::DATA_TYPE_REFUND;
    }

    /**
     * Получить сообщение уведомления
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->getNotification()->getEvent();
    }

    /**
     * Получить идентификатор платежа (или возврата) в ЮKassa
     *
     * @return string
     */
    public function getTransactionReference(): string
    {
        $object = $this->getData();
        if ($object instanceof PaymentInterface || $object instanceof RefundInterface) {
            return $object->getId();
        }

        return '';
    }

    /**
     * Получить статус платежа по типу события уведомления
     *
     * @return string
     */
    public function getTransactionStatus(): string
    {
        switch ($this->getNotification()->getEvent())
        {
            case NotificationEventType::PAYMENT_SUCCEEDED:
            case NotificationEventType::REFUND_SUCCEEDED:
                return static::STATUS_COMPLETED;
            case NotificationEventType::PAYMENT_CANCELED:
                return static::STATUS_FAILED;
            case NotificationEventType::PAYMENT_WAITING_FOR_CAPTURE:
            default:
                return static::STATUS_PENDING;
        }
    }
}
