<?php

namespace App\Modules\Billing\Omnipay\YooKassa\Message;

use Omnipay\Common\Exception\InvalidRequestException;
use Omnipay\Common\Message\AbstractRequest as BaseAbstractRequest;
use Omnipay\Common\Message\ResponseInterface;
use YooKassa\Client;
use YooKassa\Common\Exceptions;
use YooKassa\Model\ReceiptInterface;

/**
 * Общий класс запросов
 */
abstract class AbstractRequest extends BaseAbstractRequest
{
    /**
     * Получить идентификатор магазина в ЮKassa
     *
     * @return string
     */
    public function getShopId(): string
    {
        return $this->getParameter('shopId');
    }

    /**
     * Установить идентификатор магазина в ЮKassa
     *
     * @param string $value
     * @return $this
     */
    public function setShopId(string $value): self
    {
        return $this->setParameter('shopId', $value);
    }

    /**
     * Получить секретный ключ API в ЮKassa
     *
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->getParameter('secretKey');
    }

    /**
     * Установить секретный ключ API в ЮKassa
     *
     * @param string $value
     * @return $this
     */
    public function setSecretKey(string $value): self
    {
        return $this->setParameter('secretKey', $value);
    }

    /**
     * Получить экземпляр клиента SDK ЮKassa
     *
     * @return Client
     */
    public function getSDKClient(): Client
    {
        $client = new Client();
        $client->setAuth($this->getShopId(), $this->getSecretKey());
        return $client;
    }

    /**
     * Получить объект данных для выписки чека
     *
     * @return ReceiptInterface
     */
    public function getReceipt(): ReceiptInterface
    {
        return $this->getParameter('receipt');
    }

    /**
     * Установить объект данных для выписки чека
     *
     * @param ReceiptInterface $value
     * @return $this
     */
    public function setReceipt(ReceiptInterface $value): self
    {
        return $this->setParameter('receipt', $value);
    }

    /**
     * Получить данные запроса
     *
     * @return array
     * @throws InvalidRequestException
     */
    abstract public function getData(): array;

    /**
     * Отправить запрос
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws Exceptions\ApiException
     * @throws Exceptions\BadApiRequestException
     * @throws Exceptions\ForbiddenException
     * @throws Exceptions\InternalServerError
     * @throws Exceptions\NotFoundException
     * @throws Exceptions\ResponseProcessingException
     * @throws Exceptions\TooManyRequestsException
     * @throws Exceptions\UnauthorizedException
     * @throws InvalidRequestException
     */
    abstract public function sendData($data): ResponseInterface;
}
