<?php

namespace App\Modules\Billing\Omnipay\YooKassa\Message;

use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Billing\Omnipay\Common\Message\PaymentResponse as BasePaymentResponse;
use DateTime;
use Omnipay\Common\Exception\InvalidResponseException;
use YooKassa\Model\Confirmation\ConfirmationRedirect;
use YooKassa\Model\Payment;
use YooKassa\Model\PaymentStatus;

/**
 * Ответ с данными платежа
 */
class PaymentResponse extends BasePaymentResponse
{
    /**
     * Получить объект платежа из ответа ЯKassa
     *
     * @return Payment
     */
    protected function getPayment(): Payment
    {
        return $this->getData();
    }

    /**
     * Поддерживается только подтверждение через redirect, поэтому isSuccessful() возвращает false
     *
     * @inheritDoc
     */
    public function isSuccessful(): bool
    {
        return false;
    }

    /** @inheritDoc */
    public function isRedirect(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     * @throws InvalidResponseException
     */
    public function getRedirectUrl(): string
    {
        $confirmationData = $this->getPayment()->getConfirmation();
        if (!($confirmationData instanceof ConfirmationRedirect)) {
            throw new InvalidResponseException('Only confirmation by redirect is supported for YooKassa');
        }

        return $confirmationData->getConfirmationUrl();
    }

    /**
     * Получить идентификатор платежа в ЮKassa
     *
     * @return string|null
     */
    public function getTransactionReference(): ?string
    {
        return $this->getPayment()->getId();
    }

    /** @inheritDoc */
    public function getStatus(): string
    {
        return $this->getPayment()->getStatus();
    }

    /** @inheritDoc */
    public function getCommonStatus(): string
    {
        switch ($this->getStatus()) {
            case PaymentStatus::SUCCEEDED:
                return BillingOperation::STATUS_SUCCESS;
            case PaymentStatus::CANCELED:
                return BillingOperation::STATUS_FAILED;
            case PaymentStatus::PENDING:
            case PaymentStatus::WAITING_FOR_CAPTURE:
            default:
                return BillingOperation::STATUS_PENDING;
        }
    }

    /** @inheritDoc */
    public function getAmount(): float
    {
        $amountData = $this->getPayment()->getAmount();
        return floatval($amountData->getValue());
    }

    /** @inheritDoc */
    public function getDescription(): ?string
    {
        return $this->getPayment()->getDescription() ?: null;
    }

    /** @inheritDoc */
    public function getCancellationReason(): ?string
    {
        return $this->getStatus() === PaymentStatus::CANCELED
            ? $this->getPayment()->getCancellationDetails()->getReason()
            : null;
    }

    /** @inheritDoc */
    public function getCapturedAt(): ?DateTime
    {
        $tz = new \DateTimeZone('Europe/Moscow');
        return $this->getPayment()->getCapturedAt() ? $this->getPayment()->getCapturedAt()->setTimezone($tz) : null;
    }
}
