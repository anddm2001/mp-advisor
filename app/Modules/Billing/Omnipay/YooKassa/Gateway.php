<?php

namespace App\Modules\Billing\Omnipay\YooKassa;

use App\Modules\Billing\Omnipay\YooKassa\Message\CreatePaymentRequest;
use App\Modules\Billing\Omnipay\YooKassa\Message\GetPaymentInfoRequest;
use App\Modules\Billing\Omnipay\YooKassa\Message\Notification;
use Illuminate\Support\Str;
use Omnipay\Common\AbstractGateway;
use Omnipay\Common\Message\NotificationInterface;
use Omnipay\Common\Message\RequestInterface;

/**
 * Гейт Omnipay для принятия оплаты через ЮKassa
 *
 * @method \Omnipay\Common\Message\RequestInterface authorize(array $options = array())
 * @method \Omnipay\Common\Message\RequestInterface capture(array $options = array())
 * @method \Omnipay\Common\Message\RequestInterface completeAuthorize(array $options = array())
 * @method \Omnipay\Common\Message\RequestInterface completePurchase(array $options = array())
 * @method \Omnipay\Common\Message\RequestInterface refund(array $options = array())
 * @method \Omnipay\Common\Message\RequestInterface void(array $options = array())
 * @method \Omnipay\Common\Message\RequestInterface createCard(array $options = array())
 * @method \Omnipay\Common\Message\RequestInterface updateCard(array $options = array())
 * @method \Omnipay\Common\Message\RequestInterface deleteCard(array $options = array())
 */
class Gateway extends AbstractGateway
{
    /** @inheritDoc */
    public function getName(): string
    {
        return 'YooKassa';
    }

    /** @inheritDoc */
    public function getShortName(): string
    {
        return Str::kebab($this->getName());
    }

    /** @inheritDoc */
    public function getDefaultParameters(): array
    {
        return [
            'shopId' => '',
            'secretKey' => '',
            'currency' => '',
            'clientIp' => '',
            'testMode' => false,
        ];
    }

    /**
     * Установить идентификатор магазина в ЮKassa
     *
     * @param string $value
     * @return $this
     */
    public function setShopId(string $value): self
    {
        return $this->setParameter('shopId', $value);
    }

    /**
     * Установить секретный ключ API в ЮKassa
     *
     * @param string $value
     * @return $this
     */
    public function setSecretKey(string $value): self
    {
        return $this->setParameter('secretKey', $value);
    }

    /**
     * Установить клиентский IP адрес
     *
     * @param string $value
     * @return $this
     */
    public function setClientIp(string $value): self
    {
        return $this->setParameter('clientIp', $value);
    }

    /**
     * Принять входящее уведомление
     * [
     *  'body' => тело входящего запроса (JSON)
     * ]
     *
     * @inheritDoc
     */
    public function acceptNotification(array $options): NotificationInterface
    {
        return new Notification(!empty($options['body']) ? json_decode($options['body'], true) : []);
    }

    /**
     * Создать платёж
     * [
     *  'amount' => сумма платежа
     *  'description' => описание платежа
     *  'receipt' => объект данных чека \YooKassa\Model\ReceiptInterface
     *  'returnUrl' => URL, куда вернуть пользователя после оплаты
     * ]
     *
     * @inheritDoc
     */
    public function purchase(array $options): RequestInterface
    {
        return $this->createRequest(CreatePaymentRequest::class, $options);
    }

    /**
     * Получить данные платежа
     * [
     *  'transactionReference' => идентификатор платежа в ЯKassa
     * ]
     *
     * @inheritDoc
     */
    public function fetchTransaction(array $options): RequestInterface
    {
        return $this->createRequest(GetPaymentInfoRequest::class, $options);
    }
}
