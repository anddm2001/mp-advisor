<?php

namespace App\Modules\Billing\Omnipay\Common\Message;

use Omnipay\Common\Message\NotificationInterface;

/**
 * Уведомление
 */
abstract class Notification implements NotificationInterface
{
    /** @var string Тип данных - платёж */
    public const DATA_TYPE_PAYMENT = 'payment';
    /** @var string Тип данных - возврат */
    public const DATA_TYPE_REFUND = 'refund';

    /**
     * Получить тип данных уведомления
     *
     * @return string
     */
    abstract public function getDataType(): string;
}
