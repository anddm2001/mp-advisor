<?php

namespace App\Modules\Billing\Omnipay\Common\Message;

use DateTime;
use Omnipay\Common\Message\AbstractResponse;

/**
 * Ответ с данными платежа
 */
abstract class PaymentResponse extends AbstractResponse
{
    /**
     * Получить статус платежа в платёжной системе
     *
     * @return string
     */
    abstract public function getStatus(): string;

    /**
     * Получить общий системный статус платежа
     *
     * @return string
     */
    abstract public function getCommonStatus(): string;

    /**
     * Получить сумму платежа
     *
     * @return float
     */
    abstract public function getAmount(): float;

    /**
     * Получить описание платежа
     *
     * @return string|null
     */
    abstract public function getDescription(): ?string;

    /**
     * Получить причину отмены платежа
     *
     * @return string|null
     */
    abstract public function getCancellationReason(): ?string;

    /**
     * Получить время подтверждения платежа
     *
     * @return DateTime|null
     */
    abstract public function getCapturedAt(): ?DateTime;
}
