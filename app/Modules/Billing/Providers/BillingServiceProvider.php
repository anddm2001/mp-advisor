<?php

namespace App\Modules\Billing\Providers;

use App\Modules\Billing\Console\Commands\BillingSyncPendingCommand;
use App\Modules\Billing\Omnipay\YooKassa\Gateway;
use App\Modules\Billing\Http\Middleware\BillingNotificationMiddleware;
use App\Modules\Billing\Http\Middleware\YooKassaCheckNotificationIp;
use App\Modules\Billing\Services\BillingService;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use Omnipay\Omnipay;

/**
 * Провайдер сервисов для модуля биллинга
 */
class BillingServiceProvider extends ServiceProvider
{
    public function register()
    {
        // сервис биллинга
        $this->app->singleton(BillingService::class, function(Application $app) {
            // ЮKassa в качестве платёжной системы
            $gateway = Omnipay::create('\\' . Gateway::class);

            /** @var Repository $config */
            $config = $app->get('config');
            $parameters = $config->get('yookassa', []);
            $parameters['clientIp'] = Request::ip();

            $gateway->initialize($parameters);

            return new BillingService($gateway);
        });

        // middleware для проверки входящего уведоаления
        $this->app->bind(BillingNotificationMiddleware::class, YooKassaCheckNotificationIp::class);
    }

    public function boot()
    {
        $this->commands([
            BillingSyncPendingCommand::class,
        ]);
    }
}
