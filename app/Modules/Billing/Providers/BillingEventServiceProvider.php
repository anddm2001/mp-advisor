<?php

namespace App\Modules\Billing\Providers;

use App\Modules\Billing\Events\BillingChargeEvent;
use App\Modules\Billing\Events\BillingPaymentToEventLogEvent;
use App\Modules\Billing\Events\BillingTrialPeriodFinishSetEvent;
use App\Modules\Billing\Listeners\BillingChargeListener;
use App\Modules\Common\Listeners\LogEventListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;

/**
 * Регистрация обработчиков событий биллинга
 */
class BillingEventServiceProvider extends EventServiceProvider
{
    /** @inheritdoc */
    protected $listen = [
        BillingChargeEvent::class => [BillingChargeListener::class],
        BillingTrialPeriodFinishSetEvent::class => [LogEventListener::class],
        BillingPaymentToEventLogEvent::class => [LogEventListener::class],
    ];
}
