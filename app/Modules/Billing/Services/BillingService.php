<?php

namespace App\Modules\Billing\Services;

use App\Modules\Billing\Events\BillingPaymentToEventLogEvent;
use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Billing\Models\BillingOperationInitiatorContract;
use App\Modules\Billing\Omnipay\Common\Message\Notification;
use App\Modules\Billing\Omnipay\Common\Message\PaymentResponse;
use App\Modules\Common\Services\Logger;
use App\Modules\Merchant\Models\Merchant;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Omnipay\Common\GatewayInterface;
use YooKassa\Model\CancellationDetailsReasonCode;

/**
 * Сервис биллинга
 */
class BillingService
{
    /** @var GatewayInterface Гейт платёжной системы */
    protected GatewayInterface $gateway;

    /** @var Builder */
    protected Builder $billingPaymentQuery;

    /** @var Logger */
    protected Logger $paymentSystemLogger;

    /**
     * Определение параметров сервиса
     *
     * @param GatewayInterface $gateway
     */
    public function __construct(GatewayInterface $gateway)
    {
        $this->gateway = $gateway;
        $this->billingPaymentQuery = with(new BillingOperation())
            ->newModelQuery()
            ->ofPaymentSystem($this->gateway->getShortName());
        $this->paymentSystemLogger = with(new BillingLogger(BillingLogger::CATEGORY_PAYMENT_SYSTEM));
    }

    /**
     * Получение данных платежа по внешнему идентификатору
     *
     * @param string $transactionReference
     * @return PaymentResponse
     * @throws Exception
     */
    public function getPayment(string $transactionReference): PaymentResponse
    {
        try {
            $this->paymentSystemLogger->info('Получение данных платежа ' . $transactionReference);
            $this->paymentSystemLogger->info('Создание запроса GetPaymentInfo');
            $fetchTransactionRequest = $this->gateway->fetchTransaction(compact('transactionReference'));
            $this->paymentSystemLogger->info('Отправка запроса GetPaymentInfo и получение ответа');
            /** @var PaymentResponse $paymentResponse */
            $paymentResponse = $fetchTransactionRequest->send();
            $this->paymentSystemLogger->info('Данные платежа ' . $transactionReference . ' успешно получены');
            return $paymentResponse;
        } catch (Exception $exception) {
            $this->paymentSystemLogger->error('Ошибка при получении данных платежа: ' . $exception->getMessage());
            throw $exception;
        }
    }

    /**
     * Создать платёж
     *
     * @param Merchant $merchant
     * @param array $data
     * @param BillingOperationInitiatorContract|null $user
     * @return array
     * @throws Exception
     */
    public function createPayment(Merchant $merchant, array $data, ?BillingOperationInitiatorContract $user): array
    {
        /** @var BillingOperation $billingPayment */
        $billingPayment = $merchant->billingOperations()->create([
            'initiator_type' => $user ? $user->getMorphClass() : null,
            'initiator_id' => $user ? $user->getKey() : null,
            'billing_rate_id' => $merchant->billing_rate_id,
            'billing_rate_name' => $merchant->billingRate ? $merchant->billingRate->name : null,
            'type' => BillingOperation::TYPE_PAYMENT,
            'status' => BillingOperation::STATUS_NEW,
            'amount' => $data['amount'] ?? 0,
            'payment_system' => $this->gateway->getShortName(),
        ]);

        $data['amount'] = number_format($billingPayment->amount, 2, '.', '');
        $data['description'] = 'Пополнение баланса для мерчанта №' . $merchant->id;
        $data['receipt'] = $merchant->getYooKassaReceipt($data['description'], $billingPayment->amount);
        $data['returnUrl'] = self::frontendPaymentUrl($billingPayment->id);

        try {
            $this->paymentSystemLogger->info('Создание платежа [' . $billingPayment->id . ']: ' . $data['description']);
            $this->paymentSystemLogger->info('Создание запроса CreatePayment');
            $purchaseRequest = $this->gateway->purchase($data);
            $this->paymentSystemLogger->info('Отправка запроса CreatePayment и получение ответа');
            /** @var PaymentResponse $paymentResponse */
            $paymentResponse = $purchaseRequest->send();
            $this->paymentSystemLogger->info('Платёж ' . $paymentResponse->getTransactionReference() . ' успешно создан');
        } catch (Exception $exception) {
            $this->paymentSystemLogger->error('Ошибка при создании платежа: ' . $exception->getMessage());
            throw $exception;
        }

        $billingPayment->update([
            'status' => $paymentResponse->getCommonStatus(),
            'amount' => $paymentResponse->getAmount(),
            'description' => $paymentResponse->getDescription(),
            'payment_reference' => $paymentResponse->getTransactionReference(),
            'captured_at' => $paymentResponse->getCapturedAt(),
        ]);

        return [
            'billing_payment' => $billingPayment,
            'redirect_url' => $paymentResponse->getRedirectUrl(),
        ];
    }

    /**
     * Обработка входящего уведомления
     *
     * @param Request $request
     * @return void
     * @throws Exception
     */
    public function handleWebhook(Request $request): void
    {
        $this->paymentSystemLogger->info('Принятие входящего уведомления');
        try {
            /** @var Notification $notification */
            $notification = $this->gateway->acceptNotification(['body' => $request->getContent()]);
            $this->paymentSystemLogger->info('Сообщение уведомления: ' . $notification->getMessage());
        } catch (Exception $exception) {
            $this->paymentSystemLogger->error('Ошибка при обработке уведомления: ' . $exception->getMessage());
            throw $exception;
        }

        if ($notification->getDataType() === Notification::DATA_TYPE_PAYMENT) {
            $this->paymentSystemLogger->info('Уведомление по платежу, запускается получение и обновление данных');
            $paymentResponse = $this->getPayment($notification->getTransactionReference());
            $this->updateBillingPayment($paymentResponse, true);
        }
    }

    /**
     * Обновить данные платежа по данным ответа платёжной системы
     *
     * @param PaymentResponse $paymentResponse
     * @param bool $fromWebhook
     * @return void
     */
    public function updateBillingPayment(PaymentResponse $paymentResponse, bool $fromWebhook = false): void
    {
        $paymentReference = $paymentResponse->getTransactionReference();
        if ($billingPayment = $this->billingPaymentQuery->where('payment_reference', $paymentReference)->first()) {
            /** @var BillingOperation $billingPayment */
            $billingPaymentStatus = $billingPayment->status;
            $cancellationReason = $paymentResponse->getCancellationReason();
            $error = self::errorDataByCancellationReason($cancellationReason);
            $billingPayment->update([
                'status' => $paymentResponse->getCommonStatus(),
                'description' => $paymentResponse->getDescription(),
                'cancellation_reason' => $cancellationReason,
                'error_title' => $error['title'] ?? null,
                'error_description' => $error['description'] ?? null,
                'captured_at' => $paymentResponse->getCapturedAt(),
            ]);
            $billingPayment->merchant->updateBalance(true);

            // обновление инициировано платёжной системой и статус изменился на финальный
            $isFinalStatus = in_array($billingPayment->status, [
                BillingOperation::STATUS_SUCCESS,
                BillingOperation::STATUS_FAILED,
            ], true);
            if ($fromWebhook && $billingPayment->status !== $billingPaymentStatus && $isFinalStatus) {
                // отправка уведомления о статусе платежа владельцу мерчанта
                $billingPayment->merchant->owner->sendBillingPaymentStatusNotification($billingPayment);
                // лог событий пользователя
                if ($billingPayment->status === BillingOperation::STATUS_SUCCESS) {
                    event(new BillingPaymentToEventLogEvent($billingPayment));
                }
            }
        }
    }

    /**
     * Читаемые название и описание ошибки по коду причины отмены платежа
     *
     * @param string|null $cancellationReason
     * @return array
     */
    protected static function errorDataByCancellationReason(?string $cancellationReason): array
    {
        $error = ['title' => null, 'description' => null];

        switch ($cancellationReason) {
            case CancellationDetailsReasonCode::THREE_D_SECURE_FAILED:
                $error['title'] = 'Не пройдена аутентификация по 3-D Secure';
                break;
            case CancellationDetailsReasonCode::CALL_ISSUER:
                $error['title'] = 'Оплата данным платежным средством отклонена по неизвестным причинам';
                $error['description'] = 'Следует обратиться в организацию, выпустившую платежное средство.';
                break;
            case CancellationDetailsReasonCode::CARD_EXPIRED:
                $error['title'] = 'Истек срок действия банковской карты';
                $error['description'] = 'Следует использовать другое платежное средство.';
                break;
            case CancellationDetailsReasonCode::COUNTRY_FORBIDDEN:
                $error['title'] = 'Нельзя заплатить банковской картой, выпущенной в этой стране';
                $error['description'] = 'Следует использовать другое платежное средство.';
                break;
            case CancellationDetailsReasonCode::FRAUD_SUSPECTED:
                $error['title'] = 'Платеж заблокирован из-за подозрения в мошенничестве';
                $error['description'] = 'Следует использовать другое платежное средство.';
                break;
            case CancellationDetailsReasonCode::GENERAL_DECLINE:
                $error['title'] = 'Причина не детализирована';
                $error['description'] = 'Следует обратиться к инициатору отмены платежа за уточнением подробностей.';
                break;
            case CancellationDetailsReasonCode::IDENTIFICATION_REQUIRED:
                $error['title'] = 'Превышены ограничения на платежи для кошелька ЮMoney';
                $error['description'] = 'Следует идентифицировать кошелек или выбрать другое платежное средство.';
                break;
            case CancellationDetailsReasonCode::INSUFFICIENT_FUNDS:
                $error['title'] = 'Не хватает денег для оплаты';
                $error['description'] = 'Следует пополнить баланс или использовать другое платежное средство.';
                break;
            case CancellationDetailsReasonCode::INVALID_CARD_NUMBER:
                $error['title'] = 'Неправильно указан номер карты';
                $error['description'] = 'Следует ввести корректные данные.';
                break;
            case CancellationDetailsReasonCode::INVALID_CSC:
                $error['title'] = 'Неправильно указан код CVV2 (CVC2, CID)';
                $error['description'] = 'Следует ввести корректные данные.';
                break;
            case CancellationDetailsReasonCode::ISSUER_UNAVAILABLE:
                $error['title'] = 'Организация, выпустившая платежное средство, недоступна';
                $error['description'] = 'Следует использовать другое платежное средство или повторить оплату позже.';
                break;
            case CancellationDetailsReasonCode::PAYMENT_METHOD_LIMIT_EXCEEDED:
                $error['title'] = 'Исчерпан лимит платежей для данного платежного средства';
                $error['description'] = 'Следует использовать другое платежное средство или повторить оплату на следующий день.';
                break;
            case CancellationDetailsReasonCode::PAYMENT_METHOD_RESTRICTED:
                $error['title'] = 'Запрещены операции данным платежным средством';
                $error['description'] = 'Следует обратиться в организацию, выпустившую платежное средство.';
                break;
            case CancellationDetailsReasonCode::PERMISSION_REVOKED:
                $error['title'] = 'Нельзя провести безакцептное списание';
                break;
            case CancellationDetailsReasonCode::INTERNAL_TIMEOUT:
                $error['title'] = 'Технические неполадки на стороне ЮKassa';
                break;
            case CancellationDetailsReasonCode::CANCELED_BY_MERCHANT:
                $error['title'] = 'Платеж отменен';
                break;
            case CancellationDetailsReasonCode::PAYMENT_EXPIRED:
                $error['title'] = 'Истекло время на оплату';
                break;
            case CancellationDetailsReasonCode::EXPIRED_ON_CONFIRMATION:
                $error['title'] = 'Истекло время на оплату';
                $error['description'] = 'На проведение оплаты отведён 1 часа. За это время оплата не поступила.';
                break;
            case CancellationDetailsReasonCode::EXPIRED_ON_CAPTURE:
                $error['title'] = 'Истек срок списания оплаты у двухстадийного платежа';
                break;
        }

        return $error;
    }

    /**
     * Вернуть URL на фронтэнде для возврата клиента после платежа
     *
     * @param int $billingPaymentId
     * @return string
     */
    protected static function frontendPaymentUrl(int $billingPaymentId): string
    {
        /** @var UrlGenerator $urlGenerator */
        $urlGenerator = url();
        $urlGenerator->forceRootUrl(config('app.url'));
        $url = $urlGenerator->to('payment/status/' . $billingPaymentId);
        $urlGenerator->forceRootUrl(null);
        return $url;
    }
}
