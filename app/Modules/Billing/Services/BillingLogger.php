<?php

namespace App\Modules\Billing\Services;

use App\Modules\Common\Services\Logger;

/**
 * Логер для биллинга
 */
class BillingLogger extends Logger
{
    /** @var string Префикс кодов категорий */
    public const PREFIX_FOR_CATEGORY = 'billing.';
    /** @var string Префикс для читаемых названий категорий */
    public const PREFIX_FOR_NAME_OF_CATEGORY = 'Биллинг — ';

    /** @var string Логи взаимодействия с платёжной системой */
    public const CATEGORY_PAYMENT_SYSTEM = 'payment-system';
    /** @var string Логи взаимодействия с платёжной системой */
    public const NAME_OF_CATEGORY_PAYMENT_SYSTEM = 'Платёжная система';
    /** @var string Логи смены тарифа */
    public const CATEGORY_RATE_CHANGE = 'rate-change';
    /** @var string Логи смены тарифа */
    public const NAME_OF_CATEGORY_RATE_CHANGE = 'Смена тарифа';

    /** @var string */
    protected string $category;

    /**
     * Определение категории логирования биллинга
     *
     * @param string $category
     */
    public function __construct(string $category)
    {
        $this->category = $category;
    }

    /** @inheritDoc */
    public function getCategory(): string
    {
        return self::PREFIX_FOR_CATEGORY . $this->category;
    }

    /** @inheritDoc */
    public static function getCategoryName(string $category = ''): string
    {
        $category = str_replace(self::PREFIX_FOR_CATEGORY, '', $category);
        $categoryNameMap = self::getCategoryNameMap();
        return self::PREFIX_FOR_NAME_OF_CATEGORY . ($categoryNameMap[$category] ?? '?');
    }

    /**
     * Названия категорий по коду
     *
     * @return string[]
     */
    public static function getCategoryNameMap(): array
    {
        return [
            self::CATEGORY_PAYMENT_SYSTEM => self::NAME_OF_CATEGORY_PAYMENT_SYSTEM,
            self::CATEGORY_RATE_CHANGE => self::NAME_OF_CATEGORY_RATE_CHANGE,
        ];
    }
}
