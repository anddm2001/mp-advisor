<?php

namespace App\Modules\Billing\Notifications;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\CommonDataHelper;
use App\Modules\Common\Helpers\UrlHelper;
use App\Modules\Merchant\Models\Merchant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;

/**
 * Уведомление об окончании пробного периода
 */
class BillingTrialPeriodFinishNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /** @var Merchant */
    protected Merchant $merchant;

    /**
     * Определение параметров для уведомления
     *
     * @param Merchant $merchant
     */
    public function __construct(Merchant $merchant)
    {
        $this->merchant = $merchant;
    }

    /**
     * Получить модель мерчанта
     *
     * @return Merchant
     */
    public function getMerchant(): Merchant
    {
        return $this->merchant;
    }

    /**
     * Каналы отправки уведомления
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Сборка письма для отправки уведомления по e-mail
     *
     * @param  User  $notifiable
     * @return MailMessage
     */
    public function toMail(User $notifiable): MailMessage
    {
        $merchant = $this->getMerchant();
        $today = Carbon::today();
        $daysToFinish = $merchant->trial_period_finish ? $merchant->trial_period_finish->diffInDays($today) : null;
        $daysToFinishText = $daysToFinish
            ? $daysToFinish . ' ' . CommonDataHelper::numDeclension($daysToFinish, ['день', 'дня', 'дней'])
            : 'несколько дней';
        $activeItems = (int) $merchant->marketplaceConnections->sum('item_active_count');
        $activeItemsText = $activeItems . ' ' .
            CommonDataHelper::numDeclension($activeItems, ['активный', 'активных', 'активных']) . ' SKU';
        $billingRateName = $merchant->billingRate ? $merchant->billingRate->name : 'Без названия';
        $billingRateAmount = $merchant->billingRate ? $merchant->billingRate->amount : 0;
        $monthAmount = $merchant->billingRate ? $merchant->billingRate->amount * 30 : 0;

        return (new MailMessage())
            ->subject('mpAdvisor — Завершение пробного периода')
            ->view('mail.billing-trial-period-finish', [
                'userName' => $notifiable->name,
                'daysToFinishText' => $daysToFinishText,
                'activeItemsText' => $activeItemsText,
                'billingRateName' => $billingRateName,
                'billingRateAmount' => $billingRateAmount,
                'monthAmount' => $monthAmount,
                'topUpUrl' => UrlHelper::ApiRoutedUrlToRootAppUrl(route('billing.balance.top-up', ['s' => $monthAmount])),
                'pollUrl' => 'https://forms.gle/7FtC4eft9nSPXLsQ6',
            ]);
    }
}
