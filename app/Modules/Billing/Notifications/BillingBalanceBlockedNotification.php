<?php

namespace App\Modules\Billing\Notifications;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\UrlHelper;
use App\Modules\Merchant\Models\Merchant;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Уведомление о блокировке по балансу
 */
class BillingBalanceBlockedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /** @var Merchant */
    protected Merchant $merchant;

    /**
     * Определение параметров для уведомления
     *
     * @param Merchant $merchant
     */
    public function __construct(Merchant $merchant)
    {
        $this->merchant = $merchant;
    }

    /**
     * Получить модель мерчанта
     *
     * @return Merchant
     */
    public function getMerchant(): Merchant
    {
        return $this->merchant;
    }

    /**
     * Каналы отправки уведомления
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Сборка письма для отправки уведомления по e-mail
     *
     * @param  User  $notifiable
     * @return MailMessage
     */
    public function toMail(User $notifiable): MailMessage
    {
        $merchant = $this->getMerchant();
        $monthAmount = $merchant->billingRate ? $merchant->billingRate->amount * 30 : 0;

        return (new MailMessage())
            ->subject('mpAdvisor — Нехватка средств на балансе')
            ->view('mail.billing-balance-blocked', [
                'userName' => $notifiable->name,
                'monthAmount' => $monthAmount,
                'topUpUrl' => UrlHelper::ApiRoutedUrlToRootAppUrl(route('billing.balance.top-up', ['s' => $monthAmount])),
            ]);
    }
}
