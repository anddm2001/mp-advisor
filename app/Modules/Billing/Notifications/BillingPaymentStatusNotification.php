<?php

namespace App\Modules\Billing\Notifications;

use App\Modules\Auth\Models\User;
use App\Modules\Billing\Models\BillingOperation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Уведомление пользователю о статусе платежа
 */
class BillingPaymentStatusNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /** @var BillingOperation */
    protected BillingOperation $billingOperation;

    /**
     * Определение параметров для уведомления
     *
     * @param BillingOperation $billingOperation
     */
    public function __construct(BillingOperation $billingOperation)
    {
        $this->billingOperation = $billingOperation;
    }

    /**
     * Получить модель операции биллинга
     *
     * @return BillingOperation
     */
    public function getBillingOperation(): BillingOperation
    {
        return $this->billingOperation;
    }

    /**
     * Каналы отправки уведомления
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Сборка письма для отправки уведомления по e-mail
     *
     * @param  User  $notifiable
     * @return MailMessage
     */
    public function toMail(User $notifiable): MailMessage
    {
        $isSuccessful = $this->billingOperation->status === BillingOperation::STATUS_SUCCESS;
        $subject = $isSuccessful ? 'Платёж завершён успешно' : 'Платёж завершён с ошибкой';
        return (new MailMessage())
            ->subject('mpAdvisor — ' . $subject)
            ->view('mail.billing-payment-status', [
                'userName' => $notifiable->name,
                'billingOperation' => $this->billingOperation,
                'isSuccessful' => $isSuccessful,
            ]);
    }
}
