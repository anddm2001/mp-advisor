<?php

namespace App\Modules\Billing\Http\Responses;

use App\Modules\Billing\Http\Resources\BillingPaymentResource;
use App\Modules\Billing\Models\BillingOperation;
use Illuminate\Http\JsonResponse;

/**
 * Ответ на запрос создания нового платежа
 */
class StorePaymentResponse extends JsonResponse
{
    /**
     * Конвертация параметров в соответствие спецификации API
     *
     * @param BillingOperation $billingPayment
     * @param string $redirectUrl
     */
    public function __construct(BillingOperation $billingPayment, string $redirectUrl)
    {
        parent::__construct([
            'payment' => new BillingPaymentResource($billingPayment),
            'redirect_url' => $redirectUrl,
        ]);
    }
}
