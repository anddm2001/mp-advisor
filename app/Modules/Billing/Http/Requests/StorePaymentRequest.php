<?php

namespace App\Modules\Billing\Http\Requests;

use App\Modules\Common\Requests\FormRequest;
use App\Modules\Merchant\Models\Merchant;

/**
 * Запрос на создание нового платежа
 */
class StorePaymentRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        /** @var Merchant|null $merchant */
        $merchant = $this->route('merchant');
        $billingRateMinAmount = $merchant && $merchant->billingRate ? $merchant->billingRate->min_amount : null;
        $minAmount = $billingRateMinAmount ?? 0.01;

        return [
            'amount' => 'required|numeric|min:' . $minAmount,
        ];
    }

    /** @inheritDoc */
    public function attributes(): array
    {
        return [
            'amount' => 'Сумма пополнения',
        ];
    }

    /** @inheritDoc */
    public function messages(): array
    {
        return [
            'amount.min' => 'Минимальная сумма пополнения :min руб.',
        ];
    }
}
