<?php

namespace App\Modules\Billing\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Billing\Http\Requests\StorePaymentRequest;
use App\Modules\Billing\Http\Resources\BillingPaymentResource;
use App\Modules\Billing\Http\Responses\StorePaymentResponse;
use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Billing\Services\BillingService;
use App\Modules\Merchant\Models\Merchant;
use Exception;

/**
 * Платежи
 */
class PaymentController extends ApiController
{
    /** @var BillingService  */
    protected BillingService $service;

    /**
     * Инициализация сервиса
     *
     * @param BillingService $service
     */
    public function __construct(BillingService $service)
    {
        $this->service = $service;
    }

    /**
     * Создание платежа — пополнение баланса
     *
     * @param Merchant $merchant
     * @param StorePaymentRequest $request
     * @return StorePaymentResponse
     * @throws Exception
     */
    public function store(Merchant $merchant, StorePaymentRequest $request): StorePaymentResponse
    {
        $result = $this->service->createPayment($merchant, $request->validated(), $request->user());
        return new StorePaymentResponse($result['billing_payment'], $result['redirect_url']);
    }

    /**
     * Данные платежа
     *
     * @param Merchant $merchant
     * @param BillingOperation $billingOperation
     * @return BillingPaymentResource
     */
    public function show(Merchant $merchant, BillingOperation $billingOperation): BillingPaymentResource
    {
        return new BillingPaymentResource($billingOperation);
    }
}
