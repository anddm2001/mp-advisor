<?php

namespace App\Modules\Billing\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Auth\Models\User;
use App\Modules\Billing\Http\Resources\BillingBalanceResource;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

/**
 * Баланс
 */
class BalanceController extends ApiController
{
    /**
     * Данные для пополнения баланса
     *
     * @param Request $request
     * @return BillingBalanceResource
     * @throws AuthenticationException
     */
    public function topUp(Request $request): BillingBalanceResource
    {
        /** @var User|null $user */
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException();
        }

        return new BillingBalanceResource($user);
    }
}
