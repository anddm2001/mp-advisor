<?php

namespace App\Modules\Billing\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\IpUtils;

/**
 * Проверка IP адреса отправителя входящего запроса для ЮKassa
 */
class YooKassaCheckNotificationIp extends BillingNotificationMiddleware
{
    /** @inheritDoc */
    public function handle(Request $request, Closure $next)
    {
        $notificationIpRanges = config('yookassa.notificationIpRanges', []);
        // проверяется только если указаны диапазоны IP
        if (!empty($notificationIpRanges)) {
            $clientIps = $request->ips();
            foreach ($clientIps as $clientIp) {
                if (IpUtils::checkIp($clientIp, $notificationIpRanges)) {
                    return $next($request);
                }
            }
        }

        throw new AuthenticationException();
    }
}
