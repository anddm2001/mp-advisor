<?php

namespace App\Modules\Billing\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

/**
 * Абстрантный middleware для проверки входящего уведомления, от которого наследуются конкретные middleware
 */
abstract class BillingNotificationMiddleware
{
    /**
     * Проверка запроса входящего уведомления
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    abstract public function handle(Request $request, Closure $next);
}
