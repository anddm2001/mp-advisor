<?php

namespace App\Modules\Billing\Http\Resources;

use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Данные баланса пользователя в разрезе мерчанта
 */
class BillingBalanceMerchantResource extends JsonResource
{
    /**
     * @param Merchant $resource
     */
    public function __construct(Merchant $resource)
    {
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var Merchant $merchant */
        $merchant = $this->resource;
        return [
            'merchant_id' => $merchant->id,
            'owner_user_id' => $merchant->owner_id,
            'balance' => $merchant->balance,
            'balance_days' => $merchant->balance_days,
            'lack_amount' => $merchant->lack_amount,
            'charge_on' => (int) $merchant->charge_on->format('Uv'),
            'marketplace_item_counts' => $merchant->marketplaceConnections
                ->mapWithKeys(function (MerchantConnection $connection) {
                    return [$connection->marketplace => $connection->only([
                        'marketplace',
                        'item_active_count',
                        'item_total_count',
                    ])];
                }),
            'billing_rate' => new BillingRateResource($merchant->billingRate),
        ];
    }
}
