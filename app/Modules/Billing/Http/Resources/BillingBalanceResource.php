<?php

namespace App\Modules\Billing\Http\Resources;

use App\Modules\Auth\Models\User;
use App\Modules\Merchant\Models\Merchant;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Данные баланса пользователя
 */
class BillingBalanceResource extends JsonResource
{
    /**
     * @param User $resource
     */
    public function __construct(User $resource)
    {
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var User $user */
        $user = $this->resource;

        /** @var Collection|Merchant[] $merchants */
        $merchants = $user->merchants()->with(['billingRate', 'marketplaceConnections'])->get();

        return BillingBalanceMerchantResource::collection($merchants)->toArray($request);
    }
}
