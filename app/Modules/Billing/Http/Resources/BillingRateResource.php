<?php

namespace App\Modules\Billing\Http\Resources;

use App\Modules\Billing\Models\BillingRate;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Данные тарифа
 */
class BillingRateResource extends JsonResource
{
    /**
     * @param BillingRate $resource
     */
    public function __construct(BillingRate $resource)
    {
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var BillingRate $billingRate */
        $billingRate = $this->resource;
        return $billingRate->only([
            'name',
            'amount',
            'min_amount',
            'min_item_count',
            'max_item_count',
        ]);
    }
}
