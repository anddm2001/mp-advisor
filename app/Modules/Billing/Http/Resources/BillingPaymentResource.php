<?php

namespace App\Modules\Billing\Http\Resources;

use App\Modules\Billing\Models\BillingOperation;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Данные платежа
 */
class BillingPaymentResource extends JsonResource
{
    /**
     * @param BillingOperation $resource
     */
    public function __construct(BillingOperation $resource)
    {
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var BillingOperation $billingPayment */
        $billingPayment = $this->resource;
        return [
            'id' => $billingPayment->id,
            'status' => $billingPayment->status,
            'amount' => $billingPayment->amount,
            'description' => $billingPayment->description,
            'error_title' => $billingPayment->error_title,
            'error_description' => $billingPayment->error_description,
            'captured_at' => $billingPayment->captured_at ? (int) $billingPayment->captured_at->format('Uv') : null,
        ];
    }
}
