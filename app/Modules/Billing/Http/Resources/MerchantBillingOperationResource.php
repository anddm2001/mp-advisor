<?php

namespace App\Modules\Billing\Http\Resources;

use App\Modules\Billing\Models\BillingOperation;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Данные биллинг-операции
 */
class MerchantBillingOperationResource extends JsonResource
{
    /**
     * @param BillingOperation $resource
     */
    public function __construct(BillingOperation $resource)
    {
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var BillingOperation $operation */
        $operation = $this->resource;
        return [
            'date' => $operation->action_date ? (int) $operation->action_date->format('Uv') : null,
            'type' => $operation->type,
            'type_name' => $operation->type_name,
            'amount' => abs($operation->amount),
            'amount_sign' => $operation->amount >= 0 ? '+' : html_entity_decode('&minus;'),
            'billing_rate_name' => $operation->billing_rate_name,
            'prev_billing_rate_name' => $operation->prev_billing_rate_name,
            'operation_type' => $operation->operation_type,
            'operation_type_name' => $operation->operation_type_name,
            'status' => $operation->status,
            'status_name' => $operation->status_name,
            'number' => $operation->full_number,
        ];
    }
}
