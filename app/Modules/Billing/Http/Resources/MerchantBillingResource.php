<?php

namespace App\Modules\Billing\Http\Resources;

use App\Modules\Common\Resources\LengthAwarePaginationResource;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Сводная биллинг-информация по мерчанту
 */
class MerchantBillingResource extends JsonResource
{
    /** @var MerchantConnection|null Подключение к маркетплейсу */
    protected ?MerchantConnection $connection;

    /** @var LengthAwarePaginator Операции */
    protected LengthAwarePaginator $operations;

    /**
     * @param Merchant $resource
     * @param MerchantConnection|null $connection
     * @param LengthAwarePaginator $operations
     */
    public function __construct(Merchant $resource, ?MerchantConnection $connection, LengthAwarePaginator $operations)
    {
        $this->connection = $connection;
        $this->operations = $operations;
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var Merchant $merchant */
        $merchant = $this->resource;
        $connection = $this->connection;
        $operations = $this->operations;

        return [
            'item_active_count' => $connection ? $connection->item_active_count : null,
            'billing_rate_amount' => $merchant->billingRate ? $merchant->billingRate->amount : null,
            'balance' => $merchant->balance,
            'charge_on' => $merchant->charge_on ? (int) $merchant->charge_on->format('Uv') : null,
            'operations' => [
                'data' => MerchantBillingOperationResource::collection($operations),
                'pagination' => new LengthAwarePaginationResource($operations),
            ],
        ];
    }
}
