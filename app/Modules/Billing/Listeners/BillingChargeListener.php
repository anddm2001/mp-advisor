<?php

namespace App\Modules\Billing\Listeners;

use App\Modules\Billing\Events\BillingChargeEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillingChargeListener implements ShouldQueue
{
    /**
     * Списание средств с мерчанта по тарифу
     *
     * @param BillingChargeEvent $event
     */
    public function handle(BillingChargeEvent $event): void
    {
        $merchant = $event->getMerchant();
        $merchant->charge();
    }
}
