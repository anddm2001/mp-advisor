<?php

namespace App\Modules\Billing\Exceptions;

use Exception;

/**
 * Требуется оплата
 */
class PaymentRequiredException extends Exception
{
    /** @var array дополнительные данные */
    protected array $data;

    /**
     * Исключение с дополнительными данными
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct();
        $this->data = $data;
    }

    /**
     * Получение дополнительных данных
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
