<?php

namespace App\Modules\Billing\Models;

use App\Modules\Auth\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Параметры писем-уведомлений
 *
 * @property int $id
 * @property int $user_id
 * @property bool|null $is_trial_period_finish_sent Письмо об окончании пробного периода отправлено
 * @property int|null $blocked_sent_count Сколько отправлено писем о блокировке
 * @property Carbon|null $blocked_last_sent_date Дата отправки последнего письма о блокировке
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $user
 */
class BillingNotification extends Model
{
    /** @var int Максимальное кол-во писем о блокировке */
    public const MAX_BLOCKED_SENT_COUNT = 3;

    /** @var int Кол-во дней между письмами о блокировке */
    public const BLOCKED_SENDING_DAYS = 5;

    /** @var int За сколько дней до окончания пробного периода отправить письмо */
    public const TRIAL_PERIOD_BEFORE_FINISH_DAYS = 3;

    /** @inheritdoc */
    protected $table = 'billing_notification';

    /** @inheritdoc */
    protected $fillable = [
        'user_id',
        'is_trial_period_finish_sent',
        'blocked_sent_count',
        'blocked_last_sent_date',
    ];

    /** @inheritdoc */
    protected $casts = [
        'is_trial_period_finish_sent' => 'boolean',
        'blocked_last_sent_date' => 'datetime',
    ];

    // Relationships ================================================================================================ //

    /**
     * Пользователь
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id', 'user');
    }
}
