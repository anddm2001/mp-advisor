<?php

namespace App\Modules\Billing\Models;

use Illuminate\Database\Eloquent\Concerns\HasRelationships;
use Illuminate\Database\Eloquent\Model;

/**
 * Интерфейс модели пользователя — инициатора операции биллинга
 */
interface BillingOperationInitiatorContract
{
    /**
     * Get the class name for polymorphic relations.
     *
     * @see HasRelationships
     * @return string
     */
    public function getMorphClass();

    /**
     * Get the value of the model's primary key.
     *
     * @see Model
     * @return mixed
     */
    public function getKey();

    /**
     * Получить имя пользователя — инициатора операции биллинга
     *
     * @return string
     */
    public function getBillingOperationInitiatorName(): string;
}
