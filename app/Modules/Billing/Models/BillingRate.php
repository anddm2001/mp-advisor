<?php

namespace App\Modules\Billing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Тариф
 *
 * @property int $id
 * @property bool $is_active Активен
 * @property string $name Название
 * @property float $amount Сумма ежедневного списания
 * @property float|null $min_amount Минимальная сумма пополнения
 * @property int $min_item_count Минимальное кол-во SKU
 * @property int|null $max_item_count Максимальное кол-во SKU
 * @property int|null $trial_period_days Пробный период в днях
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property array $item_count
 */
class BillingRate extends Model
{
    /** @inheritdoc */
    protected $table = 'billing_rate';

    /** @inheritdoc */
    protected $fillable = [
        'is_active',
        'name',
        'amount',
        'min_amount',
        'min_item_count',
        'max_item_count',
        'trial_period_days',
    ];

    /** @inheritdoc */
    protected $casts = [
        'amount' => 'float',
        'min_amount' => 'float',
    ];

    // Accessors & Mutators ========================================================================================= //

    /**
     * Получить количества SKU в виде массива
     *
     * @return array
     */
    public function getItemCountAttribute(): array
    {
        return [
            'from' => $this->min_item_count,
            'to' => $this->max_item_count,
        ];
    }

    /**
     * Сохранить количества SKU из массива
     *
     * @param array $itemCount
     * @return void
     */
    public function setItemCountAttribute(array $itemCount): void
    {
        $this->attributes['min_item_count'] = $itemCount['from'] ?? 0;
        $this->attributes['max_item_count'] = $itemCount['to'] ?? null;
    }
}
