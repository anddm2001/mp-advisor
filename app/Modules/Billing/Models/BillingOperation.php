<?php

namespace App\Modules\Billing\Models;

use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;

/**
 * Операция в биллинге
 *
 * @property int $id
 * @property int $merchant_id Мерчант
 * @property int|null $merchant_connection_id Подключение
 * @property string|null $initiator_type
 * @property int|null $initiator_id
 * @property int|null $billing_rate_id Применённый тариф
 * @property string|null $billing_rate_name Название применённого тарифа
 * @property string|null $prev_billing_rate_name Название предыдущего тарифа
 * @property int|null $item_total_count Общее кол-во позиций
 * @property int|null $item_active_count Кол-во активных позиций
 * @property string $type Тип операции
 * @property string $status Статус операции
 * @property float $amount Сумма операции
 * @property string|null $description Описание операции
 * @property string|null $error_title Название ошибки
 * @property string|null $error_description Описание ошибки
 * @property string|null $payment_system Платёжная система
 * @property string|null $payment_reference Идентификатор платежа в ПС
 * @property string|null $payment_cancellation_reason Код причины отмены платежа
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $captured_at Время проведения
 * @property-read DateTime|null $action_date
 * @property-read string $full_number
 * @property-read string $operation_type
 * @property-read string $operation_type_name
 * @property-read string|null $payment_system_name
 * @property-read string $status_name
 * @property-read string $status_style
 * @property-read string $type_name
 * @property-read BillingOperationInitiatorContract|null $initiator
 * @property-read MerchantConnection|null $marketplaceConnection
 * @property-read Merchant $merchant
 * @method static Builder|BillingOperation ofPaymentSystem(string $paymentSystem)
 * @method static Builder|BillingOperation ofStatus(string $status)
 */
class BillingOperation extends Model
{
    /** @var string Тип операции — пополнение (платёж) */
    public const TYPE_PAYMENT = 'payment';
    /** @var string Тип операции — списание (оплата тарифа) */
    public const TYPE_EXPENSE = 'expense';
    /** @var string Тип операции — смена тарифа */
    public const TYPE_RATE_CHANGE = 'rate-change';

    /** @var string Статус операции - новая */
    public const STATUS_NEW = 'new';
    /** @var string Статус операции - в процессе */
    public const STATUS_PENDING = 'pending';
    /** @var string Статус операции - успешно */
    public const STATUS_SUCCESS = 'success';
    /** @var string Статус операции - провал */
    public const STATUS_FAILED = 'failed';

    /** @var string Платёжная система - ЮKassa */
    public const PAYMENT_SYSTEM_YOO_KASSA = 'yoo-kassa';
    /** @var string Платёжная система - mpAdvisor */
    public const PAYMENT_SYSTEM_MPADVISOR = 'mpadvisor';

    /** @inheritdoc */
    protected $table = 'billing_operation';

    /** @inheritdoc */
    protected $fillable = [
        'merchant_id',
        'merchant_connection_id',
        'initiator_type',
        'initiator_id',
        'billing_rate_id',
        'billing_rate_name',
        'prev_billing_rate_name',
        'item_total_count',
        'item_active_count',
        'type',
        'status',
        'amount',
        'description',
        'error_title',
        'error_description',
        'payment_system',
        'payment_reference',
        'payment_cancellation_reason',
        'captured_at',
    ];

    /** @inheritdoc */
    protected $casts = [
        'amount' => 'float',
        'captured_at' => 'datetime',
    ];

    // Scopes ======================================================================================================= //

    /**
     * Операции с определенным статусом
     *
     * @param Builder $query
     * @param string $status
     * @return Builder
     */
    public function scopeOfStatus(Builder $query, string $status): Builder
    {
        return $query->where('status', $status);
    }

    /**
     * Платежи определенной платёжной системы
     *
     * @param Builder $query
     * @param string $paymentSystem
     * @return Builder
     */
    public function scopeOfPaymentSystem(Builder $query, string $paymentSystem): Builder
    {
        return $query->where('type', self::TYPE_PAYMENT)->where('payment_system', $paymentSystem);
    }

    // Relationships ================================================================================================ //

    /**
     * Мерчант
     *
     * @return BelongsTo
     */
    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id', 'merchant');
    }

    /**
     * Подключение к маркетплейсу
     *
     * @return BelongsTo
     */
    public function marketplaceConnection(): BelongsTo
    {
        return $this->belongsTo(MerchantConnection::class, 'merchant_connection_id', 'id', 'marketplaceConnection');
    }

    /**
     * Полиморфная связь с инициатором операции
     *
     * @return MorphTo
     */
    public function initiator(): MorphTo
    {
        return $this->morphTo();
    }

    // Accessors & Mutators ========================================================================================= //

    /**
     * Получить название типа операции
     *
     * @return string
     */
    public function getTypeNameAttribute(): string
    {
        $types = self::getTypes();
        return $types[$this->type] ?? $this->type;
    }

    /**
     * Получить код типа операции для отображения пользователю
     *
     * @return string
     */
    public function getOperationTypeAttribute(): string
    {
        if ($this->type === self::TYPE_PAYMENT) {
            return $this->payment_system ?: $this->type;
        }

        return $this->type;
    }

    /**
     * Получить название типа операции для отображения пользователю
     *
     * @return string
     */
    public function getOperationTypeNameAttribute(): string
    {
        if ($this->type === self::TYPE_PAYMENT) {
            return $this->payment_system ? $this->payment_system_name : $this->type_name;
        }

        return $this->type_name;
    }

    /**
     * Получить название статуса операции
     *
     * @return string
     */
    public function getStatusNameAttribute(): string
    {
        $statuses = self::getStatuses();
        return $statuses[$this->status] ?? $this->status;
    }

    /**
     * Получить стиль статуса операции
     *
     * @return string
     */
    public function getStatusStyleAttribute(): string
    {
        $statusStyles = self::getStatusStyles();
        return $statusStyles[$this->status] ?? $this->status;
    }

    /**
     * Получить название платёжной системы
     *
     * @return string|null
     */
    public function getPaymentSystemNameAttribute(): ?string
    {
        $paymentSystems = self::getPaymentSystems();
        return $paymentSystems[$this->payment_system] ?? $this->payment_system;
    }

    /**
     * Получить полный номер операции для отображения
     *
     * @return string
     */
    public function getFullNumberAttribute(): string
    {
        $prefix = '';
        switch ($this->type) {
            case self::TYPE_PAYMENT:
                $prefix = 'П-';
                break;
            case self::TYPE_EXPENSE:
                $prefix = 'С-';
                break;
            case self::TYPE_RATE_CHANGE:
                $prefix = 'Т-';
                break;
        }
        return $prefix . sprintf('%08d', $this->id);
    }

    /**
     * Получить дату действия по операции
     *
     * @return DateTime|null
     */
    public function getActionDateAttribute(): ?DateTime
    {
        if ($this->captured_at) {
            return $this->captured_at->toDateTime();
        }

        if ($this->created_at) {
            return $this->created_at->toDateTime();
        }

        return null;
    }

    // Stuff ======================================================================================================== //

    /**
     * Типы операций
     *
     * @return string[]
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_PAYMENT => 'Пополнение баланса',
            self::TYPE_EXPENSE => 'Списание средств',
            self::TYPE_RATE_CHANGE => 'Смена тарифа',
        ];
    }

    /**
     * Статусы операций
     *
     * @return string[]
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_PENDING => 'Ожидание',
            self::STATUS_SUCCESS => 'Успешно',
            self::STATUS_FAILED => 'Ошибка',
        ];
    }

    /**
     * Стили статусов операций
     *
     * @return string[]
     */
    public static function getStatusStyles(): array
    {
        return [
            self::STATUS_NEW => 'info',
            self::STATUS_PENDING => 'warning',
            self::STATUS_SUCCESS => 'success',
            self::STATUS_FAILED => 'danger',
        ];
    }

    /**
     * Платёжные системы
     *
     * @return string[]
     */
    public static function getPaymentSystems(): array
    {
        return [
            self::PAYMENT_SYSTEM_YOO_KASSA => 'ЮKassa',
            self::PAYMENT_SYSTEM_MPADVISOR => 'mpAdvisor',
        ];
    }
}
