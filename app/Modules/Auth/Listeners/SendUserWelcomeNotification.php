<?php

namespace App\Modules\Auth\Listeners;

use App\Modules\Auth\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Contracts\Auth\MustVerifyEmail;

/**
 * Отправка приветственного сообщения пользователю после активации
 */
class SendUserWelcomeNotification
{
    /**
     * Handle the event.
     *
     * @param Verified $event
     * @return void
     */
    public function handle(Verified $event)
    {
        /** @var User $user */
        $user = $event->user;

        if ($user instanceof MustVerifyEmail && $user->hasVerifiedEmail()) {
            $user->sendUserWelcomeNotification();
        }
    }
}
