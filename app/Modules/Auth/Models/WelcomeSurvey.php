<?php

namespace App\Modules\Auth\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Модель приветственного опросника
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $nomenclature_category
 * @property string|null $wildberries_income_freq
 * @property array|null $marketplaces
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user Привязка к пользователю
 *
 * @package App\Modules\Auth\Models
 */
class WelcomeSurvey extends Model
{
    /** @inheritdoc  */
    protected $table = 'welcome_survey';

    /** @inheritdoc  */
    protected $fillable = [
        'user_id',
        'nomenclature_category',
        'wildberries_income_freq',
        'marketplaces',
    ];

    /** @inheritdoc  */
    protected $casts = [
        'marketplaces' => 'array',
    ];

    /**
     * Получить модель пользователя
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id', 'user');
    }
}
