<?php

namespace App\Modules\Auth\Models;

use App\Modules\Auth\Notifications\ResetPasswordNotification;
use App\Modules\Auth\Notifications\UserWelcomeNotification;
use App\Modules\Billing\Models\BillingNotification;
use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Billing\Models\BillingOperationInitiatorContract;
use App\Modules\Billing\Notifications\BillingBalanceBlockedNotification;
use App\Modules\Billing\Notifications\BillingPaymentStatusNotification;
use App\Modules\Billing\Notifications\BillingTrialPeriodFinishNotification;
use App\Modules\Common\Models\EventLog;
use App\Modules\Common\Models\MailLog;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantUser;
use App\Modules\Storage\Models\File;
use Exception;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Laravel\Sanctum\HasApiTokens;

/**
 * Модель пользователя
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int|null $photo_file_id
 * @property string|null $phone
 * @property string|null $company_name
 * @property string|null $company_post
 * @property-read BillingNotification|null $billingNotification
 * @property-read Collection|BillingOperation[] $billingOperations
 * @property-read int|null $billing_operations_count
 * @property-read string|null $phone_formatted
 * @property-read string $status
 * @property-read string $status_name
 * @property-read Collection|MailLog[] $mailLogs
 * @property-read int|null $mail_logs_count
 * @property-read Collection|Merchant[] $merchants
 * @property-read int|null $merchants_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read File|null $photoFile
 * @property-read Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @property-read WelcomeSurvey|null $welcomeSurvey
 */
class User extends Authenticatable implements MustVerifyEmail, BillingOperationInitiatorContract
{
    use HasApiTokens;
    use Notifiable;

    /** @var string Статус "Активен" */
    public const STATUS_ACTIVE = 'active';
    /** @var string Статус "Активация" */
    public const STATUS_PENDING = 'pending';

    // название токена для аутентификации пользователя через API
    public const TOKEN_NAME = 'api-auth-token';

    /** @inheritdoc */
    protected $table = 'user';

    /** @inheritdoc */
    protected $fillable = [
        'photo_file_id',
        'name',
        'phone',
        'email',
        'company_name',
        'company_post',
        'password',
    ];

    /** @inheritdoc */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /** @inheritdoc  */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Relationships ================================================================================================ //

    /**
     * Получить модель файла с фото пользователя в хранилище
     *
     * @return BelongsTo
     */
    public function photoFile(): BelongsTo
    {
        return $this->belongsTo(File::class, 'photo_file_id', 'id', 'photoFile');
    }

    /**
     * Получить массив доступных мерчантов
     *
     * @return HasManyThrough
     */
    public function merchants(): HasManyThrough
    {
        return $this->hasManyThrough(
            Merchant::class,
            MerchantUser::class,
            'user_id',
            'id',
            'id',
            'merchant_id'
        );
    }

    /**
     * Получить модель приветственного опросника
     *
     * @return HasOne
     */
    public function welcomeSurvey(): HasOne
    {
        return $this->hasOne(WelcomeSurvey::class, 'user_id', 'id');
    }

    /**
     * Операции биллинга, инициированные этим пользователем
     *
     * @return MorphMany
     */
    public function billingOperations(): MorphMany
    {
        return $this->morphMany(BillingOperation::class, 'initiator');
    }

    /**
     * Параметры писем-уведомлений
     *
     * @return HasOne
     */
    public function billingNotification(): HasOne
    {
        return $this->hasOne(BillingNotification::class, 'user_id', 'id');
    }

    /**
     * Записи в логе отправленных писем
     *
     * @return HasMany
     */
    public function mailLogs(): HasMany
    {
        return $this->hasMany(MailLog::class, 'user_id', 'id');
    }

    /**
     * Записи в логе событий
     *
     * @return HasMany
     */
    public function eventLogs(): HasMany
    {
        return $this->hasMany(EventLog::class, 'user_id', 'id');
    }

    // Accessors & Mutators ========================================================================================= //

    /**
     * Код статуса пользователя
     *
     * @return string
     */
    public function getStatusAttribute(): string
    {
        $status = self::STATUS_ACTIVE;
        if (!$this->hasVerifiedEmail()) {
            $status = self::STATUS_PENDING;
        }

        return $status;
    }

    /**
     * Отформатированный номер телефона
     *
     * @return string|null
     */
    public function getPhoneFormattedAttribute(): ?string
    {
        $phone = $this->phone ?? '';
        $phone = preg_replace('/[^\d]+/', '', $phone);
        if (strpos($phone, '8') === 0) {
            $phone = '7' . substr($phone, 1);
        }
        return $phone ?: null;
    }

    /**
     * Название статуса пользователя
     *
     * @return string
     */
    public function getStatusNameAttribute(): string
    {
        $statusNames = self::getStatusNames();
        return $statusNames[$this->status] ?? $this->status;
    }

    // Stuff ======================================================================================================== //

    /**
     * Список названия статусов пользователя
     *
     * @return array
     */
    public static function getStatusNames(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_PENDING => 'Активация',
        ];
    }

    /**
     * Отправка приветственного уведомления пользвоателю
     *
     * @return void
     */
    public function sendUserWelcomeNotification(): void
    {
        $this->notify(new UserWelcomeNotification());
    }

    /**
     * Отправка уведомления со статусом платежа
     *
     * @param BillingOperation $billingOperation
     * @return void
     */
    public function sendBillingPaymentStatusNotification(BillingOperation $billingOperation): void
    {
        $this->notify(new BillingPaymentStatusNotification($billingOperation));
    }

    /**
     * Проверка и отправка необходимых писем-уведомлений по событиям биллинга
     *
     * @param Merchant $merchant
     */
    public function sendBillingNotifications(Merchant $merchant): void
    {
        /** @var BillingNotification $billingNotification */
        $billingNotification = $this->billingNotification()->firstOrCreate();

        // Уведомление о блокировке по балансу
        if ($merchant->is_balance_blocked &&
            (int) $billingNotification->blocked_sent_count < BillingNotification::MAX_BLOCKED_SENT_COUNT &&
            (
                is_null($billingNotification->blocked_last_sent_date) ||
                $billingNotification->blocked_last_sent_date <= Carbon::now()->subDays(BillingNotification::BLOCKED_SENDING_DAYS)
            )
        ) {
            $this->notify(new BillingBalanceBlockedNotification($merchant));
            $billingNotification->update([
                'blocked_sent_count' => (int) $billingNotification->blocked_sent_count + 1,
                'blocked_last_sent_date' => Carbon::now(),
            ]);
        }

        // Уведомление об окончании пробного периода
        if (!$billingNotification->is_trial_period_finish_sent &&
            $merchant->trial_period_finish &&
            $merchant->trial_period_finish >= Carbon::today() &&
            $merchant->trial_period_finish <= Carbon::today()->addDays(BillingNotification::TRIAL_PERIOD_BEFORE_FINISH_DAYS)
        ) {
            $this->notify(new BillingTrialPeriodFinishNotification($merchant));
            $billingNotification->update(['is_trial_period_finish_sent' => true]);
        }
    }

    /**
     * Создать мерчанта для пользователя
     *
     * @throws Exception
     */
    public function createMerchant(): void
    {
        // добавление мерчанта пользователю
        $merchant = new Merchant([
            'owner_id' => $this->id,
            'name' => $this->name,
        ]);
        if (!$merchant->save()) {
            throw new Exception('Не удалось создать мерчанта.');
        }
        // связь пользователя и мерчанта
        $merchantUser = new MerchantUser([
            'merchant_id' => $merchant->id,
            'user_id' => $this->id,
        ]);
        if (!$merchantUser->save()) {
            throw new Exception('Не удалось связать пользователя с мерчантом.');
        }
    }

    /** @inheritDoc */
    public function getBillingOperationInitiatorName(): string
    {
        return $this->name . ($this->company_name ? ' (' . $this->company_name . ')' : '');
    }

    /** @inheritDoc */
    public function sendPasswordResetNotification($token): void
    {
        $url = URL::route('password.reset', compact('token'), true);
        $this->notify(new ResetPasswordNotification($url));
    }
}
