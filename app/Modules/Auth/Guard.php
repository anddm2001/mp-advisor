<?php

namespace App\Modules\Auth;

use App\Modules\Auth\Models\PersonalAccessToken;
use Illuminate\Http\Request;
use Laravel\Sanctum\Guard as SanctumGuard;
use Laravel\Sanctum\Sanctum;

class Guard extends SanctumGuard
{
    /**
     * Retrieve the authenticated user for the incoming request.
     *
     * @param Request $request
     * @return mixed|void
     */
    public function __invoke(Request $request)
    {
        if ($token = $request->bearerToken()) {
            /** @var PersonalAccessToken $model */
            $model = Sanctum::$personalAccessTokenModel;
            $accessToken = $model::findToken($token);

            if (!$accessToken ||
                ($this->expiration && $accessToken->created_at->lte(now()->subMinutes($this->expiration))) ||
                !$this->hasValidProvider($accessToken->tokenable)
            ) {
                return;
            }

            return $this->supportsTokens($accessToken->tokenable) ? $accessToken->tokenable->withAccessToken(
                tap($accessToken->forceFill(['last_used_at' => now()]))->save()
            ) : null;
        }

        return;
    }
}
