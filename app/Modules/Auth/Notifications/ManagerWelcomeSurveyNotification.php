<?php

namespace App\Modules\Auth\Notifications;

use App\Modules\Auth\Models\User;
use App\Modules\Auth\Models\WelcomeSurvey;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Уведомление управляющему об отправке данных приветственного опросника
 */
class ManagerWelcomeSurveyNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /** @var User модель пользователя */
    protected User $user;

    /** @var WelcomeSurvey модель опросника */
    protected WelcomeSurvey $welcomeSurvey;

    /**
     * Определение параметров для уведомления
     *
     * @param User $user
     * @param WelcomeSurvey $welcomeSurvey
     */
    public function __construct(User $user, WelcomeSurvey $welcomeSurvey)
    {
        $this->user = $user;
        $this->welcomeSurvey = $welcomeSurvey;
    }

    /**
     * Каналы отправки уведомления
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Сборка письма для отправки уведомления по e-mail
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        $mailMessage = new MailMessage();
        $mailMessage->subject('Новый пользователь mpAdvisor');
        $mailMessage->greeting(' ');
        $mailMessage->line('Новый пользователь mpAdvisor отправил ответы на приветственный опросник.');
        if ($this->welcomeSurvey->nomenclature_category) {
            $mailMessage->line('*Категория товаров:* ' . $this->welcomeSurvey->nomenclature_category);
        }
        if ($this->welcomeSurvey->wildberries_income_freq) {
            $mailMessage->line('*Частота поставок:* ' . $this->welcomeSurvey->wildberries_income_freq);
        }
        if (!empty($this->welcomeSurvey->marketplaces)) {
            $mailMessage->line('*Маркетплейсы:* ' . implode(', ', $this->welcomeSurvey->marketplaces));
        }
        $mailMessage->line('**Данные пользователя**');
        $mailMessage->line('*Имя:* ' . $this->user->name);
        $mailMessage->line('*E-mail:* ' . $this->user->email);
        $mailMessage->salutation(' ');
        return $mailMessage;
    }
}
