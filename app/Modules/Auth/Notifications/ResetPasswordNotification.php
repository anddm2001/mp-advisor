<?php

namespace App\Modules\Auth\Notifications;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\UrlHelper;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Письмо-уведомление со ссылкой на сброс пароля
 */
class ResetPasswordNotification extends Notification
{
    /** @var string */
    protected string $url;

    /**
     * Определение URL ссылки для сброса пароля
     *
     * @param string $url Ссылка, сгенерированная для API (например, URL::route())
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * Каналы отправки уведомления
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Сборка письма для отправки уведомления по e-mail
     *
     * @param  User  $notifiable
     * @return MailMessage
     */
    public function toMail(User $notifiable): MailMessage
    {
        return (new MailMessage())
            ->subject('Восстановление пароля в mpAdvisor — сервисе аналитики маркетплейсов')
            ->view('mail.password-recovery', [
                'userName' => $notifiable->name,
                'appUrl' => config('app.url'),
                'appName' => config('app.name'),
                'resetUrl' => UrlHelper::ApiRoutedUrlToRootAppUrl($this->url),
            ]);
    }
}
