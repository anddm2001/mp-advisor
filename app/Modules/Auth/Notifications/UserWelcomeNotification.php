<?php

namespace App\Modules\Auth\Notifications;

use App\Modules\Auth\Models\User;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Приветственное уведомление новому пользователю
 */
class UserWelcomeNotification extends Notification
{
    /**
     * Каналы отправки уведомления
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Сборка письма для отправки уведомления по e-mail
     *
     * @param  User  $notifiable
     * @return MailMessage
     */
    public function toMail(User $notifiable): MailMessage
    {
        return (new MailMessage())
            ->subject('Добро пожаловать в mpAdvisor — сервис аналитики маркетплейсов')
            ->view('mail.user-welcome', [
                'userName' => $notifiable->name,
                'appUrl' => config('app.url'),
            ]);
    }
}
