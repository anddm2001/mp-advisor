<?php

namespace App\Modules\Auth\Providers;

use App\Modules\Auth\Listeners\SendUserWelcomeNotification;
use App\Modules\Common\Listeners\LogEventListener;
use App\Modules\Common\Listeners\NotificationSentMailLog;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Notifications\Events\NotificationSent;

/**
 * Провайдер сервисов событий аутентифакации и регистрации
 */
class AuthEventServiceProvider extends ServiceProvider
{
    /** @inheritdoc  */
    protected $listen = [
        // при входе в систему сохранить лог
        Login::class => [
            LogEventListener::class,
        ],
        // при регистрации отправить ссылку на активацию
        Registered::class => [
            SendEmailVerificationNotification::class,
            LogEventListener::class,
        ],
        // при активации отправить приветственное письмо
        Verified::class => [
            SendUserWelcomeNotification::class,
            LogEventListener::class,
        ],

        // после отправки письма-уведомления
        NotificationSent::class => [
            NotificationSentMailLog::class,
        ],
    ];
}
