<?php

namespace App\Modules\Auth\Providers;

use App\Modules\Auth\Console\Commands\AddUserCommand;
use App\Modules\Auth\Guard;
use App\Modules\Auth\Models\PersonalAccessToken;
use Illuminate\Auth\AuthManager;
use Illuminate\Auth\RequestGuard;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\Sanctum;

/**
 * Провайдер сервисов модуля авторизации и регистрации
 *
 * @property Application $app
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * @inheritdoc
     */
    public function register()
    {
        $this->registerConsoleCommands();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->configureGuard();
    }

    /**
     * Регистрация консольных команд
     */
    protected function registerConsoleCommands()
    {
        $this->commands([
            AddUserCommand::class
        ]);
    }

    /**
     * Configure the Sanctum authentication guard.
     *
     * @return void
     */
    protected function configureGuard(): void
    {
        Auth::resolved(function ($auth) {
            $auth->extend('sanctum', function ($app, $name, array $config) use ($auth) {
                return tap($this->createGuard($auth, $config), function ($guard) {
                    $this->app->refresh('request', $guard, 'setRequest');
                });
            });
        });
    }

    /**
     * Register the guard.
     *
     * @param AuthManager $auth
     * @param array $config
     * @return RequestGuard
     */
    protected function createGuard(AuthManager $auth, array $config): RequestGuard
    {
        return new RequestGuard(
            new Guard($auth, config('sanctum.expiration'), $config['provider']),
            $this->app['request'],
            $auth->createUserProvider($config['provider'] ?? null)
        );
    }

    /**
     * Define custom Personal Access Token model.
     *
     * @return void
     */
    protected function definePersonalAccessTokenModel(): void
    {
        Sanctum::usePersonalAccessTokenModel(PersonalAccessToken::class);
    }
}
