<?php

namespace App\Modules\Auth\Providers;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\UrlHelper;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\ServiceProvider;

/**
 * Провайдер сервисов регистрации новых пользователей
 */
class RegistrationServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        // письмо для активации аккаунта
        VerifyEmail::toMailUsing(function (User $notifiable, $url) {
            return (new MailMessage())
                ->subject('Пожалуйста, активируйте ваш аккаунт в mpAdvisor')
                ->view('mail.email-verification', [
                    'userName' => $notifiable->name,
                    'verificationUrl' => UrlHelper::ApiRoutedUrlToRootAppUrl($url),
                ]);
        });
    }
}
