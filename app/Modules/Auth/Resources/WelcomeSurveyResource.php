<?php

namespace App\Modules\Auth\Resources;

use App\Modules\Auth\Models\WelcomeSurvey;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Данные приветственного опросника
 */
class WelcomeSurveyResource extends JsonResource
{
    /**
     * @param WelcomeSurvey $resource
     */
    public function __construct(WelcomeSurvey $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        /** @var WelcomeSurvey $welcomeSurvey */
        $welcomeSurvey = $this->resource;
        return [
            'nomenclature_category' => $welcomeSurvey->nomenclature_category,
            'wildberries_income_freq' => $welcomeSurvey->wildberries_income_freq,
            'marketplaces' => $welcomeSurvey->marketplaces,
        ];
    }
}
