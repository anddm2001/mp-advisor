<?php

namespace App\Modules\Auth\Resources;

use App\Modules\Auth\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Успешная аутентификация
 */
class AuthResource extends JsonResource
{
    /** @var string Аутентификационный токен */
    protected string $token;

    /**
     * @param string $token
     * @param User $resource
     */
    public function __construct(string $token, User $resource)
    {
        $this->token = $token;
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var User $user */
        $user = $this->resource;

        return [
            'token' => $this->token,
            'user' => new UserResource($user),
        ];
    }
}
