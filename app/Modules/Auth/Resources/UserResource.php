<?php

namespace App\Modules\Auth\Resources;

use App\Modules\Auth\Models\User;
use App\Modules\Merchant\Resources\MerchantResource;
use App\Modules\Storage\Contracts\StorageServiceContract;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Пользователь
 */
class UserResource extends JsonResource
{
    /**
     * @param User $resource
     */
    public function __construct(User $resource)
    {
        parent::__construct($resource);
    }

    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        /** @var User $user */
        $user = $this->resource;

        /** @var StorageServiceContract $storageService */
        $storageService = resolve(StorageServiceContract::class);

        return [
            'id' => $user->id,
            'photo' => $user->photoFile ? $storageService->url($user->photoFile) : null,
            'name' => $user->name,
            'phone' => $user->phone,
            'email' => $user->email,
            'company_name' => $user->company_name,
            'company_post' => $user->company_post,
            'merchants' => MerchantResource::collection($user->merchants),
        ];
    }
}
