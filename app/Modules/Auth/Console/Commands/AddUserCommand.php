<?php

namespace App\Modules\Auth\Console\Commands;

use App\Modules\Auth\Models\User;
use App\Modules\Merchant\Contracts\MerchantConnectionManagerContract;
use App\Modules\Merchant\Models\Merchant;
use App\Modules\Merchant\Models\MerchantConnection;
use App\Modules\Merchant\Models\MerchantUser;
use App\Modules\Merchant\Requests\StoreMerchantConnectionRequest;
use App\Modules\Report\Contracts\MarketPlaceContract;
use Illuminate\Console\Command;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

/**
 * Добавление нового пользователя в систему
 *
 * @package App\Modules\Auth\Console\Commands
 */
class AddUserCommand extends Command
{
    public $signature = 'auth:new-user {name : Имя пользователя} {email : E-mail пользователя} {base64Key : Base64 ключ подключения к Wildberries API}';

    public $description = 'Добавление нового пользователя в систему';

    /**
     * Создать подключение к Wildberries и сохранить его в БД
     *
     * @param Merchant $merchant
     * @param string $base64Key
     * @param MerchantConnectionManagerContract $connectionManager
     *
     * @return bool
     */
    protected function storeWBBase64Key(Merchant $merchant, string $base64Key, MerchantConnectionManagerContract $connectionManager): bool
    {
        $request = new StoreMerchantConnectionRequest([
            'marketplace' => MarketPlaceContract::WILDBERRIES,
            'key' => $base64Key,
        ]);
        $request->setContainer(app());
        $request->setUserResolver(function() use ($merchant) {
            return $merchant->owner;
        });

        $request->setValidator(Validator::make(
            $request->all(),
            $request->rules($connectionManager),
            $request->messages(),
            $request->attributes()
        ));

        try {
            return $connectionManager->store($request, $merchant, new MerchantConnection());
        } catch (ValidationException $ex) {
            $this->error('Ошибка валидации подключения к Wildberries:');
            foreach ($ex->errors() as $key => $error) {
                $error = reset($error);
                $this->info("$key: $error");
            }
            throw $ex;
        }
    }

    /**
     * Добавление нового пользователя в систему
     *
     * @param User $user Новая модель пользователя для добавления
     * @param Merchant $merchant Новая модель мерчанта для добавления
     * @param MerchantUser $merchantUser Модель для связи мерчанта и пользователя
     * @param Hasher $hasher Хешер паролей
     * @param MerchantConnectionManagerContract $connectionsManager Сервис подключений к Marketplace API
     *
     * @return int
     */
    public function handle(
        User $user,
        Merchant $merchant,
        MerchantUser $merchantUser,
        Hasher $hasher,
        MerchantConnectionManagerContract $connectionsManager): int
    {
        $name = $this->argument('name');
        $email = $this->argument('email');
        $base64Key = $this->argument('base64Key');

        $password = Str::random(10);

        $user->name = $name;
        $user->email = $email;
        $user->password = $hasher->make($password);

        // создание нового мерчанта под пользователя
        $merchant->name = $name;

        $result = $user->getConnection()->transaction(function() use ($user, $merchant, $merchantUser, $base64Key, $connectionsManager) {
            if (!$user->save()) {
                $this->error('Не удалось добавить нового пользователя');
                return false;
            }

            $merchant->owner_id = $user->id;

            if (!$merchant->save()) {
                $this->error('Не удалось добавить нового мерчанта');
                return false;
            }

            $merchantUser->user_id = $user->id;
            $merchantUser->merchant_id = $merchant->id;

            if (!$merchantUser->save()) {
                $this->error('Не удалось связать мерчанта и пользователя');
                return false;
            }

            if (!$this->storeWBBase64Key($merchant, $base64Key, $connectionsManager)) {
                $this->error('Не удалось сохранить подключение к Wildberries');
                return false;
            }

            return true;
        });

        if (!$result) {
            return 1;
        }

        $this->info('Идентификатор нового пользователя: ' . $user->id);
        $this->info('Пароль нового пользователя: ' . $password);

        return 0;
    }
}
