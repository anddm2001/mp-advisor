<?php

namespace App\Modules\Auth\Exceptions;

use Exception;

/**
 * Исключение, когда пользователь не подтвердил свой e-mail
 */
class UnverifiedEmailException extends Exception
{
    /** @var array дополнительные данные */
    protected array $data;

    /**
     * Исключение с дополнительными данными
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        parent::__construct();
        $this->data = $data;
    }

    /**
     * Получение дополнительных данных
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}