<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Auth\Requests\RegisterRequest;
use App\Modules\Auth\Models\User;
use App\Modules\Common\Responses\SuccessResponse;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Hashing\Hasher;

/**
 * Регистрация пользователей
 */
class RegistrationController extends ApiController
{
    /**
     * Регистрация нового пользователя
     *
     * @param RegisterRequest $request
     * @param Hasher $hasher
     * @return SuccessResponse
     */
    public function register(RegisterRequest $request, Hasher $hasher): SuccessResponse
    {
        $validatedData = $request->validated();
        $validatedData['password'] = $hasher->make($validatedData['password']);
        $user = new User($validatedData);
        if ($user->save()) {
            event(new Registered($user));
        }
        return new SuccessResponse();
    }
}
