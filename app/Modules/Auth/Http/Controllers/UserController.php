<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Auth\Models\User;
use App\Modules\Auth\Requests\ChangeUserPasswordRequest;
use App\Modules\Auth\Requests\UpdateUserRequest;
use App\Modules\Auth\Resources\UserResource;
use App\Modules\Common\Responses\SuccessResponse;
use App\Modules\Storage\Contracts\StorageServiceContract;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

/**
 * Контроллер пользователя
 */
class UserController extends ApiController
{
    /**
     * Получение информации о текущем пользователе
     *
     * @param Request $request
     * @return UserResource
     * @throws AuthenticationException
     */
    public function show(Request $request): UserResource
    {
        /** @var User|null $user */
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException();
        }

        return new UserResource($user);
    }

    /**
     * Обновить данные профиля пользователя
     *
     * @param UpdateUserRequest $request
     * @param StorageServiceContract $storageService
     * @return UserResource
     * @throws AuthenticationException
     */
    public function update(UpdateUserRequest $request, StorageServiceContract $storageService): UserResource
    {
        $validatedData = $request->validated();
        $oldPhotoFile = null;

        /** @var User|null $user */
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException();
        }

        /** @var UploadedFile|null $photo */
        $photo = Arr::pull($validatedData, 'photo');
        if ($photo) {
            $file = $storageService->upload($photo);
            $validatedData['photo_file_id'] = $file->id;
            if ($user->photoFile) {
                $oldPhotoFile = clone $user->photoFile;
            }
        }

        if ($user->update($validatedData)) {
            // удаление старого файла с фотографией пользователя
            if ($oldPhotoFile) {
                try {
                    $storageService->remove($oldPhotoFile);
                } catch (Exception $exception) {
                    Log::error('Failed to delete user profile photo file: ' . $exception->getMessage());
                }
            }
        }

        return new UserResource($user);
    }

    /**
     * Изменить пароль пользователя
     *
     * @param ChangeUserPasswordRequest $request
     * @param Hasher $hasher
     * @return SuccessResponse
     * @throws AuthenticationException
     * @throws ValidationException
     */
    public function password(ChangeUserPasswordRequest $request, Hasher $hasher): SuccessResponse
    {
        $validatedData = $request->validated();

        /** @var User|null $user */
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException();
        }

        if (!$hasher->check($validatedData['old_password'], $user->password)) {
            throw ValidationException::withMessages(['old_password' => 'Вы ввели неверный текущий пароль.']);
        }

        $user->update(['password' => $hasher->make($validatedData['new_password'])]);

        return new SuccessResponse();
    }
}
