<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Auth\Exceptions\FailedPasswordRecoveryException;
use App\Modules\Auth\Requests\RequestPasswordRecoveryRequest;
use App\Modules\Auth\Models\User;
use App\Modules\Auth\Requests\ResetPasswordRecoveryRequest;
use App\Modules\Auth\Resources\AuthResource;
use App\Modules\Common\Responses\SuccessResponse;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Contracts\Auth\PasswordBroker as PasswordBrokerContact;
use Illuminate\Contracts\Auth\PasswordBrokerFactory;
use Illuminate\Contracts\Hashing\Hasher;

/**
 * Восстановление пароля пользователя
 */
class PasswordRecoveryController extends ApiController
{
    /** @var PasswordBrokerContact */
    protected PasswordBrokerContact $passwordBroker;

    /** @var Hasher */
    protected Hasher $hasher;

    /**
     * Определение брокера и хэшера паролей
     *
     * @param PasswordBrokerFactory $passwordBrokerManager
     * @param Hasher $hasher
     */
    public function __construct(PasswordBrokerFactory $passwordBrokerManager, Hasher $hasher)
    {
        $this->passwordBroker = $passwordBrokerManager->broker('users');
        $this->hasher = $hasher;
    }

    /**
     * Отправить письмо со ссылкой для восстановления пароля
     *
     * @param RequestPasswordRecoveryRequest $request
     * @return SuccessResponse
     * @throws FailedPasswordRecoveryException
     */
    public function request(RequestPasswordRecoveryRequest $request): SuccessResponse
    {
        $credentials = $request->validated();

        $status = $this->passwordBroker->sendResetLink($credentials);
        if ($status === PasswordBrokerContact::RESET_LINK_SENT) {
            return new SuccessResponse();
        }

        throw new FailedPasswordRecoveryException(['email' => [__($status)]]);
    }

    /**
     * Сменить пароль пользователя
     *
     * @param string $token
     * @param ResetPasswordRecoveryRequest $request
     * @return AuthResource
     * @throws AuthenticationException
     * @throws FailedPasswordRecoveryException
     */
    public function reset(string $token, ResetPasswordRecoveryRequest $request): AuthResource
    {
        $credentials = $request->validated();
        $credentials['token'] = $token;

        /** @var PasswordBroker $passwordBroker */
        $passwordBroker = $this->passwordBroker;
        /** @var User|null $user */
        $user = $passwordBroker->getUser($credentials);
        if (!$user) {
            throw new AuthenticationException();
        }

        $status = $passwordBroker->reset($credentials, function (User $user, string $password) {
            $user->update(['password' => $this->hasher->make($password)]);
        });
        if ($status === PasswordBrokerContact::PASSWORD_RESET) {
            // удалить все выданные ранее аутентификационные токены
            $user->tokens()->delete();
            // получить новый токен
            $frontendApiToken = $user->createToken(User::TOKEN_NAME);
            // событие успешного входа в систему
            event(new Login('api', $user, true));

            return new AuthResource($frontendApiToken->plainTextToken, $user);
        }

        throw new FailedPasswordRecoveryException(['email' => [__($status)]]);
    }
}
