<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Auth\Http\Middleware\ResolveUserByRouteParameter;
use App\Modules\Auth\Resources\UserResource;
use App\Modules\Auth\Models\User;
use App\Modules\Common\Responses\SuccessResponse;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Проверка e-mail пользователя
 */
class EmailVerificationController extends ApiController
{
    /** @inheritdoc  */
    protected $middleware = [
        [
            // резолв пользователя по id из роута для подтверждения e-mail
            'middleware' => ResolveUserByRouteParameter::class,
            'options' => ['only' => 'verify']
        ]
    ];

    /**
     * Повторная отправка письма со ссылкой для подтверждения e-mail
     *
     * @param Request $request
     * @return SuccessResponse
     * @throws AuthenticationException
     * @throws AuthorizationException
     */
    public function resend(Request $request): SuccessResponse
    {
        /** @var MustVerifyEmail|null $user */
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException();
        }

        // если пользователь уже активирован
        if ($user->hasVerifiedEmail()) {
            throw new AuthorizationException('Пользователь уже активирован.');
        }

        $user->sendEmailVerificationNotification();

        return new SuccessResponse();
    }

    /**
     * Подтверждение e-mail
     *
     * @param EmailVerificationRequest $request
     * @return JsonResponse
     * @throws AuthenticationException
     * @throws Exception
     */
    public function verify(EmailVerificationRequest $request): JsonResponse
    {
        /** @var User|null $user пользователь срезолвленный по параметру запроса */
        $user = $request->user();
        if (!$user) {
            throw new AuthenticationException();
        }

        // если пользователь уже активирован
        if ($user->hasVerifiedEmail()) {
            throw new AuthorizationException('Пользователь уже был активирован ранее.');
        }

        // создание мерчанта и связь с ним
        $user->createMerchant();
        // активация пользователя
        $request->fulfill();
        // обновление мерчантов пользователя
        $user->refresh();
        // получение токена для фронтэнд API
        $token = $user->createToken(User::TOKEN_NAME);
        // событие успешного входа в систему
        event(new Login('api', $user, true));

        return new JsonResponse([
            'token' => $token->plainTextToken,
            'user' => new UserResource($user),
        ]);
    }
}
