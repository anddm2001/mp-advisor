<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Auth\Requests\LoginRequest;
use App\Modules\Auth\Resources\AuthResource;
use App\Modules\Auth\Models\User;
use App\Modules\Common\Responses\SuccessResponse;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/**
 * Контроллер аутентификации пользователя
 */
class AuthController extends ApiController
{
    /**
     * Аутентификация пользователя
     *
     * @param LoginRequest $loginRequest
     * @return AuthResource
     * @throws ValidationException
     */
    public function login(LoginRequest $loginRequest): AuthResource
    {
        $loginRequestData = $loginRequest->validated();
        /** @var User|null $user */
        $user = with(new User())->newModelQuery()->where('email', $loginRequestData['username'])->first();

        if (!$user || !Hash::check($loginRequestData['password'], $user->password)) {
            throw ValidationException::withMessages(['password' => trans('auth.failed')]);
        }

        $frontendApiToken = $user->createToken(User::TOKEN_NAME);
        // событие успешного входа в систему
        event(new Login('api', $user, true));

        return new AuthResource($frontendApiToken->plainTextToken, $user);
    }

    /**
     * Выход из системы
     *
     * @param Request $request
     * @return SuccessResponse
     * @throws AuthenticationException
     */
    public function logout(Request $request): SuccessResponse
    {
        if (!$request->user()) {
            throw new AuthenticationException();
        }

        $request->user()->currentAccessToken()->delete();

        return new SuccessResponse();
    }
}
