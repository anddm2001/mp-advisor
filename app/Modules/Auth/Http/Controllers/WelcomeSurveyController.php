<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Auth\Models\User;
use App\Modules\Auth\Models\WelcomeSurvey;
use App\Modules\Auth\Notifications\ManagerWelcomeSurveyNotification;
use App\Modules\Auth\Requests\WelcomeSurveyUpdateRequest;
use App\Modules\Auth\Resources\WelcomeSurveyResource;
use App\Modules\Common\Responses\SuccessResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

/**
 * Контроллер приветственного опросника
 */
class WelcomeSurveyController extends ApiController
{
    /**
     * Получить уже имеющиеся данные по приветственному опроснику
     *
     * @param Request $request
     * @return WelcomeSurveyResource
     */
    public function show(Request $request): WelcomeSurveyResource
    {
        /** @var User $user */
        $user = $request->user();

        /** @var WelcomeSurvey $welcomeSurvey */
        $welcomeSurvey = $user->welcomeSurvey()->firstOrCreate();

        return new WelcomeSurveyResource($welcomeSurvey);
    }

    /**
     * Обновить данные приветственного опросника
     *
     * @param WelcomeSurveyUpdateRequest $request
     * @return SuccessResponse
     */
    public function update(WelcomeSurveyUpdateRequest $request): SuccessResponse
    {
        /** @var User $user */
        $user = $request->user();

        /** @var WelcomeSurvey $welcomeSurvey */
        $welcomeSurvey = $user->welcomeSurvey()->updateOrCreate([], $request->validated());

        // отправка уведомления о прохождении опроса менеджеру (через очередь)
        Notification::route('mail', 'info@mpadvisor.ru')
            ->notify(new ManagerWelcomeSurveyNotification($user, $welcomeSurvey));

        return new SuccessResponse();
    }
}
