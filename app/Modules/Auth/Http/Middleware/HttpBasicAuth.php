<?php


namespace App\Modules\Auth\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Авторизация через HTTP Basic Auth
 *
 * @package App\Modules\Auth\Http\Middleware
 */
class HttpBasicAuth
{
    public function handle(Request $request, \Closure $next)
    {
        return Auth::onceBasic() ?: $next($request);
    }
}
