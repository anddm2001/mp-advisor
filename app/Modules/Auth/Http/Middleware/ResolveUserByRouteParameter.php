<?php

namespace App\Modules\Auth\Http\Middleware;

use App\Modules\Auth\Models\User;
use Closure;
use Illuminate\Http\Request;

/**
 * Резолв пользователя из параметра URL
 */
class ResolveUserByRouteParameter
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $userId = $request->route('id');
        $request->setUserResolver(function() use ($userId) {
            return with(new User())->newModelQuery()->find($userId);
        });

        return $next($request);
    }
}
