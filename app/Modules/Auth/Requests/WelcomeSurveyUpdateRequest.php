<?php

namespace App\Modules\Auth\Requests;

use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на обновление данных в приветственном опроснике
 */
class WelcomeSurveyUpdateRequest extends FormRequest
{
    /**
     * Правила валидации запроса на обновление данных в приветственном опроснике
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nomenclature_category' => ['nullable', 'string', 'max:255'],
            'wildberries_income_freq' => ['nullable', 'string', 'max:255'],
            'marketplaces' => ['nullable', 'array'],
            'marketplaces.*' => ['string', 'max:255'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributes(): array
    {
        return [
            'nomenclature_category' => 'Категория товаров',
            'wildberries_income_freq' => 'Частота поставок',
            'marketplaces' => 'Маркетплейсы',
        ];
    }
}
