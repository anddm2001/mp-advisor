<?php

namespace App\Modules\Auth\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\CommonDataHelper;
use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на регистрацию
 */
class RegisterRequest extends FormRequest
{
    /**
     * Правила валидации запроса на регистрацию
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:50'],
            'email' => ['required', 'string', 'max:255', 'email', 'unique:' . User::class . ',email'],
            'password' => ['required', 'string', 'confirmed'],
            'terms_acceptance' => ['accepted'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation()
    {
        $email = $this->input('email', '');
        $this->merge([
            'email' => CommonDataHelper::prepareEmail($email ?? ''),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function attributes(): array
    {
        return [
            'name' => 'Имя',
            'phone' => 'Номер телефона',
            'email' => 'E-mail',
            'password' => 'Пароль',
            'terms_acceptance' => 'условия',
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages(): array
    {
        return [
            'email.unique' => 'Пользователь с таким e-mail уже зарегистрирован.'
        ];
    }
}
