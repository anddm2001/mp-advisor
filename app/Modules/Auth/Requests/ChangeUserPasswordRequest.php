<?php

namespace App\Modules\Auth\Requests;

use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на изменение пароля пользователя
 */
class ChangeUserPasswordRequest extends FormRequest
{
    /**
     * Правила валидации запроса на изменение пароля пользователя
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'old_password' => ['required', 'string'],
            'new_password' => ['required', 'string', 'confirmed'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributes(): array
    {
        return [
            'old_password' => 'Старый пароль',
            'new_password' => 'Новый пароль',
        ];
    }
}
