<?php

namespace App\Modules\Auth\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\CommonDataHelper;
use App\Modules\Common\Requests\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Запрос на обновление данных профиля пользователя
 */
class UpdateUserRequest extends FormRequest
{
    /**
     * Правила валидации запроса на обновление данных профиля пользователя
     *
     * @return array
     */
    public function rules(): array
    {
        /** @var User $user */
        $user = $this->user();

        return [
            'photo' => ['nullable', 'image', 'max:5120'],
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:50'],
            'email' => [
                'required',
                'string',
                'max:255',
                'email',
                Rule::unique(User::class, 'email')->ignoreModel($user),
            ],
            'company_name' => ['nullable', 'string', 'max:50'],
            'company_post' => ['nullable', 'string', 'max:50'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation()
    {
        $email = $this->input('email', '');
        $this->merge([
            'email' => CommonDataHelper::prepareEmail($email ?? ''),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function attributes(): array
    {
        return [
            'photo' => 'Фотография',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'company_name' => 'Название компании',
            'company_post' => 'Должность',
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages(): array
    {
        return [
            'email.unique' => 'Невозможно изменить e-mail, потому что такой адрес уже используется другим пользователем.'
        ];
    }
}
