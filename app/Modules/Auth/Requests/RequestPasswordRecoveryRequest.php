<?php

namespace App\Modules\Auth\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\CommonDataHelper;
use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на отправку письма со ссылкой для сброса пароля пользователя
 */
class RequestPasswordRecoveryRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'max:255', 'email', 'exists:' . User::class . ',email'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation(): void
    {
        $email = $this->input('email', '');
        $this->merge([
            'email' => CommonDataHelper::prepareEmail($email ?? ''),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function attributes(): array
    {
        return [
            'email' => 'E-mail',
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages(): array
    {
        return [
            'email.exists' => 'Пользователь с таким E-mail не найден.'
        ];
    }
}
