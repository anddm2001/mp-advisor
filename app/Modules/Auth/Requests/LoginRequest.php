<?php

namespace App\Modules\Auth\Requests;

use App\Modules\Common\Helpers\CommonDataHelper;
use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на аутентификацию
 */
class LoginRequest extends FormRequest
{
    /**
     * Правила валидации запроса на аутентификацию
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'username' => ['required', 'string', 'max:255', 'email'],
            'password' => ['required', 'string'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation()
    {
        $username = $this->input('username', '');
        $this->merge([
            'username' => CommonDataHelper::prepareEmail($username ?? ''),
        ]);
    }
}
