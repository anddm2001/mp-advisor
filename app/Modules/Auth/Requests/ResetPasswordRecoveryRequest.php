<?php

namespace App\Modules\Auth\Requests;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\CommonDataHelper;
use App\Modules\Common\Requests\FormRequest;

/**
 * Запрос на смену пароля пользователя
 */
class ResetPasswordRecoveryRequest extends FormRequest
{
    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'max:255', 'email', 'exists:' . User::class . ',email'],
            'password' => ['required', 'string', 'confirmed'],
        ];
    }

    /** @inheritDoc */
    protected function prepareForValidation(): void
    {
        $email = $this->input('email', '');
        $this->merge([
            'email' => CommonDataHelper::prepareEmail($email ?? ''),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function attributes(): array
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages(): array
    {
        return [
            'email.exists' => 'Пользователь с таким E-mail не найден.'
        ];
    }
}
