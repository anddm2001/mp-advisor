<?php

namespace App\Modules\Common\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Modules\Common\Models\LinkHandbook;
use App\Modules\Common\Resources\LinksHandbook;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Str;

/**
 * Самые общие справочники
 */
class HandbookController extends ApiController
{
    /**
     * Справочник ссылок
     *
     * @return AnonymousResourceCollection
     */
    public function links(): AnonymousResourceCollection
    {
        return LinksHandbook::collection(LinkHandbook::all()->keyBy(function (LinkHandbook $link): string {
            return Str::snake(Str::camel($link->code));
        }));
    }
}
