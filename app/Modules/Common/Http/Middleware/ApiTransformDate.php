<?php

namespace App\Modules\Common\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\TransformsRequest;

/**
 * Трансформация даты из API unix epoch в d.m.Y
 *
 * На вход необходимо передать параметры, которые нужно трансформировать.
 *
 * @package App\Modules\Common\Http\Middleware
 */
class ApiTransformDate extends TransformsRequest
{
    /**
     * @var string[]
     */
    protected array $fieldsToTransform = [];

    /**
     * @inheritDoc
     */
    public function handle($request, Closure $next, string ...$fields)
    {
        $this->fieldsToTransform = $fields;
        return parent::handle($request, $next);
    }

    /**
     * @inheritDoc
     */
    public function transform($key, $value)
    {
        if (in_array($key, $this->fieldsToTransform) && !is_null($value) && is_numeric($value)) {
            $value = date('d.m.Y', $value / 1000);
        }

        return $value;
    }
}
