<?php

namespace App\Modules\Common\Resources;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Данные паджинации
 */
class LengthAwarePaginationResource extends JsonResource
{
    /**
     * @param LengthAwarePaginator $resource
     */
    public function __construct(LengthAwarePaginator $resource)
    {
        parent::__construct($resource);
    }

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var LengthAwarePaginator $paginator */
        $paginator = $this->resource;

        return [
            'current_page' => $paginator->currentPage(),
            'last_page' => $paginator->lastPage(),
            'per_page' => $paginator->perPage(),
            'total' => $paginator->total(),
        ];
    }
}
