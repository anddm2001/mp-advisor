<?php

namespace App\Modules\Common\Resources;

use App\Modules\Common\Models\LinkHandbook;
use Illuminate\Http\Resources\Json\JsonResource;

class LinksHandbook extends JsonResource
{
    /** @var bool Сохранить ключи при формировании коллекции */
    public bool $preserveKeys = true;

    /** @inheritDoc */
    public function toArray($request): array
    {
        /** @var LinkHandbook $link */
        $link = $this->resource;

        return [
            'code' => $link->code,
            'name' => $link->name,
            'link' => $link->link,
        ];
    }
}
