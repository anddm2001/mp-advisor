<?php

namespace App\Modules\Common\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * Трейт для исполнения составных первичных ключей в Eloquent, т.к. по умолчанию их в Eloquent нет
 *
 * Взято отсюда:
 *
 * @link https://github.com/laravel/framework/issues/5355#issuecomment-161376267
 *
 * @package App\Modules\Common\Models
 */
trait HasCompositePrimaryKeyTrait
{
    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        /** @var string|array $keyName */
        $keyName = $this->getKeyName();
        if (is_array($keyName)) {
            foreach ($keyName as $key) {
                // UPDATE: Added isset() per devflow's comment.
                if (isset($this->$key))
                    $query->where($key, '=', $this->$key);
                else
                    throw new \Exception(__METHOD__ . 'Missing part of the primary key: ' . $key);
            }
        }

        return $query;
    }

    /**
     * Execute a query for a single record by ID.
     *
     * @param  array  $ids Array of keys, like [column => value].
     * @param  array  $columns
     * @return mixed|static
     */
    public static function find($ids, $columns = ['*'])
    {
        $me = new self;
        $query = $me->newQuery();
        /** @var string|array $keyName */
        $keyName = $me->getKeyName();
        if (is_array($keyName)) {
            foreach ($keyName as $key) {
                $query->where($key, '=', $ids[$key]);
            }
        }
        return $query->first($columns);
    }
}
