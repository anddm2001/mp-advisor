<?php

namespace App\Modules\Common\Models;

/**
 * Upsert модели без использования Eloquent
 *
 * @package App\Modules\Common\Models
 */
trait HasUpsert
{
    /**
     * Upsert модели без использования Eloquent
     *
     * @return bool
     */
    public function upsert(): bool
    {
        $connection = $this->getConnection();
        $table = $this->getTable();
        $attributes = $this->getAttributes();

        /** @var string|array $primaryKey */
        $primaryKey = $this->primaryKey;

        $where = [];

        if (is_array($primaryKey)) {
            foreach ($primaryKey as $key) {
                $where[$key] = $attributes[$key];
            }
        } else {
            $where[$primaryKey] = $attributes[$primaryKey];
        }

        $updated = $connection->table($table)->where($where)->update($attributes);

        if ($updated > 0) {
            $this->exists = true;
            return true;
        }

        if ($connection->table($table)->insert($attributes)) {
            $this->exists = true;
            return true;
        }

        return false;
    }
}
