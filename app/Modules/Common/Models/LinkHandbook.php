<?php

namespace App\Modules\Common\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Справочник ссылок для фронтэнда
 *
 * @property int $id
 * @property string $code Код
 * @property string $name Название
 * @property string $link Ссылка
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class LinkHandbook extends Model
{
    /** @inheritdoc */
    protected $table = 'link_handbook';

    /** @inheritdoc */
    protected $fillable = [
        'code',
        'name',
        'link',
    ];
}
