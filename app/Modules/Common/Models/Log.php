<?php

namespace App\Modules\Common\Models;

use App\Modules\Billing\Services\BillingLogger;
use App\Modules\Common\Services\Logger;
use App\Modules\Merchant\Services\MerchantConnectionLogger;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Запись в логе
 *
 * @property int $id
 * @property Carbon $date
 * @property string $category
 * @property int $level
 * @property string|null $message
 */
class Log extends Model
{
    /** @var string Метка начала зашифрованных данных */
    public const ENCRYPTED_DATA_BEGIN_TAG = '#ENC-BEG#';
    /** @var string Метка конца зашифрованных данных */
    public const ENCRYPTED_DATA_END_TAG = '#ENC-END#';

    /** @inheritdoc */
    protected $table = 'log';

    /** @inheritdoc */
    public $timestamps = false;

    /** @inheritdoc */
    protected $fillable = [
        'date',
        'category',
        'level',
        'message',
    ];

    /** @inheritdoc */
    protected $casts = [
        'date' => 'datetime',
    ];

    /**
     * Категории, для которых сообщения могут быть зашифрованы
     *
     * @var string[]
     */
    protected static array $encryptableCategories = [
        MerchantConnectionLogger::PREFIX_FOR_CATEGORY . MerchantConnectionLogger::CATEGORY_VALIDATION_FAILED,
    ];

    // Accessors & Mutators ========================================================================================= //

    /**
     * Получить сообщение лога (с расшифровыванием по необходимости)
     *
     * @param string $message
     * @return string
     */
    public function getMessageAttribute(string $message): string
    {
        if (!in_array($this->category, self::$encryptableCategories) ||
            strpos($message, self::ENCRYPTED_DATA_BEGIN_TAG) === false ||
            strpos($message, self::ENCRYPTED_DATA_END_TAG) === false
        ) {
            return $message;
        }

        /** @var Encrypter $encrypter */
        $encrypter = resolve(Encrypter::class);
        [$messagePrefix, $message] = explode(self::ENCRYPTED_DATA_BEGIN_TAG, $message, 2);
        [$message, $messagePostfix] = explode(self::ENCRYPTED_DATA_END_TAG, $message, 2);
        $messageData = $encrypter->decrypt($message);
        $message = '';
        foreach ($messageData as $key => $value) {
            $message .= $key . ' — ' . $value . '; ';
        }
        return $messagePrefix . rtrim($message, '; ') . $messagePostfix;
    }

    // Stuff ======================================================================================================== //

    /**
     * Код уровня записи в логе
     *
     * @return string|null
     */
    public function getLevelName(): ?string
    {
        $list = array_flip(Logger::getLevelMap());
        return $list[$this->level] ?? null;
    }

    /**
     * Читаемое название категории
     *
     * @return string
     */
    public function getCategoryName(): string
    {
        if (strpos($this->category, 'billing.') === 0) {
            return BillingLogger::getCategoryName($this->category);
        }

        if (strpos($this->category, 'merchant-connection.') === 0) {
            return MerchantConnectionLogger::getCategoryName($this->category);
        }

        return '?';
    }
}
