<?php

namespace App\Modules\Common\Models;

use App\Modules\Auth\Models\User;
use App\Modules\Merchant\Models\Merchant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Запись в логе отправленных писем
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $merchant_id
 * @property string $email
 * @property string $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read string $type_name
 * @property-read Merchant $merchant
 * @property-read User $user
 */
class MailLog extends Model
{
    /** @var string Тип письма — Активация */
    public const TYPE_ACTIVATION = 'activation';
    /** @var string Тип письма — Приветствие */
    public const TYPE_WELCOME = 'welcome';
    /** @var string Тип письма — Сброс пароля */
    public const TYPE_PASSWORD_RESET = 'password-reset';
    /** @var string Тип письма — Статус платежа */
    public const TYPE_BILLING_PAYMENT_STATUS = 'billing-payment-status';
    /** @var string Тип письма — Блокировка по балансу */
    public const TYPE_BALANCE_BLOCKED = 'balance-blocked';
    /** @var string Тип письма — Окончание пробного периода */
    public const TYPE_TRIAL_EXPIRED = 'trial-expired';

    /** @inheritdoc */
    protected $table = 'mail_log';

    /** @inheritdoc */
    protected $fillable = [
        'user_id',
        'merchant_id',
        'email',
        'type',
    ];

    // Relationships ================================================================================================ //

    /**
     * Пользователь
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id', 'user');
    }

    /**
     * Мерчант
     *
     * @return BelongsTo
     */
    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id', 'merchant');
    }

    // Accessors & Mutators ========================================================================================= //

    /**
     * Получить название типа письма
     *
     * @return string
     */
    public function getTypeNameAttribute(): string
    {
        $typeNames = self::getTypeNames();
        return $typeNames[$this->type] ?? $this->type;
    }

    // Stuff ======================================================================================================== //

    /**
     * Список названий типов писем
     *
     * @return array
     */
    public static function getTypeNames(): array
    {
        return [
            self::TYPE_ACTIVATION => 'Активация',
            self::TYPE_WELCOME => 'Приветствие',
            self::TYPE_PASSWORD_RESET => 'Сброс пароля',
            self::TYPE_BILLING_PAYMENT_STATUS => 'Статус платежа',
            self::TYPE_BALANCE_BLOCKED => 'Блокировка по балансу',
            self::TYPE_TRIAL_EXPIRED => 'Окончание пробного периода',
        ];
    }
}
