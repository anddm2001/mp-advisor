<?php

namespace App\Modules\Common\Models;

use App\Modules\Auth\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Лог событий
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $type Тип события
 * @property Carbon $triggered_at Момент события
 * @property-read string $type_name
 * @property-read User|null $user
 */
class EventLog extends Model
{
    /** @var string Тип события — Вход в систему */
    public const TYPE_LOGIN = 'login';
    /** @var string Тип события — Регистрация */
    public const TYPE_REGISTRATION = 'registration';
    /** @var string Тип события — Активация */
    public const TYPE_ACTIVATION = 'activation';
    /** @var string Тип события — Указание параметров доступа к WB */
    public const TYPE_CREDENTIALS_WB = 'credentials-wb';
    /** @var string Тип события — Окончание пробного периода */
    public const TYPE_TRIAL_PERIOD_FINISH = 'trial-period-finish';
    /** @var string Тип события — Первый платёж */
    public const TYPE_FIRST_PAYMENT = 'first-payment';
    /** @var string Тип события — Второй платёж */
    public const TYPE_SECOND_PAYMENT = 'second-payment';
    /** @var string Тип события — Третий платёж */
    public const TYPE_THIRD_PAYMENT = 'third-payment';

    /** @inheritdoc */
    protected $table = 'event_log';

    /** @inheritdoc */
    public $timestamps = false;

    /** @inheritdoc */
    protected $fillable = [
        'user_id',
        'type',
        'triggered_at',
    ];

    protected $casts = [
        'triggered_at' => 'datetime',
    ];

    // Relationships ================================================================================================ //

    /**
     * Пользователь
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id', 'user');
    }

    // Accessors & Mutators ========================================================================================= //

    /**
     * Получить название типа события
     *
     * @return string
     */
    public function getTypeNameAttribute(): string
    {
        $typeNames = self::getTypeNames();
        return $typeNames[$this->type] ?? $this->type;
    }

    // Stuff ======================================================================================================== //

    /**
     * Список названий типов событий
     *
     * @return array
     */
    public static function getTypeNames(): array
    {
        return [
            self::TYPE_LOGIN => 'Успешные авторизации',
            self::TYPE_REGISTRATION => 'Новые пользователи',
            self::TYPE_ACTIVATION => 'Активации',
            self::TYPE_CREDENTIALS_WB => 'Добавления ключей',
            self::TYPE_TRIAL_PERIOD_FINISH => 'Завершения пробных периодов',
            self::TYPE_FIRST_PAYMENT => 'Первые оплаты',
            self::TYPE_SECOND_PAYMENT => 'Вторые оплаты',
            self::TYPE_THIRD_PAYMENT => 'Третьи оплаты',
        ];
    }
}
