<?php

namespace App\Modules\Common\Requests;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Http\FormRequest as BaseFormRequest;

/**
 * Обработка запроса
 */
abstract class FormRequest extends BaseFormRequest implements FormRequestInterface
{
    /** @inheritDoc */
    public function __construct(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    )
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    /**
     * Создать запрос из массива
     *
     * @param array $params
     * @param Authenticatable|null $forceUser
     * @return static
     */
    public static function createFromArray(array $params, ?Authenticatable $forceUser = null): self
    {
        $request = new static($params);
        $request->setContainer(app());
        if (!is_null($forceUser)) {
            // Использовать для запроса определенную модель пользователя
            $request->setUserResolver(fn() => $forceUser);
        }
        return $request;
    }
}
