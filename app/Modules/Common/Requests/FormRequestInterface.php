<?php

namespace App\Modules\Common\Requests;

/**
 * Интерфейс FormRequest для фиксирования конструктора
 */
interface FormRequestInterface
{
    /**
     * FormRequest constructor
     *
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null $content
     */
    public function __construct(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    );
}
