<?php

namespace App\Modules\Common\Services;

use App\Modules\Common\Models\Log;
use Illuminate\Support\Carbon;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;
use Psr\Log\LogLevel;

/**
 * Абстрактный логгер
 */
abstract class Logger implements LoggerInterface
{
    use LoggerTrait;

    /**
     * Код категории лога
     *
     * @return string
     */
    abstract public function getCategory(): string;

    /**
     * Название категории лога
     *
     * @param string $category
     * @return string
     */
    abstract public static function getCategoryName(string $category = ''): string;

    /** @inheritDoc */
    public function log($level, $message, array $context = [])
    {
        $levels = self::getLevelMap();

        with(new Log())->newModelQuery()->create([
            'date' => Carbon::now(),
            'category' => $this->getCategory(),
            'level' => $levels[$level] ?? 0,
            'message' => $message,
        ]);
    }

    /**
     * Карта соответсвия уровней по PSR
     *
     * @return int[]
     */
    final public static function getLevelMap(): array
    {
        return [
            LogLevel::EMERGENCY => 1,
            LogLevel::ALERT => 2,
            LogLevel::CRITICAL => 3,
            LogLevel::ERROR => 4,
            LogLevel::WARNING => 5,
            LogLevel::NOTICE => 6,
            LogLevel::INFO => 7,
            LogLevel::DEBUG => 8,
        ];
    }
}
