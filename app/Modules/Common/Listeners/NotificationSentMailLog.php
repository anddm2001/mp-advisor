<?php

namespace App\Modules\Common\Listeners;

use App\Modules\Auth\Models\User;
use App\Modules\Auth\Notifications\ResetPasswordNotification;
use App\Modules\Auth\Notifications\UserWelcomeNotification;
use App\Modules\Billing\Notifications\BillingBalanceBlockedNotification;
use App\Modules\Billing\Notifications\BillingPaymentStatusNotification;
use App\Modules\Billing\Notifications\BillingTrialPeriodFinishNotification;
use App\Modules\Common\Models\MailLog;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Events\NotificationSent;

/**
 * Сохранение в лог отправленных писем-уведомлений
 */
class NotificationSentMailLog
{
    /**
     * Handle the event.
     *
     * @param NotificationSent $event
     * @return void
     */
    public function handle(NotificationSent $event): void
    {
        /** @var User $user */
        $user = $event->notifiable;
        if (!($user instanceof User)) {
            return;
        }

        $notification = $event->notification;

        $type = null;
        $merchantId = null;
        switch (get_class($notification)) {
            // активация после регистрации
            case VerifyEmail::class:
                $type = MailLog::TYPE_ACTIVATION;
                break;
            // приветственное письмо после активации
            case UserWelcomeNotification::class:
                $type = MailLog::TYPE_WELCOME;
                break;
            // письмо со ссылкой для сброса пароля
            case ResetPasswordNotification::class:
                $type = MailLog::TYPE_PASSWORD_RESET;
                break;
            // статус платежа
            case BillingPaymentStatusNotification::class:
                /** @var BillingPaymentStatusNotification $notification */
                $billingOperation = $notification->getBillingOperation();
                $merchantId = $billingOperation->merchant_id;
                $type = MailLog::TYPE_BILLING_PAYMENT_STATUS;
                break;
            // окончание пробного периода
            case BillingTrialPeriodFinishNotification::class:
                /** @var BillingTrialPeriodFinishNotification $notification */
                $merchantId = $notification->getMerchant()->id;
                $type = Maillog::TYPE_TRIAL_EXPIRED;
                break;
            // блокировка по балансу
            case BillingBalanceBlockedNotification::class:
                /** @var BillingBalanceBlockedNotification $notification */
                $merchantId = $notification->getMerchant()->id;
                $type = Maillog::TYPE_BALANCE_BLOCKED;
                break;
        }

        if (!$type) {
            return;
        }

        $user->mailLogs()->create([
            'merchant_id' => $merchantId,
            'email' => $user->email,
            'type' => $type,
        ]);
    }
}
