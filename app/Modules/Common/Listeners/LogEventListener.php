<?php

namespace App\Modules\Common\Listeners;

use App\Modules\Auth\Models\User;
use App\Modules\Billing\Events\BillingPaymentToEventLogEvent;
use App\Modules\Billing\Events\BillingTrialPeriodFinishSetEvent;
use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Common\Models\EventLog;
use App\Modules\Merchant\Events\MerchantConnectionCredentialsCreatedEvent;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Carbon;

/**
 * Сохранение в лог событий
 */
class LogEventListener
{
    /**
     * Handle the event.
     *
     * @param mixed $event
     * @return void
     */
    public function handle($event): void
    {
        $type = null;
        $user = null;
        $triggeredAt = Carbon::now();
        switch (get_class($event)) {
            case Login::class:
                /** @var Login $event */
                $type = EventLog::TYPE_LOGIN;
                $user = $event->user;
                break;
            case Registered::class:
                /** @var Registered $event */
                $type = EventLog::TYPE_REGISTRATION;
                $user = $event->user;
                break;
            case Verified::class:
                /** @var Verified $event */
                $type = EventLog::TYPE_ACTIVATION;
                $user = $event->user;
                break;
            case MerchantConnectionCredentialsCreatedEvent::class:
                /** @var MerchantConnectionCredentialsCreatedEvent $event */
                $type = EventLog::TYPE_CREDENTIALS_WB;
                $user = $event->getUser();
                break;
            case BillingTrialPeriodFinishSetEvent::class:
                /** @var BillingTrialPeriodFinishSetEvent $event */
                $type = EventLog::TYPE_TRIAL_PERIOD_FINISH;
                $user = $event->getMerchant()->owner;
                $triggeredAt = $event->getTrialPeriodFinishDate();
                break;
            case BillingPaymentToEventLogEvent::class:
                /** @var BillingPaymentToEventLogEvent $event */
                $type = self::getTypeByBillingOperation($event->getBillingOperation());
                $user = $event->getBillingOperation()->initiator;
                $triggeredAt = $event->getBillingOperation()->captured_at;
                break;
        }

        if (!$type || !$user || !($user instanceof User)) {
            return;
        }

        /** @var User $user */
        $user->eventLogs()->create([
            'type' => $type,
            'triggered_at' => $triggeredAt,
        ]);
    }

    /**
     * Получить тип события платежа по операции биллинга
     *
     * @param BillingOperation $operation
     * @return string|null
     */
    protected static function getTypeByBillingOperation(BillingOperation $operation): ?string
    {
        $type = null;
        if (!($operation->initiator instanceof User)) {
            return $type;
        }

        $oldestPayments = BillingOperation::query()
            ->where([
                'initiator_type' => $operation->initiator_type,
                'initiator_id' => $operation->initiator_id,
                'type' => BillingOperation::TYPE_PAYMENT,
                'status' => BillingOperation::STATUS_SUCCESS,
                'payment_system' => $operation->payment_system,
            ])
            ->oldest('captured_at')
            ->take(3)
            ->pluck('id')
            ->values();

        foreach ($oldestPayments as $i => $paymentId) {
            if ($operation->id === $paymentId) {
                switch ($i + 1) {
                    case 1:
                        $type = EventLog::TYPE_FIRST_PAYMENT;
                        break;
                    case 2:
                        $type = EventLog::TYPE_SECOND_PAYMENT;
                        break;
                    case 3:
                        $type = EventLog::TYPE_THIRD_PAYMENT;
                        break;
                }
            }
        }

        return $type;
    }
}
