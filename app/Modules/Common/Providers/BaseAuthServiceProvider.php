<?php

namespace App\Modules\Common\Providers;

use App\Modules\Merchant\Policies\MerchantPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

/**
 * Сервис провайдер для регистрации прав доступа
 *
 * @package App\Modules\Common\Providers
 */
abstract class BaseAuthServiceProvider extends ServiceProvider
{
    /**
     * Права доступа в формате:
     *
     * ```php
     * return [
     *      '<ключ_в_middleware>' => 'PolicyClass@method',
     * ];
     * ```
     *
     * @return array
     */
    abstract protected function policies(): array;

    protected function registerPolicies()
    {
        foreach ($this->policies() as $key => $callback) {
            Gate::define($key, $callback);
        }
    }

    public function boot()
    {
        $this->registerPolicies();
    }
}
