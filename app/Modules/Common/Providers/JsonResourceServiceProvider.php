<?php

namespace App\Modules\Common\Providers;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\ServiceProvider;

/**
 * Настройки ресурсов для JSON
 */
class JsonResourceServiceProvider extends ServiceProvider
{
    /**
     * Отключение обёртки ресурсов для JSON
     */
    public function boot()
    {
        JsonResource::withoutWrapping();
    }
}
