<?php

namespace App\Modules\Common\Errors;

/**
 * Ошибка аутентификации
 */
class AuthenticationError extends Error
{
    private const ERROR_CODE = 401;
    private const ERROR_MESSAGE = 'Ошибка аутентификации';

    /**
     * Определение параметров ошибки аутентификации
     */
    public function __construct()
    {
        parent::__construct(self::ERROR_CODE, self::ERROR_MESSAGE);
    }
}
