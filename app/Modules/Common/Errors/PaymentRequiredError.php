<?php

namespace App\Modules\Common\Errors;

/**
 * Требуется оплата
 */
class PaymentRequiredError extends Error
{
    private const ERROR_CODE = 402;
    private const ERROR_MESSAGE = 'Требуется оплата';

    /**
     * Определение параметров ошибки
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        parent::__construct(self::ERROR_CODE, self::ERROR_MESSAGE, $data);
    }
}
