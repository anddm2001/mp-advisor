<?php

namespace App\Modules\Common\Errors;

/**
 * Базовый класс ошибки
 */
class Error
{
    /**
     * @var integer
     */
    protected int $code;

    /**
     * @var string
     */
    protected string $message;

    /**
     * @var array|null
     */
    protected ?array $errors;

    /**
     * Создание объекта ошибки
     * @param int $code
     * @param string $message
     * @param array|null $errors
     */
    public function __construct(int $code, string $message, ?array $errors = null)
    {
        $this->code = $code;
        $this->message = $message;

        if (!empty($errors)) {
            $this->errors = $errors;
        }
    }

    /**
     * Код ошибки
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * Общий текст ошибки
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Ошибки
     * @return string[]|array|null
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }

    /**
     * Ошибка в виде массива
     *
     * @return array
     */
    public function toArray(): array
    {
        $errorArray = [
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
        ];
        if (!empty($this->errors)) {
            $errorArray['errors'] = $this->getErrors();
        }

        return $errorArray;
    }
}
