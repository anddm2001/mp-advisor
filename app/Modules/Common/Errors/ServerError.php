<?php

namespace App\Modules\Common\Errors;

/**
 * Ошибка сервера
 */
class ServerError extends Error
{
    private const ERROR_CODE = 500;
    private const ERROR_MESSAGE = 'Ошибка сервера';

    /**
     * Определение параметров ошибки сервера
     */
    public function __construct()
    {
        parent::__construct(self::ERROR_CODE, self::ERROR_MESSAGE);
    }
}
