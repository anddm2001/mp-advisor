<?php

namespace App\Modules\Common\Errors;

use Illuminate\Validation\ValidationException;

/**
 * Ошибка валидации
 */
class ValidationError extends Error
{
    private const ERROR_CODE = 400;
    private const ERROR_MESSAGE = 'Ошибка валидации данных';

    /**
     * Определение параметров ошибки валидации
     *
     * @param ValidationException $validationException
     */
    public function __construct(ValidationException $validationException)
    {
        parent::__construct(self::ERROR_CODE, self::ERROR_MESSAGE, $validationException->errors());
    }
}
