<?php

namespace App\Modules\Common\Errors;

/**
 * Ошибка при смене пароля пользователя
 */
class FailedPasswordRecoveryError extends Error
{
    private const ERROR_CODE = 400;
    private const ERROR_MESSAGE = 'Ошибка при сбросе пароля пользователя';

    /**
     * Определение параметров ошибки
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        parent::__construct(self::ERROR_CODE, self::ERROR_MESSAGE, $data);
    }
}
