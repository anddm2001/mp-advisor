<?php

namespace App\Modules\Common\Errors;

/**
 * E-mail пользователя не подтверждён
 */
class UnverifiedEmailError extends Error
{
    private const ERROR_CODE = 403;
    private const ERROR_MESSAGE = 'E-mail пользователя не подтверждён';

    /**
     * Определение параметров ошибки
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        parent::__construct(self::ERROR_CODE, self::ERROR_MESSAGE, $data);
    }
}
