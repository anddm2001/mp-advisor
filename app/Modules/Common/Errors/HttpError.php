<?php

namespace App\Modules\Common\Errors;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Ошибка на основе исключения HttpException
 */
class HttpError extends Error
{
    /**
     * Определение параметров ошибки по исключению
     *
     * @param HttpException $httpException
     */
    public function __construct(HttpException $httpException)
    {
        parent::__construct($httpException->getStatusCode(), $this->getMessageByException($httpException));
    }

    /**
     * Проверка и дополнение сообщения об ошибке
     *
     * @param HttpException $httpException
     * @return string
     */
    private function getMessageByException(HttpException $httpException): string
    {
        // текст ошибки из исключения
        $httpExceptionMessage = (string)$httpException->getMessage();

        // если текст из исключения не пустой
        if ($httpExceptionMessage !== '') {
            $message = trans($httpExceptionMessage);
            return is_string($message) ? $message : $httpExceptionMessage;
        }

        // текст ошибки по коду (для исключений, у которых нет своих классов, наследуемых от HttpException)
        $httpExceptionStatusCode = $httpException->getStatusCode();
        $message = null;
        switch ($httpExceptionStatusCode) {
            case 419:
                $message = trans('Token Mismatch');
                break;
            case 500:
                $message = trans('Server Error');
                break;
        }
        if (!is_null($message) && is_string($message)) {
            return $message;
        }

        // если текст исключения пустой
        $message = 'Неизвестная ошибка';
        $httpExceptionClass = get_class($httpException);
        switch ($httpExceptionClass) {
            case UnauthorizedHttpException::class:
                // 401
                $message = trans('Unauthorized');
                break;
            case AccessDeniedHttpException::class:
                // 403
                $message = trans('Access Denied');
                break;
            case NotFoundHttpException::class:
                // 404
                $message = trans('Not Found');
                break;
            case TooManyRequestsHttpException::class:
                // 429
                $message = trans('Too Many Requests');
                break;
        }

        return is_string($message) ? $message : $httpExceptionMessage;
    }
}
