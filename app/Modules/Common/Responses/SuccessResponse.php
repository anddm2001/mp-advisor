<?php

namespace App\Modules\Common\Responses;

use Illuminate\Http\JsonResponse;

/**
 * Ответ об успешном выполнении действия
 */
class SuccessResponse extends JsonResponse
{
    /**
     * @param bool $success
     */
    public function __construct(bool $success = true)
    {
        parent::__construct([
            'success' => $success,
        ]);
    }
}
