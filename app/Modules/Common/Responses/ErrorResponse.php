<?php

namespace App\Modules\Common\Responses;

use App\Modules\Common\Errors\Error;
use Illuminate\Http\JsonResponse;

/**
 * Ответ с ошибкой
 */
class ErrorResponse extends JsonResponse
{
    /**
     * ErrorResponse constructor.
     * @param Error $error
     * @param array $headers
     */
    public function __construct(Error $error, array $headers = [])
    {
        parent::__construct($error->toArray(), $error->getCode(), $headers, JSON_UNESCAPED_UNICODE);
    }
}
