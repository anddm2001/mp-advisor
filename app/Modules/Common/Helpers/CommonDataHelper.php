<?php

namespace App\Modules\Common\Helpers;

/**
 * Хелперы для общих данных
 */
class CommonDataHelper
{
    /**
     * Подготовка e-mail адреса
     *
     * @param string $email
     * @return string
     */
    public static function prepareEmail(string $email): string
    {
        return strtolower(preg_replace('/\s+/', '', $email));
    }

    /**
     * Подготовка названия чего-нибудь с опцией "all", которую надо преобразовать к пустой строке
     *
     * @param string $name
     * @return string
     */
    public static function prepareNameWithAllOption(string $name): string
    {
        return $name === 'all' ? '' : $name;
    }

    /**
     * Выбор склонения в соответствии с числом
     *
     * @param int $number
     * @param array $declensions
     * @return string
     */
    public static function numDeclension(int $number, array $declensions): string
    {
        if ($number > 20) {
            $number %= 10;
        }

        if ($number === 1) {
            return $declensions[0] ?? 'штука';
        }

        if ($number > 1 && $number < 5) {
            return $declensions[1] ?? 'штуки';
        }

        return $declensions[2] ?? 'штук';
    }
}
