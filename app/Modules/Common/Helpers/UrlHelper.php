<?php

namespace App\Modules\Common\Helpers;

use Illuminate\Routing\UrlGenerator;

/**
 * Хелперы для работы со ссылками
 */
class UrlHelper
{
    /**
     * Преобразование URL, сгенерированного для API (например, через URL::route()), в URL для приложения
     *
     * @param string $url
     * @return string
     */
    public static function ApiRoutedUrlToRootAppUrl(string $url): string
    {
        // URL относительно корня API (без /api/)
        $relativeToApiRootUrl = str_replace(url('/'), '', $url);
        /** @var UrlGenerator $urlGenerator */
        $urlGenerator = url();
        // считаем корнем для генератора URL приложения
        $urlGenerator->forceRootUrl(config('app.url'));
        // собираем "внешнюю" (относительно API) ссылку
        $url = $urlGenerator->to($relativeToApiRootUrl);
        // возвращаем генератору корень
        $urlGenerator->forceRootUrl(null);

        return $url;
    }
}
