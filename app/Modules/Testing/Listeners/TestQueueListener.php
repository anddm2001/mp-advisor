<?php

namespace App\Modules\Testing\Listeners;

use App\Modules\Testing\Console\Commands\TestQueueCommand;
use App\Modules\Testing\Events\TestQueueEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Обработка в очереди тестового сообщения
 *
 * @see TestQueueEvent
 * @see TestQueueCommand
 *
 * @package App\Modules\Testing\Listeners
 */
class TestQueueListener implements ShouldQueue
{
    /**
     * @var string Путь к лог файлу, в котором сохранить сообщение
     */
    protected string $logFile;

    /**
     * TestQueueListener constructor.
     *
     * @param string $logFile Путь к лог файлу, в котором сохранить сообщение
     */
    public function __construct(string $logFile)
    {
        $this->logFile = $logFile;
    }

    /**
     * @param TestQueueEvent $event
     */
    public function handle(TestQueueEvent $event): void
    {
        file_put_contents($this->logFile, $event->getText() . "\n", FILE_APPEND);
    }
}
