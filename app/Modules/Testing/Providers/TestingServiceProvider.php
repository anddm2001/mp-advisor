<?php

namespace App\Modules\Testing\Providers;

use App\Modules\Testing\Console\Commands\TestGenerateHashPassword;
use App\Modules\Testing\Console\Commands\TestMailCommand;
use App\Modules\Testing\Console\Commands\TestQueueCommand;
use App\Modules\Testing\Events\TestQueueEvent;
use App\Modules\Testing\Listeners\TestQueueListener;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;

/**
 * Сервис провайдер для каких-либо экспериментальных или тестовых вещей
 *
 * @package App\Modules\Testing\Providers
 */
class TestingServiceProvider extends ServiceProvider
{
    protected function registerListeners()
    {
        /** @var Dispatcher $events */
        $events = $this->app->make('events');

        // листенер для прослушивания тестовых сообщений в очереди
        $events->listen(TestQueueEvent::class, TestQueueListener::class . '@handle');
    }

    public function register()
    {
        $this->app->bind(TestQueueListener::class, function($app) {
            return new TestQueueListener(storage_path('logs/test-queue.log'));
        });

        $this->commands([
            TestQueueCommand::class,
            TestGenerateHashPassword::class,
            TestMailCommand::class,
        ]);
    }

    public function boot()
    {
        $this->registerListeners();
    }
}
