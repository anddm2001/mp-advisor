<?php

namespace App\Modules\Testing\Console\Commands;

use App\Modules\Testing\Events\TestQueueEvent;
use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;

/**
 * Команда для тестирования системы очередей
 *
 * @package App\Modules\Testing\Console\Commands
 */
class TestQueueCommand extends Command
{
    protected $description = 'Тестирование системы очередей';

    protected $signature = 'test:queue {text : Текст, помещаемый в очередь и записываемый в лог}';

    public function handle(Dispatcher $events)
    {
        $text = $this->argument('text');

        if (empty($text)) {
            $this->error('Передан пустой текст');
            return 1;
        }

        $events->dispatch(new TestQueueEvent($text));

        $this->info('Сообщение отправлено в очередь');
        return 0;
    }
}
