<?php

namespace App\Modules\Testing\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

/**
 * Тестирование отправки почты
 *
 * @package App\Modules\Testing\Console\Commands
 */
class TestMailCommand extends Command
{
    protected $signature = 'test:mail {to : Получатель} {subject : Текст заголовка} {text : Текст письма}';

    protected $description = 'Тестирование отправки e-mail';

    public function handle()
    {
        Mail::raw($this->argument('text'), function(Message $mail) {
            $mail->to($this->argument('to'))
                ->subject($this->argument('subject'));
        });
    }
}
