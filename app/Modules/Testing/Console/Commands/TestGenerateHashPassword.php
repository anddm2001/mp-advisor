<?php

namespace App\Modules\Testing\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Hashing\Hasher;

/**
 * Генерация хеша пароля и вывод на экран
 *
 * @package App\Modules\Testing\Console\Commands
 */
class TestGenerateHashPassword extends Command
{
    protected $signature = 'test:generate-hash-password';

    protected $description = 'Хеширование пароля';

    /**
     * Генерация хеша пароля
     *
     * @param Hasher $hasher
     *
     * @return int
     */
    public function handle(Hasher $hasher)
    {
        $password = $this->ask("Введите пароль для хеширования: ");

        if (strlen($password) < 6) {
            $this->error('Пароль должен быть не менее 6 символов');
            return 1;
        }

        $hashed = $hasher->make($password);

        $this->info("Сгенерированный хеш пароля: $hashed");

        return 0;
    }
}
