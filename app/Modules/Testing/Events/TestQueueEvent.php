<?php

namespace App\Modules\Testing\Events;

/**
 * Тестовое событие в очередь для тестирования системы очередей
 *
 * @package App\Modules\Testing\Events
 */
class TestQueueEvent
{
    /**
     * @var string Текст, помещаемый в очередь
     */
    protected string $text;

    /**
     * TestQueueEvent constructor.
     *
     * @param string $text
     */
    public function __construct(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }
}
