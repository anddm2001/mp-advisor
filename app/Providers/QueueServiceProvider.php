<?php

namespace App\Providers;

use App\Queue\Worker;
use Illuminate\Contracts\Debug\ExceptionHandler;
use App\Queue\Consumer;
use Illuminate\Queue\QueueServiceProvider as BaseServiceProvider;

/**
 * Переопределить некоторые сервисы для работы с очередями
 *
 * @package App\Providers
 */
class QueueServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        parent::register();

        $this->app->singleton('rabbitmq.consumer', function () {
            $isDownForMaintenance = function () {
                return $this->app->isDownForMaintenance();
            };

            return new Consumer(
                $this->app['queue'],
                $this->app['events'],
                $this->app[ExceptionHandler::class],
                $isDownForMaintenance
            );
        });
    }

    protected function registerWorker()
    {
        $this->app->singleton('queue.worker', function ($app) {
            $isDownForMaintenance = function () {
                return $this->app->isDownForMaintenance();
            };

            return new Worker(
                $app['queue'],
                $app['events'],
                $app[ExceptionHandler::class],
                $isDownForMaintenance
            );
        });
    }
}
