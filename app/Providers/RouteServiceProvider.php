<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            // API
            Route::middleware('api')
                ->group(base_path('routes/api.php'));

            // Control Panel
            Route::name('cp.')
                ->middleware('cp')
                ->namespace('App\Modules\ControlPanel\Http\Controllers')
                ->group(base_path('routes/control-panel.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(180)->by(optional($request->user())->id ?: $request->ip());
        });
        RateLimiter::for('cp-login', function (Request $request) {
            return Limit::perMinute(5)->by($request->input('email') ?? $request->ip());
        });
        RateLimiter::for('cp-password', function (Request $request) {
            return Limit::perMinute(3)->by($request->ip());
        });
    }
}
