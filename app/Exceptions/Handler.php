<?php

namespace App\Exceptions;

use App\Modules\Auth\Exceptions\FailedPasswordRecoveryException;
use App\Modules\Auth\Exceptions\UnverifiedEmailException;
use App\Modules\Billing\Exceptions\PaymentRequiredException;
use App\Modules\Common\Errors\AuthenticationError;
use App\Modules\Common\Errors\FailedPasswordRecoveryError;
use App\Modules\Common\Errors\HttpError;
use App\Modules\Common\Errors\PaymentRequiredError;
use App\Modules\Common\Errors\ServerError;
use App\Modules\Common\Errors\UnverifiedEmailError;
use App\Modules\Common\Errors\ValidationError;
use App\Modules\Common\Responses\ErrorResponse;
use Exception;
use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        PaymentRequiredException::class,
        UnverifiedEmailException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
        'base64_key',
        'key',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        // отображение ошибок аутентификации
        $this->renderable(function (AuthenticationException $authenticationException, $request) {
            return new ErrorResponse(new AuthenticationError());
        });

        // отображение ошибки "требуется оплата"
        $this->renderable(function (PaymentRequiredException $paymentRequiredException, $request) {
            return new ErrorResponse(new PaymentRequiredError($paymentRequiredException->getData()));
        });

        // отображение HTTP-ошибок
        $this->renderable(function (HttpException $httpException, $request) {
            return new ErrorResponse(new HttpError($httpException), $httpException->getHeaders());
        });

        // отображение ошибок валидации
        $this->renderable(function (ValidationException $validationException, $request) {
            return new ErrorResponse(new ValidationError($validationException));
        });

        // отображение ошибки неподтверждённого e-mail
        $this->renderable(function (UnverifiedEmailException $unverifiedEmailException, $request) {
            return new ErrorResponse(new UnverifiedEmailError($unverifiedEmailException->getData()));
        });

        // отображение ошибки при сбросе пароля
        $this->renderable(function (FailedPasswordRecoveryException $failedPasswordRecoveryException, $request) {
            return new ErrorResponse(new FailedPasswordRecoveryError($failedPasswordRecoveryException->getData()));
        });

        // отображение остальных ошибок как 500 Server Error
        $this->renderable(function (Exception $exception, $request) {
            return new ErrorResponse(new ServerError());
        });
    }

    /**
     * @inheritDoc
     */
    public function report(Throwable $e)
    {
        if (app()->bound('sentry') && $this->shouldReport($e)) {
            app('sentry')->captureException($e);
        }

        parent::report($e);
    }
}
