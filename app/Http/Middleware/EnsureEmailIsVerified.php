<?php

namespace App\Http\Middleware;

use App\Modules\Auth\Exceptions\UnverifiedEmailException;
use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;

/**
 * Проверка, что пользователь подтвердил свой e-mail
 */
class EnsureEmailIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws UnverifiedEmailException
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        if (!$user || ($user instanceof MustVerifyEmail && !$user->hasVerifiedEmail())) {
            throw new UnverifiedEmailException([
                'resend_verification_email_action' => route('verification.send'),
            ]);
        }

        return $next($request);
    }
}
