<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class JsonToRequestData
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // если пришли данные в JSON
        if ($request->json()->count() > 0) {
            // засовываем их в параметры запроса
            $request->request->replace($request->json()->all());
        }

        return $next($request);
    }
}
