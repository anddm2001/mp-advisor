<?php

namespace Database\Seeders;

use App\Modules\Auth\Models\PersonalAccessToken;
use App\Modules\Auth\Models\User;
use App\Modules\Billing\Models\BillingOperation;
use App\Modules\Common\Models\EventLog;
use App\Modules\Report\Contracts\MarketPlaceContract;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Seeder;

class GatherEventLog extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        /** @var User[] $users */
        $users = User::with([
            'tokens' => function (MorphMany $query) {
                $query->oldest();
            },
            'merchants' => function (HasManyThrough $query) {
                $query->oldest();
            },
            'merchants.marketplaceConnections' => function (HasMany $query) {
                $query->where('marketplace', MarketPlaceContract::WILDBERRIES)
                    ->where('credentials', '!=', '')
                    ->whereNotNull('created_at');
            },
            'merchants.billingOperations' => function (HasMany $query) {
                $query->where([
                    'type' => BillingOperation::TYPE_PAYMENT,
                    'status' => BillingOperation::STATUS_SUCCESS,
                    'payment_system' => BillingOperation::PAYMENT_SYSTEM_YOO_KASSA,
                ])->whereNotNull('captured_at')->oldest('captured_at');
            },
        ])->oldest()->get();

        foreach ($users as $user) {
            // Login
            foreach ($user->tokens as $token) {
                /** @var PersonalAccessToken $token */
                if (!$token->created_at) {
                    continue;
                }

                $user->eventLogs()->updateOrCreate(['type' => EventLog::TYPE_LOGIN], [
                    'triggered_at' => $token->created_at,
                ]);
            }

            // Registration
            $user->eventLogs()->updateOrCreate(['type' => EventLog::TYPE_REGISTRATION], [
                'triggered_at' => $user->created_at,
            ]);

            // Activation
            if ($user->email_verified_at) {
                $user->eventLogs()->updateOrCreate(['type' => EventLog::TYPE_ACTIVATION], [
                    'triggered_at' => $user->email_verified_at,
                ]);
            }

            foreach ($user->merchants as $merchant) {
                // Trial period finish
                if ($merchant->trial_period_finish) {
                    $user->eventLogs()->updateOrCreate(['type' => EventLog::TYPE_TRIAL_PERIOD_FINISH], [
                        'triggered_at' => $merchant->trial_period_finish,
                    ]);
                }

                // Payments
                $n = 0;
                foreach ($merchant->billingOperations as $operation) {
                    if (++$n > 3) {
                        break;
                    }

                    if ($n === 1 && $operation->captured_at) {
                        $user->eventLogs()->updateOrCreate(['type' => EventLog::TYPE_FIRST_PAYMENT], [
                            'triggered_at' => $operation->captured_at,
                        ]);
                    }
                    if ($n === 2 && $operation->captured_at) {
                        $user->eventLogs()->updateOrCreate(['type' => EventLog::TYPE_SECOND_PAYMENT], [
                            'triggered_at' => $operation->captured_at,
                        ]);
                    }
                    if ($n === 3 && $operation->captured_at) {
                        $user->eventLogs()->updateOrCreate(['type' => EventLog::TYPE_THIRD_PAYMENT], [
                            'triggered_at' => $operation->captured_at,
                        ]);
                    }
                }

                // WB credentials
                foreach ($merchant->marketplaceConnections as $connection) {
                    if (empty($connection->credentials) || !$connection->created_at) {
                        continue;
                    }

                    $user->eventLogs()->updateOrCreate(['type' => EventLog::TYPE_CREDENTIALS_WB], [
                        'triggered_at' => $connection->created_at,
                    ]);
                }
            }
        }
    }
}
