<?php

namespace Database\Seeders;

use App\Modules\Auth\Models\User;
use App\Modules\Common\Helpers\CommonDataHelper;
use Illuminate\Database\Seeder;

class PrepareUserEmails extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach ($users as $user) {
            $user->update(['email' => CommonDataHelper::prepareEmail($user->email)]);
        }
    }
}
