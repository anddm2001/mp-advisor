<?php

namespace Database\Seeders;

use App\Modules\ControlPanel\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        with(new Admin())->newModelQuery()->updateOrCreate(['email' => 'anddm2001@gmail.com'], [
            'password' => '$2y$10$uPG2PhnWpzBSlDswqcQfCe1YqVkgLXVbwU4vfpcMHBj3J3o9f.x8y',
            'first_name' => 'Андрей',
            'last_name' => 'Дмитриев',
            'is_active' => true,
        ]);
    }
}
