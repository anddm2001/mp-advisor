<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropUnusedReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('report_nomenclature_turnover');
        Schema::dropIfExists('report_nomenclature_sale');
        Schema::dropIfExists('report_nomenclature_in_stock');
        Schema::dropIfExists('report_nomenclature_in_order');
        Schema::dropIfExists('report_lost_orders_result');
        Schema::dropIfExists('report_file');
        Schema::dropIfExists('report_delivery_result');
        Schema::dropIfExists('report_nomenclature');
        Schema::dropIfExists('report');
        Schema::dropIfExists('nomenclature');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
