<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixMailLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mail_log', function (Blueprint $table) {
            $table->dropForeign(['merchant_id']);
            $table->foreign('merchant_id')->references('id')->on('merchant')->nullOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mail_log', function (Blueprint $table) {
            $table->dropForeign(['merchant_id']);
            $table->foreign('merchant_id')->references('id')->on('user')->nullOnDelete()->cascadeOnUpdate();
        });
    }
}
