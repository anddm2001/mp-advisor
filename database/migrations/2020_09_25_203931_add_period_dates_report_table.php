<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddPeriodDatesReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report', function(Blueprint $table) {
            $table->dropColumn('period');

            $table->date('period_from')->nullable(true)->comment('Левая граница периода');
            $table->date('period_to')->nullable(true)->comment('Правая граница периода');
        });

        $previousMonth = (new DateTime())->sub(new DateInterval('P1M'));

        DB::table('report')->update([
            'period_from' => $previousMonth->format('Y-m-01'), // начало предыдущего месяца
            'period_to' => $previousMonth->format('Y-m-t'), // конец предыдущего месяца
        ]);

        Schema::table('report', function(Blueprint $table) {
            $table->date('period_from')->nullable(false)->change();
            $table->date('period_to')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report', function(Blueprint $table) {
            $table->dropColumn('period_from');
            $table->dropColumn('period_to');

            $table->string('period')->nullable(true)->comment('Период отчёта в формате ym (годмесяц)');
        });
    }
}
