<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReportNomenclatureActiveFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nomenclature', function(Blueprint $table) {
            $table->dropColumn('is_active');
        });

        Schema::table('report_nomenclature', function(Blueprint $table) {
            $table->boolean('is_active')->nullable(false)->default(true)->comment('Использовать в результатах');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nomenclature', function(Blueprint $table) {
            $table->boolean('is_active')->nullable(false)->default(true)->comment('Активность');
        });

        Schema::table('report_nomenclature', function(Blueprint $table) {
            $table->dropColumn('is_active');
        });
    }
}
