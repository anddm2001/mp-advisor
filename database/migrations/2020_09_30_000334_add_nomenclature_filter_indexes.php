<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNomenclatureFilterIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nomenclature', function(Blueprint $table) {
            $table->index('name');
            $table->index('brand');
            $table->index('vendor_code');
            $table->index('marketplace_nomenclature_code');
        });

        Schema::table('report_nomenclature', function(Blueprint $table) {
            $table->index('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nomenclature', function(Blueprint $table) {
            $table->dropIndex('nomenclature_name_index');
            $table->dropIndex('nomenclature_brand_index');
            $table->dropIndex('nomenclature_vendor_code_index');
            $table->dropIndex('nomenclature_marketplace_nomenclature_code_index');
        });

        Schema::table('report_nomenclature', function(Blueprint $table) {
            $table->dropIndex('report_nomenclature_is_active_index');
        });
    }
}
