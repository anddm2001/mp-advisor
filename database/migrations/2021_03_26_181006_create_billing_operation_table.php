<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_operation', function (Blueprint $table) {
            $table->id();
            $table->foreignId('merchant_id')->comment('Мерчант')
                ->constrained('merchant')->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('merchant_connection_id')->nullable()->comment('Подключение')
                ->constrained('merchant_connection')->cascadeOnUpdate()->nullOnDelete();
            $table->nullableMorphs('initiator');
            $table->foreignId('billing_rate_id')->nullable()->comment('Применённый тариф')
                ->constrained('billing_rate')->cascadeOnUpdate()->nullOnDelete();
            $table->string('billing_rate_name')->nullable()->comment('Название применённого тарифа');
            $table->string('prev_billing_rate_name')->nullable()->comment('Название предыдущего тарифа');
            $table->unsignedInteger('item_total_count')->nullable()->comment('Общее кол-во позиций');
            $table->unsignedInteger('item_active_count')->nullable()->comment('Кол-во активных позиций');
            $table->string('type')->index()->comment('Тип операции');
            $table->string('status')->index()->comment('Статус операции');
            $table->decimal('amount')->comment('Сумма операции');
            $table->string('description')->nullable()->comment('Описание операции');
            $table->string('error_title')->nullable()->comment('Название ошибки');
            $table->string('error_description')->nullable()->comment('Описание ошибки');
            $table->string('payment_system')->nullable()->index()->comment('Платёжная система');
            $table->string('payment_reference')->nullable()->index()->comment('Идентификатор платежа в ПС');
            $table->string('payment_cancellation_reason')->nullable()->comment('Код причины отмены платежа');
            $table->timestamps();
            $table->timestamp('captured_at')->nullable()->comment('Время проведения');

            $table->unique(['payment_system', 'payment_reference']);
            $table->index(['merchant_id', 'status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_operation');
    }
}
