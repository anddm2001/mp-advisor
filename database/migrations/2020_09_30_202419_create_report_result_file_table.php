<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportResultFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_result_file', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('report_id')->nullable(false)->comment('Привязка к отчёту');
            $table->unsignedBigInteger('file_id')->nullable(true)->comment('Привязка к файлу');
            $table->smallInteger('generate_status')->nullable(false)->comment('Статус генерации файла');

            $table->foreign('report_id')
                ->on('report')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('file_id')
                ->on('file')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index('report_id');
            $table->index('file_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_result_file');
    }
}
