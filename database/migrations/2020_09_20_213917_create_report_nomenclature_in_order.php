<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportNomenclatureInOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_nomenclature_in_order', function (Blueprint $table) {
            $table->uuid('nomenclature_uuid')->nullable(false);
            $table->unsignedBigInteger('report_id')->nullable(false)->comment('Привязка к отчёту');

            $table
                ->uuid('size_uuid')->nullable(false)
                ->nullable(false)
                ->comment('Идентификатор размера');
            $table->string('vendor_code')->nullable(false)->comment('Артикул поставщика');
            $table->string('barcode')->nullable(true)->comment('Штрихкод');
            $table->string('marketplace_nomenclature_code')->nullable(false)->comment('Код в маркетплейсе');
            $table->integer('to_order')->nullable(false)->default(0)->comment('В пути к клиенту');
            $table->integer('from_order')->nullable(false)->default(0)->comment('В пути от клиента');

            $table->primary(['nomenclature_uuid', 'report_id', 'size_uuid']);

            $table->foreign('report_id')
                ->on('report')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_nomenclature_in_order');
    }
}
