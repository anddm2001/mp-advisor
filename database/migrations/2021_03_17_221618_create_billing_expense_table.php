<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_expense', function (Blueprint $table) {
            $table->id();
            $table->foreignId('merchant_id')->constrained('merchant')->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('merchant_connection_id')->nullable()
                ->constrained('merchant_connection')->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('billing_rate_id')->nullable()
                ->constrained('billing_rate')->cascadeOnUpdate()->nullOnDelete();
            $table->string('billing_rate_name')->comment('Название применённого тарифа');
            $table->unsignedInteger('item_total_count')->comment('Общее кол-во позиций');
            $table->unsignedInteger('item_active_count')->comment('Кол-во активных позиций');
            $table->string('status')->index()->comment('Статус списания');
            $table->unsignedDecimal('amount')->comment('Сумма списания');
            $table->string('description')->nullable()->comment('Описание списания');
            $table->string('error_title')->nullable()->comment('Название ошибки');
            $table->string('error_description')->nullable()->comment('Описание ошибки');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_expense');
    }
}
