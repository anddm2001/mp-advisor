<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportNomenclatureInStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_nomenclature_in_stock', function (Blueprint $table) {
            $table->uuid('nomenclature_uuid')->nullable(false);

            $table
                ->uuid('size_uuid')->nullable(false)
                ->nullable(false)
                ->comment('Идентификатор размера');
            $table->unsignedBigInteger('report_id')->nullable(false)->comment('Привязка к отчёту');
            $table->string('vendor_code')->nullable(false)->comment('Артикул поставщика');
            $table->string('marketplace_nomenclature_code')->nullable(false)->comment('Код номенклатуры в маркетплейсе');
            $table->string('barcode')->nullable(true)->comment('Штрихкод');
            $table->unsignedInteger('in_stock')->nullable(false)->comment('Остаток на складах');
            $table->jsonb('stock_by_storage')->nullable(true)->comment('Остаток по складам');
            $table->float('discount')->nullable(true)->comment('Скидка');

            $table->primary(['nomenclature_uuid', 'report_id', 'size_uuid']);

            $table->foreign('report_id')
                ->on('report')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_nomenclature_in_stock');
    }
}
