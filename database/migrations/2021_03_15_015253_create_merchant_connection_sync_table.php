<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantConnectionSyncTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_connection_sync', function (Blueprint $table) {
            $table->id();
            $table->foreignId('merchant_connection_id')
                ->constrained('merchant_connection')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->dateTime('date')->index();
            $table->string('type')->comment('Тип синхронизации');
            $table->string('status')->comment('Статус синхронизации');
            $table->text('data')->nullable()->comment('Данные по результатам синхронизации');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_connection_sync');
    }
}
