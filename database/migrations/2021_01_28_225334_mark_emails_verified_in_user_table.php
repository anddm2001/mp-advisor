<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class MarkEmailsVerifiedInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('user')->whereNull('email_verified_at')->update(['email_verified_at' => DB::raw('NOW()')]);
    }
}
