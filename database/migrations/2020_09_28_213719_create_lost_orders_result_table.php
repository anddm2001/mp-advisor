<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLostOrdersResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_lost_orders_result', function (Blueprint $table) {
            $table->uuid('nomenclature_uuid')->nullable(false)->comment('Привязка к номенклатуре');
            $table->unsignedBigInteger('report_id')->nullable(false)->comment('Привязка к отчёту');

            $table
                ->uuid('size_uuid')->nullable(false)
                ->nullable(false)
                ->comment('Идентификатор размера');
            $table->integer('lost_orders')->nullable(false)->default(0)->comment('Упущено заказов');
            $table->float('lost_paid')->nullable(false)->default(0)->comment('Упущено выручки');

            $table->primary(['nomenclature_uuid', 'report_id', 'size_uuid']);
            $table->foreign(['nomenclature_uuid', 'report_id', 'size_uuid'])
                ->references(['nomenclature_uuid', 'report_id', 'size_uuid'])
                ->on('report_nomenclature')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_lost_orders_result');
    }
}
