<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('path')->comment('Путь к файлу относительно хранилища');
            $table->string('name')->comment('Название файла в хранилище');
            $table->string('original_name')->comment('Оригинальное название файла');
            $table->integer('size')->comment('Размер файла в байтах');
            $table->string('mime')->comment('mime-тип файла');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file');
    }
}
