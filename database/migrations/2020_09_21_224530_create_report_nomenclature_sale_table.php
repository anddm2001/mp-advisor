<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportNomenclatureSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_nomenclature_sale', function (Blueprint $table) {
            $table->uuid('uuid')->nullable(false)->primary();
            $table->uuid('nomenclature_uuid')->nullable(false)->comment('Идентификатор номенклатуры');

            $table
                ->uuid('size_uuid')->nullable(false)
                ->nullable(false)
                ->comment('Идентификатор размера');
            $table->unsignedBigInteger('report_id')->nullable(false)->comment('Идентификатор отчёта');
            $table->string('vendor_code')->nullable(false)->comment('Артикул поставщика');
            $table->string('marketplace_nomenclature_code')->nullable(false)->comment('Код номенклатуры в маркетплейсе');
            $table->string('barcode')->nullable(true)->comment('Штрихкод');
            $table->float('ordered_count')->nullable(true)->comment('Заказанное количество');
            $table->float('base_cost')->nullable(true)->comment('Себестоимость');
            $table->float('paid_count')->nullable(true)->comment('Оплаченное количество');
            $table->float('paid_cost')->nullable(true)->comment('Оплаченная стоимость');

            $table->index(['nomenclature_uuid', 'report_id', 'size_uuid']);

            $table->foreign('report_id')
                ->on('report')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_nomenclature_sale');
    }
}
