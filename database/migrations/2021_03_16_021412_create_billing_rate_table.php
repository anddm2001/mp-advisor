<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_rate', function (Blueprint $table) {
            $table->id();
            $table->boolean('is_active')->default(false)->index()->comment('Активен');
            $table->string('name')->comment('Название');
            $table->unsignedDecimal('amount')->comment('Сумма ежедневного списания');
            $table->unsignedInteger('min_item_count')->index()->comment('Минимальное кол-во SKU');
            $table->unsignedInteger('max_item_count')->index()->nullable()->comment('Максимальное кол-во SKU');
            $table->unsignedInteger('trial_period_days')->nullable()->comment('Пробный период в днях');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_rate');
    }
}
