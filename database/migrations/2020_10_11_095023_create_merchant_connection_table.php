<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantConnectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_connection', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('merchant_id')->nullable(false)->comment('Идентификатор мерчанта');
            $table->unsignedSmallInteger('marketplace')->nullable(false)->comment('Идентификатор маркетплейса');
            $table->text('credentials')->nullable(false)->comment('Закодированные данные подключения');

            $table->foreign('merchant_id')
                ->on('merchant')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unique(['merchant_id', 'marketplace']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_connection');
    }
}
