<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddItemCountsToMerchantConnectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_connection', function (Blueprint $table) {
            $table->unsignedInteger('item_total_count')->nullable()->comment('Общее кол-во позиций');
            $table->unsignedInteger('item_active_count')->nullable()->comment('Кол-во активных позиций');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_connection', function (Blueprint $table) {
            $table->dropColumn('item_total_count');
            $table->dropColumn('item_active_count');
        });
    }
}
