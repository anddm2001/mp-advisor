<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportNomenclatureItemStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_nomenclature_item_state', function (Blueprint $table) {
            $table->string('nomenclature_key', 100)->nullable(false)->comment('Ключ номенклатуры (артикул поставщика + размер)');
            $table->unsignedBigInteger('user_id')->nullable(false)->comment('Идентификатор пользователя, для которого менять состояние');
            $table->boolean('hide')->nullable(false)->default(false)->comment('Скрывать номенклатуры в отчёте');
            $table->boolean('export')->nullable(false)->default(false)->comment('Выгружать номенклатуры в файл');

            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->primary(['nomenclature_key', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_nomenclature_item_state');
    }
}
