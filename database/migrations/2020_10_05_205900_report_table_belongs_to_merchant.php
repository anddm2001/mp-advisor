<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class ReportTableBelongsToMerchant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report', function(Blueprint $table) {
            $table->unsignedBigInteger('merchant_id')->nullable(true)->comment('Мерчант, владелец отчёта');
        });

        $reportsToUsers = DB::select('SELECT u.id AS user_id, u.name, r.id AS report_id FROM report r LEFT JOIN "user" u ON r.user_id = u.id');
        // юзеры, для которых уже созданы мерчанты
        $alreadyCreatedMerchant = [];

        foreach ($reportsToUsers as $item) {
            /** @var stdClass $item */
            $item = is_array($item) ? $item : get_object_vars($item);
            if (empty($item['user_id'])) {
                // удаление отчёта, если нет юзера
                DB::table('report')->where('id', $item['report_id'])->delete();
            }

            $merchantId = $alreadyCreatedMerchant[$item['user_id']] ?? null;

            if (is_null($merchantId)) {
                // создать мерчанта с таким же именем
                $merchantId = DB::table('merchant')->insertGetId([
                    'name' => $item['name'],
                    'owner_id' => $item['user_id']
                ]);

                DB::table('merchant_user')->insert([
                    'user_id' => $item['user_id'],
                    'merchant_id' => $merchantId
                ]);

                $alreadyCreatedMerchant[$item['user_id']] = $merchantId;
            }

            DB::table('report')->where('id', $item['report_id'])->update([
                'merchant_id' => $merchantId
            ]);
        }

        Schema::table('report', function(Blueprint $table) {
            $table->dropColumn('user_id');

            $table->unsignedBigInteger('merchant_id')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $user = new \App\Modules\Auth\Models\User();
        $user->id = 10;

        Schema::table('report', function(Blueprint $table) {
            $table->dropColumn('merchant_id');
            $table->unsignedBigInteger('user_id')->nullable(true);
        });

        // первому пользователю назначем обратно все отчёты
        $res = DB::select('SELECT id FROM "user" LIMIT 1');
        $user = reset($res);
        $userId = is_array($user) ? $user['id'] : $user->{"id"};

        DB::table('report')->update([
            'user_id' => $userId
        ]);

        Schema::table('report', function(Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable(false)->change();
        });
    }
}
