<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_file', function (Blueprint $table) {
            $table->id();
            $table->integer('report_id')->comment('Идентификатор отчёта');
            $table->integer('file_id')->comment('Идентификатор файла');
            $table->smallInteger('file_type')->comment('Тип файла');

            $table->foreign('report_id')
                ->references('id')
                ->on('report')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('file_id')
                ->references('id')
                ->on('file')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index('file_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_file');
    }
}
