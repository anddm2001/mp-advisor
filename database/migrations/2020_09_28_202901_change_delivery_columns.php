<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDeliveryColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report', function(Blueprint $table) {
            $table->integer('current_delivery_days')->nullable(true)->comment('Дата текущей поставки через (дней)');
            $table->integer('next_delivery_days')->nullable(true)->comment('Дата следущей доставки через (дней) после текущей');
            $table->integer('stock_days')->nullable(true)->comment('Запас (дней)');
        });

        Schema::table('report_delivery_result', function(Blueprint $table) {
            $table->dropColumn('days_current');
            $table->dropColumn('days_next');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report', function(Blueprint $table) {
            $table->dropColumn('current_delivery_days');
            $table->dropColumn('next_delivery_days');
            $table->dropColumn('stock_days');
        });

        Schema::table('report_delivery_result', function(Blueprint $table) {
            $table->integer('days_current')->nullable(true);
            $table->integer('days_next')->nullable(true);
        });
    }
}
