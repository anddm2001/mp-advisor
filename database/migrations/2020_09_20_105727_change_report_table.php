<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class ChangeReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report', function(Blueprint $table) {
            $table->smallInteger('marketplace')->nullable(true)->comment('Идентификатор маркетплейса')->after('title');
        });

        DB::table('report')->update([
            'marketplace' => 1
        ]);

        Schema::table('report', function(Blueprint $table) {
            $table->smallInteger('marketplace')->nullable(false)->change();
            $table->index('marketplace', 'report_marketplace_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report', function(Blueprint $table) {
            $table->dropColumn('marketplace');
        });
    }
}
