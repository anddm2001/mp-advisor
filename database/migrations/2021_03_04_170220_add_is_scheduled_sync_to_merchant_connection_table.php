<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsScheduledSyncToMerchantConnectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_connection', function (Blueprint $table) {
            $table->boolean('is_scheduled_sync')->default(false)->comment('Последняя синхронизация была добором данных');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_connection', function (Blueprint $table) {
            $table->dropColumn('is_scheduled_sync');
        });
    }
}
