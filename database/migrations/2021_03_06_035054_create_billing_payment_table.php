<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_payment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('merchant_id')->index()->comment('Мерчант');
            $table->string('payment_system')->index()->comment('Платёжная система');
            $table->string('reference')->index()->comment('Идентификатор в платёжной системе');
            $table->string('status')->index()->comment('Статус платежа');
            $table->unsignedDecimal('amount')->comment('Сумма платежа');
            $table->string('description')->nullable()->comment('Описание платежа');
            $table->string('cancellation_reason')->nullable()->comment('Код причины отмены платежа');
            $table->string('error_title')->nullable()->comment('Название ошибки');
            $table->string('error_description')->nullable()->comment('Описание ошибки');
            $table->timestamps();
            $table->timestamp('captured_at')->nullable()->comment('Время подтверждения платежа');

            $table->unique(['payment_system', 'reference']);

            $table->foreign('merchant_id')
                ->on('merchant')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_payment');
    }
}
