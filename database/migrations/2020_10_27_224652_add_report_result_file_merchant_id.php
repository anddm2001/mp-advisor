<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReportResultFileMerchantId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_result_file', function(Blueprint $table) {
            $table->unsignedBigInteger('merchant_id')->nullable(true)->comment('Привязка к мерчанту');

            $table->foreign('merchant_id')
                ->on('merchant')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index(['report_id', 'merchant_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_result_file', function(Blueprint $table) {
            $table->dropColumn('merchant_id');
        });
    }
}
