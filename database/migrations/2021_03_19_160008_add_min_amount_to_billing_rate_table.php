<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMinAmountToBillingRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_rate', function (Blueprint $table) {
            $table->unsignedDecimal('min_amount')->nullable()->comment('Минимальная сумма пополнения');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_rate', function (Blueprint $table) {
            $table->dropColumn('min_amount');
        });
    }
}
