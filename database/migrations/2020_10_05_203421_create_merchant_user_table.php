<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_user', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->comment('Привязка к пользователю');
            $table->unsignedBigInteger('merchant_id')->comment('Привязка к мерчанту');

            $table->foreign('user_id')
                ->on('user')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('merchant_id')
                ->on('merchant')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->primary(['user_id', 'merchant_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_user');
    }
}
