<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportNomenclatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nomenclature', function (Blueprint $table) {
            $table
                ->uuid('nomenclature_uuid')->nullable(false)
                ->comment('Уникальный идентификатор номенклатуры для отчёта');

            $table
                ->uuid('size_uuid')->nullable(false)
                ->nullable(false)
                ->comment('Идентификатор размера');

            $table->string('size')->nullable(true)->comment('Название размера');
            $table->string('color')->nullable(true)->comment('Код цвета');
            $table->boolean('is_active')->nullable(false)->default(true)->comment('Активность');
            $table->text('name')->nullable(false)->comment('Название');
            $table->unsignedSmallInteger('marketplace')->nullable(false)->comment('Идентификатор маркетплейса');
            $table->text('brand')->nullable(true)->comment('Название производителя');
            $table->string('barcode')->nullable(false)->comment('Штрихкод');
            $table->string('vendor_code')->nullable(false)->comment('Артикул поставщика');
            $table->string('internal_code')->nullable(true)->comment('Внутренний ИД поставщика');
            $table->string('internal_property_code')->nullable(true)->comment('Внутренний код характеристик поставщика');
            $table->string('marketplace_nomenclature_code')->nullable(false)->comment('Идентификатор в маркетплейсе');
            $table->string('marketplace_property_code')->nullable(true)->comment('Идентификатор характеристики в маркетплейсе');
            $table->float('price')->nullable(false)->comment('Цена');

            $table->primary(['nomenclature_uuid', 'size_uuid']);
        });

        Schema::create('report_nomenclature', function (Blueprint $table) {
            $table->uuid('nomenclature_uuid')->nullable(false)->comment('Привязка к таблице номенклатур');
            $table->unsignedBigInteger('report_id')->nullable(false)->comment('Привязка к отчёту');
            $table->uuid('size_uuid')->nullable(false)->comment('Код размера');

            $table->primary(['nomenclature_uuid', 'report_id', 'size_uuid']);

            $table->foreign('report_id')
                ->on('report')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_nomenclature');
        Schema::dropIfExists('nomenclature');
    }
}
