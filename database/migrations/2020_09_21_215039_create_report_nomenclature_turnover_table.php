<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportNomenclatureTurnoverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_nomenclature_turnover', function (Blueprint $table) {
            $table->uuid('nomenclature_uuid')->nullable(false);
            $table->unsignedBigInteger('report_id')->nullable(false)->comment('Привязка к отчёту');
            $table->string('vendor_code')->nullable(false)->comment('Артикул поставщика');
            $table->string('marketplace_nomenclature_code')->nullable(false)->comment('Код номенклатуры в маркетплейсе');
            $table->float('turnover_days')->nullable(true)->default(0)->comment('Оборачиваемость, дней');

            $table->primary(['nomenclature_uuid', 'report_id']);

            $table->foreign('report_id')
                ->on('report')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_nomenclature_turnover');
    }
}
