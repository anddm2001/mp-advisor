<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkHandbookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_handbook', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique()->comment('Код');
            $table->string('name')->comment('Название');
            $table->string('link', 1000)->comment('Ссылка');
            $table->timestamps();
        });

        $now = \Illuminate\Support\Carbon::now();

        \Illuminate\Support\Facades\DB::table('link_handbook')->insert([
            'code' => 'license-agreement',
            'name' => 'Лицензионное соглашение',
            'link' => '',
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        \Illuminate\Support\Facades\DB::table('link_handbook')->insert([
            'code' => 'user-agreement',
            'name' => 'Пользовательское соглашение',
            'link' => '',
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        \Illuminate\Support\Facades\DB::table('link_handbook')->insert([
            'code' => 'personal-data-processing',
            'name' => 'Обработка персональных данных',
            'link' => '',
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_handbook');
    }
}
