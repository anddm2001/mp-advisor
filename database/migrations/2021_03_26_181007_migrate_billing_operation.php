<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class MigrateBillingOperation extends Migration
{
    protected const BILLING_OPERATION_TABLE = 'billing_operation';
    protected const BILLING_PAYMENT_TABLE = 'billing_payment';
    protected const BILLING_EXPENSE_TABLE = 'billing_expense';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table(self::BILLING_OPERATION_TABLE)->truncate();

        DB::beginTransaction();
        try {
            DB::table(self::BILLING_PAYMENT_TABLE)->eachById(function (stdClass $payment) {
                DB::table(self::BILLING_OPERATION_TABLE)->insert([
                    'merchant_id' => $payment->merchant_id,
                    'type' => 'payment',
                    'status' => $payment->status,
                    'amount' => $payment->amount,
                    'description' => $payment->description ?? null,
                    'error_title' => $payment->error_title ?? null,
                    'error_description' => $payment->error_description ?? null,
                    'payment_system' => $payment->payment_system ?? null,
                    'payment_reference' => $payment->reference ?? null,
                    'payment_cancellation_reason' => $payment->cancellation_reason ?? null,
                    'created_at' => $payment->created_at ?? null,
                    'updated_at' => $payment->updated_at ?? null,
                    'captured_at' => $payment->captured_at ?? null,
                ]);
            });

            DB::table(self::BILLING_EXPENSE_TABLE)->eachById(function (stdClass $expense) {
                DB::table(self::BILLING_OPERATION_TABLE)->insert([
                    'merchant_id' => $expense->merchant_id,
                    'merchant_connection_id' => $expense->merchant_connection_id ?? null,
                    'billing_rate_id' => $expense->billing_rate_id ?? null,
                    'billing_rate_name' => $expense->billing_rate_name ?? null,
                    'item_total_count' => $expense->item_total_count ?? null,
                    'item_active_count' => $expense->item_active_count ?? null,
                    'type' => 'expense',
                    'status' => $expense->status,
                    'amount' => 0 - $expense->amount,
                    'description' => $expense->description ?? null,
                    'error_title' => $expense->error_title ?? null,
                    'error_description' => $expense->error_description ?? null,
                    'created_at' => $payment->created_at ?? null,
                    'updated_at' => $payment->updated_at ?? null,
                ]);
            });

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();
            dd($exception->getMessage());
        }
    }
}
