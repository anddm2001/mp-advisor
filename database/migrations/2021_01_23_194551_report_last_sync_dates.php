<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReportLastSyncDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_connection', function(Blueprint $table) {
            $table->timestamp('last_sync')->nullable(true)->comment('Дата последней синхронизации');
            $table->string('error')->nullable(true)->comment('Ошибка последней синхронизации');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_connection', function(Blueprint $table) {
            $table->dropColumn(['last_sync', 'error']);
        });
    }
}
