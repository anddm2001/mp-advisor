<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('user')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('merchant_id')->constrained('merchant')->cascadeOnDelete()->cascadeOnUpdate();
            $table->string('email')->index()->comment('Почта пользователя');
            $table->string('category')->comment('Что случилось');
            $table->string('subcategory')->comment('Точнее');
            $table->text('message')->comment('Текст сообщения');
            $table->string('file')->nullable()->comment('Путь к файлу');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
