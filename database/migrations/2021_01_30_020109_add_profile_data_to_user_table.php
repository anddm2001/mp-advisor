<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProfileDataToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->foreignId('photo_file_id')->nullable()->constrained('file')->cascadeOnDelete()->cascadeOnUpdate();
            $table->string('phone', 50)->nullable();
            $table->string('company_name', 50)->nullable();
            $table->string('company_post', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropForeign(['photo_file_id']);
            $table->dropColumn('photo_file_id');
            $table->dropColumn('phone');
            $table->dropColumn('company_name');
            $table->dropColumn('company_post');
        });
    }
}
