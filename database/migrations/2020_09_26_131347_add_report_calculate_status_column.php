<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReportCalculateStatusColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report', function(Blueprint $table) {
            $table->smallInteger('calculate_status')->nullable(false)->default(0)->comment('Статус калькуляции');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report', function(Blueprint $table) {
            $table->dropColumn('calculate_status');
        });
    }
}
