<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddReportFileStatusColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_file', function(Blueprint  $table) {
            $table->smallInteger('process_status')->nullable(true)->comment('Статус обработки');
        });

        DB::table('report_file')->update([
            'process_status' => 0
        ]);

        Schema::table('report_file', function(Blueprint $table) {
            $table->smallInteger('process_status')->nullable(false)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_file', function(Blueprint $table) {
            $table->dropColumn('process_status');
        });
    }
}
