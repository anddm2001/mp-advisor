<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportDeliveryResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_delivery_result', function (Blueprint $table) {
            $table->uuid('nomenclature_uuid')->nullable(false)->comment('Привязка к номенклатуре');
            $table->unsignedBigInteger('report_id')->nullable(false)->comment('Привязка к отчёту');

            $table
                ->uuid('size_uuid')->nullable(false)
                ->nullable(false)
                ->comment('Идентификатор размера');
            $table->integer('days_current')->nullable(false)->comment('Дней до текущей поставки');
            $table->integer('days_next')->nullable(false)->comment('Дней до следующей поставки');
            $table->integer('need_current_count')->nullable(true)->comment('Необходимо обеспечить товара в текущей поставке');
            $table->integer('need_next_count')->nullable(true)->comment('Необходимо обеспечить товара в следующей поставке');

            $table->primary(['nomenclature_uuid', 'report_id', 'size_uuid']);
            $table->foreign(['nomenclature_uuid', 'report_id', 'size_uuid'])
                ->references(['nomenclature_uuid', 'report_id', 'size_uuid'])
                ->on('report_nomenclature')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_delivery_result');
    }
}
