<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_notification', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unique()->constrained('user')->cascadeOnDelete()->cascadeOnUpdate();
            $table->boolean('is_trial_period_finish_sent')->nullable()->index()->comment('Письмо об окончании пробного периода отправлено');
            $table->unsignedTinyInteger('blocked_sent_count')->nullable()->index()->comment('Сколько отправлено писем о блокировке');
            $table->dateTime('blocked_last_sent_date')->nullable()->index()->comment('Дата отправки последнего письма о блокировке');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_notification');
    }
}
